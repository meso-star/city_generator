# city_generator

The purpose of "city_generator" is to produce the wavefront .obj file
and all materials definition files that can be used by the HTRDR program
in order to render an image of a city within a cloudy atmosphere.

## How to build

city_generator is written in fortran; you should first install
a fortran compiler. It was developped using gfortran, but if you
want to use any other compiler, you should replace "gfortran"
by the name of your compiler in the "Makefile".

Then the program can be compiled using command "make all". This should
produce the "city_generator.exe" executable, that can be run using
command "./city_generator.exe".

Whenever a modidication is made to a source file, running the "make all" command
again will recompile the modified file(s), then link the object files
again in order to produce the new executable.

If a include file is modified, the executable must be recompiled
from scratch. In order to do this, run the "make clean" command
before running the "make all" command again, in order to clean
existing object files. Or just run the "make clean all" command.

## Usage

A ./data directory should contain the definition file for every element
that belong to the scene. These element definition files have to be
produced by the "procedural_generator" program. You should therefore
perform the following actions prior to using "city_generator":
- clone the "procedural_generator" project
- compile and run it; all definition files now reside within the
procedural_generator/results folder
- link the procedural_generator/results folder to the ./data folder;
use the following command within the city_generator/ folder:
> ln -s ../procedural_generator/results ./data
- you can finally use "city_generator":
>./city_generator

The resulting files are:
- results/city.obj: the wavefront file
- results/*.dat: materials spectral properties definition files

## License

Copyright (C) 2020 [|Meso|Star>](http://www.meso-star.com). city_generator is free
software released under the GPL v3+ license: GNU GPL version 3 or later. You
are welcome to redistribute it under certain conditions; refer to the COPYING
file for details.
