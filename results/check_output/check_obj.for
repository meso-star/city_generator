      program check_obj
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to verify the integrity of a OBJ file
c     
c     Variables
      character*(Nchar_mx) objfile
      integer ios,ioerr
      logical keep_reading
      character*(Nchar_mx) line
      integer Nline,Nv,Nf
      integer fidx(1:3)
c     label
      character*(Nchar_mx) label
      label='program check_obj'
      
      objfile='./city_mtl.obj'
      open(10,file=trim(objfile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(objfile)
         stop
      endif
      keep_reading=.true.
      Nline=0
      Nv=0
      Nf=0
      do while (keep_reading)
         read(10,10,iostat=ioerr) line
         if (ioerr.lt.0) then
            keep_reading=.false.
         else
            Nline=Nline+1
            if (line(1:2).eq.'v ') then
               Nv=Nv+1
            endif
            if (line(1:2).eq.'f ') then
               Nf=Nf+1
c     Debug
c               write(*,*) Nline,trim(line)
c     Debug
               call retrieve_face_indexes(Nline,line,fidx)
               if (fidx(1).gt.Nv) then
                  call error(label)
                  write(*,*) 'fidx(1)=',fidx(1)
                  write(*,*) '> Nv=',Nv
                  write(*,*) 'line:'
                  write(*,*) trim(line)
                  write(*,*) 'line index:',Nline
                  stop
               endif
               if (fidx(2).gt.Nv) then
                  call error(label)
                  write(*,*) 'fidx(2)=',fidx(2)
                  write(*,*) '> Nv=',Nv
                  write(*,*) 'line:'
                  write(*,*) trim(line)
                  write(*,*) 'line index:',Nline
                  stop
               endif
               if (fidx(3).gt.Nv) then
                  call error(label)
                  write(*,*) 'fidx(3)=',fidx(3)
                  write(*,*) '> Nv=',Nv
                  write(*,*) 'line:'
                  write(*,*) trim(line)
                  write(*,*) 'line index:',Nline
                  stop
               endif
            endif
         endif
      enddo                     ! while (keep_reading)
      close(10)
      write(*,*) 'Number of lines: ',Nline
      write(*,*) 'Nv=',Nv
      write(*,*) 'Nf=',Nf
      write(*,*) 'No issue found'
      
      end
