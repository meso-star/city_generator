	integer Ndim_mx
	integer Ncs_mx
        integer Nv_so_mx
        integer Nf_so_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Ncs_mx=8)
        parameter(Nv_so_mx=1000000)
        parameter(Nf_so_mx=1000000)
	parameter(Nchar_mx=1000)
