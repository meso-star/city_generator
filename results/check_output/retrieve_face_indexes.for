      subroutine retrieve_face_indexes(line_idx,line,fidx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to analyze a line that contains the definition of a face
c     in order to extract the indexes of the vertex that compose this face      
c     
c     Input:
c       + line_idx: line index
c       + line: character string; it is supposed to contain the "f" character
c         followed by the indexes of 3 vertices
c     
c     Output:
c       + fidx: indexes of the 3 vertex for the triangle
c     
c     I/O
      integer line_idx
      character*(Nchar_mx) line
      integer fidx(1:3)
c     temp
      character*(Nchar_mx) str
      integer num,err
      logical keep_looking
      integer i,i1,i2
c     label
      character*(Nchar_mx) label
      label='subroutine retrieve_face_indexes'

c     First vertex
      if (line(1:2).ne.'f ') then
         call error(label)
         write(*,*) 'Line:'
         write(*,*) trim(line)
         write(*,*) 'does not start by "f "'
         stop
      endif
      i=2
      keep_looking=.true.
      do while (keep_looking)
         if (line(i:i).ne.' ') then
            keep_looking=.false.
            i1=i
         else
            i=i+1
         endif
      enddo                     ! while (keep_looking)
      i=i1+1
      keep_looking=.true.
      do while (keep_looking)
         if (line(i:i).eq.' ') then
            keep_looking=.false.
            i2=i-1
         else
            i=i+1
         endif
      enddo                     ! while (keep_looking)
      str=line(i1:i2)
      call str2num(str,num,err)
      if (err.eq.0) then
         fidx(1)=num
      else
         write(*,*) 'could not convert to numeric:'
         write(*,*) 'str=',trim(str)
         stop
      endif
c     Second vertex
      if (line(i2+1:i2+1).ne.' ') then
         call error(label)
         write(*,*) 'No separator between indexes of v1 and v2'
         write(*,*) 'line=',trim(line)
         write(*,*) 'line_idx=',line_idx
         stop
      endif
      i=i2+1
      keep_looking=.true.
      do while (keep_looking)
         if (line(i:i).ne.' ') then
            keep_looking=.false.
            i1=i
         else
            i=i+1
         endif
      enddo                     ! while (keep_looking)
      i=i1+1
      keep_looking=.true.
      do while (keep_looking)
         if (line(i:i).eq.' ') then
            keep_looking=.false.
            i2=i-1
         else
            i=i+1
         endif
      enddo                     ! while (keep_looking)
      str=line(i1:i2)
      call str2num(str,num,err)
      if (err.eq.0) then
         fidx(2)=num
      else
         write(*,*) 'could not convert to numeric:'
         write(*,*) 'str=',trim(str)
         stop
      endif
c     Third vertex
      if (line(i2+1:i2+1).ne.' ') then
         call error(label)
         write(*,*) 'No separator between indexes of v2 and v3'
         write(*,*) 'line=',trim(line)
         write(*,*) 'line_idx=',line_idx
         stop
      endif
      i=i2+1
      keep_looking=.true.
      do while (keep_looking)
         if (line(i:i).ne.' ') then
            keep_looking=.false.
            i1=i
         else
            i=i+1
         endif
      enddo                     ! while (keep_looking)
      i2=len_trim(line)
      str=line(i1:i2)
      call str2num(str,num,err)
      if (err.eq.0) then
         fidx(3)=num
      else
         write(*,*) 'could not convert to numeric:'
         write(*,*) 'str=',trim(str)
         stop
      endif
      
      return
      end
