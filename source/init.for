c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine init(obj_file,mtllib_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: perform global initialiation tasks
c
c     Input:
c       + obj_file: obj file that will be produced by the code
c       + mtllib_file: mtllib file that will be used by the obj file
c
c     I/O
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
c     temp
      character*(Nchar_mx) command,exec_file,objmtl_file
      logical file_exists,dot_found
      integer i,idot
c     label
      character*(Nchar_mx) label
      label='subroutine init'

      write(*,*) 'Initialization...'
      command='rm -f ./results/city*.obj'
      call exec(command)
      command='rm -f ./results/'//trim(mtllib_file)
      call exec(command)
      command='rm -f ./results/*.dat'
      call exec(command)
      command='find ./results/obj -maxdepth 1 -name "*.obj" -delete'
      call exec(command)
c     Compilation of "triangle"
      exec_file='./triangle/triangle'
      command='rm -f '//trim(exec_file)
      command='cd ./triangle; ./compile_triangle.bash'
      call exec(command)
      inquire(file=trim(exec_file),exist=file_exists)
      if (.not.file_exists) then
         write(*,*) 'Compilation of TRIANGLE failed'
         stop
      endif
      
c     Initialize obj file
      dot_found=.false.
      do i=len_trim(obj_file),1,-1
         if (obj_file(i:i).eq.'.') then
            dot_found=.true.
            idot=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.dot_found) then
         call error(label)
         write(*,*) 'could not find a . in filename:',trim(obj_file)
         stop
      else
         objmtl_file=obj_file(1:idot-1)
     &        //'_mtl'
     &        //obj_file(idot:len_trim(obj_file))
      endif                     ! dot_found=F
c     
      open(11,file=trim(objmtl_file))
      write(11,10) 'mtllib '//trim(mtllib_file)
      close(11)
c     Debug
c      write(*,*) trim(label),' : out'
c     Debug

      return
      end
      
