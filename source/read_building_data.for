c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine identify_Nbuilding(Nbuilding)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of buildings defined by the user
c
c     Input:
c
c     Output:
c       + Nbuilding: number of buildings
c
c     I/O
      integer Nbuilding
c     temp
      logical keep_looking,file_exists
      integer i
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine identify_Nbuilding'

      Nbuilding=0
      keep_looking=.true.
      do while (keep_looking)
         i=Nbuilding+1
         call num2str3(i,str3)
         filename='./data/building'//trim(str3)//'.in'
         inquire(file=trim(filename),exist=file_exists)
         if (file_exists) then
            Nbuilding=Nbuilding+1
         else
            keep_looking=.false.
         endif                  ! file_exists
      enddo                     ! while (keep_looking)

      return
      end



      subroutine read_building_data(dim,
     &     Nbuilding,ibuilding,
     &     type,Nx,Ny,Nz,a,e,position,angle,
     &     wwf,fhf,e_glass,roof_height,roof_thickness,
     &     internal_mat,external_mat,glass_panes_mat,roof_mat,
     &     Ncontour,Nppt,contour)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to read input data file for buildings
c
c     Input:
c       + dim: dimension of space
c
c     Output:
c       + Nbuilding: number of buildings
c       + ibuilding: index of the building definition file to read
c       + type: type of building
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + a: dimension of a single cell along each axis (m)
c       + e: thickness of solid boundaries (internal / external) (m)
c       + position: (x,y) shift of the building (m)
c       + angle: rotation angle (deg)
c       + wwf: fraction of the wall used for windows
c       + fhf: fraction of the floor height used by windows
c       + e_glass: thickness of the glass)
c       + roof_height: height of the roof (m)
c       + roof_thickness: thickness of the roof (m)
c       + internal_mat: name of the material for internal walls
c       + external_mat: name of the material for external walls
c       + glass_panes_mat: name of the material for glass panes
c       + roof_mat: name of the material for the roof
c       + Ncontour: number of contours for the building
c       + Nppt: number of points that define each contour of each building
c       + contour: list of coordinates for each point that define each contour of each building
c
c     I/O
      integer dim
      integer Nbuilding
      integer ibuilding
      integer type
      integer Nx
      integer Ny
      integer Nz
      double precision a(1:Ndim_mx)
      double precision e(1:2)
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision wwf
      double precision fhf
      double precision e_glass
      double precision roof_height
      double precision roof_thickness
      character*(Nchar_mx) internal_mat
      character*(Nchar_mx) external_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      integer Ncontour
      integer Nppt(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
c     temp
      integer ios,j,iostatus
      logical keep_reading
      character*3 str3
      character*(Nchar_mx) filename,line
      integer Nbuilding_with_contour
      double precision alpha
c     functions
      integer nitems
c     label
      character*(Nchar_mx) label
      label='subroutine read_building_data'

      call num2str3(ibuilding,str3)
      filename='./data/building'//trim(str3)//'.in'
      open(14,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do j=1,3
            read(14,*)
         enddo                  ! j
         read(14,*) type
         if (type.eq.0) then
            read(14,*)
            read(14,*) Nx
            read(14,*)
            read(14,*) Ny
            read(14,*)
            read(14,*) Nz
            read(14,*)
            do j=1,dim
               read(14,*) a(j)
            enddo               ! j
            read(14,*)
            read(14,*) e(1)
            read(14,*)
            read(14,*) e(2)
            read(14,*)
            read(14,*) position(1)
            read(14,*)
            read(14,*) position(2)
            read(14,*)
            read(14,*) angle
            read(14,*)
            read(14,*) wwf
            read(14,*)
            read(14,*) e_glass
            read(14,*)
            read(14,*) internal_mat
            read(14,*)
            read(14,*) external_mat
            read(14,*)
            read(14,*) glass_panes_mat
         else if ((type.eq.1).or.(type.eq.2)
     &           .or.(type.eq.3)) then
            read(14,*)
            read(14,*) Nx
            read(14,*)
            read(14,*) Ny
            read(14,*)
            read(14,*) Nz
            read(14,*)
            do j=1,dim
               read(14,*) a(j)
            enddo               ! j
            read(14,*)
            read(14,*) e(1)
            read(14,*)
            read(14,*) e(2)
            read(14,*)
            read(14,*) position(1)
            read(14,*)
            read(14,*) position(2)
            read(14,*)
            read(14,*) angle
            read(14,*)
            read(14,*) wwf
            read(14,*)
            read(14,*) e_glass
            read(14,*)
            read(14,*) roof_height
            read(14,*)
            read(14,*) roof_thickness
            read(14,*)
            read(14,*) internal_mat
            read(14,*)
            read(14,*) external_mat
            read(14,*)
            read(14,*) glass_panes_mat
            read(14,*)
            read(14,*) roof_mat
         else if (type.eq.4) then
            read(14,*)
            read(14,*) Nz
            read(14,*)
            read(14,*) a(3)
            read(14,*)
            read(14,*) e(1)
            read(14,*)
            read(14,*) e(2)
            read(14,*)
c     "position" is no longer a input parameter
c            read(14,*) (position(j),j=1,2)
c            read(14,*)
            do j=1,2
               position(j)=0.0D+0
            enddo               ! j
c     
            read(14,*) angle
            read(14,*)
            read(14,*) wwf
            read(14,*)
            read(14,*) fhf
            read(14,*)
            read(14,*) e_glass
            read(14,*)
            read(14,*) roof_height
            read(14,*)
            read(14,*) roof_thickness
            read(14,*)
            read(14,*) internal_mat
            read(14,*)
            read(14,*) external_mat
            read(14,*)
            read(14,*) glass_panes_mat
            read(14,*)
            read(14,*) roof_mat
            read(14,*)
            Ncontour=1
            Nppt(Ncontour)=0
            keep_reading=.true.
            do while (keep_reading)
               read(14,10,iostat=iostatus) line
               if (iostatus.eq.0) then
                  if (nitems(line).eq.2) then
                     backspace(14)
                     Nppt(Ncontour)=Nppt(Ncontour)+1
                     if (Nppt(Ncontour).gt.Nppt_mx) then
                        call error(label)
                        write(*,*) 'ibuilding=',ibuilding
                        write(*,*) 'Nppt(',',',Ncontour,')=',
     &                       Nppt(Ncontour)
                        write(*,*) '> Nppt_mx=',Nppt_mx
                        stop
                     endif
                     read(14,*) (contour(Ncontour,
     &                    Nppt(Ncontour),j),j=1,2)
                  else          ! nitems(line).ne.2
                     Ncontour=Ncontour+1
                     if (Ncontour.gt.Ncontour_mx) then
                        call error(label)
                        write(*,*) 'ibuilding=',ibuilding
                        write(*,*) 'Ncontour(',')=',Ncontour
                        write(*,*) '> Ncontour_mx=',Ncontour_mx
                        stop
                     endif
                     Nppt(Ncontour)=0
                  endif         ! nitems(line)=2
               else             ! iostatus.ne.0
                  keep_reading=.false.
               endif            ! iostatus=0
            enddo               ! while (keep_reading)
         else
            call error(label)
            write(*,*) 'Building index: ',ibuilding
            write(*,*) 'type=',type
            write(*,*) 'not implemented yet'
            stop
         endif                  ! type
c     
c     Conversions
         e_glass=e_glass*1.0D-3 ! mm->m
c     
c     Consistency checks
         if (type.lt.0) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'type=',type
            write(*,*) '< 0'
            stop
         endif
         if ((type.le.3).and.(Nx.lt.1)) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'Nx=',Nx
            write(*,*) '< 1'
            stop
         endif
         if ((type.le.3).and.(Nx.gt.Nx_mx)) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'Nx=',Nx
            write(*,*) '> Nx_mx=',Nx_mx
            stop
         endif
         if ((type.le.3).and.(Ny.lt.1)) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'Ny=',Ny
            write(*,*) '< 1'
            stop
         endif
         if ((type.le.3).and.(Ny.gt.Ny_mx)) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'Ny=',Ny
            write(*,*) '> Ny_mx=',Ny_mx
            stop
         endif
         if (Nz.lt.1) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'Nz=',Nz
            write(*,*) '< 1'
            stop
         endif
         if ((type.le.3).and.(Nz.gt.Nz_mx)) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'Nz=',Nz
            write(*,*) '> Nz_mx=',Nz_mx
            stop
         endif
         do j=1,dim
            if (a(j).lt.0.0D+0) then
               call error(label)
               write(*,*) 'ibuilding=',ibuilding
               write(*,*) 'a(',',',j,')=',a(j)
               write(*,*) '< 0'
               stop
            endif
         enddo                  ! j
         do j=1,2
            if (e(j).lt.0.0D+0) then
               call error(label)
               write(*,*) 'ibuilding=',ibuilding
               write(*,*) 'e(',',',j,')=',e(j)
               write(*,*) '< 0'
               stop
            endif
         enddo                  ! j
         do j=1,dim
            if ((type.le.3).and.(a(j).lt.e(1))) then
               call error(label)
               write(*,*) 'ibuilding=',ibuilding
               write(*,*) 'a(',j,')=',a(j)
               write(*,*) '< e(1)=',e(1)
               stop
            endif
         enddo                  ! j
         if (wwf.lt.0.0D+0) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'wwf=',wwf
            write(*,*) '< 0'
            stop
         endif
         if (wwf.gt.1.0D+0) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'wwf=',wwf
            write(*,*) '> 1'
            stop
         endif
         if (type.eq.4) then
            if (fhf.lt.0.0D+0) then
               call error(label)
               write(*,*) 'ibuilding=',ibuilding
               write(*,*) 'fhf=',fhf
               write(*,*) '< 0'
               stop
            endif
            if (fhf.gt.1.0D+0) then
               call error(label)
               write(*,*) 'ibuilding=',ibuilding
               write(*,*) 'fhf=',fhf
               write(*,*) '> 1'
               stop
            endif
            alpha=(2*e(2)+Nz*a(3)+(Nz-1)*e(1))
     &           *wwf/(a(3)*Nz*fhf)
            if (alpha.lt.0.0) then
               call error(label)
               write(*,*) 'ibuilding=',ibuilding
               write(*,*) 'alpha=',alpha
               write(*,*) 'should be > 0'
               stop
            endif
            if (alpha.ge.1.0) then
               call error(label)
               write(*,*) 'ibuilding=',ibuilding
               write(*,*) 'alpha=',alpha
               write(*,*) 'should be < 1'
               write(*,*) 'reduce fraction of the surface '
     &              //'used by windows'
               write(*,*) 'or increase fraction of floor height '
     &              //'used by windows'
               stop
            endif
         endif
         if (e_glass.lt.0.0D+0) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'e_glass=',e_glass
            write(*,*) '< 0'
            stop
         endif
         if (e_glass.gt.e(1)) then
            call error(label)
            write(*,*) 'ibuilding=',ibuilding
            write(*,*) 'e_glass=',e_glass
            write(*,*) '> e(','1,)=',e(1)
            stop
         endif
c     End of tests
      endif                     !  file exists
      close(14)
      
      return
      end
