c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine zone(dim,thickness,zone_mat,
     &     Ncontour,Np,contour,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'parameters.inc'
c
c     Purpose: to produce a zone
c
c     Input:
c       + dim: dimension of space
c       + thickness: thickness of the zone [m]
c       + zone_mat: material for zone
c       + Ncontour: number of contours (including holes)
c       + Np: number of 2D coordinates that define the contour of the object
c       + contour: list of 2D coordinates that define the contour of the object
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      double precision thickness
      character*(Nchar_mx) zone_mat
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer iobj,i,j,ic,ip
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
      integer icontour
c     label
      character*(Nchar_mx) label
      label='subroutine zone'
      
c     Debug
c      write(*,*) 'Ncontour=',Ncontour
c      do ic=1,Ncontour
c         write(*,*) 'ic=',ic
c         write(*,*) 'Np=',Np(ic)
c         do ip=1,Np(ic)
c            write(*,*) 'contour(',ic,',',ip,')=',
c     &           (contour(ic,ip,j),j=1,2)
c         enddo                  ! ip
c      enddo                     ! ic
c     Debug

      iobj=0                    ! total number of individual surfaces groups for the bridge
c     ---------------------------------------------------------------------------
c     Production of objects:
c     ---------------------------------------------------------------------------
c     + zone
c     ---------------------------------------------------------------------------
      iobj=iobj+1
c     create object
      call obj_contour(dim,Ncontour,Np,contour,
     &     thickness,
     &     Nv01,Nf01,v01,f01)
c     Debug
c      write(*,*) 'Nv01=',Nv01,' Nf01=',Nf01
c      file_out='./results/zone001.obj'
c      call write_single_obj(dim,file_out,.false.,
c     &     Nv01,Nf01,v01,f01)
c      stop
c     Debug
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(zone_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     
      Nobj=Nobj+iobj
c      
      return
      end
