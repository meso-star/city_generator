c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine lake(dim,
     &     Ncontour,Np,contour,
     &     position,scale,water_mat,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'parameters.inc'
c
c     Purpose: to produce a lake
c
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours for the lake
c       + Np: number of 2D coordinates that define the contour of the object
c       + contour: list of 2D coordinates that define the contour of the object
c       + position: (x,y) shift of the bridge (m)
c       + scale: scaling factor
c       + water_mat: material for lake water
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      double precision position(1:Ndim_mx-1)
      double precision scale
      character*(Nchar_mx) water_mat
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer iobj,i,j
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
c     label
      character*(Nchar_mx) label
      label='subroutine lake'

      iobj=0                    ! total number of individual surfaces groups for the bridge
c     ---------------------------------------------------------------------------
c     Production of objects:
c     ---------------------------------------------------------------------------
c     + lake
c     ---------------------------------------------------------------------------
      iobj=iobj+1
c     create object
      call obj_contour(dim,Ncontour,Np,contour,
     &     river_thickness,
     &     Nv01,Nf01,v01,f01)
c     scaling
      call scale_obj(dim,Nv01,v01,scale)
c     position
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(water_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     
      Nobj=Nobj+iobj
c      
      return
      end
      


      subroutine lake01(dim,thickness,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce a "flat" obj of a object defined by its contour
c     
c     Input:
c       + dim: dimension of space
c       + thickness: thickness of the object
c     
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      double precision thickness
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      double precision alpha
      integer iv,iface,j,iseg
      integer i1,i2,i3,i4
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     label
      character*(Nchar_mx) label
      label='subroutine lake01'

      alpha=20.0D+0 ! multiplication factor
      
      Nnode=0
      Nface=0
      
      Nv01=65
      v01(1,1)=1.0D+0
      v01(1,2)=0.0D+0
      v01(2,1)=2.0D+0
      v01(2,2)=0.8D+0
      v01(3,1)=3.0D+0
      v01(3,2)=1.1D+0
      v01(4,1)=4.0D+0
      v01(4,2)=1.3D+0
      v01(5,1)=5.0D+0
      v01(5,2)=1.8D+0
      v01(6,1)=6.0D+0
      v01(6,2)=2.1D+0
      v01(7,1)=7.0D+0
      v01(7,2)=2.2D+0
      v01(8,1)=8.0D+0
      v01(8,2)=2.2D+0
      v01(9,1)=9.0D+0
      v01(9,2)=2.2D+0
      v01(10,1)=10.0D+0
      v01(10,2)=2.25D+0
      v01(11,1)=11.0D+0
      v01(11,2)=2.3D+0
      v01(12,1)=12.0D+0
      v01(12,2)=2.8D+0
      v01(13,1)=12.5D+0
      v01(13,2)=3.0D+0
      v01(14,1)=13.0D+0
      v01(14,2)=4.0D+0
      v01(15,1)=13.80D+0
      v01(15,2)=6.0D+0
      v01(16,1)=14.0D+0
      v01(16,2)=6.4D+0
      v01(17,1)=14.5D+0
      v01(17,2)=6.8D+0
      v01(18,1)=15.0D+0
      v01(18,2)=7.0D+0
      v01(19,1)=15.7D+0
      v01(19,2)=7.8D+0
      v01(20,1)=16.0D+0
      v01(20,2)=9.0D+0
      v01(21,1)=16.2D+0
      v01(21,2)=10.0D+0
      v01(22,1)=16.5D+0
      v01(22,2)=10.7D+0
      v01(23,1)=17.0D+0
      v01(23,2)=11.2D+0
      v01(24,1)=19.0D+0
      v01(24,2)=11.8D+0
      v01(25,1)=20.0D+0
      v01(25,2)=12.2D+0
      v01(26,1)=20.5D+0
      v01(26,2)=12.5D+0
      v01(27,1)=21.0D+0
      v01(27,2)=13.0D+0
      v01(28,1)=21.3D+0
      v01(28,2)=14.0D+0
      v01(29,1)=21.5D+0
      v01(29,2)=15.0D+0
      v01(30,1)=21.5D+0
      v01(30,2)=16.0D+0
      v01(31,1)=21.4D+0
      v01(31,2)=17.0D+0
      v01(32,1)=21.0D+0
      v01(32,2)=18.0D+0
      v01(33,1)=20.0D+0
      v01(33,2)=19.0D+0
      v01(34,1)=20.0D+0
      v01(34,2)=18.0D+0
      v01(35,1)=20.4D+0
      v01(35,2)=17.3D+0
      v01(36,1)=20.5+0
      v01(36,2)=17.0D+0
      v01(37,1)=20.0D+0
      v01(37,2)=16.6D+0
      v01(38,1)=19.6D+0
      v01(38,2)=16.4D+0
      v01(39,1)=17.2D+0
      v01(39,2)=16.5D+0
      v01(40,1)=13.3D+0
      v01(40,2)=18.6D+0
      v01(41,1)=12.0D+0
      v01(41,2)=18.8D+0
      v01(42,1)=11.0D+0
      v01(42,2)=18.6D+0
      v01(43,1)=10.0D+0
      v01(43,2)=18.1D+0
      v01(44,1)=9.8D+0
      v01(44,2)=17.6D+0
      v01(45,1)=9.0D+0
      v01(45,2)=16.9D+0
      v01(46,1)=8.0D+0
      v01(46,2)=16.6D+0
      v01(47,1)=7.2D+0
      v01(47,2)=16.4D+0
      v01(48,1)=7.1D+0
      v01(48,2)=16.2D+0
      v01(49,1)=7.1D+0
      v01(49,2)=14.3D+0
      v01(50,1)=7.0D+0
      v01(50,2)=13.6D+0
      v01(51,1)=6.6D+0
      v01(51,2)=13.0D+0
      v01(52,1)=6.0D+0
      v01(52,2)=12.2D+0
      v01(53,1)=5.5D+0
      v01(53,2)=11.7D+0
      v01(54,1)=5.0D+0
      v01(54,2)=11.4D+0
      v01(55,1)=4.0D+0
      v01(55,2)=11.2D+0
      v01(56,1)=3.5D+0
      v01(56,2)=10.9D+0
      v01(57,1)=3.0D+0
      v01(57,2)=10.6D+0
      v01(58,1)=2.4D+0
      v01(58,2)=9.8D+0
      v01(59,1)=2.4D+0
      v01(59,2)=9.0D+0
      v01(60,1)=2.5D+0
      v01(60,2)=8.3D+0
      v01(61,1)=2.9D+0
      v01(61,2)=7.0D+0
      v01(62,1)=3.0D+0
      v01(62,2)=6.4D+0
      v01(63,1)=2.0D+0
      v01(63,2)=2.5D+0
      v01(64,1)=1.8D+0
      v01(64,2)=2.0D+0
      v01(65,1)=0.0D+0
      v01(65,2)=1.0D+0
      do iv=1,Nv01
         v01(iv,1)=v01(iv,1)*alpha
         v01(iv,2)=v01(iv,2)*alpha
         v01(iv,3)=0.0D+0
      enddo                     ! i
      
      Nf01=63
      f01(1,1)=1
      f01(1,2)=64
      f01(1,3)=65
      f01(2,1)=1
      f01(2,2)=2
      f01(2,3)=64
      f01(3,1)=2
      f01(3,2)=63
      f01(3,3)=64
      f01(4,1)=2
      f01(4,2)=3
      f01(4,3)=63
      f01(5,1)=3
      f01(5,2)=62
      f01(5,3)=63
      f01(6,1)=3
      f01(6,2)=4
      f01(6,3)=62
      f01(7,1)=4
      f01(7,2)=5
      f01(7,3)=62
      f01(8,1)=5
      f01(8,2)=6
      f01(8,3)=62
      f01(9,1)=6
      f01(9,2)=61
      f01(9,3)=62
      f01(10,1)=6
      f01(10,2)=60
      f01(10,3)=61
      f01(11,1)=6
      f01(11,2)=59
      f01(11,3)=60
      f01(12,1)=6
      f01(12,2)=58
      f01(12,3)=59
      f01(13,1)=6
      f01(13,2)=57
      f01(13,3)=58
      f01(14,1)=6
      f01(14,2)=7
      f01(14,3)=57
      f01(15,1)=7
      f01(15,2)=56
      f01(15,3)=57
      f01(16,1)=7
      f01(16,2)=8
      f01(16,3)=56
      f01(17,1)=8
      f01(17,2)=55
      f01(17,3)=56
      f01(18,1)=8
      f01(18,2)=9
      f01(18,3)=55
      f01(19,1)=9
      f01(19,2)=10
      f01(19,3)=55
      f01(20,1)=10
      f01(20,2)=54
      f01(20,3)=55
      f01(21,1)=10
      f01(21,2)=53
      f01(21,3)=54
      f01(22,1)=10
      f01(22,2)=52
      f01(22,3)=53
      f01(23,1)=10
      f01(23,2)=11
      f01(23,3)=52
      f01(24,1)=11
      f01(24,2)=12
      f01(24,3)=52
      f01(25,1)=12
      f01(25,2)=13
      f01(25,3)=52
      f01(26,1)=13
      f01(26,2)=14
      f01(26,3)=52
      f01(27,1)=14
      f01(27,2)=15
      f01(27,3)=52
      f01(28,1)=15
      f01(28,2)=51
      f01(28,3)=52
      f01(29,1)=15
      f01(29,2)=16
      f01(29,3)=51
      f01(30,1)=16
      f01(30,2)=17
      f01(30,3)=51
      f01(31,1)=17
      f01(31,2)=18
      f01(31,3)=51
      f01(32,1)=18
      f01(32,2)=19
      f01(32,3)=51
      f01(33,1)=19
      f01(33,2)=50
      f01(33,3)=51
      f01(34,1)=19
      f01(34,2)=20
      f01(34,3)=50
      f01(35,1)=20
      f01(35,2)=49
      f01(35,3)=50
      f01(36,1)=20
      f01(36,2)=21
      f01(36,3)=49
      f01(37,1)=21
      f01(37,2)=48
      f01(37,3)=49
      f01(38,1)=21
      f01(38,2)=22
      f01(38,3)=48
      f01(39,1)=22
      f01(39,2)=47
      f01(39,3)=48
      f01(40,1)=22
      f01(40,2)=46
      f01(40,3)=47
      f01(41,1)=22
      f01(41,2)=23
      f01(41,3)=46
      f01(42,1)=23
      f01(42,2)=45
      f01(42,3)=46
      f01(43,1)=23
      f01(43,2)=44
      f01(43,3)=45
      f01(44,1)=23
      f01(44,2)=43
      f01(44,3)=44
      f01(45,1)=23
      f01(45,2)=42
      f01(45,3)=43
      f01(46,1)=23
      f01(46,2)=41
      f01(46,3)=42
      f01(47,1)=23
      f01(47,2)=40
      f01(47,3)=41
      f01(48,1)=23
      f01(48,2)=24
      f01(48,3)=40
      f01(49,1)=24
      f01(49,2)=39
      f01(49,3)=40
      f01(50,1)=24
      f01(50,2)=25
      f01(50,3)=39
      f01(51,1)=25
      f01(51,2)=26
      f01(51,3)=39
      f01(52,1)=26
      f01(52,2)=27
      f01(52,3)=39
      f01(53,1)=27
      f01(53,2)=38
      f01(53,3)=39
      f01(54,1)=27
      f01(54,2)=28
      f01(54,3)=38
      f01(55,1)=28
      f01(55,2)=29
      f01(55,3)=38
      f01(56,1)=29
      f01(56,2)=37
      f01(56,3)=38
      f01(57,1)=29
      f01(57,2)=30
      f01(57,3)=37
      f01(58,1)=30
      f01(58,2)=36
      f01(58,3)=37
      f01(59,1)=30
      f01(59,2)=31
      f01(59,3)=36
      f01(60,1)=31
      f01(60,2)=35
      f01(60,3)=36
      f01(61,1)=31
      f01(61,2)=32
      f01(61,3)=35
      f01(62,1)=32
      f01(62,2)=34
      f01(62,3)=35
      f01(63,1)=32
      f01(63,2)=33
      f01(63,3)=34

      call add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nnode,Nface,vertices,faces)

      do iv=1,Nv01
         v01(iv,dim)=v01(iv,dim)+thickness         
      enddo                     ! iv
      call add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nnode,Nface,vertices,faces)
      if (Nface+2*Nv01.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      else
         iface=Nface
         do iseg=1,Nv01
            i1=iseg
            if (iseg.lt.Nv01) then
               i2=iseg+1
            else
               i2=1
            endif
            i3=i2+Nv01
            i4=i1+Nv01
            iface=iface+1
            faces(iface,1)=i1
            faces(iface,2)=i2
            faces(iface,3)=i4
            iface=iface+1
            faces(iface,1)=i2
            faces(iface,2)=i3
            faces(iface,3)=i4
         enddo                  ! iseg
         Nface=Nface+2*Nv01
      endif

      return
      end
