c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine write_obj(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to write the main mesh to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine write_obj'

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif

      if (invert_normal) then
         call invert_normals(Nf,faces)
      endif ! invert_normal

      open(12,file=trim(output_file))
      do iv=1,Nv
         write(12,21) 'v',(vertices(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf
         write(12,22) 'f',(faces(iface,iv),iv=1,3)
      enddo ! iface
      close(12)

      return
      end



      subroutine write_single_obj(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to append the mesh for a single surface to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine write_single_obj'

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif

      if (invert_normal) then
         call invert_normals(Nf,faces)
      endif ! invert_normal

      open(12,file=trim(output_file))
      do iv=1,Nv
         write(12,21) 'v',(vertices(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf
         write(12,22) 'f',(faces(iface,iv),iv=1,3)
      enddo ! iface
      close(12)
      
      return
      end


      
      subroutine append_single_obj(dim,output_file,invert_normal,
     &     Nv,Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to append the mesh for a single surface to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file to append
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: total number of vertices
c       + Nv01: number of vertices of the object
c       + Nf01: number of faces of the object
c       + v01: array of verices definition (3D position of each vertex)
c       + f01: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine append_single_obj'

      if (Nv01.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv01
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif
c
      if (invert_normal) then
         call invert_normals(Nf01,f01)
      endif                     ! invert_normal
c
      open(81,file=trim(output_file),access='append')
      do iv=1,Nv01
         write(81,21) 'v',(v01(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf01
         write(81,22) 'f',(Nv+f01(iface,iv),iv=1,3)
      enddo ! iface
      close(81)
c     
      return
      end


      
      subroutine append_single_obj_with_mtl(dim,
     &     output_file,mtllib_file,invert_normal,
     &     front_is_st,back_is_st,back_is_thin,
     &     front_name,middle_name,back_name,
     &     Nv,Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to append the mesh for a single surface to a wavefront file
c     with the mtl file corresponding to input material/medium data
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file to append
c       + mtllib_file: mtllib file
c       + invert_normal: true if the direction of normals has to be inverted
c       + front_is_st: true if front is semi-transparent
c       + back_is_st: 1 if back is semi-transparent
c       + back_is_thin: true if back material is thin
c       + front_name: name of the material or medium on the front
c       + middle_name: name of the material or medium on the middle (thin material only)
c       + back_name: name of the material or medium on the back
c       + Nv: total number of vertices
c       + Nv01: number of vertices of the object
c       + Nf01: number of faces of the object
c       + v01: array of verices definition (3D position of each vertex)
c       + f01: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      character*(Nchar_mx) mtllib_file
      logical invert_normal
      logical front_is_st
      logical back_is_st
      logical back_is_thin
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      integer Nv,Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer idim,i,idot
      logical dot_found
      integer*8 iv,iface,tmp1
      character*(Nchar_mx) front_mtl_filename
      character*(Nchar_mx) middle_mtl_filename
      character*(Nchar_mx) back_mtl_filename
      character*(Nchar_mx) objmtl_file
c     label
      character*(Nchar_mx) label
      label='subroutine append_single_obj_with_mtl'

      if (Nv01.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv01
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif
c
      if (invert_normal) then
         call invert_normals(Nf01,f01)
      endif                     ! invert_normal

      call mtl_file_name(front_is_st,front_name,mtllib_file,
     &     front_mtl_filename)
      call mtl_file_name(back_is_st,back_name,mtllib_file,
     &     back_mtl_filename)
c     Append vertexes and faces to main OBJ file
      open(81,file=trim(output_file),access='append')
      if (back_is_thin) then
         write(81,10) 'usemtl '//trim(back_name)
     &        //':'//trim(middle_name)
     &        //':'//trim(front_name)
      else
         write(81,10) 'usemtl '//trim(back_name)
     &        //':'//trim(front_name)
      endif
      do iv=1,Nv01
         write(81,21) 'v',(v01(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf01
         write(81,22) 'f',(Nv+f01(iface,iv),iv=1,3)
      enddo ! iface
      close(81)
c     Append vertexes and faces to the OBJ file that uses a mtllib
      dot_found=.false.
      do i=len_trim(output_file),1,-1
         if (output_file(i:i).eq.'.') then
            dot_found=.true.
            idot=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.dot_found) then
         call error(label)
         write(*,*) 'could not find a . in filename:',trim(output_file)
         stop
      else
         objmtl_file=output_file(1:idot-1)
     &        //'_mtl'
     &        //output_file(idot:len_trim(output_file))
      endif                     ! dot_found=F
c     
      open(82,file=trim(objmtl_file),access='append')
      if (back_is_thin) then
         write(82,10) 'usemtl '//trim(middle_name)
      else
         write(82,10) 'usemtl '//trim(back_name)
      endif
      do iv=1,Nv01
         write(82,21) 'v',(v01(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf01
         write(82,22) 'f',(Nv+f01(iface,iv),iv=1,3)
      enddo ! iface
      close(82)
c     
      return
      end
