c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine sp(dim,thickness,
     &     Ncontour,Np,contour,
     &     edge_mat,edge_width,edge_thickness,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'parameters.inc'
c
c     Purpose: to produce a swimming pool
c
c     Input:
c       + dim: dimension of space
c       + thickness: thickness of the zone [m]
c       + Ncontour: number of contours (including holes)
c       + Np: number of 2D coordinates that define the contour of the object
c       + contour: list of 2D coordinates that define the contour of the object
c       + edge_mat: name of the material for the edge
c       + edge_width: width of the edge [m]
c       + edge_thickness: thickness of the edge [m]
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      double precision thickness
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      character*(Nchar_mx) edge_mat
      double precision edge_width
      double precision edge_thickness
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer iobj,i,j,icontour
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      integer Ncontour_edge
      integer Np_edge(1:Ncontour_mx)
      double precision contour_edge(1:Ncontour_mx,1:Nppt_mx,1:2)
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n_prev(1:Ndim_mx)
      double precision n_next(1:Ndim_mx)
      double precision pa(1:Ndim_mx)
      double precision pb(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)      
c     label
      character*(Nchar_mx) label
      label='subroutine sp'

      iobj=0                    ! total number of individual surfaces groups for the bridge
c     ---------------------------------------------------------------------------
c     Production of objects:
c     ---------------------------------------------------------------------------
c     + swimming area
c     ---------------------------------------------------------------------------
      iobj=iobj+1
c     Override creation process from the provided contour
      call obj_contour(dim,Ncontour,Np,contour,
     &     thickness,
     &     Nv01,Nf01,v01,f01)
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name='blue_water'
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     ---------------------------------------------------------------------------
c     + edge: contours
c     ---------------------------------------------------------------------------
      do icontour=1,Ncontour
         iobj=iobj+1
         Ncontour_edge=2
c     contour 1: external contour of the edge
         Np_edge(1)=Np(1)
         do i=1,Np_edge(1)
c     current segment
            do j=1,2
               p1(j)=contour(icontour,i,j)
               if (i.eq.Np_edge(1)) then
                  p2(j)=contour(icontour,1,j)
               else
                  p2(j)=contour(icontour,i+1,j)
               endif            ! i
            enddo               ! j
            p1(dim)=0.0D+0
            p2(dim)=0.0D+0
            call normal_for_segment(dim,p1,p2,n)
c     previous segment
            do j=1,2
               if (i.eq.1) then
                  pa(j)=contour(icontour,Np_edge(1),j)
                  pb(j)=contour(icontour,1,j)
               else
                  pa(j)=contour(icontour,i-1,j)
                  pb(j)=contour(icontour,i,j)
               endif            ! i
            enddo               ! j
            pa(dim)=0.0D+0
            pb(dim)=0.0D+0
            call normal_for_segment(dim,pa,pb,n_prev)
c     normal @ p1
            call add_vectors(dim,n_prev,n,u)
            call normalize_vector(dim,u,n1)
c            if (icontour.gt.1) then
               call scalar_vector(dim,-1.0D+0,n1,n1)
c            endif               ! icontour > 1
c     new position p3
            call scalar_vector(dim,edge_width,n1,t)
            call add_vectors(dim,p1,t,p3)
c     add point to contour
            do j=1,dim-1
               contour_edge(1,i,j)=p3(j)
            enddo               ! j
         enddo                  ! i
c     contour 2: the hole (swimming area)
         Np_edge(2)=Np(1)
         do i=1,Np_edge(2)
            do j=1,dim-1
               contour_edge(2,i,j)=contour(icontour,i,j)
            enddo               ! j
         enddo                  ! i
c     generate the obj of the edge
         call obj_contour(dim,Ncontour_edge,Np_edge,contour_edge,
     &        edge_thickness,
     &        Nv01,Nf01,v01,f01)
c     Add individual surface to global trianglemesh (for visualization)
         invert_normal=.false.
         sindex=Nobj+iobj
         call num2str(sindex,str,err)
         if (err) then
            call error(label)
            write(*,*) 'while converting to string:'
            write(*,*) 'sindex=',sindex
            stop
         else
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(edge_mat)
            middle_name=''
            call append_single_obj_with_mtl(dim,obj_file,
     &           mtllib_file,
     &           invert_normal,
     &           front_is_st,back_is_st,.false.,
     &           front_name,middle_name,back_name,
     &           Nv,Nv01,Nf01,v01,f01)
c            call retrieve_code(back_name,color_code)
c            call append_single_off(dim,off_file,invert_normal,
c     &           Nv,Nv01,Nf01,v01,f01,color_code)
            if (generate_individual_obj_files) then
               filename='S'//trim(str)//'.obj'
               file_out='./results/obj/'//trim(filename)
               call write_single_obj(dim,file_out,invert_normal,
     &              Nv01,Nf01,v01,f01)
            endif
            Nv=Nv+Nv01
            Nf=Nf+Nf01
         endif                  ! err
      enddo                     ! icontour
c     
      Nobj=Nobj+iobj
c      
      return
      end
