c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine tree(dim,datafile,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to generate trees
c
c     Input:
c       + dim: dimension of space
c       + datafile: the tree definition file to read
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      character*(Nchar_mx) datafile
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer Ntree
      double precision tree_position(1:2)
      double precision tree_scale
      double precision tree_angle
      character*(Nchar_mx) trunk_mat
      character*(Nchar_mx) trunk_file
      character*(Nchar_mx) foliage_mat
      character*(Nchar_mx) foliage_file
      integer iobj,itree,iv,j,i,ios,nl
      character*(Nchar_mx) objfile
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      logical err
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
c     status
      integer len,ndone,Ntodo
      logical cerr
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
c     label
      character*(Nchar_mx) label
      label='subroutine tree'
      
      axe(1)=0.0D+0
      axe(2)=0.0D+0
      axe(3)=1.0D+0
      len=6
      call num2str(len,str,cerr)
      if (.not.cerr) then
         fmt0='(a,i'//trim(str)//',a)'
         fmt='(i'//trim(str)//',a)'
      endif

      iobj=0                    ! total number of individual surfaces groups for the road
c     ---------------------------------------------------------------------------
      open(20,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         call get_nlines(datafile,nl)
         Ntree=Nl-3
         if (Ntree.lt.1) then
            call error(label)
            write(*,*) 'Number of definition lines in file: ',
     &           trim(datafile)
            write(*,*) 'was found to be:',Ntree
            write(*,*) 'should be at least equal to 1'
            stop
         endif                  ! Ntree<1
         do i=1,3
            read(20,*)
         enddo                  ! i
c     status
         fdone0=0
         ndone=0
         pifdone=0
         Ntodo=Ntree
         write(*,trim(fmt0),advance='no')
     &        'Done:   ',floor(fdone0),' %'
c     
         do itree=1,Ntree
            read(20,*) tree_position(1),
     &           tree_position(2),
     &           tree_scale,
     &           tree_angle,
     &           trunk_mat,
     &           trunk_file,
     &           foliage_mat,
     &           foliage_file
            iobj=iobj+1
c     read trunk object file
            objfile='./data/obj_bank/'//trim(trunk_file)
            call read_obj(objfile,dim,
     &           Nv01,Nf01,v01,f01)
c     scale, move and rotate
            if (tree_scale.ne.1.0D+0) then
               do iv=1,Nv01
                  do j=1,dim
                     v01(iv,j)=v01(iv,j)*tree_scale
                  enddo         ! j
               enddo            ! iv
            endif               ! tree_scale.ne.1
            if (tree_angle.ne.0.0D+0) then
               alpha=tree_angle*pi/180.0D+0 ! rad
               call rotation_matrix(dim,alpha,axe,M)
               call rotate_obj(dim,Nv01,v01,M)
            endif
            if ((tree_position(1).ne.0.0D+0).or.
     &           (tree_position(2).ne.0.0D+0)) then
               center(1)=tree_position(1)
               center(2)=tree_position(2)
               center(3)=0.0D+0
               call move_obj(dim,Nv01,v01,center)
            endif
c     Add individual surface to global trianglemesh (for visualization)
            invert_normal=.false.
            sindex=Nobj+iobj
            call num2str(sindex,str,err)
            if (err) then
               call error(label)
               write(*,*) 'while converting to string:'
               write(*,*) 'sindex=',sindex
               stop
            else
               invert_normal=.false.
               front_is_st=.true.
               front_name='air'
               back_is_st=.false.
               back_name=trim(trunk_mat)
               middle_name=''
               call append_single_obj_with_mtl(dim,obj_file,
     &              mtllib_file,
     &              invert_normal,
     &              front_is_st,back_is_st,.false.,
     &              front_name,middle_name,back_name,
     &              Nv,Nv01,Nf01,v01,f01)
               if (generate_individual_obj_files) then
                  filename='S'//trim(str)//'.obj'
                  file_out='./results/obj/'//trim(filename)
                  call write_single_obj(dim,file_out,invert_normal,
     &                 Nv01,Nf01,v01,f01)
               endif
               Nv=Nv+Nv01
               Nf=Nf+Nf01
            endif               ! err
            iobj=iobj+1
c     read foliage object file
            objfile='./data/obj_bank/'//trim(foliage_file)
            call read_obj(objfile,dim,
     &           Nv01,Nf01,v01,f01)
c     scale, move and rotate
            if (tree_scale.ne.1.0D+0) then
               do iv=1,Nv01
                  do j=1,dim
                     v01(iv,j)=v01(iv,j)*tree_scale
                  enddo         ! j
               enddo            ! iv
            endif               ! tree_scale.ne.1
            if (tree_angle.ne.0.0D+0) then
               alpha=tree_angle*pi/180.0D+0 ! rad
               call rotation_matrix(dim,alpha,axe,M)
               call rotate_obj(dim,Nv01,v01,M)
            endif
            if ((tree_position(1).ne.0.0D+0).or.
     &           (tree_position(2).ne.0.0D+0)) then
               center(1)=tree_position(1)
               center(2)=tree_position(2)
               center(3)=0.0D+0
               call move_obj(dim,Nv01,v01,center)
            endif
c     Add individual surface to global trianglemesh (for visualization)
            invert_normal=.false.
            sindex=Nobj+iobj
            call num2str(sindex,str,err)
            if (err) then
               call error(label)
               write(*,*) 'while converting to string:'
               write(*,*) 'sindex=',sindex
               stop
            else
               invert_normal=.false.
               front_is_st=.true.
               front_name='air'
               back_is_st=.false.
c     Debug
c               write(*,*) 'itree=',itree,
c     &              ' foliage_file=',trim(foliage_file),
c     &              ' foliage_mat=',trim(foliage_mat)
c     Debug
               middle_name=trim(foliage_mat)
               back_name='air'
               call append_single_obj_with_mtl(dim,obj_file,
     &              mtllib_file,
     &              invert_normal,
     &              front_is_st,back_is_st,.true.,
     &              front_name,middle_name,back_name,
     &              Nv,Nv01,Nf01,v01,f01)
               if (generate_individual_obj_files) then
                  filename='S'//trim(str)//'.obj'
                  file_out='./results/obj/'//trim(filename)
                  call write_single_obj(dim,file_out,invert_normal,
     &                 Nv01,Nf01,v01,f01)
               endif
               Nv=Nv+Nv01
               Nf=Nf+Nf01
            endif               ! err
c     print status
            ndone=ndone+1
            fdone=dble(ndone)/dble(Ntodo)*1.0D+2
            ifdone=floor(fdone)
            if (ifdone.gt.pifdone) then
               do j=1,len+2
                  write(*,"(a)",advance='no') "\b"
               enddo            ! j
               write(*,trim(fmt),advance='no')
     &              floor(fdone),' %'
               pifdone=ifdone
            endif               ! ifdone > pifdone
c     
         enddo                  ! itree
         write(*,*)
c     ---------------------------------------------------------------------------
c     
      endif                     ! ios.ne.0
      close(20)
      Nobj=Nobj+iobj
c
      return
      end
