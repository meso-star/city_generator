c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine identify_Nroad(Nroad)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of roads defined by the user
c
c     Input:
c
c     Output:
c       + Nroad: number of roads
c
c     I/O
      integer Nroad
c     temp
      logical keep_looking,file_exists
      integer i
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine identify_Nroad'

      Nroad=0
      keep_looking=.true.
      do while (keep_looking)
         i=Nroad+1
         call num2str3(i,str3)
         filename='./data/road'//trim(str3)//'.in'
         inquire(file=trim(filename),exist=file_exists)
         if (file_exists) then
            Nroad=Nroad+1
         else
            keep_looking=.false.
         endif                  ! file_exists
      enddo                     ! while (keep_looking)

      return
      end



      subroutine read_road_data(dim,
     &     Nroad,iroad,width,draw_wbands,
     &     Nppt,central_track,
     &     road_mat,wband_mat)
      implicit none
      include 'max.inc'
c
c     Purpose: to read input data file for roads
c
c     Input:
c       + dim: dimension of space
c
c     Output:
c       + Nroad: number of roads
c       + iroad: index of the road definition file to read
c       + width: width of each road [m]
c       + draw_wbands: T if drawing white bands is required
c       + Nppt: number of points for the description of the central track for each road
c       + central_track: list of positions that describe the central track of each road
c       + road_mat: material for the road
c       + wband_road: material for white bands
c
c     I/O
      integer dim
      integer Nroad
      integer iroad
      double precision width
      logical draw_wbands
      integer Nppt
      double precision central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wband_mat
c     temp
      integer ios,j,k,nl
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine read_road_data'

      call num2str3(iroad,str3)
      filename='./data/road'//trim(str3)//'.in'
      open(15,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         call get_nlines(filename,nl)
         Nppt=nl-11
c     Debug
c         write(*,*) trim(filename),nl,Nppt
c     Debug
         if (Nppt.lt.2) then
            call error(label)
            write(*,*) 'Central track for road index: ',iroad
            write(*,*) 'is described using: ',Nppt,' points'
            write(*,*) 'which should be > 1'
            stop
         endif                  ! Nppt<2
         if (Nppt.gt.Nppt_mx) then
            call error(label)
            write(*,*) 'Central track for road index: ',iroad
            write(*,*) 'is described using: ',Nppt,' points'
            write(*,*) 'which is > Nppt_mx=',Nppt_mx
            stop
         endif                  ! Nppt>Nppt_mx
         do j=1,3
            read(15,*)
         enddo                  ! j
         read(15,*) road_mat
         read(15,*) 
         read(15,*) draw_wbands
         read(15,*)
         read(15,*) wband_mat
         read(15,*)
         read(15,*) width
         read(15,*)
         do j=1,Nppt
            read(15,*) (central_track(j,k),k=1,2)
         enddo                  ! j
c     Consistency checks
         if (width.lt.0) then
            call error(label)
            write(*,*) 'width=',width
            write(*,*) '< 0'
            stop
         endif
c     End of tests
      endif                     !  file exists
      close(15)
      
      return
      end
