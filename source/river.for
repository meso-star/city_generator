c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine river(dim,
     &     width,
     &     Nppt,central_track,
     &     river_mat,
     &     draw_ground,xmin,xmax,ymin,ymax,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
c
c     Purpose: to produce a river
c
c     Input:
c       + dim: dimension of space
c       + width: width of the river [m]
c       + Nppt: number of points for the description of the central track
c       + central_track: list of positions that describe the central track
c       + river_mat: material for the river
c       + draw_ground: T when drawing ground is required
c       + xmin,xmax,ymin,ymax: limits of the ground
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      double precision width
      integer Nppt
      double precision central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) river_mat
      logical draw_ground
      double precision xmin,xmax,ymin,ymax
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer iseg,Nseg,iobj,i
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n_tmp(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision d(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
c     label
      character*(Nchar_mx) label
      label='subroutine river'

      Nseg=Nppt-1
      if (Nseg.lt.1) then
         call error(label)
         write(*,*) 'Nseg=',Nseg
         write(*,*) 'should be > 0'
         stop
      endif
      iobj=0                    ! total number of individual surfaces groups for the river
      do iseg=1,Nseg
         p1(1)=central_track(iseg,1)
         p1(2)=central_track(iseg,2)
         p1(3)=0.0D+0
         p2(1)=central_track(iseg+1,1)
         p2(2)=central_track(iseg+1,2)
         p2(3)=0.0D+0
         call normal_for_segment(dim,p1,p2,n)
c     n is the normal for the current segment
         if (iseg.eq.1) then
            call copy_vector(dim,n,n1)
         else                   ! iseg>1
            p0(1)=central_track(iseg-1,1)
            p0(2)=central_track(iseg-1,2)
            p0(3)=0.0D+0
            call normal_for_segment(dim,p0,p1,n_tmp)
            call add_vectors(dim,n_tmp,n,u)
            call normalize_vector(dim,u,n1)
         endif                  ! iseg=1
c     n1 is the normal to use for P1
         if (iseg.eq.Nseg) then
            call copy_vector(dim,n,n2)
         else                   ! iseg>1
            p0(1)=central_track(iseg+2,1)
            p0(2)=central_track(iseg+2,2)
            p0(3)=0.0D+0
            call normal_for_segment(dim,p2,p0,n_tmp)
            call add_vectors(dim,n_tmp,n,u)
            call normalize_vector(dim,u,n2)
         endif                  ! iseg=Nseg
c     n2 is the normal to use for P2
         call scalar_vector(dim,width/2.0D+0,n1,d)
         call substract_vectors(dim,p1,d,x1)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x1)
         endif
         call add_vectors(dim,p1,d,x4)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x4)
         endif
         call scalar_vector(dim,width/2.0D+0,n2,d)
         call substract_vectors(dim,p2,d,x2)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x2)
         endif
         call add_vectors(dim,p2,d,x3)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x3)
         endif
c     ---------------------------------------------------------------------------
c     ---------------------------------------------------------------------------
c     Production of objects:
c     ---------------------------------------------------------------------------
c     + section of the river
c     ---------------------------------------------------------------------------
         iobj=iobj+1
         call obj_segment(dim,x1,x2,x3,x4,
     &        Nv01,Nf01,v01,f01)
c     Add individual surface to global trianglemesh (for visualization)
         invert_normal=.false.
         sindex=Nobj+iobj
         call num2str(sindex,str,err)
         if (err) then
            call error(label)
            write(*,*) 'while converting to string:'
            write(*,*) 'sindex=',sindex
            stop
         else
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(river_mat)
            middle_name=''
            call append_single_obj_with_mtl(dim,obj_file,
     &           mtllib_file,
     &           invert_normal,
     &           front_is_st,back_is_st,.false.,
     &           front_name,middle_name,back_name,
     &           Nv,Nv01,Nf01,v01,f01)
c            call retrieve_code(back_name,color_code)
c            call append_single_off(dim,off_file,invert_normal,
c     &           Nv,Nv01,Nf01,v01,f01,color_code)
            if (generate_individual_obj_files) then
               filename='S'//trim(str)//'.obj'
               file_out='./results/obj/'//trim(filename)
               call write_single_obj(dim,file_out,invert_normal,
     &              Nv01,Nf01,v01,f01)
            endif
            Nv=Nv+Nv01
            Nf=Nf+Nf01
         endif                  ! err
      enddo                     ! iseg
c     ---------------------------------------------------------------------------
c     
      Nobj=Nobj+iobj
c
      return
      end
