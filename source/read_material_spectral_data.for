c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_material_spectral_data(datafile,
     &     Nmaterial,material_name,Nlambda,lambda,sdata)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read a .csv solid material spectral database
c     
c     Input:
c       + datafile: name of the .csv data file to read
c     
c     Output:
c       + Nmaterial: number of materials
c       + material_name: array of material identification
c       + Nlambda: number of wavelength points
c       + lambda: array of wavelengths
c       + sdata: array of spectral data for each material, each wavelength
c     
c     I/O
      character*(Nchar_mx) datafile
      integer Nmaterial
      character*(Nchar_mx) material_name(1:Nmaterial_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision sdata(1:Nlambda_mx,1:Nmaterial_mx)
c     temp
      integer imat,ilambda,ios,i0,i,nl
      character*(Nchar_mx) line,st
      logical keep_looking,run
c     label
      character*(Nchar_mx) label
      label='subroutine read_material_spectral_data'

      Nmaterial=0
      
      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         goto 666
      else
         call get_nlines(datafile,nl)
         Nlambda=nl-1
         if (Nlambda.gt.Nlambda_mx) then
            call error(label)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) '> Nlambda_mx=',Nlambda_mx
            stop
         endif                  ! Nlambda > Nlambda_mx
c     -----------------------------------------------------------------
c     read the first line
c     -----------------------------------------------------------------
         read(11,10) line
         i0=0
         keep_looking=.true.
         do while (keep_looking)
            i=1
            run=.true.
            do while (run)
               if (i0+i.gt.len_trim(line)) then
                  run=.false.
                  keep_looking=.false.
                  goto 111
               endif
               if ((line(i0+i:i0+i).eq.',').or.
     &              (i0+i.eq.len_trim(line))) then ! character index i0+i is a comma ','
                  if (line(i0+i:i0+i).eq.',') then
                     st=line(i0+1:i0+i-1)
                  else
                     st=line(i0+1:i0+i)
                  endif
c     Debug
c                  write(*,*) 'st=',trim(st)
c     Debug
                  if (trim(st).ne.'"wavelength"') then
                     Nmaterial=Nmaterial+1
                     if (Nmaterial.gt.Nmaterial_mx) then
                        call error(label)
                        write(*,*) 'Nmaterial_mx was reached'
                        stop
                     else
                        if ((st(1:1).eq.'"').and.
     &                       (st(len_trim(st):len_trim(st)).eq.'"'))
     &                       then
                           material_name(Nmaterial)=st(2:len_trim(st)-1)
                        else
                           material_name(Nmaterial)=trim(st)
                        endif
                     endif      ! Nmaterial.gt.Nmaterial_mx
c     Debug
c                     write(*,*) Nmaterial,trim(material_name(Nmaterial))
c     Debug
                  endif         ! trim(st).ne.'"wavelength"'
                  run=.false.
               endif            ! character is a comma
               if (run) then
                  i=i+1
               endif
            enddo               !  while (run)
 111        continue
            if (keep_looking) then
               i0=i0+i
            endif               ! keep_looking
         enddo                  !  while (keep_looking)
c     -----------------------------------------------------------------
c     read all other lines
c     -----------------------------------------------------------------
         do ilambda=1,Nlambda
            read(11,*) lambda(ilambda),
     &           (sdata(ilambda,imat),imat=1,Nmaterial)
         enddo                  ! ilambda
c     -----------------------------------------------------------------
      endif
      close(11)

 666  continue
c     Debug
c      stop
c     Debug
      return
      end
