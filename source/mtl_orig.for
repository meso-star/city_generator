c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine mtl_file_name(front_is_st,back_is_st,
     &     front_name,back_name,
     &     mtl_filename)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the mtl file corresponding
c     to input material / medium data
c     
c     Input:
c       + front_is_st: true if front is semi-transparent
c       + back_is_st: 1 if back is semi-transparent
c       + front_name: name of the material or medium on the front
c       + back_name: name of the material or medium on the back
c     
c     Output:
c       + mtl_filename: name of the mtl file
c     
c     I/O
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) back_name
      character*(Nchar_mx) mtl_filename
c     temp
      logical file_exists
c     label
      character*(Nchar_mx) label
      label='subroutine mtl_file_name'

      mtl_filename='./results/'//trim(front_name)
     &     //'_'//trim(back_name)//'.mtl'
      inquire(file=trim(mtl_filename),exist=file_exists)
      if (.not.file_exists) then
         call generate_mtl_file(front_is_st,back_is_st,
     &        front_name,back_name,mtl_filename)
      endif

      return
      end


      
      subroutine generate_mtl_file(front_is_st,back_is_st,
     &     front_name,back_name,mtl_filename)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to generate a mtl file
c     
c     Input:
c       + front_is_st: true if front is semi-transparent
c       + back_is_st: 1 if back is semi-transparent
c       + front_name: name of the material or medium on the front
c       + back_name: name of the material or medium on the back
c       + mtl_filename: name of the mtl file
c     
c     Output:
c       + the required mtl file in /results
c     
c     I/O
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) back_name
      character*(Nchar_mx) mtl_filename
c     temp
      character*(Nchar_mx) name,side,bline
      logical found,is_st
      integer index,pass
      integer ilambda
      character*(Nchar_mx) aspect
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision nref(1:Nlambda_mx)
      double precision ka(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision emissivity(1:Nlambda_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_mtl_file'

      open(14,file=trim(mtl_filename))
      write(14,*) 'interface:'
      do pass=1,2
         bline=achar(9)         ! achar(9) is a tabulation
         if (pass.eq.1) then
            is_st=front_is_st
            name=trim(front_name)
            side='front:'
         else
            is_st=back_is_st
            name=trim(back_name)
            side='back:'
         endif
         call identify_name(is_st,name,found,index)
c     ---- special case of 'air' ----
         if ((.not.found).and.(trim(name).eq.'air')) then
            goto 111
         endif
c     ---- special case of 'air' ----
         if (.not.found) then
            if (is_st) then
               call add_name_to_medium_list(name)
               index=Nmedium_known
            else
               call add_name_to_material_list(name)
               index=Nmaterial_known
            endif               ! is_st
         endif                  ! .not.found
         if (is_st) then
            Nlambda=medium_Nlambda(index)
            if (Nlambda.eq.0) then
               nref(1)=medium_nref(index,1)
               ka(1)=medium_ka(index,1)
               ks(1)=medium_ks(index,1)
            else
               do ilambda=1,Nlambda
                  lambda(ilambda)=medium_lambda(index,ilambda)
                  nref(ilambda)=medium_nref(index,ilambda)
                  ka(ilambda)=medium_ka(index,ilambda)
                  ks(ilambda)=medium_ks(index,ilambda)
               enddo            ! ilambda
            endif               ! Nlambda=0
         else                   ! is_st=F
            aspect=material_aspect(index)
            Nlambda=material_Nlambda(index)
            if (Nlambda.eq.0) then
               reflectivity(1)=material_reflectivity(index,1)
               emissivity(1)=material_emissivity(index,1)
            else
               do ilambda=1,Nlambda
                  lambda(ilambda)=material_lambda(index,ilambda)
                  reflectivity(ilambda)=
     &                 material_reflectivity(index,ilambda)
                  emissivity(ilambda)=material_emissivity(index,ilambda)
               enddo            ! ilambda
            endif               ! Nlambda=0
         endif                  ! is_st
 111     continue
         write(14,*) trim(bline)//trim(side)
         bline=trim(bline)//achar(9)
         if (is_st) then
            write(14,*) trim(bline)//'medium:'
            bline=trim(bline)//achar(9)
            write(14,*) trim(bline)//trim(name)
c     ---- special case of 'air' ----
            if (trim(name).eq.'air') then
               goto 112
            endif ! name='air'
c     ---- special case of 'air' ----
            if (Nlambda.eq.0) then
               write(14,*) trim(bline)//'refraction_index: {',
     &              nref(1),'}'
               write(14,*) trim(bline)//'absorption_coefficient: {',
     &              ka(1),'}'
               write(14,*) trim(bline)//'scattering_coefficient: {',
     &              ks(1),'}'
            else
               write(14,*) trim(bline)//'refraction_index: {',
     &              ('{',lambda(ilambda),nref(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
               write(14,*) trim(bline)//'absorption_coefficient: {',
     &              ('{',lambda(ilambda),ka(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
               write(14,*) trim(bline)//'scattering_coefficient: {',
     &              ('{',lambda(ilambda),ks(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
            endif               ! Nlambda=0
         else
            write(14,*) trim(bline)//'material:'
            bline=trim(bline)//achar(9)
            call add_semicolon_if_absent(aspect)
            write(14,*) trim(bline)//trim(aspect)
            bline=trim(bline)//achar(9)
            write(14,*) trim(bline)//trim(name)
            if (Nlambda.eq.0) then
               write(14,*) trim(bline)//'reflectivity: {',
     &              reflectivity(1),'}'
               write(14,*) trim(bline)//'emissivity: {',
     &              emissivity(1),'}'
            else
               write(14,*) trim(bline)//'reflectivity: {',
     &              ('{',lambda(ilambda),reflectivity(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
               write(14,*) trim(bline)//'emissivity: {',
     &              ('{',lambda(ilambda),emissivity(ilambda),'}',
     &              ilambda=1,Nlambda),' }'
            endif               ! Nlambda=0
         endif                  ! st
 112     continue
      enddo                     ! pass
      close(14)

      return
      end


      
      subroutine generate_mtl_file2(front_is_st,back_is_st,
     &     front_name,back_name,mtl_filename)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
      include 'formats.inc'
c     
c     Purpose: to generate a mtl file
c     
c     Input:
c       + front_is_st: true if front is semi-transparent
c       + back_is_st: 1 if back is semi-transparent
c       + front_name: name of the material or medium on the front
c       + back_name: name of the material or medium on the back
c       + mtl_filename: name of the mtl file
c     
c     Output:
c       + the required mtl file in /results
c     
c     I/O
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) back_name
      character*(Nchar_mx) mtl_filename
c     temp
      character*(Nchar_mx) name,side,bline
      logical found,is_st
      integer index,pass
      integer ilambda
      character*(Nchar_mx) aspect
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision nref(1:Nlambda_mx)
      double precision ka(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision emissivity(1:Nlambda_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_mtl_file2'

      open(14,file=trim(mtl_filename))
      write(14,10) '# Type (interface, shell):'
      write(14,10) 'interface'
      do pass=1,2
         if (pass.eq.1) then
            is_st=front_is_st
            name=trim(front_name)
            side='front'
         else
            is_st=back_is_st
            name=trim(back_name)
            side='back'
         endif
         call identify_name(is_st,name,found,index)
c     ---- special case of 'air' ----
         if ((.not.found).and.(trim(name).eq.'air')) then
            goto 111
         endif
c     ---- special case of 'air' ----
         if (.not.found) then
            if (is_st) then
               call add_name_to_medium_list(name)
               index=Nmedium_known
            else
               call add_name_to_material_list(name)
               index=Nmaterial_known
            endif               ! is_st
         endif                  ! .not.found
         if (is_st) then
            Nlambda=medium_Nlambda(index)
            if (Nlambda.eq.0) then
               nref(1)=medium_nref(index,1)
               ka(1)=medium_ka(index,1)
               ks(1)=medium_ks(index,1)
            else
               do ilambda=1,Nlambda
                  lambda(ilambda)=medium_lambda(index,ilambda)
                  nref(ilambda)=medium_nref(index,ilambda)
                  ka(ilambda)=medium_ka(index,ilambda)
                  ks(ilambda)=medium_ks(index,ilambda)
               enddo            ! ilambda
            endif               ! Nlambda=0
         else                   ! is_st=F
            aspect=material_aspect(index)
            Nlambda=material_Nlambda(index)
            if (Nlambda.eq.0) then
               reflectivity(1)=material_reflectivity(index,1)
               emissivity(1)=material_emissivity(index,1)
            else
               do ilambda=1,Nlambda
                  lambda(ilambda)=material_lambda(index,ilambda)
                  reflectivity(ilambda)=
     &                 material_reflectivity(index,ilambda)
                  emissivity(ilambda)=material_emissivity(index,ilambda)
               enddo            ! ilambda
            endif               ! Nlambda=0
         endif                  ! is_st
 111     continue
         write(14,*) trim(bline)//trim(side)
         bline=trim(bline)//achar(9)
         if (is_st) then
            write(14,*) trim(bline)//'medium:'
            bline=trim(bline)//achar(9)
            write(14,*) trim(bline)//trim(name)
c     ---- special case of 'air' ----
            if (trim(name).eq.'air') then
               goto 112
            endif ! name='air'
c     ---- special case of 'air' ----
            if (Nlambda.eq.0) then
               write(14,*) trim(bline)//'refraction_index: {',
     &              nref(1),'}'
               write(14,*) trim(bline)//'absorption_coefficient: {',
     &              ka(1),'}'
               write(14,*) trim(bline)//'scattering_coefficient: {',
     &              ks(1),'}'
            else
               write(14,*) trim(bline)//'refraction_index: {',
     &              ('{',lambda(ilambda),nref(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
               write(14,*) trim(bline)//'absorption_coefficient: {',
     &              ('{',lambda(ilambda),ka(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
               write(14,*) trim(bline)//'scattering_coefficient: {',
     &              ('{',lambda(ilambda),ks(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
            endif               ! Nlambda=0
         else
            write(14,*) trim(bline)//'material:'
            bline=trim(bline)//achar(9)
            call add_semicolon_if_absent(aspect)
            write(14,*) trim(bline)//trim(aspect)
            bline=trim(bline)//achar(9)
            write(14,*) trim(bline)//trim(name)
            if (Nlambda.eq.0) then
               write(14,*) trim(bline)//'reflectivity: {',
     &              reflectivity(1),'}'
               write(14,*) trim(bline)//'emissivity: {',
     &              emissivity(1),'}'
            else
               write(14,*) trim(bline)//'reflectivity: {',
     &              ('{',lambda(ilambda),reflectivity(ilambda),'}',
     &              ilambda=1,Nlambda),'}'
               write(14,*) trim(bline)//'emissivity: {',
     &              ('{',lambda(ilambda),emissivity(ilambda),'}',
     &              ilambda=1,Nlambda),' }'
            endif               ! Nlambda=0
         endif                  ! st
 112     continue
      enddo                     ! pass
      close(14)

      return
      end
      


      subroutine identify_name(is_st,name,found,index)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to identify a name in the list of material or the list of medium
c     
c     Input:
c       + is_st: true if semi-transparent
c       + name: name
c     
c     Output:
c       + found: true if name was found in the "material_name" list for is_st=F
c                and in the "medium_name" list for is_st=T
c       + index: index in the list for found=T
c
c     I/O
      logical is_st
      character*(Nchar_mx) name
      logical found
      integer index
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine identify_name'

      found=.false.
      if (is_st) then
         do i=1,Nmedium_known
            if (trim(name).eq.trim(medium_name(i))) then
               found=.true.
               index=i
               goto 111
            endif
         enddo                  ! i
      else                      ! is_st=F
         do i=1,Nmaterial_known
            if (trim(name).eq.trim(material_name(i))) then
               found=.true.
               index=i
               goto 111
            endif
         enddo                  ! i
      endif                     ! st
 111  continue

      return
      end

      

      subroutine add_name_to_material_list(name)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to add a material to the material list
c     
c     Input:
c       + name: name of the material to add
c     
c     Output:
c       + updated values of "Nmaterial_known", "material_name", "material_Nlambda",
c         "material_reflectivity" and "material_emissivity"
c         (defined in "mtl.inc")
c      
c     I/O
      character*(Nchar_mx) name
c     temp
      character*(Nchar_mx) datafile
      logical file_exists
      character*(Nchar_mx) name_read,aspect_read
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision emissivity(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      integer ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine add_name_to_material_list'

      datafile='./data/material_properties/'//trim(name)//'.properties'
      inquire(file=trim(datafile),exist=file_exists)
      if (.not.file_exists) then
         call error(label)
         write(*,*) 'Material definition file not found for:'
         write(*,*) trim(name)
         stop
      else
         call read_material_properties(datafile,
     &        name_read,aspect_read,
     &        Nlambda,lambda,reflectivity,emissivity)
         if (trim(name).ne.trim(name_read)) then
            call error(label)
            write(*,*) 'In file: ',trim(datafile)
            write(*,*) 'material label is: ',trim(name_read)
            write(*,*) 'while looking for name= ',trim(name)
            stop
         endif
         Nmaterial_known=Nmaterial_known+1
         if (Nmaterial_known.gt.Nmaterial_mx) then
            call error(label)
            write(*,*) 'Nmaterial_mx was reached'
            stop
         endif
         material_name(Nmaterial_known)=trim(name_read)
         material_aspect(Nmaterial_known)=trim(aspect_read)
         material_Nlambda(Nmaterial_known)=Nlambda
         if (Nlambda.eq.0) then
            material_reflectivity(Nmaterial_known,1)=reflectivity(1)
            material_emissivity(Nmaterial_known,1)=emissivity(1)
         else
            do ilambda=1,Nlambda
               material_lambda(Nmaterial_known,ilambda)=lambda(ilambda)
               material_reflectivity(Nmaterial_known,ilambda)=
     &              reflectivity(ilambda)
               material_emissivity(Nmaterial_known,ilambda)=
     &              emissivity(ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
      endif                     ! file_exists

      return
      end



      subroutine add_name_to_medium_list(name)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to add a medium to the medium list
c     
c     Input:
c       + name: name of the medium to add
c     
c     Output:
c       + updated values of "Nmedium_known", "medium_name", "medium_Nlambda",
c         "medium_nref", "medium_ka" and "medium_ks"
c         (defined in "mtl.inc")
c      
c     I/O
      character*(Nchar_mx) name
c     temp
      character*(Nchar_mx) datafile
      logical file_exists
      character*(Nchar_mx) name_read
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision nref(1:Nlambda_mx)
      double precision ka(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      integer ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine add_name_to_medium_list'

      datafile='./data/medium_properties/'//trim(name)//'.properties'
      inquire(file=trim(datafile),exist=file_exists)
      if (.not.file_exists) then
         call error(label)
         write(*,*) 'Medium definition file not found for:'
         write(*,*) trim(name)
         stop
      else
         call read_medium_properties(datafile,
     &        name_read,Nlambda,lambda,nref,ka,ks)
         if (trim(name).ne.trim(name_read)) then
            call error(label)
            write(*,*) 'In file: ',trim(datafile)
            write(*,*) 'medium label is: ',trim(name_read)
            write(*,*) 'while looking for name= ',trim(name)
            stop
         endif
         Nmedium_known=Nmedium_known+1
         if (Nmedium_known.gt.Nmedium_mx) then
            call error(label)
            write(*,*) 'Nmedium_mx was reached'
            stop
         endif
         medium_name(Nmedium_known)=trim(name_read)
         medium_Nlambda(Nmedium_known)=Nlambda
         if (Nlambda.eq.0) then
            medium_nref(Nmedium_known,1)=nref(1)
            medium_ka(Nmedium_known,1)=ka(1)
            medium_ks(Nmedium_known,1)=ks(1)
         else
            do ilambda=1,Nlambda
               medium_lambda(Nmedium_known,ilambda)=lambda(ilambda)
               medium_nref(Nmedium_known,ilambda)=nref(ilambda)
               medium_ka(Nmedium_known,ilambda)=ka(ilambda)
               medium_ks(Nmedium_known,ilambda)=ks(ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
      endif                     ! file_exists

      return
      end
