c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_global_data(datafile,
     &     draw_ground,
     &     draw_buildings,
     &     draw_roads,
     &     draw_rivers,
     &     draw_bridges,
     &     draw_lakes,
     &     draw_zones,
     &     draw_trees,
     &     draw_decorations,
     &     draw_swimmingpools)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read global settings
c     
c     Input:
c       + datafile: name of the file to read
c     
c     Output:
c       + draw_ground: T when drawing ground is required
c       + draw_buildings: T when drawing buildings is required
c       + draw_roads: T when drawing roads is required
c       + draw_rivers: T when drawing rivers is required
c       + draw_bridges: T when drawing bridges is required
c       + draw_lakes: T when drawing lakes is required
c       + draw_zones: T when drawing zones is required
c       + draw_trees: T when drawing trees is required
c       + draw_decorations: T when drawing decorations is required
c       + draw_swimmingpools: T when drawing swimming pools is required
c
c     I/O
      character*(Nchar_mx) datafile
      logical draw_ground
      logical draw_buildings
      logical draw_roads
      logical draw_rivers
      logical draw_bridges
      logical draw_lakes
      logical draw_zones
      logical draw_trees
      logical draw_decorations
      logical draw_swimmingpools
c     temp
      integer i,ios,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_global_data'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:',trim(datafile)
         stop
      else
         do i=1,3
            read(11,*)
         enddo                  ! i
         read(11,*) draw_ground
         read(11,*)
         read(11,*) draw_buildings
         read(11,*)
         read(11,*) draw_roads
         read(11,*)
         read(11,*) draw_rivers
         read(11,*)
         read(11,*) draw_bridges
         read(11,*)
         read(11,*) draw_lakes
         read(11,*)
         read(11,*) draw_zones
         read(11,*)
         read(11,*) draw_trees
         read(11,*)
         read(11,*) draw_decorations
         read(11,*)
         read(11,*) draw_swimmingpools
      endif
      close(11)

      return
      end
