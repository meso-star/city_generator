c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_ground_data(datafile,
     &     xmin,xmax,ymin,ymax,ground_mat)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read ground settings
c     
c     Input:
c       + datafile: name of the file to read
c     
c     Output:
c       + xmin: lowest value of X map extension
c       + xmax: highest value of X map extension
c       + ymin: lowest value of Y map extension
c       + ymax: highest value of Y map extension
c       + ground_mat: name of the material for the ground
c
c     I/O
      character*(Nchar_mx) datafile
      double precision xmin,xmax
      double precision ymin,ymax
      character*(Nchar_mx) ground_mat
c     temp
      integer i,ios,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_ground_data'
      
      open(13,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:',trim(datafile)
         stop
      else
         do i=1,3
            read(13,*)
         enddo                  ! i
         read(13,*) xmin
         read(13,*) xmax
         read(13,*)
         read(13,*) ymin
         read(13,*) ymax
         read(13,*)
         read(13,*) ground_mat
      endif                     ! ios
      close(13)
      
c     Consistency checks
      if (xmax.le.xmin) then
         call error(label)
         write(*,*) 'xmax=',xmax
         write(*,*) 'should be > xmin=',xmin
         stop
      endif
      if (ymax.le.ymin) then
         call error(label)
         write(*,*) 'ymax=',ymax
         write(*,*) 'should be > ymin=',ymin
         stop
      endif

      return
      end
      
