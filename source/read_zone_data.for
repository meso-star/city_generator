c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine identify_Nzone(Nzone)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of zones defined by the user
c
c     Input:
c
c     Output:
c       + Nzone: number of zones
c
c     I/O
      integer Nzone
c     temp
      logical keep_looking,file_exists
      integer i
      character*4 str4
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine identify_Nzone'

      Nzone=0
      keep_looking=.true.
      do while (keep_looking)
         i=Nzone+1
         call num2str4(i,str4)
         filename='./data/zone'//trim(str4)//'.in'
         inquire(file=trim(filename),exist=file_exists)
         if (file_exists) then
            Nzone=Nzone+1
         else
            keep_looking=.false.
         endif                  ! file_exists
      enddo                     ! while (keep_looking)

      return
      end



      subroutine read_zone_data(dim,
     &     Nzone,izone,zone_mat,zone_thickness,
     &     Ncontour,Nppt,contour_track)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to read input data file for zones
c
c     Input:
c       + dim: dimension of space
c
c     Output:
c       + Nzone: number of zones
c       + izone: index of the zone definition file to read
c       + zone_mat: material for the zone
c       + zone_thickness: thickness for the zone
c       + Ncontour: number of contours for each zone
c       + Nppt: number of points for the description of each contour track for each zone
c       + contour_track: list of positions that describe the central track of each contour for each zone
c
c     I/O
      integer dim
      integer Nzone
      integer izone
      character*(Nchar_mx) zone_mat
      double precision zone_thickness
      integer Ncontour
      integer Nppt(1:Ncontour_mx)
      double precision contour_track(1:Ncontour_mx,1:Nppt_mx,1:2)
c     temp
      integer ios,j,k,nl
      logical keep_reading
      character*4 str4
      character*(Nchar_mx) filename
      character*(Nchar_mx) line
c     functions
      integer nitems
c     label
      character*(Nchar_mx) label
      label='subroutine read_zone_data'

      call num2str4(izone,str4)
      filename='./data/zone'//trim(str4)//'.in'
      open(19,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do j=1,3
            read(19,*)
         enddo                  ! j
         read(19,*) zone_mat
         read(19,*)
         read(19,*) zone_thickness
         read(19,*)
         Ncontour=1
         Nppt(Ncontour)=0
         keep_reading=.true.
         do while (keep_reading)
            read(19,10,iostat=ios) line
            if (ios.eq.0) then
               if (nitems(line).eq.2) then
                  backspace(19)
                  Nppt(Ncontour)=Nppt(Ncontour)+1
                  if (Nppt(Ncontour).gt.Nppt_mx) then
                     call error(label)
                     write(*,*) 'Nppt(',',',Ncontour,')=',
     &                    Nppt(Ncontour)
                     write(*,*) '> Nppt_mx=',Nppt_mx
                     stop
                  endif
                  read(19,*) (contour_track(Ncontour,
     &                 Nppt(Ncontour),j),j=1,2)
               else             ! nitems(line).ne.2
                  Ncontour=Ncontour+1
                  if (Ncontour.gt.Ncontour_mx) then
                     call error(label)
                     write(*,*) 'Ncontour(',')=',Ncontour
                     write(*,*) '> Ncontour_mx=',Ncontour_mx
                     stop
                  endif
                  Nppt(Ncontour)=0
               endif            ! nitems(line)=2
            else
               keep_reading=.false.
            endif               ! ios=0
         enddo                  ! while (keep_reading)
      endif                     !  file exists
      close(19)
      
      return
      end
