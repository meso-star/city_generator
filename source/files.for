c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine record_gnuplot_contour(filename,dim,
     &     Ncontour,Nppc,contour)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a gnuplot file for visualizing contours
c     
c     Input:
c       + filename: name of the file to generate
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     Output: the requested file
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,icontour,j
c     label
      character*(Nchar_mx) label
      label='subroutine record_gnuplot_contour'

      open(22,file=trim(filename))
      do icontour=1,Ncontour
         do i=1,Nppc(icontour)
            write(22,*) (contour(icontour,i,j),j=1,dim-1)
         enddo                  ! i
         write(22,*) (contour(icontour,1,j),j=1,dim-1)
         write(22,*)
      enddo                     ! icontour
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end



      subroutine record_area_file(filename,dim,Abuilding,Aglass)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record the output file that provides information
c     about areas
c     
c     Input:
c       + filename: name of the file to generate
c       + dim: dimension of space
c       + Abuilding: total lateral area of buildings, per building type
c       + Aglass: glass area of buildings, per building type
c     
c     Output: the required output file
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      double precision Abuilding(0:4)
      double precision Aglass(0:4)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine record_area_file'

      open(15,file=trim(filename))
      write(15,10) '# Unit of area: square meters'
      write(15,10) '# Total lateral area of buildings '
     &     //'(one line for each type):'
      do i=0,4
         write(15,*) Abuilding(i)
      enddo                     ! i
      write(15,10) '# Total glass area of buildings '
     &     //'(one line for each type):'
      do i=0,4
         write(15,*) Aglass(i)
      enddo                     ! i
      write(15,10) '# Fraction of lateral area used by glass '
     &     //'(one line for each type):'
      do i=0,4
         write(15,*) Aglass(i)/Abuilding(i)
      enddo                     ! i
      close(15)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end


      
      subroutine record_volume_file(filename,dim,Vroof)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record the output file that provides information
c     about areas
c     
c     Input:
c       + filename: name of the file to generate
c       + dim: dimension of space
c       + Vroof: volume of the roof (Vroof(*,1)) and of the attic (Vroof(*,2))
c     
c     Output: the required output file
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      double precision Vroof(0:4,1:2)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine record_volume_file'

      open(15,file=trim(filename))
      write(15,10) '# Unit of area: cubic meters'
      write(15,10) '# Total volume of solid in roofes '
     &     //'(one line for each type):'
      do i=0,4
         write(15,*) Vroof(i,1)
      enddo                     ! i
      write(15,10) '# Total volume of air in attics '
     &     //'(one line for each type):'
      do i=0,4
         write(15,*) Vroof(i,2)
      enddo                     ! i
      close(15)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end
