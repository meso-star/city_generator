      subroutine sp(dim,thickness,
     &     Ncontour,Np,contour,
     &     position,angle,
     &     edge_mat,edge_width,edge_thickness,
     &     generate_individual_obj_files,
     &     obj_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'parameters.inc'
c
c     Purpose: to produce a swimming pool
c
c     Input:
c       + dim: dimension of space
c       + thickness: thickness of the zone [m]
c       + Ncontour: number of contours (including holes)
c       + Np: number of 2D coordinates that define the contour of the object
c       + contour: list of 2D coordinates that define the contour of the object
c       + position: (x,y) shift of the bridge (m)
c       + angle: rotation angle (deg)
c       + edge_mat: name of the material for the edge
c       + edge_width: width of the edge [m]
c       + edge_thickness: thickness of the edge [m]
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      double precision thickness
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      double precision position(1:Ndim_mx-1)
      double precision angle
      character*(Nchar_mx) edge_mat
      double precision edge_width
      double precision edge_thickness
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer iobj,i,j,icontour
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
      integer Nseg,iseg
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n_prev(1:Ndim_mx)
      double precision n_next(1:Ndim_mx)
      double precision pa(1:Ndim_mx)
      double precision pb(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)      
c     label
      character*(Nchar_mx) label
      label='subroutine sp'

      iobj=0                    ! total number of individual surfaces groups for the bridge
c     ---------------------------------------------------------------------------
c     Production of objects:
c     ---------------------------------------------------------------------------
c     + swimming area
c     ---------------------------------------------------------------------------
      iobj=iobj+1
c     Override creation process from the provided contour
      call obj_contour(dim,Ncontour,Np,contour,
     &     thickness,
     &     Nv01,Nf01,v01,f01)
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name='blue_water'
         call append_single_obj_with_mtl(dim,obj_file,
     &        invert_normal,
     &        front_is_st,back_is_st,front_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
         call retrieve_code(back_name,color_code)
         call append_single_off(dim,off_file,invert_normal,
     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     ---------------------------------------------------------------------------
c     + edge
c     ---------------------------------------------------------------------------
      iobj=iobj+1
      Nseg=Np(1)                ! contour of the swimming area
      do iseg=1,Nseg
c     current segment
         do j=1,2
            p1(j)=contour(1,iseg,j)
            if (iseg.eq.Nseg) then
               p2(j)=contour(1,1,j)
            else
               p2(j)=contour(1,iseg+1,j)
            endif               ! iseg
         enddo                  ! j
         p1(dim)=0.0D+0
         p2(dim)=0.0D+0
         call normal_for_segment(dim,p1,p2,n)
c     previous segment
         do j=1,2
            if (iseg.eq.1) then
               pa(j)=contour(1,Nseg,j)
               pb(j)=contour(1,1,j)
            else
               pa(j)=contour(1,iseg-1,j)
               pb(j)=contour(1,iseg,j)
            endif               ! iseg
         enddo                  ! j
         pa(dim)=0.0D+0
         pb(dim)=0.0D+0
         call normal_for_segment(dim,pa,pb,n_prev)
c     next_segment
         do j=1,2
            if (iseg.eq.Nseg) then
               pa(j)=contour(1,1,j)
               pb(j)=contour(1,2,j)
            else if (iseg.eq.Nseg-1) then
               pa(j)=contour(1,iseg+1,j)
               pb(j)=contour(1,1,j)
            else
               pa(j)=contour(1,iseg+1,j)
               pb(j)=contour(1,iseg+2,j)
            endif               ! iseg
         enddo                  ! j
         pa(dim)=0.0D+0
         pb(dim)=0.0D+0
         call normal_for_segment(dim,pa,pb,n_next)
c     normals @ p1 and p2
         call add_vectors(dim,n_prev,n,u)
         call normalize_vector(dim,u,n1)
         call add_vectors(dim,n_next,n,u)
         call normalize_vector(dim,u,n2)
c     edge segment
         call scalar_vector(dim,edge_width,n1,t)
         call add_vectors(dim,p1,t,p3)
         call scalar_vector(dim,edge_width,n2,t)
         call add_vectors(dim,p2,t,p4)         
         Nv01=8
         do j=1,dim
            v01(1,j)=p4(j)
            v01(2,j)=p3(j)
            v01(3,j)=p1(j)
            v01(4,j)=p2(j)
         enddo                  ! j
         do i=5,8
            do j=1,2
               v01(i,j)=v01(i-4,j)
            enddo               ! j
            v01(i,dim)=v01(i-4,dim)+edge_thickness
         enddo                  ! i
         Nf01=8
         f01(1,1)=1
         f01(1,2)=4
         f01(1,3)=3
         f01(2,1)=4
         f01(2,2)=3
         f01(2,3)=2
         f01(3,1)=3
         f01(3,2)=4
         f01(3,3)=8
         f01(4,1)=3
         f01(4,2)=8
         f01(4,3)=7
         f01(5,1)=5
         f01(5,2)=6
         f01(5,3)=8
         f01(6,1)=6
         f01(6,2)=7
         f01(6,3)=8
         f01(7,1)=1
         f01(7,2)=2
         f01(7,3)=5
         f01(8,1)=2
         f01(8,2)=6
         f01(8,3)=5
c     Add individual surface to global trianglemesh (for visualization)
         invert_normal=.false.
         sindex=Nobj+iobj
         call num2str(sindex,str,err)
         if (err) then
            call error(label)
            write(*,*) 'while converting to string:'
            write(*,*) 'sindex=',sindex
            stop
         else
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(edge_mat)
            call append_single_obj_with_mtl(dim,obj_file,
     &           invert_normal,
     &           front_is_st,back_is_st,front_name,back_name,
     &           Nv,Nv01,Nf01,v01,f01)
            call retrieve_code(back_name,color_code)
            call append_single_off(dim,off_file,invert_normal,
     &           Nv,Nv01,Nf01,v01,f01,color_code)
            if (generate_individual_obj_files) then
               filename='S'//trim(str)//'.obj'
               file_out='./results/obj/'//trim(filename)
               call write_single_obj(dim,file_out,invert_normal,
     &              Nv01,Nf01,v01,f01)
            endif
            Nv=Nv+Nv01
            Nf=Nf+Nf01
         endif                  ! err
c     
      enddo                     ! iseg
c     
      Nobj=Nobj+iobj
c      
      return
      end
