c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine identify_Nbridge(Nbridge)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of bridges defined by the user
c
c     Input:
c
c     Output:
c       + Nbridge: number of bridges
c
c     I/O
      integer Nbridge
c     temp
      logical keep_looking,file_exists
      integer i
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine identify_Nbridge'

      Nbridge=0
      keep_looking=.true.
      do while (keep_looking)
         i=Nbridge+1
         call num2str3(i,str3)
         filename='./data/bridge'//trim(str3)//'.in'
         inquire(file=trim(filename),exist=file_exists)
         if (file_exists) then
            Nbridge=Nbridge+1
         else
            keep_looking=.false.
         endif                  ! file_exists
      enddo                     ! while (keep_looking)

      return
      end



      subroutine read_bridge_data(dim,
     &     Nbridge,ibridge,position,angle,length,
     &     deck_width,side_width,
     &     deck_height,side_height,
     &     deck_mat,side_mat,
     &     draw_wbands,wband_mat)
      implicit none
      include 'max.inc'
c
c     Purpose: to read input data file for bridges
c
c     Input:
c       + dim: dimension of space
c
c     Output:
c       + Nbridge: number of bridges
c       + ibridge: index of the bridge definition file to read
c       + position: (x,y) shift of the bridge (m)
c       + angle: rotation angle (deg)
c       + length: length of each bridge [m]
c       + deck_width: width of the deck for each bridge [m]
c       + side_width: width of the sides for each bridge [m]
c       + deck_height: height of the deck for each bridge [m]
c       + side_height: height of the sides for each bridge [m]
c       + deck_mat: material for the deck
c       + side_mat: material for the sides
c       + draw_wbands: T if drawing white bands is required
c       + wband_mat: material for white bands
c
c     I/O
      integer dim
      integer Nbridge
      integer ibridge
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision length
      double precision deck_width
      double precision side_width
      double precision deck_height
      double precision side_height
      character*(Nchar_mx) deck_mat
      character*(Nchar_mx) side_mat
      logical draw_wbands
      character*(Nchar_mx) wband_mat
c     temp
      integer ios,i,j,k,nl
      logical keep_looking
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine read_bridge_data'

      call num2str3(ibridge,str3)
      filename='./data/bridge'//trim(str3)//'.in'
      open(17,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do j=1,3
            read(17,*)
         enddo                  ! j
         read(17,*) position(1)
         read(17,*)
         read(17,*) position(2)
         read(17,*)
         read(17,*) angle
         read(17,*)
         read(17,*) length
         read(17,*)
         read(17,*) deck_width
         read(17,*)
         read(17,*) side_width
         read(17,*)
         read(17,*) deck_height
         read(17,*)
         read(17,*) side_height
         read(17,*)
         read(17,*) deck_mat
         read(17,*)
         read(17,*) side_mat
         read(17,*) 
         read(17,*) draw_wbands
         read(17,*)
         read(17,*) wband_mat
c     Consistency checks
         if (length.lt.0) then
            call error(label)
            write(*,*) 'length(',ibridge,')=',length
            write(*,*) '< 0'
            stop
         endif
         if (deck_width.lt.0) then
            call error(label)
            write(*,*) 'deck_width(',ibridge,')=',deck_width
            write(*,*) '< 0'
            stop
         endif
         if (side_width.lt.0) then
            call error(label)
            write(*,*) 'side_width(',ibridge,')=',side_width
            write(*,*) '< 0'
            stop
         endif
         if (deck_height.lt.0) then
            call error(label)
            write(*,*) 'deck_height(',ibridge,')=',deck_height
            write(*,*) '< 0'
            stop
         endif
         if (side_height.lt.0) then
            call error(label)
            write(*,*) 'side_height(',ibridge,')=',side_height
            write(*,*) '< 0'
            stop
         endif
c     End of tests
      endif                     !  file exists
      close(17)
      
      return
      end
