c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine ground(dim,
     &     Xmin,Xmax,Ymin,Ymax,
     &     ground_mat,
     &     position,angle,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
c      
c     Purpose: to produce the .obj for the ground
c     and add it to the .pbrt file
c
c     Input:
c       + dim: dimension of space
c       + Xmin: min X for the ground
c       + Xmax: max X for the ground
c       + Ymin: min Y for the ground
c       + Ymax: max Y for the ground
c       + ground_mat: name of the material for the ground
c       + position: (x,y) shift (m)
c       + angle: rotation angle (deg)
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     Input
      integer dim
      double precision Xmin,Xmax
      double precision Ymin,Ymax
      character*(Nchar_mx) ground_mat
      double precision position(1:Ndim_mx-1)
      double precision angle
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
c     Output
      integer Nobj
      integer Nv,Nf
c     temp
      integer iobj,j
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      double precision color_code(1:3)
      integer sindex
      double precision alpha
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision center(1:Ndim_mx)
      character*(Nchar_mx) str
      logical err,invert_normal,ok
      character*(Nchar_mx) file_out,filename
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
c     label
      character*(Nchar_mx) label
      label='subroutine ground'
c
      iobj=0
c     produce individual .obj
      iobj=iobj+1
      call obj_ground(dim,Xmin,Xmax,Ymin,Ymax,
     &     Nv01,Nf01,v01,f01)
      sindex=Nobj+iobj
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.true.
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(ground_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     ---------------------------------------------------------------------------
      Nobj=Nobj+iobj
      
      return
      end
      
