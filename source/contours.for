c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine add_contour(Ncontour,Np,contour,
     &     Npoints,track)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a given contour to a list of contours
c
c     Input:
c       + Ncontour: number of contours in the existing list of contours
c       + Np: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + Npoints: number of points for the contour to add
c       + track: track for the contour to add
c
c     Output:
c     updated values of "Ncontour", "Np" and "contour"
c
c     I/O
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_contour'

      Ncontour=Ncontour+1
      if (Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
      Np(Ncontour)=Npoints
      do i=1,Np(Ncontour)
         do j=1,2
            contour(Ncontour,i,j)=track(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine get_contour(Ncontour,Np,contour,icontour,
     &     Npoints,track)
      implicit none
      include 'max.inc'
c
c     Purpose: to extract a given contour from a list of contours
c
c     Input:
c       + Ncontour: number of contours in the existing list of contours
c       + Np: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + icontour: index of the contour to extract
c     
c     Output:
c       + Npoints: number of points for the extracted contour
c       + track: track for the extracted contour
c
c     I/O
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer icontour
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine get_contour'

      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'while Ncontour=',Ncontour
         stop
      endif

      Npoints=Np(icontour)
      do i=1,Npoints
         do j=1,2
            track(i,j)=contour(icontour,i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end
      


      subroutine get_position_from_ctr(dim,Npoints,ctr,i,position)
      implicit none
      include 'max.inc'
c     
c     Purpose: to extract a position from a contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the contour
c       + ctr: contour
c       + i: index of the position to extract
c     
c     Output:
c       + position: required position
c     
c     I/O
      integer dim
      integer Npoints
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i
      double precision position(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine get_position_from_ctr'

      if (i.lt.1) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be > 0'
         stop
      endif
      if (i.gt.Npoints) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be <= Npoints=',Npoints
         stop
      endif

      do j=1,dim-1
         position(j)=ctr(i,j)
      enddo                     ! j
      position(dim)=0.0D+0
      
      return
      end
      


      subroutine set_position_in_ctr(dim,Npoints,ctr,i,position)
      implicit none
      include 'max.inc'
c     
c     Purpose: to set a position in a contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the contour
c       + ctr: contour
c       + i: index of the position to set
c     
c     Output:
c       + position: position to set
c     
c     I/O
      integer dim
      integer Npoints
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i
      double precision position(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine get_position_from_ctr'

      if (i.lt.1) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be > 0'
         stop
      endif
      if (i.gt.Npoints) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be <= Npoints=',Npoints
         stop
      endif

      do j=1,dim-1
         ctr(i,j)=position(j)
      enddo                     ! j
      
      return
      end

      
      
      subroutine add_point_to_ctr(dim,x,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to add a position to a contour definition
c     
c     Input:
c       + dim: dimension of space
c       + x: position to add
c       + Np: number of points in the "ctr" contour
c       + ctr: definition of the contour
c     
c     Output:
c       + updated values for Np and ctr
c     
c     I/O
      integer dim
      double precision x(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer j
      double precision x0(1:Ndim_mx)
      logical check,identical
c     label
      character*(Nchar_mx) label
      label='subroutine add_point_to_ctr'

      if (Np.eq.0) then
         check=.false.
      else
         check=.true.
         do j=1,dim-1
            x0(j)=ctr(Np,j)
         enddo                  ! j
         x0(dim)=0.0D+0
      endif                     ! Np>0
      if (check) then
         call identical_positions(dim,x0,x,identical)
      endif
      if ((.not.check).or.((check).and.(.not.identical))) then
         Np=Np+1
         if (Np.gt.Nppc_mx) then
            call error(label)
            write(*,*) 'Np=',Np
            write(*,*) '> Nppc_mx=',Nppc_mx
            stop
         else
            do j=1,dim-1
               ctr(Np,j)=x(j)
            enddo               ! j
         endif                  ! Np>Nppc_mx
      endif                     ! .not.identical

      return
      end
      

      
      subroutine generate_inner_contour(dim,debug,
     &     Ncontour,Nppc,contour,width)
      implicit none
      include 'max.inc'
c     
c     Purpose: to analyze whether or not a inner contour should be
c     generated for a type 4 building, and if yes,
c     generate the "best looking" inner contour from its outer contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours (updated)
c       + Nppc: number of points that define each contour (updated)
c       + contour: list of coordinates for each contour (updated)
c       + width: required width
c     
c     Output:
c       + possibly updated values for "Ncontour", "Nppc" and "contour"
c     
c     I/O
      integer dim
      logical debug
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx-1)
      double precision width
c     temp
      integer Nppc_out
      double precision ctr_out(1:Nppt_mx,1:2)
      integer Nppc_in
      double precision ctr_in(1:Nppt_mx,1:2)
      integer i,j
      integer i01,i02,i11,i12,i21,i22
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision T1(1:Ndim_mx)
      double precision T2(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision n0(1:Ndim_mx)
      double precision d,dmin,sp,dop
      logical gen_inner_contour
      integer variant
      integer iseg_dmin,iseg1,iseg2
      logical intersection_found,parallel
      double precision dvp
      double precision A(1:Ndim_mx)
      logical A_is_at_end
      integer end_side
      integer Ncontour_tmp
      integer Nppc_tmp(1:Ncontour_mx)
      double precision contour_tmp(1:Ncontour_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical intersection,self_intersection
c     label
      character*(Nchar_mx) label
      label='subroutine generate_inner_contour'

c     Debug
      if (debug) then
         write(*,*) 'In: ',trim(label)
      endif
c     Debug
      
c     Get the outer contour
      Nppc_out=Nppc(1)
      do i=1,Nppc(1)
         do j=1,dim-1
            ctr_out(i,j)=contour(1,i,j)
         enddo                  ! j
      enddo                     ! i
c     Debug
      if (debug) then
         write(*,*) 'Nppc_out=',Nppc_out
         do i=1,Nppc_out
            write(*,*) (ctr_out(i,j),j=1,dim-1)
         enddo                  ! i
      endif
c     Debug
c     Identify smallest segment: iseg_dmin
      call identify_smallest_segment(dim,Nppc_out,ctr_out,
     &     dmin,iseg_dmin,i01,i02,dop)
c     + previous segment
      call identify_close_segments(dim,Nppc_out,ctr_out,iseg_dmin,
     &     iseg1,iseg2,i11,i12,i21,i22)
c     Debug
      if (debug) then
         write(*,*) 'iseg_dmin=',iseg_dmin
         write(*,*) 'iseg1=',iseg1,' iseg2=',iseg2
      endif
c     Debug
      do j=1,dim-1
         x1(j)=ctr_out(i11,j)
         x2(j)=ctr_out(i12,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call normalize_vector(dim,tmp,u1) ! u1: direction for segment "iseg1"
      call normal_for_segment(dim,x1,x2,n1)
      call scalar_vector(dim,width,n1,tmp)
      call add_vectors(dim,x1,tmp,P1)
      call add_vectors(dim,x2,tmp,P2)
c     Debug
      if (debug) then
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'u1=',u1
         write(*,*) 'n1=',n1
         write(*,*) 'P1=',P1
         write(*,*) 'P2=',P2
      endif
c     Debug
c     + next segment
      do j=1,dim-1
         x1(j)=ctr_out(i21,j)
         x2(j)=ctr_out(i22,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call normalize_vector(dim,tmp,u2) ! u2: direction for segment "iseg2"
      call normal_for_segment(dim,x1,x2,n2)
      call scalar_vector(dim,width,n2,tmp)
      call add_vectors(dim,x1,tmp,P3)
      call add_vectors(dim,x2,tmp,P4)
c     Debug
      if (debug) then
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'u2=',u2
         write(*,*) 'n2=',n2
         write(*,*) 'P3=',P3
         write(*,*) 'P4=',P4
      endif
c     Debug
c     + detect whether or not previous and next segments are parallel
      call scalar_product(dim,u1,u2,sp)
      if (dabs(sp).gt.0.98D+0) then
         parallel=.true.
      else
         parallel=.false.
      endif
c     Debug
      if (debug) then
         write(*,*) 'parallel=',parallel
      endif
c     Debug
c     Find whether or not a inner contour should be generated
      gen_inner_contour=.false. ! default
      if (parallel) then
c     If those segments are parallel, generating a inner contour is worth it
c     only when "dmin" is large enough compared to "width"
         if (dmin.ge.3.0D+0*width) then
            gen_inner_contour=.true.
            variant=1
         endif
      else                      ! parallel=F
c     If previous and next segments are not parallel, a inner contour has
c     to be generated no matter what. Only the method changes with the
c     value of "dmin"
         if ((dmin.le.3.0D+0*width).and.(dop.ge.1.0D+0*width)) then
            gen_inner_contour=.true.
            variant=2
         endif
         if ((dmin.ge.3.0D+0*width).and.(dop.ge.3.0D+0*width)) then
            gen_inner_contour=.true.
            variant=1
         endif
      endif
c     Debug
      if (debug) then
         write(*,*) 'gen_inner_contour=',gen_inner_contour
         write(*,*) 'variant=',variant
      endif
c     Debug
      if (.not.gen_inner_contour) then
c     Debug
c         write(*,*) '--------------------------------------'
c         write(*,*) 'gen_inner_contour=',gen_inner_contour
c         if (parallel) then
c            write(*,*) 'parallel, but dmin=',dmin,
c     &           ' <3*width=',3.0D+0*width
c         else
c            write(*,*) 'NOT parallel, and: dmin=',dmin
c            write(*,*) 'dop=',dop
c            write(*,*) '1.5*width=',1.50D+0*width
c            write(*,*) '3*width=',3.0D+0*width
c         endif
c     Debug
         goto 666
      endif

c     basic inner ctr (if variant=1, that is all)
      call generate_close_contour(dim,
     &     Nppc_out,ctr_out,width,.true.,
     &     Nppc_in,ctr_in)
c     Debug
      if (debug) then
         write(*,*) 'Alter generate_close_contour:'
         write(*,*) 'Nppc_in=',Nppc_in
         do i=1,Nppc_in
            write(*,*) (ctr_in(i,j),j=1,dim-1)
         enddo                  ! i
      endif
c     Debug
      
c     
      if (variant.eq.2) then
c     Intersection between the two lines that are parallel
c     to segments "iseg1" and "iseg2", at a distance "width"
c     Debug
         if (debug) then
            write(*,*) 'calling lli 1'
         endif
c     Debug
         call line_line_intersection(dim,P1,P2,P3,P4,.false.,
     &        intersection_found,Pint)
c     Debug
         if (debug) then
            write(*,*) 'intersection_found=',intersection_found
            if (intersection_found) then
               write(*,*) 'Pint=',Pint
            endif
         endif
c     Debug
         if (.not.intersection_found) then
            goto 666
         endif
c     Distance to "iseg_dmin" segment
         do j=1,dim-1
            x1(j)=ctr_out(i01,j)
            x2(j)=ctr_out(i02,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
c     Debug
         if (debug) then
            write(*,*) 'x1=',x1
            write(*,*) 'x2=',x2
         endif
c     Debug
         call normal_for_segment(dim,x1,x2,n0)
         call distance_to_vector(dim,x1,x2,Pint,dvp,A,
     &        A_is_at_end,end_side)
c     Debug
         if (debug) then
            write(*,*) 'dvp=',dvp,' width=',width
         endif
c     Debug
c         if (dvp.gt.width) then
            call substract_vectors(dim,Pint,A,tmp)
            call normalize_vector(dim,tmp,u)
            call scalar_product(dim,n0,u,sp)
c     Debug
            if (debug) then
               write(*,*) 'sp=',sp
            endif
c     Debug
            if (sp.le.0.0D+0) then
c     negative scalar_product: the intersection occurs on the outer side
c     of the external contour
               goto 666
            else
               d=1.50D+0*dvp
            endif               ! sp < 0
c         else
c            d=dvp
c         endif                  ! dvp > width
         call scalar_vector(dim,d,u,tmp)
         call add_vectors(dim,x1,tmp,T1)
         call add_vectors(dim,x2,tmp,T2)
         T1(dim)=0.0D+0
         T2(dim)=0.0D+0
c     new position at the beginning of "iseg_dmin" segment's inner ctr
c     Debug
         if (debug) then
            write(*,*) 'd=',d
            write(*,*) 'u=',u
            write(*,*) 'tmp=',tmp
            write(*,*) 'calling lli 2'
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
            write(*,*) 'T1=',T1
            write(*,*) 'T2=',T2
         endif
c     Debug
         call line_line_intersection(dim,P1,P2,T1,T2,.false.,
     &        intersection_found,Pint)
c     Debug
         if (debug) then
            write(*,*) 'intersection_found=',intersection_found
            if (intersection_found) then
               write(*,*) 'Pint=',Pint
            endif
         endif
c     Debug
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found'
            write(*,*) 'between (P1P2) and (T1T2)'
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
            write(*,*) 'T1=',T1
            write(*,*) 'T2=',T2
            stop
         else
            do j=1,dim-1
               ctr_in(i01,j)=Pint(j)
            enddo               ! j
         endif
c     new position at the end of "iseg_dmin" segment's inner ctr
c     Debug
         if (debug) then
            write(*,*) 'calling lli 3'
         endif
c     Debug
         call line_line_intersection(dim,P3,P4,T1,T2,.false.,
     &        intersection_found,Pint)
c     Debug
         if (debug) then
            write(*,*) 'intersection_found=',intersection_found
            if (intersection_found) then
               write(*,*) 'Pint=',Pint
            endif
         endif
c     Debug
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found'
            write(*,*) 'between (P3P4) and (T1T2)'
            stop
         else
            do j=1,dim-1
               ctr_in(i02,j)=Pint(j)
            enddo               ! j
         endif
      endif                     ! variant=2
c     Debug
      if (debug) then
         write(*,*) 'Final inner contour:'
         write(*,*) 'Nppc_in=',Nppc_in
         do i=1,Nppc_in
            write(*,*) (ctr_in(i,j),j=1,dim-1)
         enddo                  ! i
      endif
c     Debug

      Ncontour_tmp=0
      call add_ctr_to_contour(dim,Ncontour_tmp,Nppc_tmp,contour_tmp,
     &     Nppc_out,ctr_out)
      call add_ctr_to_contour(dim,Ncontour_tmp,Nppc_tmp,contour_tmp,
     &     Nppc_in,ctr_in)
c     check contour for self_intersection
c     Debug
c      write(*,*) 'ok1: ',trim(label)
c     Debug
      call contour_self_intersects(dim,
     &     0.0D+0,0.0D+0,
     &     Ncontour_tmp,Nppc_tmp,contour_tmp,2,
     &     self_intersection)
c     Debug
c      write(*,*) 'ok2: ',trim(label)
c     Debug
c     check for intersection with outer contour
      call contours_intersect(dim,
     &     0.0D+0,0.0D+0,
     &     Ncontour_tmp,Nppc_tmp,contour_tmp,2,1,
     &     intersection)
      if ((.not.intersection).and.(.not.self_intersection)) then
c     Update contours
         Ncontour=Ncontour+1
         Nppc(Ncontour)=Nppc_in
         do i=1,Nppc_in
            do j=1,dim-1
               contour(Ncontour,i,j)=ctr_in(i,j)
            enddo               ! j
         enddo                  ! i
      endif
      
 666  continue
c     Debug
      if (debug) then
         write(*,*) 'Out: ',trim(label)
         stop
      endif
c     Debug
      return
      end



      subroutine contour_length(dim,Nppc,ctr,length)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the length of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c     
c     Output:
c       + length: length of the contour
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppt_mx,1:2)
      double precision length
c     temp
      integer Nseg,iseg,i1,i2,j
      double precision d
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine contour_length'

      length=0.0D+0
      Nseg=Nppc
      do iseg=1,Nseg
         i1=iseg
         if (iseg.eq.Nseg) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,tmp)
         call vector_length(dim,tmp,d)
         length=length+d
      enddo                     ! iseg

      return
      end
      


      subroutine identify_smallest_segment(dim,Nppc,ctr,
     &     dmin,iseg_dmin,i01,i02,dop)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the index of the smallest segment
c     of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c     
c     Output:
c       + dmin: length of the smallest segment
c       + iseg_dmin: index of the smallest segment
c       + i01 & i02: indexes of the two points for segment index "iseg_dmin"
c       + dop: length of the segment opposite to the smallest segment
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppt_mx,1:2)
      double precision dmin
      integer iseg_dmin
      integer i01,i02
      double precision dop
c     temp
      integer Nseg,iseg,j,iseg_op,i1,i2
      double precision d
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine identify_smallest_segment'

      Nseg=Nppc
      do iseg=1,Nseg
         i1=iseg
         if (iseg.eq.Nseg) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,tmp)
         call vector_length(dim,tmp,d)
c     Debug
         if (d.le.0.0D+0) then
            call error(label)
            write(*,*) 'd=',d
            write(*,*) 'Nseg=',Nseg
            write(*,*) 'iseg=',iseg
            write(*,*) 'i1=',i1,' i2=',i2
            write(*,*) 'x1=',x1
            write(*,*) 'x2=',x2
            stop
         endif
c     Debug
         if (iseg.eq.1) then
            dmin=d
            iseg_dmin=iseg
         else
            if (d.lt.dmin) then
               dmin=d
               iseg_dmin=iseg
            endif               ! d < dmin
         endif                  ! iseg=1
      enddo                     ! iseg
      i01=iseg_dmin
      if (iseg_dmin.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif

      iseg_op=iseg_dmin+Nseg/2
      if (iseg_op.gt.Nseg) then
         iseg_op=iseg_op-Nseg
      endif
      i1=iseg_op
      if (iseg_op.eq.Nseg) then
         i2=1
      else
         i2=i1+1
      endif
      do j=1,dim-1
         x1(j)=ctr(i1,j)
         x2(j)=ctr(i2,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call vector_length(dim,tmp,dop)
      
      return
      end



      subroutine identify_close_segments(dim,Nppc,ctr,iseg,
     &     iseg1,iseg2,i11,i12,i21,i22)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two surrounding segments for
c     a given segment of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c       + iseg: required segment index
c     
c     Output:
c       + iseg1: index of the segment just before "iseg"
c       + iseg2: index of the segment just after "iseg"
c       + i11 & i12: indexes of the first and second point for segment "iseg1"
c       + i21 & i22: indexes of the first and second point for segment "iseg2"
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppt_mx,1:2)
      integer iseg
      integer iseg1
      integer iseg2
      integer i11,i12
      integer i21,i22
c     temp
      integer Nseg,i1,i2
c     label
      character*(Nchar_mx) label
      label='subroutine identify_close_segments'

      Nseg=Nppc
c     + previous segment
      if (iseg.eq.1) then
         iseg1=Nseg
      else
         iseg1=iseg-1
      endif
      i11=iseg1
      if (iseg1.eq.Nseg) then
         i12=1
      else
         i12=i11+1
      endif
      if (iseg.eq.Nseg) then
         iseg2=1
      else
         iseg2=iseg+1
      endif
      i21=iseg2
      if (iseg2.eq.Nseg) then
         i22=1
      else
         i22=i21+1
      endif
      
      return
      end


      
      subroutine add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &     Npoints,ctr)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a given contour to a list of contours
c
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + Npoints: number of points for the contour to add
c       + ctr: contour to add
c
c     Output:
c     updated values of "Ncontour", "Np" and "contour"
c
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx-1)
      integer Npoints
      double precision ctr(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_ctr_to_contour'

      Ncontour=Ncontour+1
      if (Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
c      
      if (Npoints.gt.Nppt_mx) then
         call error(label)
         write(*,*) 'Npoints=',Npoints
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      else
         Nppc(Ncontour)=Npoints
      endif
c      
      do i=1,Nppc(Ncontour)
         do j=1,dim-1
            contour(Ncontour,i,j)=ctr(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine generate_close_contour(dim,
     &     Npoints,track,width,inside,
     &     new_Npoints,new_track)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a new contour at a given distance from a existing contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the input contour
c       + track: track for the input contour
c       + width: distance to new contour
c       + inside: true if the new trackhas to be generated inside the closed input contour
c     
c     Output:
c       + new_Npoints: number of points for the generated contour
c       + new_track: track for the generated contour
c     
c     I/O
      integer dim
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
      double precision width
      logical inside
      integer new_Npoints
      double precision new_track(1:Nppt_mx,1:2)
c     temp
      integer i,j
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_close_contour'
      
      new_Npoints=Npoints
      do i=1,new_Npoints
c     current and previous segment
         do j=1,dim-1
            if (i.eq.1) then
               P0(j)=track(new_Npoints,j)
            else
               P0(j)=track(i-1,j)
            endif               ! i
            P1(j)=track(i,j)
            if (i.eq.new_Npoints) then
               P2(j)=track(1,j)
            else
               P2(j)=track(i+1,j)
            endif               ! i
         enddo                  ! j
         P0(dim)=0.0D+0
         P1(dim)=0.0D+0
         P2(dim)=0.0D+0
         call segments_intersection(dim,P0,P1,P2,inside,width,
     &        intersection_found,Pint)
c     add point to contour
         do j=1,dim-1
            new_track(i,j)=Pint(j)
         enddo                  ! j
      enddo                     ! i

      return
      end


      
      subroutine generate_close_contour_new(dim,Npoints,track,width,
     &     inside,
     &     new_Npoints,new_track)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a new contour at a given distance from a existing contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the input contour
c       + track: track for the input contour
c       + width: distance to new contour
c       + inside: true if the new trackhas to be generated inside the closed input contour
c     
c     Output:
c       + new_Npoints: number of points for the generated contour
c       + new_track: track for the generated contour
c     
c     I/O
      integer dim
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
      double precision width
      logical inside
      integer new_Npoints
      double precision new_track(1:Nppt_mx,1:2)
c     temp
      integer i,j,iseg,Nseg
      integer i11,i12,i21,i22
      double precision x11(1:Ndim_mx)
      double precision x12(1:Ndim_mx)
      double precision x21(1:Ndim_mx)
      double precision x22(1:Ndim_mx)
      logical identical
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_close_contour_new'

      new_Npoints=0
      Nseg=Npoints
      do iseg=1,Nseg
c     extract coordinates of x11 and x12, the 2 points of segment "iseg"
         call segment_indexes(dim,Npoints,track,iseg,
     &        i11,i12,x11,x12)
c     extract coordinates of x21 and x22, the 2 points of segment "iseg-1"
         call previous_segment_indexes(dim,Npoints,track,iseg,
     &        i21,i21,x21,x22)
c     check
         call identical_vectors(dim,x11,x22,identical)
         if (.not.identical) then
            call error(label)
            write(*,*) 'x11=',x11
            write(*,*) 'x22=',x22
            write(*,*) 'should be identical'
            write(*,*) 'Npoints=',Npoints
            write(*,*) 'ctr:'
            do i=1,Npoints
               write(*,*) (track(i,j),j=1,dim-1)
            enddo               ! i
            write(*,*) 'iseg=',iseg
            write(*,*) 'i11=',i11,' i12=',i12
            write(*,*) 'segment previous to iseg:'
            write(*,*) 'i21=',i21,' i22=',i22
            stop
         endif
c     inner normals
         call normal_for_segment(dim,x11,x12,n1)
         call normal_for_segment(dim,x21,x22,n2)
c     if outside contour required: invert normals
         if (.not.inside) then
            call scalar_vector(dim,-1.0D+0,n1,n1)
            call scalar_vector(dim,-1.0D+0,n2,n2)
         endif
c     generate new points that define the two intersecting lines
         call scalar_vector(dim,width,n1,tmp)
         call add_vectors(dim,x11,tmp,P1)
         call add_vectors(dim,x12,tmp,P2)
         call scalar_vector(dim,width,n2,tmp)
         call add_vectors(dim,x21,tmp,P3)
         call add_vectors(dim,x22,tmp,P4)
c     intersect the two lines
         call line_line_intersection(dim,P1,P2,P3,P4,.false.,
     &        intersection_found,Pint)
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found between:'
            write(*,*) 'line 1 defined by:'
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
            call substract_vectors(dim,P2,P1,P1P2)
            call normalize_vector(dim,P1P2,u12)
            write(*,*) 'u12=',u12
            write(*,*) 'x11=',x11
            write(*,*) 'x12=',x12
            write(*,*) 'n1=',n1
            call scalar_vector(dim,width,n1,tmp)
            write(*,*) 'width*n1=',tmp
            write(*,*) 'line 2 defined by:'
            write(*,*) 'P3=',P3
            write(*,*) 'P4=',P4
            write(*,*) 'x21=',x21
            write(*,*) 'x22=',x22
            write(*,*) 'n2=',n2
            call scalar_vector(dim,width,n2,tmp)
            write(*,*) 'width*n2=',tmp
            call substract_vectors(dim,P4,P3,P3P4)
            call normalize_vector(dim,P3P4,u34)
            write(*,*) 'u34=',u34
            stop
         endif
c     add point to contour
         new_Npoints=new_Npoints+1
         do j=1,dim-1
            new_track(new_Npoints,j)=Pint(j)
         enddo                  ! j
      enddo                     ! i

      return
      end


      
      subroutine generate_close_contour2(dim,debug,Np_ref,ctr_ref,
     &     width_min,width_max,inward,try_best_fit,success,
     &     best_fit,width,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a new contour at a given distance from a reference contour
c     
c     Input:
c       + dim: dimension of space
c       + Np_ref: number of points for the reference contour
c       + ctr_ref: reference contour
c       + width_min: minimum distance between default contour and generated contour
c       + width_max: maximum distance between default contour and generated contour;
c                    This is also the value of width used when try_best_fit=F
c       + inward: true if the new contour has to be generated inside the default contour
c                 when false, the new contour is generated outward
c       + try_best_fit: when a valid contour could not be generated at a distance "width_max",
c                       the routine can try to find a valid contour between "width_min"
c                       and "width_max" when this switch is set to T. Otherwise it
c                       will not even try.
c     
c     Output:
c       + success: true when a contour could be generated at the specified "width_max"
c                  distance, or at a distance between "width_min" and "width_max" if
c                  "try_best_fit" was set to T.
c       + best_fit: when success=T, this switch specifies whether the generated contour
c                   was generated for the default "width_max" distance (best_fit=F)
c                   or is a best fit between "width_min" and "width_max"
c       + width: value of the width the best-fit contour was generated for, when
c                success=T and best_fit=T.
c       + Np: number of points for the generated contour
c       + ctr: ctr for the generated contour
c     
c     I/O
      integer dim
      logical debug
      integer Np_ref
      double precision ctr_ref(1:Nppc_mx,1:Ndim_mx-1)
      double precision width_min
      double precision width_max
      logical inward
      logical try_best_fit
      logical success
      logical best_fit
      double precision width
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,j
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      logical print_err
      logical test_inside_positions
      logical test_self_intersection
      logical test_intersection
      logical test_crossed_intersection
      logical test_clockwise
      logical valid,solution_found,keep_trying
      double precision dmin,dmax,d
      double precision Lmin,Lop
      integer iseg_Lmin,i01,i02
      integer Niter
      integer err_code
c     Debug
      integer Ncontour0
      integer Np0(1:Ncontour_mx)
      double precision contour0(1:Ncontour_mx,1:Nppt_mx,1:2)
      character*(Nchar_mx) filename
c     Debug
c     parameters
      double precision epsilon_d
      parameter(epsilon_d=1.0D-2)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_close_contour2'
c     Debug
      if (debug) then
         write(*,*) trim(label),' :in'
         write(*,*) 'width_min=',width_min
         write(*,*) 'width_max=',width_max
         write(*,*) 'inward=',inward
         write(*,*) 'try_best_fit=',try_best_fit
      endif
c     Debug

      success=.false.           ! default
c     check consistency of the reference contour
      print_err=debug
      test_inside_positions=.false.
      test_self_intersection=.true.
      test_intersection=.false.
      test_crossed_intersection=.false.
      test_clockwise=.true.
      call is_contour_valid(dim,Np_ref,ctr_ref,Np_ref,ctr_ref,
     &     print_err,test_inside_positions,test_self_intersection,
     &     test_intersection,test_crossed_intersection,test_clockwise,
     &     valid)
      if (.not.valid) then
         call error(label)
         write(*,*) 'reference contour is not valid:'
         write(*,*) 'Np_ref=',Np_ref
         do i=1,Np_ref
            write(*,*) (ctr_ref(i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif                     ! .not.valid
c     Debug
      if (debug) then
         write(*,*) 'reference contour is valid'
      endif
c     Debug

c     generate required contour at maximum possible distance
      call generate_contour(dim,Np_ref,ctr_ref,width_max,inward,
     &     err_code,Np_tmp,ctr_tmp)
c     Debug
c$$$      if (debug) then
c$$$         Ncontour0=1
c$$$         Np0(1)=Np_tmp
c$$$         do i=1,Np0(1)
c$$$            do j=1,dim-1
c$$$               contour0(1,i,j)=ctr_tmp(i,j)
c$$$            enddo               ! j
c$$$         enddo                  ! i
c$$$         filename='./gnuplot/contour05max.dat'
c$$$         call record_gnuplot_contour(filename,dim,
c$$$     &        Ncontour0,Np0,contour0)
c$$$      endif
c      if (err_code.ne.0) then
c         success=.false.
c         goto 666
c      endif
c     from now on, generated contour will be tested with these settings
      test_inside_positions=inward ! only test positions are inside the reference contour when inward=T
      test_self_intersection=.true.
      test_intersection=.true.
      test_crossed_intersection=inward ! only test crossed intersection when inward=T
      test_clockwise=.true.
c     test the farther contour
      call is_contour_valid(dim,Np_ref,ctr_ref,Np_tmp,ctr_tmp,
     &     print_err,test_inside_positions,test_self_intersection,
     &     test_intersection,test_crossed_intersection,test_clockwise,
     &     valid)
      if (valid) then
c     if it is valid, this is what was required by default
         call duplicate_ctr(dim,Np_tmp,ctr_tmp,Np,ctr)
         success=.true.
         width=width_max
         best_fit=.false.
c     Debug
         if (debug) then
            write(*,*) 'New contour at distance:'
            write(*,*) 'width_max=',width_max
            write(*,*) 'is valid'
         endif
c     Debug
         goto 666
      else
         success=.false.
c     Debug
         if (debug) then
            write(*,*) 'New contour at distance:'
            write(*,*) 'width_max=',width_max
            write(*,*) 'is invalid'
         endif
c     Debug
      endif                     ! valid
c     otherwise, a best fit can be looked for if required
      if (try_best_fit) then
c     Debug
         if (debug) then
            write(*,*) 'Trying best-fit'
         endif
c     Debug
c     produce minimum distance contour
         call generate_contour(dim,Np_ref,ctr_ref,width_min,inward,
     &        err_code,Np_tmp,ctr_tmp)
c         if (err_code.ne.0) then
c            success=.false.
c            goto 666
c         endif
c     if invalid, there is no best fit in the specified width range
         call is_contour_valid(dim,Np_ref,ctr_ref,Np_tmp,ctr_tmp,
     &        print_err,test_inside_positions,test_self_intersection,
     &        test_intersection,.false.,test_clockwise,
     &        valid)
c     Debug
         if (debug) then
            write(*,*) 'New contour at distance:'
            write(*,*) 'width_min=',width_min
            if (valid) then
               write(*,*) 'is valid'
            else
               write(*,*) 'is invalid'
            endif
         endif
c     Debug
         if (.not.valid) then
            goto 666
         endif
c     otherwise there may be a possibility...
         Niter=0
         solution_found=.false.
         dmin=width_min
         dmax=width_max
         keep_trying=.true.
         do while (keep_trying)
            Niter=Niter+1
c     Debug
            if (debug) then
               write(*,*) 'Niter=',Niter
            endif
c     Debug
            d=(dmin+dmax)/2.0D+0
c     produce contour for current distance "d"
            call generate_contour(dim,Np_ref,ctr_ref,d,inward,
     &           err_code,Np_tmp,ctr_tmp)
c            if (err_code.ne.0) then
c               success=.false.
c               goto 666
c            endif
c     if invalid, there is no best fit in the specified width range
            call is_contour_valid(dim,Np_ref,ctr_ref,Np_tmp,ctr_tmp,
     &           print_err,test_inside_positions,test_self_intersection,
     &           test_intersection,test_crossed_intersection,
     &           test_clockwise,
     &           valid)
c     Debug
            if (debug) then
               write(*,*) 'new contour at distance d=',d
               if (valid) then
                  write(*,*) 'is valid'
               else
                  write(*,*) 'is invalid'
               endif
            endif
c     Debug
            if (valid) then
               dmin=d
            else
               dmax=d
            endif
            if ((valid).and.(dabs(dmax-dmin).le.epsilon_d)) then
               solution_found=.true.
               keep_trying=.false.
c     Debug
               if (debug) then
                  write(*,*) 'exiting because solution was found'
               endif
c     Debug
            endif
            if (Niter.gt.Niter_mx) then
               keep_trying=.false.
c     Debug
               if (debug) then
                  write(*,*) 'exiting because Niter_mx was reached'
               endif
c     Debug
            endif
         enddo                  ! while (keep_trying)
         if (solution_found) then
            call duplicate_ctr(dim,Np_tmp,ctr_tmp,Np,ctr)
            width=d
            success=.true.
            best_fit=.true.
         endif                  ! solution_found
      endif                     ! try_best_fit
      
 666  continue
c     Debug
      if (debug) then
         write(*,*) trim(label),' :out'
      endif
c     Debug
      return
      end



      subroutine generate_contour(dim,Np_ref,ctr_ref,width,inward,
     &     err_code,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a contour (without any consistency check)
c     at a given distance from a reference contour
c     
c     Input:
c       + dim: dimension of space
c       + Np_ref: number of points for the reference contour
c       + ctr_ref: reference contour
c       + width: distance to generate a new contour
c       + inward: true if the new contour has to be generated inside the default contour
c                 when false, the new contour is generated outward
c     
c     Output:
c       + err_code: 0 means the required contour was generated; otherwise it failed
c       + Np: number of points for the generated contour
c       + ctr: ctr for the generated contour
c     
c     I/O
      integer dim
      integer Np_ref
      double precision ctr_ref(1:Nppc_mx,1:Ndim_mx-1)
      double precision width
      logical inward
      integer err_code
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer iseg,Nseg,i,j
      integer i11,i12,i21,i22
      double precision x11(1:Ndim_mx)
      double precision x12(1:Ndim_mx)
      double precision x21(1:Ndim_mx)
      double precision x22(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      logical identical,intersection_found
c     label
      character*(Nchar_mx) label
      label='subroutine generate_contour'

      err_code=0
      
      if (width.eq.0.0D+0) then
         call duplicate_ctr(dim,Np_ref,ctr_ref,Np,ctr)
         goto 666
      endif
      
      Np=0
      Nseg=Np_ref
      do iseg=1,Nseg
c     extract coordinates of x11 and x12, the 2 points of segment "iseg"
         call segment_indexes(dim,Np_ref,ctr_ref,iseg,
     &        i11,i12,x11,x12)
c     extract coordinates of x21 and x22, the 2 points of segment "iseg-1"
         call previous_segment_indexes(dim,Np_ref,ctr_ref,iseg,
     &        i21,i21,x21,x22)
c     check
         call identical_vectors(dim,x11,x22,identical)
         if (.not.identical) then
c$$$            call error(label)
c$$$            write(*,*) 'x11=',x11
c$$$            write(*,*) 'x22=',x22
c$$$            write(*,*) 'should be identical'
c$$$            write(*,*) 'Np_ref=',Np_ref
c$$$            write(*,*) 'ctr_ref:'
c$$$            do i=1,Np_ref
c$$$               write(*,*) (ctr_ref(i,j),j=1,dim-1)
c$$$            enddo               ! i
c$$$            write(*,*) 'iseg=',iseg
c$$$            write(*,*) 'i11=',i11,' i12=',i12
c$$$            write(*,*) 'segment previous to iseg:'
c$$$            write(*,*) 'i21=',i21,' i22=',i22
c$$$            stop
            err_code=1
            goto 666
         endif
c     inner normals
         call normal_for_segment(dim,x11,x12,n1)
         call normal_for_segment(dim,x21,x22,n2)
c     if outside contour required: invert normals
         if (.not.inward) then
            call scalar_vector(dim,-1.0D+0,n1,n1)
            call scalar_vector(dim,-1.0D+0,n2,n2)
         endif
c     generate new points that define the two intersecting lines
         call scalar_vector(dim,width,n1,tmp)
         call add_vectors(dim,x11,tmp,P1)
         call add_vectors(dim,x12,tmp,P2)
         call scalar_vector(dim,width,n2,tmp)
         call add_vectors(dim,x21,tmp,P3)
         call add_vectors(dim,x22,tmp,P4)
c     intersect the two lines
         call line_line_intersection(dim,P1,P2,P3,P4,.false.,
     &        intersection_found,Pint)
         if (.not.intersection_found) then
c$$$            call error(label)
c$$$            write(*,*) 'Intersection not found between:'
c$$$            write(*,*) 'line 1 defined by:'
c$$$            write(*,*) 'P1=',P1
c$$$            write(*,*) 'P2=',P2
c$$$            call substract_vectors(dim,P2,P1,P1P2)
c$$$            call normalize_vector(dim,P1P2,u12)
c$$$            write(*,*) 'u12=',u12
c$$$            write(*,*) 'x11=',x11
c$$$            write(*,*) 'x12=',x12
c$$$            write(*,*) 'n1=',n1
c$$$            call scalar_vector(dim,width,n1,tmp)
c$$$            write(*,*) 'width*n1=',tmp
c$$$            write(*,*) 'line 2 defined by:'
c$$$            write(*,*) 'P3=',P3
c$$$            write(*,*) 'P4=',P4
c$$$            write(*,*) 'x21=',x21
c$$$            write(*,*) 'x22=',x22
c$$$            write(*,*) 'n2=',n2
c$$$            call scalar_vector(dim,width,n2,tmp)
c$$$            write(*,*) 'width*n2=',tmp
c$$$            call substract_vectors(dim,P4,P3,P3P4)
c$$$            call normalize_vector(dim,P3P4,u34)
c$$$            write(*,*) 'u34=',u34
c$$$            stop            
            err_code=1
            goto 666
         endif
c     add point to contour
         call add_point_to_ctr(dim,Pint,Np,ctr)
      enddo                     ! i

 666  continue
      return
      end


      
      subroutine is_contour_valid(dim,Np_ref,ctr_ref,Np,ctr,
     &     print_err,test_inside_positions,test_self_intersection,
     &     test_intersection,test_crossed_intersection,test_clockwise,
     &     valid)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find whether or not a given contour is valid, relatively
c     to a reference contour.
c     
c     Input:
c       + dim: dimension of space
c       + Np_ref: number of points that define the reference contour
c       + ctr_ref: reference contour
c       + Np: number of points that define the contour to test
c       + ctr: contour to test
c       + print_err: true if origin of failure has to be displayed
c       + test_inside_positions: T when each position of "ctr" has to be tested as inside "ctr_ref"
c       + test_self_intersection: T when "ctr" has to be tested for self-intersection
c       + test_intersection: T when "ctr" has to be tested for intersecting "ctr_ref"
c       + test_crossed_intersection: T when each segment constituted by a point of "ctr_ref"
c     and its image in "ctr" has to NOT intersect "ctr"
c       + test_clockwise: T when "ctr" has to be tested for rotation direction
c     
c     Output:
c       + valid: T when "ctr" is valid
c     
c     I/O
      integer dim
      integer Np_ref
      double precision ctr_ref(1:Nppc_mx,1:Ndim_mx-1)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical print_err
      logical test_inside_positions
      logical test_self_intersection
      logical test_intersection
      logical test_crossed_intersection
      logical test_clockwise
      logical valid
c     temp
      integer i,j
      double precision P(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical inside,intersection_found,clockwise
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_valid'

      valid=.true.
c
c     prerequisite: number of points should be identical
      if (Np_ref.ne.Np) then
         valid=.false.
         goto 666
      endif
c     
      if (test_inside_positions) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_ref,ctr_ref)
         do i=1,Np
            call get_position_from_ctr(dim,Np,ctr,i,P)
            call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &           1,P,inside)
            if (.not.inside) then
               valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed inside_positions'
               endif
               goto 666
            endif               ! inside=F
         enddo                  ! i
      endif                     ! test_inside_positions
c
      if (test_self_intersection) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np,ctr)
         call contour_self_intersects(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour,Nppc,contour,1,
     &        intersection_found)
         if (intersection_found) then
            valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed self_intersects'
               endif
            goto 666
         endif                  ! intersection_found
      endif                     ! test_self_intersection
c
      if (test_intersection) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_ref,ctr_ref)
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np,ctr)
         call contours_intersect(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour,Nppc,contour,1,2,
     &        intersection_found)
         if (intersection_found) then
            valid=.false.
            if (print_err) then
               call error(label)
               write(*,*) 'contour failed intersection'
            endif
            goto 666
         endif                  ! intersection_found
      endif                     ! test_intersection
c
      if (test_crossed_intersection) then
         do i=1,Np_ref
            call get_position_from_ctr(dim,Np_ref,ctr_ref,i,P1)
            call get_position_from_ctr(dim,Np,ctr,i,P2)
            call segment_intersects_contour(dim,P1,P2,Np,ctr,
     &           intersection_found,Pint)
            if (intersection_found) then
               valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed crossed_intersection'
               endif
               goto 666
            endif               ! intersection_found
         enddo                  ! i
      endif                     ! test_crossed_intersection
c
      if (test_clockwise) then
         call is_contour_clockwise2(dim,Np,ctr,clockwise)
         if (clockwise) then
            valid=.false.
            if (print_err) then
               call error(label)
               write(*,*) 'contour failed clockwise'
            endif
            goto 666
         endif                  ! clockwise
      endif                     ! test_clockwise
c     
 666  continue
      return
      end
      


      subroutine is_contour_clockwise(dim,Np,ctr,clockwise)
      implicit none
      include 'max.inc'
c     
c     Purpose: find whether or not the provided contour
c     is clockwise (which is really bad)
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c     
c     Output:
c       + clockwise: true if contour is clockwise
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical clockwise
c     temp
      integer i,j,i1,i2,i3,Nd,Ni
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision x1x3(1:Ndim_mx)
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_clockwise'

      Nd=0
      Ni=0
      do i=1,Np
         i1=i
         if (i.eq.1) then
            i3=Np
         else
            i3=i1-1
         endif
         if (i.eq.Np) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
            x3(j)=ctr(i3,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         x3(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,x1x2)
         call substract_vectors(dim,x3,x1,x1x3)
         call normalize_vector(dim,x1x2,v12)
         call normalize_vector(dim,x1x3,v13)
         call vector_product_normalized(dim,v12,v13,w)
         if (w(3).gt.0.0D+0) then
            Nd=Nd+1
         else
            Ni=Ni+1
         endif
      enddo                     ! i
c
      if (Nd.ge.Ni) then
         clockwise=.false.
      else
         clockwise=.true.
      endif
c     
      return
      end


      
      subroutine is_contour_clockwise2(dim,Np,ctr,clockwise)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: find whether or not the provided contour
c     is clockwise (which is really bad)
c     
c     This version is based in the orgininal one, and was modified
c     by T. Nagel
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c     
c     Output:
c       + clockwise: true if contour is clockwise
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical clockwise
c     temp
      integer i,j,i1,i2,i3
      double precision lx1x2,lx1x3,in_angle,det,angle,pol_angle,prod
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision x1x3(1:Ndim_mx)
      double precision w(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_clockwise2'
      
      pol_angle=0.
      do i=1,Np
         i1=i
         if (i.eq.1) then
            i3=Np
         else
            i3=i1-1
         endif
         if (i.eq.Np) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
            x3(j)=ctr(i3,j)
         enddo                  ! j
c         
         call substract_vectors(dim-1,x2,x1,x1x2)
         x1x2(dim)=0.0D+0
c         call vector_length(dim,x1x2,lx1x2)
c         
         call substract_vectors(dim-1,x3,x1,x1x3)
         x1x3(dim)=0.0D+0
c         call vector_length(dim,x1x3,lx1x3)
c
         call scalar_product(dim,x1x2,x1x3,prod)
         in_angle=acos(prod)    ! rad
         in_angle=in_angle*180.0/pi ! deg
c
         call vector_product_normalized(dim,x1x2,x1x3,w)
         det=w(3)
         if (det.lt.0.0D+0) then
            angle=in_angle-180.0D+0
         else
            angle=180.0D+0-in_angle
         endif
         pol_angle=pol_angle+angle
      enddo                     ! i
c
      if (pol_angle<359.) then
         clockwise=.true.
      else
         clockwise=.false.
      endif
      
      return
      end

      
      
      subroutine segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make a segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of segment "iseg"
c       + i02: index of the second point of segment "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j
c     label
      character*(Nchar_mx) label
      label='subroutine segment_indexes'

      Nseg=Np
      i01=iseg
      if (iseg.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end
      

      
      subroutine previous_segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make the previous segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of the segment previous to "iseg"
c       + i02: index of the second point of the segment previous to "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j,iseg1
c     label
      character*(Nchar_mx) label
      label='subroutine previous_segment_indexes'

      Nseg=Np
      if (iseg.eq.1) then
         iseg1=Nseg
      else
         iseg1=iseg-1
      endif
      i01=iseg1
      if (iseg1.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end
      

      
      subroutine next_segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make the next segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of the segment next to "iseg"
c       + i02: index of the second point of the segment next to "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j,iseg1
c     label
      character*(Nchar_mx) label
      label='subroutine next_segment_indexes'

      Nseg=Np
      if (iseg.eq.Nseg) then
         iseg1=1
      else
         iseg1=iseg+1
      endif
      i01=iseg1
      if (iseg1.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end

      

      subroutine segments_intersection(dim,P0,P1,P2,inside,d,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Puropse: to get the intersection position between the two lines
c     parallel to 2 segments of a closed contour, [P0,P1] and [P1,P2], at a distance d.
c     
c     Input:
c       + dim: dimension of space
c       + P0: point that defines the beginning of the first segment
c       + P1: point that defines the end of the first segment / beginning of the second segment
c       + P2: point that defines the end of the second segment
c       + inside: true if the new trackhas to be generated inside the closed input contour
c     
c     Output:
c       + intersection_found: true if intersection position was found
c       + Pint: intersection position between the line parallel to (P0P1) at a distance d (in direction n0)
c     and the line parallel to (P1P2) at a distance d (in direction n1), when intersection_found=.true.
c     
c     I/O
      integer dim
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      logical inside
      double precision d
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      double precision n0(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision P0P1(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c      double precision n(1:Ndim_mx)
c      double precision u(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      double precision sp,alpha,beta,gamma
      double precision A(1:Ndim_mx)
      double precision B(1:Ndim_mx)
      logical identical
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine segments_intersection'

c     [P0P1]
      call substract_vectors(dim,P0,P1,P0P1)
      call normalize_vector(dim,P0P1,u0)
      call normal_for_segment(dim,P0,P1,n0)
      if (.not.inside) then
         call scalar_vector(dim,-1.0D+0,n0,n0)
      endif
c     [P1P2]
      call substract_vectors(dim,P2,P1,P1P2)
      call normalize_vector(dim,P1P2,u1)
      call normal_for_segment(dim,P1,P2,n1)
      if (.not.inside) then
         call scalar_vector(dim,-1.0D+0,n1,n1)
      endif
c     Intersection between parallel lines
      call scalar_product(dim,u0,u1,sp)
      if (sp.eq.-1.0D+0) then
         intersection_found=.false.
      else if (sp.eq.1.0D+0) then
         intersection_found=.true.
         call scalar_vector(dim,d,n1,t)
         call add_vectors(dim,p1,t,Pint)
      else
         intersection_found=.true.
         call scalar_vector(dim,d,n0,t)
         call add_vectors(dim,P0,t,A)
         call scalar_vector(dim,d,n1,t)
         call add_vectors(dim,P1,t,B)
         gamma=u1(2)*u0(1)-u1(1)*u0(2)
         if (gamma.eq.0.0D+0) then
            call error(label)
            write(*,*) 'gamma=',gamma
            write(*,*) 'should not happen at this point'
            stop
         endif
         beta=((A(2)-B(2))*u0(1)+(B(1)-A(1))*u0(2))/gamma
         if (u0(1).ne.0.0D+0) then
            alpha=(B(1)-A(1)+beta*u1(1))/u0(1)
         else
            if (u0(2).ne.0.0D+0) then
               alpha=(B(2)-A(2)+beta*u1(2))/u0(2)
            else
               write(*,*) 'u0=',(u0(j),j=1,2)
               write(*,*) 'is not normalized'
               stop
            endif
         endif
         call scalar_vector(dim,alpha,u0,t)
         call add_vectors(dim,A,t,Pint1)
         call scalar_vector(dim,beta,u1,t)
         call add_vectors(dim,B,t,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (.not.identical) then
            call error(label)
            write(*,*) 'Pint1=',Pint1
            write(*,*) 'Pint2=',Pint2
            write(*,*) 'are not identical'
         else
            call copy_vector(dim,Pint1,Pint)
         endif
      endif

      return
      end

      

      subroutine segment_line_intersection(dim,P1,P2,P3,v,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Puropse: to get the intersection position between a [P1,P2] segment 
c     and a line defined by a position P3 and a vector v
c     
c     Input:
c       + dim: dimension of space
c       + P1: point that defines the beginning of the segment
c       + P2: point that defines the end of the segment
c       + P3: point that defines the beginning of the intersecting line
c       + v: direction vector of the intersecting line
c     
c     Output:
c       + intersection_found: true if intersection position was found
c       + Pint: intersection position (if found)
c     
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      double precision alpha,beta
      double precision P1P2(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision d12
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      logical identical
c     label
      character*(Nchar_mx) label
      label='subroutine segment_line_intersection'

c     [P1P2]
      call substract_vectors(dim,P2,P1,P1P2)
      call normalize_vector(dim,P1P2,u12)
      call vector_length(dim,P1P2,d12)
c     beta: coordinate of Pint on the intersecting line
      if (u12(1)*v(2)-u12(2)*v(1).eq.0.0D+0) then
         call error(label)
         write(*,*) 'u12(1)*v(2)-u12(2)*v(1)=',u12(1)*v(2)-u12(2)*v(1)
         write(*,*) 'line (P3,v) is probably parallel to [P1P2]'
         stop
      else
         beta=((P3(1)-P1(1))*u12(2)-(P3(2)-P1(2))*u12(1))
     &        /(u12(1)*v(2)-u12(2)*v(1))
         if (u12(1).ne.0.0D+0) then
            alpha=(P3(1)-P1(1)+beta*v(1))/u12(1)
         else
            if (u12(2).eq.0.0D+0) then
               call error(label)
               write(*,*) 'u12(1)=',u12(1)
               write(*,*) 'u12(2)=',u12(2)
               write(*,*) 'should not both be null'
               stop
            else
               alpha=(P3(2)-P1(2)+beta*v(2))/u12(2)
            endif
         endif
         intersection_found=.false.
         call scalar_vector(dim,alpha,u12,t)
         call add_vectors(dim,P1,t,Pint1)
         call scalar_vector(dim,beta,v,t)
         call add_vectors(dim,P3,t,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (identical) then
            intersection_found=.true.
            call copy_vector(dim,Pint1,Pint)
         endif
      endif

      return
      end

      

      subroutine generate_walls_from_contour(dim,
     &     Ncontour,Np,contour,
     &     Ncontour2,Np2,contour2,
     &     Nz,az,wwf,fhf,wall_width,e_ext,e_in,e_glass,
     &     draw_windows,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02,
     &     Nv03,Nf03,v03,f03,
     &     Nv04,Nf04,v04,f04,
     &     Nv05,Nf05,v05,f05)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'parameters.inc'
c     
c     Purpose: to generate walls surfaces of a single floor
c     from the definition of the contours for a single type 4 building
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour, Np, contour: external contours
c       + Ncontour2, Np2, contour2: internal contours
c       + Nz: number of floors
c       + az: height of a floor
c       + wwf: fraction of the walls used by windows
c       + fhf: fraction of the floor height used by windows
c       + wall_width: width of the wall for each contour [m]
c       + e_ext: thickness of external solid boundaries [m]
c       + e_in: thickness of internal solid boundaries [m]
c       + e_glass: thickness of a single glass panel [m]
c       + draw_windows: when set to T, the windows will be drawn
c     
c     Output:
c       + Nv01, Nf01, v01, f01: object for the external air / external material surface
c       + Nv02, Nf02, v02, f02: object for the internal air / internal material surface
c       + Nv03, Nf03, v03, f03: object for the wall / glass interface
c       + Nv04, Nf04, v04, f04: object for the external air / glass interface
c       + Nv05, Nf05, v05, f05: object for the internal air / glass interface
c     
c     I/O
      integer dim      
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Ncontour2
      integer Np2(1:Ncontour_mx)
      double precision contour2(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Nz
      double precision az
      double precision wwf
      double precision fhf
      double precision wall_width(1:Ncontour_mx)
      double precision e_ext
      double precision e_in
      double precision e_glass
      logical draw_windows
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      integer Nv03,Nf03
      double precision v03(1:Nv_so_mx,1:Ndim_mx)
      integer f03(1:Nf_so_mx,1:3)
      integer Nv04,Nf04
      double precision v04(1:Nv_so_mx,1:Ndim_mx)
      integer f04(1:Nf_so_mx,1:3)
      integer Nv05,Nf05
      double precision v05(1:Nv_so_mx,1:Ndim_mx)
      integer f05(1:Nf_so_mx,1:3)
c     temp
      double precision axe(1:Ndim_mx)
      double precision axe_x(1:Ndim_mx)
      double precision axe_y(1:Ndim_mx)
      double precision axe_z(1:Ndim_mx)
      integer ic,i,j,iw,Nw
      double precision deltaZ
      integer building_type
      integer Ncontour12
      integer Np12(1:Ncontour_mx)
      double precision contour12(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Ncontour34
      integer Np34(1:Ncontour_mx)
      double precision contour34(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
      logical inside
      integer new_Npoints
      double precision new_track(1:Nppt_mx,1:2)
      double precision t(1:Ndim_mx)
      double precision Pt(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision n12(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision n34(1:Ndim_mx)
      double precision v0(1:Ndim_mx)
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision v3(1:Ndim_mx)
      double precision v4(1:Ndim_mx)
      double precision tmp1(1:Ndim_mx)
      double precision tmp2(1:Ndim_mx)
      double precision tmp3(1:Ndim_mx)
      double precision tmp4(1:Ndim_mx)
      double precision tmp5(1:Ndim_mx)
      double precision tmp6(1:Ndim_mx)
      double precision tmp7(1:Ndim_mx)
      double precision tmp8(1:Ndim_mx)
      double precision tmp9(1:Ndim_mx)
      double precision tmp10(1:Ndim_mx)
      double precision tmp11(1:Ndim_mx)
      double precision tmp12(1:Ndim_mx)
      double precision alpha,pv,pext,d12,d34,d
      double precision window_width,inter_window_width
      integer Nvt,Nft
      double precision vt(1:Nv_so_mx,1:Ndim_mx)
      integer ft(1:Nf_so_mx,1:3)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision theta
      double precision center(1:Ndim_mx)
      double precision fhf_min
      logical draw_specific_window
c     Debug
      integer ip
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine generate_walls_from_contour'
      
      axe_x(1)=1.0D+0
      axe_x(2)=0.0D+0
      axe_x(3)=0.0D+0
      axe_y(1)=0.0D+0
      axe_y(2)=1.0D+0
      axe_y(3)=0.0D+0
      axe_z(1)=0.0D+0
      axe_z(2)=0.0D+0
      axe_z(3)=1.0D+0

      call identify_building_type(dim,
     &     Ncontour,Np,contour,
     &     building_type)
      
c     external perimeter
      pext=0.0D+0
      do ic=1,Ncontour
         do i=1,Np(ic)
            do j=1,dim-1
               P1(j)=contour(ic,i,j)
               if (i.eq.Np(ic)) then
                  P2(j)=contour(ic,1,j)
               else
                  P2(j)=contour(ic,i+1,j)
               endif            ! i=Np(ic)
            enddo               ! j
            P1(dim)=0.0D+0
            P2(dim)=0.0D+0
            call substract_vectors(dim,P2,P1,P1P2)
            call vector_length(dim,P1P2,d12)
            pext=pext+d12
         enddo                  ! i
      enddo                     ! ic
c     fhf_min
      fhf_min=(2*e_ext+Nz*az+(Nz-1)*e_in)*wwf/(az*Nz) ! for alpha=1
      if (fhf.lt.fhf_min) then
         fhf=fhf_min
      endif
c     -------------------------------------------------------------------
c     MAGIC FORMULA: fraction of the perimeter used for glass (alpha)
c     Sw: total surface of windows for a facade
c     1. Sw=H.L.wwf with:
c     H the height of the building: H=2.e_ext+Nz.az+(Nz-1).e_in
c     L: length of the facade
c     wwf: fraction of the facade used by windows (input defined by the user)
c     2. fhf is the fraction of a floor (of height az) used by windows;
c     this is also a input parameter, defined by the user.
c     The total height of windows for a floor is therefore az.fhf
c     The total height of windows for the facade is therefore Nz.az.fhf
c     The total length of windows for the facade is L.alpha, with
c     L the length of the facade and alpha the fraction of this length
c     used by windows.
c     => Sw=Nz.az.fhf.L.alpha
c     => Nz.az.fhf.L.alpha=(2.e_ext+Nz.az+(Nz-1).e_in).L.wwf
c     => alpha=(2.e_ext+Nz.az+(Nz-1).e_in).wwf/(Nz.az.fhf)
c     This is for a single side of a building; but since fhf is the same
c     for all sides of the building, the definition of alpha remains valid
c     for the whole perimeter of the building.
      alpha=(2*e_ext+Nz*az+(Nz-1)*e_in)*wwf/(Nz*az*fhf)
      if (alpha.ge.1.0) then
         call error(label)
         write(*,*) 'alpha=',alpha
         write(*,*) 'should be < 1'
         stop
      else
c     -------------------------------------------------------------------
c     glass perimeter
         pv=pext*alpha
      endif                     ! alpha.ge.1

c     Surfaces creation
      Nv01=0
      Nf01=0
      Nv02=0
      Nf02=0
      Nv03=0
      Nf03=0
      Nv04=0
      Nf04=0
      Nv05=0
      Nf05=0
c     
      do ic=1,Ncontour
         if (wall_width(ic)-e_glass.lt.0.0D+0) then
            call error(label)
            write(*,*) 'wall_width(',ic,')=',wall_width(ic)
            write(*,*) '< e_glass=',e_glass
            stop
         endif
c     hints for potential readers: the following section generates the trianglemesh
c     for vertical surfaces (internal_air / building, atmosphere / building,
c     glass, etc...). I could not think of a more cumbersome way to do that than
c     what has finally been coded: contours and trianglemeshes are generated
c     horizontally, then are rotated in order to produce vertical surfaces.
         do i=1,Np(ic)
c     positions P1 and P2 delimit segment index 'i' on contour(ic) (external contour)
            do j=1,dim-1
               if (ic.eq.1) then
                  P1(j)=contour(ic,i,j)
                  if (i.eq.Np(ic)) then
                     P2(j)=contour(ic,1,j)
                  else
                     P2(j)=contour(ic,i+1,j)
                  endif         ! i=Np(ic)
               else
                  P2(j)=contour(ic,i,j)
                  if (i.eq.Np(ic)) then
                     P1(j)=contour(ic,1,j)
                  else
                     P1(j)=contour(ic,i+1,j)
                  endif         ! i=Np(ic)
               endif            ! ic
            enddo               ! j
            P1(dim)=0.0D+0
            P2(dim)=0.0D+0
            call substract_vectors(dim,P2,P1,P1P2)
            call vector_length(dim,P1P2,d12)
            call normalize_vector(dim,P1P2,u12)
            call normal_for_segment(dim,P1,P2,n12)
c     positions P3 and P4 delimit segment index 'i' on contour2(ic) (internal contour)
            do j=1,dim-1
               if (ic.eq.1) then
                  P3(j)=contour2(ic,i,j)
                  if (i.eq.Np2(ic)) then
                     P4(j)=contour2(ic,1,j)
                  else
                     P4(j)=contour2(ic,i+1,j)
                  endif         ! i=Np(ic)
               else
                  P4(j)=contour2(ic,i,j)
                  if (i.eq.Np2(ic)) then
                     P3(j)=contour2(ic,1,j)
                  else
                     P3(j)=contour2(ic,i+1,j)
                  endif         ! i=Np(ic)
               endif            ! ic
            enddo               ! j
            P3(dim)=0.0D+0
            P4(dim)=0.0D+0
            call substract_vectors(dim,P4,P3,P3P4)
            call vector_length(dim,P3P4,d34)
            call normalize_vector(dim,P3P4,u34)
            call normal_for_segment(dim,P3,P4,n34)
c     Number of windows over the current segment
            if (building_type.eq.1) then
               Nw=1
            else
               Nw=int(d12*alpha)+1
            endif
c     Width of a given window
            window_width=d12*alpha/dble(Nw)
            if ((draw_windows).and.(window_width.le.0.0)) then
               call error(label)
               write(*,*) 'window_width=',window_width
               write(*,*) 'should be > 0'
               stop
            endif
c     Length between two consecutive windows
            inter_window_width=(d12-Nw*window_width)/dble(Nw+1)
            if (inter_window_width.le.0.0D+0) then
               call error(label)
               write(*,*) 'inter_window_width=',inter_window_width
               write(*,*) 'should be > 0'
               stop
            endif
c     Height between the bottom of the floor and the bottom of the window
            deltaZ=az*(1.0-fhf)/2.0D+0
c     create the contours for the wall section
            Ncontour12=0
            Ncontour34=0
            Npoints=4
            track(1,1)=P1(1)
            track(1,2)=P1(2)
            track(2,1)=P2(1)
            track(2,2)=P2(2)
            call scalar_vector(dim,az,n12,t)
            call add_vectors(dim,P2,t,Pt)
            track(3,1)=Pt(1)
            track(3,2)=Pt(2)
            call add_vectors(dim,P1,t,Pt)
            track(4,1)=Pt(1)
            track(4,2)=Pt(2)
            call add_contour(Ncontour12,Np12,contour12,
     &           Npoints,track)
c     At this point, "contour12" is the contour of the external side
c     of a single floor, for the current segment
            track(1,1)=P3(1)
            track(1,2)=P3(2)
            track(2,1)=P4(1)
            track(2,2)=P4(2)
            call scalar_vector(dim,az,n34,t)
            call add_vectors(dim,P4,t,Pt)
            track(3,1)=Pt(1)
            track(3,2)=Pt(2)
            call add_vectors(dim,P3,t,Pt)
            track(4,1)=Pt(1)
            track(4,2)=Pt(2)
            call add_contour(Ncontour34,Np34,contour34,
     &           Npoints,track)
c     And "contour34" is the contour of the internal side of a single
c     floor, for the current segment
c
c           set "draw_specific_window" for the windows of the current segment
            draw_specific_window=.true.
            if ((.not.draw_windows).or.(window_width.lt.1.0D-1)) then
               draw_specific_window=.false.
            endif

c     draw windows if required
            if (draw_specific_window) then
               do iw=1,Nw       ! loop over windows, for the current segment
                  Npoints=4
                  d=inter_window_width*iw+window_width*(iw-1)
                  call scalar_vector(dim,d,u12,t)
                  call add_vectors(dim,P1,t,v1)
                  call copy_vector(dim,v1,v0)
                  call scalar_vector(dim,deltaZ,n12,t)
                  call add_vectors(dim,v1,t,v1)
                  call scalar_vector(dim,window_width,u12,t)
                  call add_vectors(dim,v1,t,v2)
                  call scalar_vector(dim,az*fhf,n12,t)
                  call add_vectors(dim,v2,t,v3)
                  call add_vectors(dim,v1,t,v4)
                  do j=1,2
                     track(1,j)=v1(j)
                     track(2,j)=v2(j)
                     track(3,j)=v3(j)
                     track(4,j)=v4(j)
                  enddo         ! j
                  call add_contour(Ncontour12,Np12,contour12,
     &                 Npoints,track)
c     "contour12" has been updated with the contour of the current window:
c     there is now 1 more hole in the external side of the current floor / segment
c     
                  call scalar_vector(dim,wall_width(ic),n12,t)
                  call add_vectors(dim,v0,t,v1)
                  call scalar_vector(dim,deltaZ,n12,t)
                  call add_vectors(dim,v1,t,v1)
                  call scalar_vector(dim,window_width,u34,t)
                  call add_vectors(dim,v1,t,v2)
                  call scalar_vector(dim,az*fhf,n34,t)
                  call add_vectors(dim,v2,t,v3)
                  call add_vectors(dim,v1,t,v4)
                  do j=1,2
                     track(1,j)=v1(j)
                     track(2,j)=v2(j)
                     track(3,j)=v3(j)
                     track(4,j)=v4(j)
                  enddo         ! j
                  call add_contour(Ncontour34,Np34,contour34,
     &                 Npoints,track)
c     "contour34" has been updated with the contour of the current window
                  
c     tmp1 to tmp4: window hole on outer surface
                  call scalar_vector(dim,deltaZ,axe_z,t)
                  call add_vectors(dim,v0,t,tmp1)
                  call scalar_vector(dim,window_width,u12,t)
                  call add_vectors(dim,tmp1,t,tmp2)
                  call scalar_vector(dim,az*fhf,axe_z,t)
                  call add_vectors(dim,tmp2,t,tmp3)
                  call add_vectors(dim,tmp1,t,tmp4)
c     tmp5 to tmp8: window hole on external face of the glass
                  call scalar_vector(dim,wall_width(ic)-e_glass,n12,t)
                  call add_vectors(dim,v0,t,tmp5)
                  call scalar_vector(dim,deltaZ,axe_z,t)
                  call add_vectors(dim,tmp5,t,tmp5)
                  call scalar_vector(dim,window_width,u12,t)
                  call add_vectors(dim,tmp5,t,tmp6)
                  call scalar_vector(dim,az*fhf,axe_z,t)
                  call add_vectors(dim,tmp6,t,tmp7)
                  call add_vectors(dim,tmp5,t,tmp8)
c     tmp9 to tmp12: window hole on interface between glass / internal surface
                  call scalar_vector(dim,wall_width(ic),n12,t)
                  call add_vectors(dim,v0,t,tmp9)
                  call scalar_vector(dim,deltaZ,axe_z,t)
                  call add_vectors(dim,tmp9,t,tmp9)
                  call scalar_vector(dim,window_width,u34,t)
                  call add_vectors(dim,tmp9,t,tmp10)
                  call scalar_vector(dim,az*fhf,axe_z,t)
                  call add_vectors(dim,tmp10,t,tmp11)
                  call add_vectors(dim,tmp9,t,tmp12)
c     add vertices / faces to 01: at this point, this is the frame of the window (in contact with the atmosphere)
                  do j=1,dim
                     v01(Nv01+1,j)=tmp1(j)
                     v01(Nv01+2,j)=tmp2(j)
                     v01(Nv01+3,j)=tmp3(j)
                     v01(Nv01+4,j)=tmp4(j)
                     v01(Nv01+5,j)=tmp5(j)
                     v01(Nv01+6,j)=tmp6(j)
                     v01(Nv01+7,j)=tmp7(j)
                     v01(Nv01+8,j)=tmp8(j)
                  enddo         ! j
                  f01(Nf01+1,1)=Nv01+1
                  f01(Nf01+1,2)=Nv01+2
                  f01(Nf01+1,3)=Nv01+5
                  f01(Nf01+2,1)=Nv01+2
                  f01(Nf01+2,2)=Nv01+6
                  f01(Nf01+2,3)=Nv01+5
                  f01(Nf01+3,1)=Nv01+2
                  f01(Nf01+3,2)=Nv01+3
                  f01(Nf01+3,3)=Nv01+6
                  f01(Nf01+4,1)=Nv01+6
                  f01(Nf01+4,2)=Nv01+3
                  f01(Nf01+4,3)=Nv01+7
                  f01(Nf01+5,1)=Nv01+3
                  f01(Nf01+5,2)=Nv01+4
                  f01(Nf01+5,3)=Nv01+7
                  f01(Nf01+6,1)=Nv01+4
                  f01(Nf01+6,2)=Nv01+8
                  f01(Nf01+6,3)=Nv01+7
                  f01(Nf01+7,1)=Nv01+4
                  f01(Nf01+7,2)=Nv01+1
                  f01(Nf01+7,3)=Nv01+5
                  f01(Nf01+8,1)=Nv01+5
                  f01(Nf01+8,2)=Nv01+8
                  f01(Nf01+8,3)=Nv01+4
                  Nv01=Nv01+8
                  Nf01=Nf01+8
c     add vertices / faces to 03: interface between the building and the glass
                  do j=1,dim
                     v03(Nv03+1,j)=tmp5(j)
                     v03(Nv03+2,j)=tmp6(j)
                     v03(Nv03+3,j)=tmp7(j)
                     v03(Nv03+4,j)=tmp8(j)
                     v03(Nv03+5,j)=tmp9(j)
                     v03(Nv03+6,j)=tmp10(j)
                     v03(Nv03+7,j)=tmp11(j)
                     v03(Nv03+8,j)=tmp12(j)
                  enddo         ! j
                  f03(Nf03+1,1)=Nv03+1
                  f03(Nf03+1,2)=Nv03+2
                  f03(Nf03+1,3)=Nv03+5
                  f03(Nf03+2,1)=Nv03+2
                  f03(Nf03+2,2)=Nv03+6
                  f03(Nf03+2,3)=Nv03+5
                  f03(Nf03+3,1)=Nv03+2
                  f03(Nf03+3,2)=Nv03+3
                  f03(Nf03+3,3)=Nv03+6
                  f03(Nf03+4,1)=Nv03+6
                  f03(Nf03+4,2)=Nv03+3
                  f03(Nf03+4,3)=Nv03+7
                  f03(Nf03+5,1)=Nv03+3
                  f03(Nf03+5,2)=Nv03+4
                  f03(Nf03+5,3)=Nv03+7
                  f03(Nf03+6,1)=Nv03+4
                  f03(Nf03+6,2)=Nv03+8
                  f03(Nf03+6,3)=Nv03+7
                  f03(Nf03+7,1)=Nv03+4
                  f03(Nf03+7,2)=Nv03+1
                  f03(Nf03+7,3)=Nv03+5
                  f03(Nf03+8,1)=Nv03+5
                  f03(Nf03+8,2)=Nv03+8
                  f03(Nf03+8,3)=Nv03+4
                  Nv03=Nv03+8
                  Nf03=Nf03+8
c     add vertices / faces to 04: atmosphere / glass interface
                  do j=1,dim
                     v04(Nv04+1,j)=tmp5(j)
                     v04(Nv04+2,j)=tmp6(j)
                     v04(Nv04+3,j)=tmp7(j)
                     v04(Nv04+4,j)=tmp8(j)
                  enddo         ! j
                  f04(Nf04+1,1)=Nv04+1
                  f04(Nf04+1,2)=Nv04+2
                  f04(Nf04+1,3)=Nv04+3
                  f04(Nf04+2,1)=Nv04+1
                  f04(Nf04+2,2)=Nv04+3
                  f04(Nf04+2,3)=Nv04+4
                  Nv04=Nv04+4
                  Nf04=Nf04+2
c     add vertices / faces to 05: internal_air / glass interface
                  do j=1,dim
                     v05(Nv05+1,j)=tmp9(j)
                     v05(Nv05+2,j)=tmp10(j)
                     v05(Nv05+3,j)=tmp11(j)
                     v05(Nv05+4,j)=tmp12(j)
                  enddo         ! j
                  f05(Nf05+1,1)=Nv05+3
                  f05(Nf05+1,2)=Nv05+2
                  f05(Nf05+1,3)=Nv05+1
                  f05(Nf05+2,1)=Nv05+4
                  f05(Nf05+2,2)=Nv05+3
                  f05(Nf05+2,3)=Nv05+1
                  Nv05=Nv05+4
                  Nf05=Nf05+2
               enddo            ! iw
            endif               ! draw_specific_window
c     
            call segment_rotation_angle(dim,P1P2,theta)
            axe(1)=dcos(theta)
            axe(2)=dsin(theta)
            axe(3)=0.0D+0
            theta=pi/2.0
            call rotation_matrix(dim,theta,axe,M)
c     contour12 is used to produce v01/f01 (interface between the atmosphere and the building)
            call delaunay_triangulation(dim,Ncontour12,Np12,contour12,
     &           Nvt,Nft,vt,ft) ! @ altitude 0; normals are directed upward
            call move_obj(dim,Nvt,vt,-P1)
            call rotate_obj(dim,Nvt,vt,M)
            call move_obj(dim,Nvt,vt,P1)
            call add_obj_to_obj(dim,Nvt,Nft,vt,ft,
     &           Nv01,Nf01,v01,f01)
c     contour34 is used to produce v02/f02 (interface between the internal air and the building)
            call delaunay_triangulation(dim,Ncontour34,Np34,contour34,
     &           Nvt,Nft,vt,ft) ! @ altitude 0; normals are directed upward
            call invert_normals(Nft,ft) ! normals are directed downward
            call move_obj(dim,Nvt,vt,-P3)
            call rotate_obj(dim,Nvt,vt,M)
            call move_obj(dim,Nvt,vt,P3)
            call add_obj_to_obj(dim,Nvt,Nft,vt,ft,
     &           Nv02,Nf02,v02,f02)
         enddo                  ! i
      enddo                     ! ic

      return
      end
      


      subroutine generate_roof_from_contour(dim,debug,
     &     Ncontour,Np,contour,
     &     Nz,az,e_ext,e_in,roof_height,roof_thickness,
     &     valid,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02,
     &     Vroof_solid,Vroof_air)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c     
c     Purpose: to generate a roof from the contour of a building
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour, Np, contour: external contours
c       + Nz: number of floors
c       + az: height of a floor
c       + e_ext: thickness of external solid boundaries [m]
c       + e_in: thickness of internal solid boundaries [m]
c       + roof_height: height of the roof [m]
c       + roof_thickness: thickness of the roof (m)
c     
c     Output:
c       + valid: true if roof could be generated
c       + Nv01, Nf01, v01, f01: object for the external air / roof material surface
c       + Nv02, Nf02, v02, f02: object for the internal air / roof material surface
c       + Vroof_solid: volume of solid for the roof [m³]
c       + Vroof_air: volume of internal air in the attic [m³]
c     
c     I/O
      integer dim
      logical debug
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Nz
      double precision az
      double precision e_ext
      double precision e_in
      double precision roof_height
      double precision roof_thickness
      logical valid
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      double precision Vroof_solid
      double precision Vroof_air
c     temp
      double precision axe(1:Ndim_mx)
      double precision axe_x(1:Ndim_mx)
      double precision axe_y(1:Ndim_mx)
      double precision axe_z(1:Ndim_mx)
      integer Ncontour_tmp
      integer Nppc_tmp(1:Ncontour_mx)
      double precision contour_tmp(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Ncontour0
      integer Np0(1:Ncontour_mx)
      double precision contour0(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Ncontour2
      integer Np2(1:Ncontour_mx)
      double precision contour2(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer ic,i,j,iw,Nw,icontour
      double precision deltaZ
      integer building_type
      integer Ncontour12
      integer Np12(1:Ncontour_mx)
      double precision contour12(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
      logical inside
      integer Npoints1
      double precision track1(1:Nppt_mx,1:2)
      integer Npoints2
      double precision track2(1:Nppt_mx,1:2)
      integer Npoints_tmp
      double precision track_tmp(1:Nppt_mx,1:2)
      double precision t(1:Ndim_mx)
      double precision Pt(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision n12(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision n34(1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision v0(1:Ndim_mx)
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision v3(1:Ndim_mx)
      double precision v4(1:Ndim_mx)
      double precision tmp1(1:Ndim_mx)
      double precision tmp2(1:Ndim_mx)
      double precision tmp3(1:Ndim_mx)
      double precision tmp4(1:Ndim_mx)
      double precision tmp5(1:Ndim_mx)
      double precision tmp6(1:Ndim_mx)
      double precision tmp7(1:Ndim_mx)
      double precision tmp8(1:Ndim_mx)
      double precision tmp9(1:Ndim_mx)
      double precision tmp10(1:Ndim_mx)
      double precision tmp11(1:Ndim_mx)
      double precision tmp12(1:Ndim_mx)
      double precision d12,d34,d
      double precision height_ref
      integer Nvt,Nft
      double precision vt(1:Nv_so_mx,1:Ndim_mx)
      integer ft(1:Nf_so_mx,1:3)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision theta
      double precision center(1:Ndim_mx)
      integer Ncontour_flat
      integer Np_flat(1:Ncontour_mx)
      double precision contour_flat(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Nv_flat,Nf_flat
      double precision v_flat(1:Nv_so_mx,1:Ndim_mx)
      integer f_flat(1:Nf_so_mx,1:3)
      integer Nv_top,Nf_top
      double precision v_top(1:Nv_so_mx,1:Ndim_mx)
      integer f_top(1:Nf_so_mx,1:3)
      integer Nv_tmp,Nf_tmp
      double precision v_tmp(1:Nv_so_mx,1:Ndim_mx)
      integer f_tmp(1:Nf_so_mx,1:3)
      integer Nv_tmp2,Nf_tmp2
      double precision v_tmp2(1:Nv_so_mx,1:Ndim_mx)
      integer f_tmp2(1:Nf_so_mx,1:3)
      integer Nv_enc,Nf_enc
      double precision v_enc(1:Nv_so_mx,1:Ndim_mx)
      integer f_enc(1:Nf_so_mx,1:3)
      character*(Nchar_mx) filename
      logical self_intersection,self_intersection_found
      logical intersection,success
      logical best_fit
      double precision width
      logical nseg_identical
      double precision Vtotal
c     label
      character*(Nchar_mx) label
      label='subroutine generate_roof_from_contour'
      
      axe_x(1)=1.0D+0
      axe_x(2)=0.0D+0
      axe_x(3)=0.0D+0
      axe_y(1)=0.0D+0
      axe_y(2)=1.0D+0
      axe_y(3)=0.0D+0
      axe_z(1)=0.0D+0
      axe_z(2)=0.0D+0
      axe_z(3)=1.0D+0
c     flat roof when the number of segments is different for the external
c     contour and subsequent internal contours
      nseg_identical=.true.
      if (Ncontour.gt.1) then
         do icontour=2,Ncontour
            if (Np(icontour).ne.Np(1)) then
               nseg_identical=.false.
               goto 111
            endif
         enddo                  ! icontour
      endif                     ! Ncontour > 1
 111  continue
      if (.not.nseg_identical) then
         valid=.false.
         goto 666
      endif
      
      call identify_building_type(dim,
     &     Ncontour,Np,contour,
     &     building_type)
c     building_type=1: residential house
c     building _type=2: group of touching buildings arranged in a closed contour
      if (building_type.eq.1) then
         call get_contour(Ncontour,Np,contour,1,Npoints,track)
         inside=.false.
         call generate_close_contour2(dim,.false.,Npoints,track,
     &        0.0D+0,roof_extension_length,inside,.true.,success,
     &        best_fit,width,Npoints1,track1)
         if (.not.success) then
            call error(label)
            write(*,*) 'could not generate external contour'
            write(*,*) 'roof_extension_length=',roof_extension_length
            stop
         endif
c     
         inside=.true.
         call generate_close_contour2(dim,.false.,Npoints,track,
     &        0.0D+0,roof_slope_length,inside,.true.,success,
     &        best_fit,width,Npoints2,track2)
         if (.not.success) then
            call error(label)
            write(*,*) 'could not generate internal contour'
            write(*,*) 'roof_slope_length=',roof_slope_length
            Ncontour0=1
            Np0(1)=Npoints
            do i=1,Np0(1)
               do j=1,dim-1
                  contour0(1,i,j)=track(i,j)
               enddo            ! j
            enddo               ! i
            filename='./gnuplot/contour.dat'
            call record_gnuplot_contour(filename,dim,
     &           Ncontour0,Np0,contour0)
            stop
         endif
c     prepare contour_tmp
         Ncontour_tmp=0
         call add_ctr_to_contour(dim,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,
     &        Npoints,track)
         call add_ctr_to_contour(dim,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,
     &        Npoints1,track1)
         call add_ctr_to_contour(dim,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,
     &        Npoints2,track2)
c     check self-intersection for new contours
         call contour_self_intersects(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,2,
     &        self_intersection_found)
         if (self_intersection_found) then
            call duplicate_ctr(dim,Npoints,track,Npoints1,track1)
         endif                  ! self_intersection_found
         call contour_self_intersects(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,3,
     &        self_intersection_found)
         if (self_intersection_found) then
            call duplicate_ctr(dim,Npoints,track,Npoints2,track2)
         endif                  ! self_intersection_found
c     check for intersection with outer contour
         call contours_intersect(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,1,2,
     &        intersection_found)
         if (intersection_found) then
            call duplicate_ctr(dim,Npoints,track,Npoints1,track1)
         endif                  ! intersection_found
         call contours_intersect(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,1,3,
     &        intersection_found)
         if (intersection_found) then
            call duplicate_ctr(dim,Npoints,track,Npoints2,track2)
         endif                  ! intersection_found
         
      else                      ! building_type=2: only 2 tracks with identical Npoints
         call get_contour(Ncontour,Np,contour,1,Npoints1,track1)
         Npoints2=Np(1)
         do i=1,Npoints2
            do j=1,dim-1
               track2(i,j)=(contour(1,i,j)+contour(2,i,j))/2.0D+0 ! track2: middle
            enddo               ! j
         enddo                  ! i
      endif                     ! building_type
      height_ref=2*e_ext+Nz*az+(Nz-1)*e_in
      if (building_type.eq.1) then
         deltaZ=roof_height*roof_extension_length/roof_slope_length
      else
         deltaZ=0.0D+0
      endif                     ! building_type
      Nv01=0
      Nf01=0
      Nv02=0
      Nf02=0
c     ----------------------------------------------------------------
c     LATERAL BAND
c     ----------------------------------------------------------------
      do ic=1,Ncontour
         do i=1,Np(ic)
            if (ic.eq.1) then
c     positions v1 and v2 delimit segment index 'i' on track1
               do j=1,dim-1
                  v1(j)=track1(i,j)
                  if (i.eq.Np(ic)) then
                     v2(j)=track1(1,j)
                  else
                     v2(j)=track1(i+1,j)
                  endif         ! i=Np(ic)
               enddo            ! j
               v1(dim)=height_ref-deltaZ
               v2(dim)=height_ref-deltaZ
            else
c     positions v1 and v2 delimit segment index 'i' on contour(ic)
               do j=1,dim-1
                  v2(j)=contour(ic,i,j)
                  if (i.eq.Np(ic)) then
                     v1(j)=contour(ic,1,j)
                  else
                     v1(j)=contour(ic,i+1,j)
                  endif         ! i=Np(ic)
               enddo            ! j
               v1(dim)=height_ref
               v2(dim)=height_ref
            endif               ! ic=1
            call scalar_vector(dim,roof_thickness,axe_z,t)
            call add_vectors(dim,v1,t,v4)
            call add_vectors(dim,v2,t,v3)
c     add vertices and faces for lateral band
            do j=1,dim
               v01(Nv01+1,j)=v1(j)
               v01(Nv01+2,j)=v2(j)
               v01(Nv01+3,j)=v3(j)
               v01(Nv01+4,j)=v4(j)
            enddo               ! j
            f01(Nf01+1,1)=Nv01+1
            f01(Nf01+1,2)=Nv01+2
            f01(Nf01+1,3)=Nv01+3
            f01(Nf01+2,1)=Nv01+1
            f01(Nf01+2,2)=Nv01+3
            f01(Nf01+2,3)=Nv01+4
            Nv01=Nv01+4
            Nf01=Nf01+2
         enddo                  ! i
      enddo                     ! ic
c     ----------------------------------------------------------------
c     FLAT SECTION OF THE ROOF
c     ----------------------------------------------------------------
      if (building_type.eq.1) then
         Ncontour_flat=0
         call add_contour(Ncontour_flat,Np_flat,contour_flat,
     &        Npoints2,track2)
         inside=.false.
         do ic=2,Ncontour
            call get_contour(Ncontour,Np,contour,ic,
     &           Npoints,track)
            call generate_close_contour2(dim,.false.,Npoints,track,
     &           0.0D+0,roof_slope_length,inside,.true.,success,
     &           best_fit,width,Npoints_tmp,track_tmp)
            if (.not.success) then
               call error(label)
               write(*,*) 'could not generate external contour'
               write(*,*) 'roof_slope_length=',roof_slope_length
               stop
            endif
            call add_contour(Ncontour_flat,Np_flat,contour_flat,
     &           Npoints_tmp,track_tmp)
         enddo                  ! ic
         call delaunay_triangulation(dim,
     &        Ncontour_flat,Np_flat,contour_flat,
     &        Nv_flat,Nf_flat,v_flat,f_flat)    ! @ altitude 0; normals are directed upward
         center(1)=0.0
         center(2)=0.0
         center(3)=height_ref+roof_height
         call move_obj(dim,Nv_flat,v_flat,center)
         call duplicate_obj(dim,Nv_flat,Nf_flat,v_flat,f_flat,
     &        Nv_tmp,Nf_tmp,v_tmp,f_tmp)
      else                      ! building_type=2
         Nv_tmp=0
         Nf_tmp=0
      endif                     ! building_type
c     Duplicate flat section of the roof for Nv02/Nf02
      call duplicate_obj(dim,Nv_tmp,Nf_tmp,v_tmp,f_tmp,
     &     Nv_tmp2,Nf_tmp2,v_tmp2,f_tmp2)
      center(1)=0.0
      center(2)=0.0
      center(3)=roof_thickness
      call move_obj(dim,Nv_tmp,v_tmp,center)
      call add_obj_to_obj(dim,Nv_tmp,Nf_tmp,v_tmp,f_tmp,
     &     Nv01,Nf01,v01,f01)
      call invert_normals(Nf_tmp2,f_tmp2)
      call add_obj_to_obj(dim,Nv_tmp2,Nf_tmp2,v_tmp2,f_tmp2,
     &     Nv02,Nf02,v02,f02)
c     ----------------------------------------------------------------
c     SLOPES
c     ----------------------------------------------------------------
c     1- slope from the top of the building, external
      Nv_tmp=0
      Nf_tmp=0
      if (building_type.eq.1) then
         do ic=1,Ncontour
            do i=1,Np(ic)
               if (ic.eq.1) then
c     positions v1 and v2 delimit segment index 'i' on track1
                  do j=1,dim-1
                     v1(j)=track1(i,j)
                     if (i.eq.Npoints1) then
                        v2(j)=track1(1,j)
                     else
                        v2(j)=track1(i+1,j)
                     endif      ! i=Np(ic)
                  enddo         ! j
                  v1(dim)=height_ref-deltaZ
                  v2(dim)=height_ref-deltaZ
               else
c     positions v1 and v2 delimit segment index 'i' on track "contour(ic)"
                  do j=1,dim-1
                     v2(j)=contour(ic,i,j)
                     if (i.eq.Np(ic)) then
                        v1(j)=contour(ic,1,j)
                     else
                        v1(j)=contour(ic,i+1,j)
                     endif      ! i=Np(ic)
                  enddo         ! j
                  v1(dim)=height_ref
                  v2(dim)=height_ref
               endif            ! ic
c     positions v3 and v4 delimit segment index 'i' on track "contour_flat(ic)"
               do j=1,dim-1
                  v4(j)=contour(ic,i,j)
                  if (i.eq.Np_flat(ic)) then
                     v3(j)=contour(ic,1,j)
                  else
                     v3(j)=contour(ic,i+1,j)
                  endif         ! i=Np_flat(ic)
               enddo            ! j
               v3(dim)=height_ref
               v4(dim)=height_ref
c     add vertices and faces for slope index 'i'
               do j=1,dim
                  v_tmp(Nv_tmp+1,j)=v1(j)
                  v_tmp(Nv_tmp+2,j)=v2(j)
                  v_tmp(Nv_tmp+3,j)=v3(j)
                  v_tmp(Nv_tmp+4,j)=v4(j)
               enddo            ! j
               f_tmp(Nf_tmp+1,1)=Nv_tmp+1
               f_tmp(Nf_tmp+1,2)=Nv_tmp+2
               f_tmp(Nf_tmp+1,3)=Nv_tmp+3
               f_tmp(Nf_tmp+2,1)=Nv_tmp+1
               f_tmp(Nf_tmp+2,2)=Nv_tmp+3
               f_tmp(Nf_tmp+2,3)=Nv_tmp+4
               Nv_tmp=Nv_tmp+4
               Nf_tmp=Nf_tmp+2
            enddo               ! i
         enddo                  ! ic
      endif ! building_type=1
      call invert_normals(Nf_tmp,f_tmp)
      call add_obj_to_obj(dim,Nv_tmp,Nf_tmp,v_tmp,f_tmp,
     &     Nv01,Nf01,v01,f01)

c     2- slope on the top of the roof
      Nv_tmp=0
      Nf_tmp=0
      do ic=1,Ncontour
         do i=1,Np(ic)
            if (ic.eq.1) then
c     positions v1 and v2 delimit segment index 'i' on track1
               do j=1,dim-1
                  v1(j)=track1(i,j)
                  if (i.eq.Npoints1) then
                     v2(j)=track1(1,j)
                  else
                     v2(j)=track1(i+1,j)
                  endif         ! i=Np(ic)
               enddo            ! j
               v1(dim)=height_ref-deltaZ
               v2(dim)=height_ref-deltaZ
            else
c     positions v1 and v2 delimit segment index 'i' on track "contour(ic)"
               do j=1,dim-1
                  v2(j)=contour(ic,i,j)
                  if (i.eq.Np(ic)) then
                     v1(j)=contour(ic,1,j)
                  else
                     v1(j)=contour(ic,i+1,j)
                  endif         ! i=Np(ic)
               enddo            ! j
               v1(dim)=height_ref
               v2(dim)=height_ref
            endif               ! ic
            if (building_type.eq.1) then
c     positions v3 and v4 delimit segment index 'i' on track "contour_flat(ic)"
               do j=1,dim-1
                  v4(j)=contour_flat(ic,i,j)
                  if (i.eq.Np_flat(ic)) then
                     v3(j)=contour_flat(ic,1,j)
                  else
                     v3(j)=contour_flat(ic,i+1,j)
                  endif         ! i=Np_flat(ic)
               enddo            ! j
            else                ! building_type=2
               do j=1,dim-1
                  if (ic.eq.1) then
                     v4(j)=track2(i,j)
                     if (i.eq.Npoints2) then
                        v3(j)=track2(1,j)
                     else
                        v3(j)=track2(i+1,j)
                     endif      ! i=Npoints2
                  else
                     v3(j)=track2(i,j)
                     if (i.eq.Npoints2) then
                        v4(j)=track2(1,j)
                     else
                        v4(j)=track2(i+1,j)
                     endif      ! i=Npoints2
                  endif         ! ic
               enddo            ! j
            endif               ! building_type
            v3(dim)=height_ref+roof_height
            v4(dim)=height_ref+roof_height
c     add vertices and faces for slope index 'i'
            do j=1,dim
               v_tmp(Nv_tmp+1,j)=v1(j)
               v_tmp(Nv_tmp+2,j)=v2(j)
               v_tmp(Nv_tmp+3,j)=v3(j)
               v_tmp(Nv_tmp+4,j)=v4(j)
            enddo               ! j
            f_tmp(Nf_tmp+1,1)=Nv_tmp+1
            f_tmp(Nf_tmp+1,2)=Nv_tmp+2
            f_tmp(Nf_tmp+1,3)=Nv_tmp+3
            f_tmp(Nf_tmp+2,1)=Nv_tmp+1
            f_tmp(Nf_tmp+2,2)=Nv_tmp+3
            f_tmp(Nf_tmp+2,3)=Nv_tmp+4
            Nv_tmp=Nv_tmp+4
            Nf_tmp=Nf_tmp+2
         enddo                  ! i
      enddo                     ! ic
      center(1)=0.0D+0
      center(2)=0.0D+0
      center(3)=roof_thickness
      call move_obj(dim,Nv_tmp,v_tmp,center)
      call add_obj_to_obj(dim,Nv_tmp,Nf_tmp,v_tmp,f_tmp,
     &     Nv01,Nf01,v01,f01)

c     3- slope from the top of the building, internal
      Nv_tmp=0
      Nf_tmp=0
      do ic=1,Ncontour
         do i=1,Np(ic)
c     positions v1 and v2 delimit segment index 'i' on track "contour(ic)"
            do j=1,dim-1
               v2(j)=contour(ic,i,j)
               if (i.eq.Np(ic)) then
                  v1(j)=contour(ic,1,j)
               else
                  v1(j)=contour(ic,i+1,j)
               endif            ! i=Np(ic)
            enddo               ! j
            v1(dim)=height_ref
            v2(dim)=height_ref
            if (building_type.eq.1) then
c     positions v3 and v4 delimit segment index 'i' on track "contour_flat(ic)"
               do j=1,dim-1
                  v4(j)=contour_flat(ic,i,j)
                  if (i.eq.Np_flat(ic)) then
                     v3(j)=contour_flat(ic,1,j)
                  else
                     v3(j)=contour_flat(ic,i+1,j)
                  endif         ! i=Np_flat(ic)
               enddo            ! j
            else                ! building_type=2
               do j=1,dim-1
                  if (ic.eq.1) then
                     v4(j)=track2(i,j)
                     if (i.eq.Npoints2) then
                        v3(j)=track2(1,j)
                     else
                        v3(j)=track2(i+1,j)
                     endif      ! i=Npoints2
                  else
                     v3(j)=track2(i,j)
                     if (i.eq.Npoints2) then
                        v4(j)=track2(1,j)
                     else
                        v4(j)=track2(i+1,j)
                     endif      ! i=Npoints2
                  endif         ! ic
               enddo            ! j
            endif               ! building_type
            v3(dim)=height_ref+roof_height
            v4(dim)=height_ref+roof_height
c     add vertices and faces for slope index 'i'
            do j=1,dim
               v_tmp(Nv_tmp+1,j)=v1(j)
               v_tmp(Nv_tmp+2,j)=v2(j)
               v_tmp(Nv_tmp+3,j)=v3(j)
               v_tmp(Nv_tmp+4,j)=v4(j)
            enddo               ! j
            f_tmp(Nf_tmp+1,1)=Nv_tmp+1
            f_tmp(Nf_tmp+1,2)=Nv_tmp+2
            f_tmp(Nf_tmp+1,3)=Nv_tmp+3
            f_tmp(Nf_tmp+2,1)=Nv_tmp+1
            f_tmp(Nf_tmp+2,2)=Nv_tmp+3
            f_tmp(Nf_tmp+2,3)=Nv_tmp+4
            Nv_tmp=Nv_tmp+4
            Nf_tmp=Nf_tmp+2
         enddo                  ! i
      enddo                     ! ic
      call invert_normals(Nf_tmp,f_tmp)
      call add_obj_to_obj(dim,Nv_tmp,Nf_tmp,v_tmp,f_tmp,
     &     Nv02,Nf02,v02,f02)
      valid=.true.
      
c     compute volumes
c     + Volume of air in the attic
      call delaunay_triangulation(dim,
     &     Ncontour,Np,contour,
     &     Nv_top,Nf_top,v_top,f_top) ! @ altitude 0; normals are directed upward
      center(1)=0.0
      center(2)=0.0
      center(3)=height_ref
      call move_obj(dim,Nv_top,v_top,center)
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv02,Nf02,v02,f02,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      call add_obj_to_obj(dim,Nv_top,Nf_top,v_top,f_top,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vroof_air)
c     + Total volume
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      call invert_normals(Nf_enc,f_enc)
      call add_obj_to_obj(dim,Nv_top,Nf_top,v_top,f_top,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vtotal)
c     + Volume of solid of the roof
      Vroof_solid=Vtotal-Vroof_air
      if (Vroof_solid.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Vroof_solid=',Vroof_solid
         write(*,*) 'Vtotal=',Vtotal
         write(*,*) 'Vroof_air=',Vroof_air
         stop
      endif

 666  continue
      return
      end


      
      subroutine duplicate_ctr(dim,Np,ctr,Np2,ctr2)
      implicit none
      include 'max.inc'
c
c     Purpose: to duplicate a contour
c
c     Input:
c       + dim: dimension of space
c       + Np: number of points for the contour du duplicate
c       + ctr: contour tp duplicate
c     
c     Output:
c       + Np2: number of points for the duplicated contour
c       + ctr2: duplicated contour
c
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine duplicate_ctr'

      Np2=Np
      do i=1,Np2
         do j=1,dim-1
            ctr2(i,j)=ctr(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end
      


      subroutine identify_building_type(dim,
     &     Ncontour,Np,contour,
     &     building_type)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c     
c     Purpose: to identify the type of building from its contour
c
c     Input:
c       + dim: dimension of space
c       + Ncontour, Np, contour: external contours
c     
c     Output:
c       + building type: building type
c     building_type=1: residential house
c     building _type=2: group of touching buildings arranged in a closed contour
c     
c     I/O
      integer dim
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
c     temp
      double precision xmin,xmax,ymin,ymax
      double precision deltaX,deltaY
      integer building_type
      integer ic,i
c     label
      character*(Nchar_mx) label
      label='subroutine indentify_building_type'
      
      
c     boundary box for all contours
      xmin=contour(1,1,1)
      xmax=contour(1,1,1)
      ymin=contour(1,1,2)
      ymax=contour(1,1,2)
      do ic=1,Ncontour
         do i=1,Np(ic)
            if (contour(ic,i,1).lt.xmin) then
               xmin=contour(ic,i,1)
            endif
            if (contour(ic,i,1).gt.xmax) then
               xmax=contour(ic,i,1)
            endif
            if (contour(ic,i,2).lt.ymin) then
               ymin=contour(ic,i,2)
            endif
            if (contour(ic,i,2).gt.ymax) then
               ymax=contour(ic,i,2)
            endif
         enddo                  ! i
      enddo                     ! ic
      deltaX=xmax-xmin
      deltaY=ymax-ymin
c     try to identify the type of building
      if ((Ncontour.gt.1).and.
     &     ((deltaX.ge.residential_house_max_extension).or.
     &     (deltaY.ge.residential_house_max_extension))) then
         building_type=2
      else
         building_type=1
      endif

      return
      end


      
      subroutine segment_rotation_angle(dim,P1P2,theta)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to compute the horizontal roation angle for segment [P1P2]
c     
c     Input:
c       + dim: dimension of space
c       + P1P2: P1PZ vector
c     
c     Output:
c     + theta: horizontal rotation angle
c     
c     I/O
      integer dim
      double precision P1P2(1:Ndim_mx)
      double precision theta
c     temp
      double precision a
c     label
      character*(Nchar_mx) label
      label='subroutine segment_rotation_angle'

      if (P1P2(1).eq.0.0D+0) then
         a=pi/2.0D+0
      else
         a=dabs(datan(P1P2(2)/P1P2(1)))
      endif
      if ((P1P2(1).ge.0.0).and.(P1P2(2).ge.0.0D+0)) then
         theta=a
      else if ((P1P2(1).le.0.0).and.(P1P2(2).ge.0.0D+0)) then
         theta=pi-a
      else if ((P1P2(1).le.0.0).and.(P1P2(2).le.0.0D+0)) then
         theta=pi+a
      else if ((P1P2(1).ge.0.0).and.(P1P2(2).le.0.0D+0)) then
         theta=2.0*pi-a
      endif
      
      return
      end

      

      subroutine generate_contour_lateral_surface_whab(dim,
     &     Ncontour,Np,contour,height,wwf,e_ext,e_glass,
     &     Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate the vertical lateral surface for a group of contours
c     and position window holes and borders
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Np: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + height: height of the lateral surface [m]
c       + wwf: fraction of the walls used by windows
c       + e_ext: thickness of external solid boundaries [m]
c       + e_glass: thickness of a single glass panel [m]
c     
c     Output:
c       + Nv01, Nf01, v01, f01: object that defines the lateral surface
c     
c     I/O
      integer dim      
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      double precision height
      double precision wwf
      double precision e_ext
      double precision e_glass
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer ic,i,j
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision v3(1:Ndim_mx)
      double precision v4(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_contour_lateral_surface_whab'

      call generate_contour_lateral_surface_wh(dim,
     &     Ncontour,Np,contour,height,wwf,
     &     Nv01,Nf01,v01,f01)
      
      return
      end
      


      subroutine generate_contour_lateral_surface_wh(dim,
     &     Ncontour,Np,contour,height,wwf,
     &     Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate the vertical lateral surface for a group of contours
c     and position window holes
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Np: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + height: height of the lateral surface [m]
c       + wwf: fraction of the walls used by windows
c     
c     Output:
c       + Nv01, Nf01, v01, f01: object that defines the lateral surface
c     
c     I/O
      integer dim      
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      double precision height
      double precision wwf
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer ic,i,j
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision v3(1:Ndim_mx)
      double precision v4(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_contour_lateral_surface_wh'

      call generate_contour_lateral_surface(dim,
     &     Ncontour,Np,contour,height,
     &     Nv01,Nf01,v01,f01)
      
      return
      end



      subroutine generate_contour_lateral_surface(dim,
     &     Ncontour,Np,contour,height,
     &     Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate the vertical lateral surface for a group of contours
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Np: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + height: height of the lateral surface [m]
c     
c     Output:
c       + Nv01, Nf01, v01, f01: object that defines the lateral surface
c     
c     I/O
      integer dim      
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      double precision height
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer ic,i,j
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision v3(1:Ndim_mx)
      double precision v4(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_contour_lateral_surface'

      Nv01=0
      Nf01=0
      do ic=1,Ncontour
         do i=1,Np(ic)
            if (ic.eq.1) then
               do j=1,dim-1
                  v1(j)=contour(ic,i,j)
                  if (i.eq.Np(ic)) then
                     v2(j)=contour(ic,1,j)
                  else
                     v2(j)=contour(ic,i+1,j)
                  endif         ! i=Np(ic)
                  v3(j)=v2(j)
                  v4(j)=v1(j)
               enddo            ! j
            else
               do j=1,dim-1
                  v2(j)=contour(ic,i,j)
                  if (i.eq.Np(ic)) then
                     v1(j)=contour(ic,1,j)
                  else
                     v1(j)=contour(ic,i+1,j)
                  endif         ! i=Np(ic)
                  v3(j)=v2(j)
                  v4(j)=v1(j)
               enddo            ! j
            endif               ! ic
            v1(dim)=0.0D+0
            v2(dim)=0.0D+0
            v3(dim)=height
            v4(dim)=height
            if (Nv01+4.gt.Nv_so_mx) then
               call error(label)
               write(*,*) 'Nv_so_mx was reached'
               stop
            else
               do j=1,dim
                  v01(Nv01+1,j)=v1(j)
                  v01(Nv01+2,j)=v2(j)
                  v01(Nv01+3,j)=v3(j)
                  v01(Nv01+4,j)=v4(j)
               enddo            ! j
            endif               ! Nv01+4>Nv_so_mx
            if (Nf01+2.gt.Nf_so_mx) then
               call error(label)
               write(*,*) 'Nf_so_mx was reached'
               stop
            else
               f01(Nf01+1,1)=Nv01+1
               f01(Nf01+1,2)=Nv01+2
               f01(Nf01+1,3)=Nv01+3
               f01(Nf01+2,1)=Nv01+1
               f01(Nf01+2,2)=Nv01+3
               f01(Nf01+2,3)=Nv01+4
            endif               ! Nf01+2>Nf_so_mx
            Nv01=Nv01+4
            Nf01=Nf01+2
         enddo                  ! i
      enddo                     ! ic
      
      return
      end
      


      subroutine is_inside_contour(debug,dim,Ncontour,Nppc,contour,
     &     icontour,P,inside)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find whether or not a given position is inside a contour
c     
c     Input:
c       + debug: true for debug mode
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: contour for the existing list of contours
c       + icontour: index of the contour
c       + P: position to test
c     
c     Output:
c       + inside: true if "P" is inside contour index "icontour"
c     
c     I/O
      logical debug
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      double precision P(1:Ndim_mx)
      logical inside
c     temp
      integer Nsegments,isegment,Npoints,i1,i2
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision d,dmin
      integer isegment_dmin,iseg2
      double precision A(1:Ndim_mx)
      double precision AP(1:Ndim_mx)
      double precision nAP(1:Ndim_mx)
      double precision A_dmin(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision sp
      logical A_is_at_end,A_is_at_end_dmin
      integer end_side,end_side_dmin
c     label
      character*(Nchar_mx) label
      label='subroutine is_inside_contour'

c     Debug
      if (debug) then
         write(*,*) trim(label),' :in'
         write(*,*) 'icontour=',icontour
         write(*,*) 'P=',P
      endif                     ! debug
c     Debug
      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be <= Ncontour=',Ncontour
         stop
      endif

      call get_contour(Ncontour,Nppc,contour,icontour,
     &     Npoints,ctr)
c     find segment "P" is the closest from
      Nsegments=Npoints
c     Debug
      if (debug) then
         write(*,*) 'Nsegments=',Nsegments
      endif                     ! debug
c     Debug
      do isegment=1,Nsegments
         i1=isegment
         call get_position_from_ctr(dim,Npoints,ctr,i1,x1)
         if (isegment.eq.Nsegments) then
            i2=1
         else
            i2=isegment+1
         endif
         call get_position_from_ctr(dim,Npoints,ctr,i2,x2)
         call distance_to_vector(dim,x1,x2,P,d,A,A_is_at_end,end_side)
c     Debug
      if (debug) then
         write(*,*) 'isegment=',isegment
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'd=',d
      endif                     ! debug
c     Debug
         if (isegment.eq.1) then
            dmin=d
            isegment_dmin=1
            call copy_vector(dim,A,A_dmin)
            A_is_at_end_dmin=A_is_at_end
            end_side_dmin=end_side
         endif
         if (d.le.dmin) then
            dmin=d
            isegment_dmin=isegment
            call copy_vector(dim,A,A_dmin)
            A_is_at_end_dmin=A_is_at_end
            end_side_dmin=end_side
         endif
         if (d.le.0.0D+0) then  ! point P is on contour: inside=T
            inside=.true.
            goto 666
         endif
      enddo                     ! i
c     Debug
      if (debug) then
         write(*,*) 'isegment_dmin=',isegment_dmin
         write(*,*) 'dmin=',dmin
         write(*,*) 'A_dmin=',A_dmin
      endif                     ! debug
c     Debug
c     
      call normal_to_segment(dim,Ncontour,Nppc,contour,
     &     icontour,isegment_dmin,normal)
c     vector AP
      call substract_vectors(dim,P,A_dmin,AP)
c     
c     Debug
      if (debug) then
         write(*,*) 'A_is_at_end_dmin=',A_is_at_end_dmin
      endif
c     Debug
      if (A_is_at_end_dmin) then
         if (end_side_dmin.eq.1) then
            if (isegment_dmin.eq.1) then
               iseg2=Nsegments
            else
               iseg2=isegment_dmin-1
            endif
         else                   ! end_side_dmin=2
            if (isegment_dmin.eq.Nsegments) then
               iseg2=1
            else
               iseg2=isegment_dmin+1
            endif
         endif                  ! end_side_dmin=1 or 2
         call normal_to_segment(dim,Ncontour,Nppc,contour,
     &        icontour,iseg2,n2)
         call add_vectors(dim,normal,n2,tmp)
c     Debug
         if (debug) then
            write(*,*) 'iseg2=',iseg2
            write(*,*) 'n2=',n2
            write(*,*) 'normal=',normal
            write(*,*) 'tmp=',tmp
         endif
c     Debug
         call scalar_product(dim,AP,tmp,sp)
      else
c     then check "P" is on the side of the normal for that segment
c     normal to the current segment
c     scalar product between the normal and vector AP
         call normalize_vector(dim,AP,nAP)
         call scalar_product(dim,normal,nAP,sp)
c     Debug
         if (debug) then
            write(*,*) 'normal=',normal
            write(*,*) 'AP=',AP
            write(*,*) 'nAP=',nAP
         endif
c     Debug
      endif
c     Debug
      if (debug) then
         write(*,*) 'sp=',sp
      endif
c     Debug
      if (sp.ge.0.0D+0) then
         inside=.true.
      else
         inside=.false.
      endif

 666  continue
c     Debug
      if (debug) then
         write(*,*) 'inside=',inside
         write(*,*) trim(label),' :out'
      endif                     ! debug
c     Debug

      return
      end



      subroutine normal_to_segment(dim,Ncontour,Nppc,contour,
     &     icontour,isegment,normal)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the normal to a given segment of a given contour
c     that is directed toward the inside of the contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: list of contours
c       + icontour: index of the contour
c       + isegment: index of the segment
c     
c     Output:
c       + normal: normal to segment index "isegment" of contour index "icontour"
c     and directed toward the inside of the contour
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer isegment
      double precision normal(1:Ndim_mx)
c     temp
      integer Npoints,Nsegments
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i1,i2,j
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision norm
c     label
      character*(Nchar_mx) label
      label='subroutine normal_to_segment'

      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be <= Ncontour=',Ncontour
         stop
      endif

      call get_contour(Ncontour,Nppc,contour,icontour,
     &     Npoints,ctr)
      Nsegments=Npoints
      if (isegment.lt.1) then
         call error(label)
         write(*,*) 'isegment=',isegment
         write(*,*) 'should be > 0'
         stop
      endif
      if (isegment.gt.Nsegments) then
         call error(label)
         write(*,*) 'isegment=',isegment
         write(*,*) 'should be <= Nsegments=',Nsegments
         stop
      endif
      i1=isegment
      call get_position_from_ctr(dim,Npoints,ctr,i1,x1)
      if (isegment.eq.Nsegments) then
         i2=1
      else
         i2=isegment+1
      endif
      call get_position_from_ctr(dim,Npoints,ctr,i2,x2)
      call substract_vectors(dim,x2,x1,u)
      norm=dsqrt(u(1)**2.0D+0+u(2)**2.0D+0)
      if (norm.le.0.0D+0) then
         call error(label)
         write(*,*) 'norm=',norm
         write(*,*) 'u=',(u(j),j=1,dim-1)
         stop
      else
         normal(1)=-u(2)/norm
         normal(2)=u(1)/norm
         normal(3)=0.0D+0
      endif

      return
      end
