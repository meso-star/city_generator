c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine identify_Nriver(Nriver)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of rivers defined by the user
c
c     Input:
c
c     Output:
c       + Nriver: number of rivers
c
c     I/O
      integer Nriver
c     temp
      logical keep_looking,file_exists
      integer i
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine identify_Nriver'

      Nriver=0
      keep_looking=.true.
      do while (keep_looking)
         i=Nriver+1
         call num2str3(i,str3)
         filename='./data/river'//trim(str3)//'.in'
         inquire(file=trim(filename),exist=file_exists)
         if (file_exists) then
            Nriver=Nriver+1
         else
            keep_looking=.false.
         endif                  ! file_exists
      enddo                     ! while (keep_looking)

      return
      end



      subroutine read_river_data(dim,
     &     Nriver,iriver,width,
     &     Nppt,central_track,
     &     river_mat)
      implicit none
      include 'max.inc'
c
c     Purpose: to read input data file for rivers
c
c     Input:
c       + dim: dimension of space
c
c     Output:
c       + Nriver: number of rivers
c       + iriver: index of the river definition file to read
c       + width: width of each river [m]
c       + Nppt: number of points for the description of the central track for each river
c       + central_track: list of positions that describe the central track of each river
c       + river_mat: material for the river
c
c     I/O
      integer dim
      integer Nriver
      integer iriver
      double precision width
      integer Nppt
      double precision central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) river_mat
c     temp
      integer ios,j,k,nl
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine read_river_data'

      call num2str3(iriver,str3)
      filename='./data/river'//trim(str3)//'.in'
      open(16,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         call get_nlines(filename,nl)
         Nppt=nl-7
         if (Nppt.lt.2) then
            call error(label)
            write(*,*) 'Central track for river index: ',iriver
            write(*,*) 'is described using: ',Nppt,' points'
            write(*,*) 'which should be > 1'
            stop
         endif                  ! Nppt<2
         if (Nppt.gt.Nppt_mx) then
            call error(label)
            write(*,*) 'Central track for river index: ',iriver
            write(*,*) 'is described using: ',Nppt,' points'
            write(*,*) 'which is > Nppt_mx=',Nppt_mx
            stop
         endif                  ! Nppt>Nppt_mx
         do j=1,3
            read(16,*)
         enddo                  ! j
         read(16,*) river_mat
         read(16,*)
         read(16,*) width
         read(16,*)
         do j=1,Nppt
            read(16,*) (central_track(j,k),k=1,2)
         enddo                  ! j
c     Consistency checks
         if (width.lt.0) then
            call error(label)
            write(*,*) 'width(',iriver,')=',width
            write(*,*) '< 0'
            stop
         endif
c     End of tests
      endif                     !  file exists
      close(16)
      
      return
      end
