c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine generate_dat_for_all_materials(mtllib_file)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to generate the results/[material].dat file
c     for all known materials
c     
c     Input:
c       + mtllib_file: mtllib file
c     
c     Output:
c     
c     I/O
      character*(Nchar_mx) mtllib_file
c     temp
      integer Nmat,imat
      character*(Nchar_mx) mat_name(1:Nmaterial_mx)
      character*(Nchar_mx) folder_in
      integer Nfolders_out,Nfiles_out,ifile
      character*(Nchar_mx) folders_out(1:Nfolders_mx)
      character*(Nchar_mx) files_out(1:Nfiles_mx)
      integer idx,idx0
      logical idx0_found
      logical mtl_is_st
      character*(Nchar_mx) mtl_name
      character*(Nchar_mx) mtl_filename
c     label
      character*(Nchar_mx) label
      label='subroutine generate_dat_for_all_materials'

c     Debug
c      write(*,*) trim(label),' : in'
c     Debug
c     1. List all possible materials:
c     1.1 Initial database (S. Kotthaus)
      Nmat=Nmat_db
      if (Nmat.gt.Nmaterial_mx) then
         call error(label)
         write(*,*) 'Nmaterial_mx was reached'
         stop
      endif
      do imat=1,Nmat_db
         mat_name(imat)=mat_name_db(imat)
      enddo                     ! imat
c     1.2 Supplemental database (Mesostar)
      folder_in='./data/material_properties'
      call base_list(folder_in,
     &     Nfolders_out,folders_out,Nfiles_out,files_out)
      do ifile=1,Nfiles_out
         if (files_out(ifile)(len_trim(files_out(ifile))-10:
     &        len_trim(files_out(ifile))).eq.'.properties') then
            Nmat=Nmat+1
            if (Nmat.gt.Nmaterial_mx) then
               call error(label)
               write(*,*) 'Nmaterial_mx was reached'
               stop
            endif
c     Looking for a '/' in character string
            idx0_found=.false.
            do idx=len_trim(files_out(ifile))-11,1,-1
               if (files_out(ifile)(idx:idx).eq.'/') then
                  idx0=idx
                  idx0_found=.true.
                  goto 111
               endif
            enddo               ! idx
 111        continue
            if (.not.idx0_found) then
               call error(label)
               write(*,*) 'Could not find a single / in:'
               write(*,*) files_out(ifile)(1:
     &              len_trim(files_out(ifile))-11)
               stop
            else
c     "idx0" is the index of the last '/' character in files_out(ifile)
               mat_name(Nmat)=files_out(ifile)(idx0+1:
     &              len_trim(files_out(ifile))-11)
            endif               ! idx0_found=F
         endif                  ! files_out(ifile) is a .properties file
      enddo                     ! ifile
c     2. generate mtl files
      do imat=1,Nmat
         mtl_is_st=.false.
c     Debug
c         write(*,*) imat,trim(mat_name(imat))
c     Debug
         call mtl_file_name(mtl_is_st,mat_name(imat),mtllib_file,
     &        mtl_filename)
      enddo                     ! imat

c     Debug
c      write(*,*) trim(label),' : out'
c     Debug      
      return
      end


      
      subroutine mtl_file_name(is_st,mtl_name,mtllib_file,
     &     mtl_filename)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the mtl file corresponding
c     to input material / medium data
c     
c     Input:
c       + is_st: true if material is semi-transparent
c       + mtl_name: name of the material or medium
c       + mtllib_file: mtllib file
c     
c     Output:
c       + mtl_filename: name of the mtl file
c     
c     I/O
      logical is_st
      character*(Nchar_mx) mtl_name
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) mtl_filename
c     temp
      logical file_exists
c     label
      character*(Nchar_mx) label
      label='subroutine mtl_file_name'

      mtl_filename='./results/'//trim(mtl_name)//'.dat'
      inquire(file=trim(mtl_filename),exist=file_exists)
      if (.not.file_exists) then
         call generate_mtl_file(is_st,mtl_name,mtl_filename)
         call append_mtllib_file(mtllib_file,mtl_name)
      endif
      
      return
      end



      subroutine append_mtllib_file(mtllib_file,mat_name)
      implicit none
      include 'max.inc'
c     
c     Purpose: to append the mtllib file with the provided material
c     
c     Input:
c       + mtllib_file: mtllib file
c       + mat_name: name of the material
c     
c     I/O
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) mat_name
c     temp
      integer i
      double precision color_code(1:3)
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine append_mtllib_file'

      filename='./results/'//trim(mtllib_file)
c
      if ((trim(mat_name).ne.'air').and.
     &     (trim(mat_name).ne.'internal_air')) then
         call retrieve_code(mat_name,color_code)
      endif
c
c     Debug
c      write(*,*) 'filename: ',trim(filename)
c     Debug
      open(11,file=trim(filename),access='append')
      write(11,*) 'newmtl '//trim(mat_name)
      if ((trim(mat_name).eq.'air').or.
     &     (trim(mat_name).eq.'internal_air')) then
         write(11,*) 'Tr ',1.0D+0
      else if (trim(mat_name).eq.'glass') then
         write(11,*) 'Ka ',(color_code(i),i=1,3)
         write(11,*) 'Kd ',(color_code(i),i=1,3)
         write(11,*) 'Ks ',(color_code(i),i=1,3)
         write(11,*) 'Ns ',10.0D+0
      else
         write(11,*) 'Ka ',(color_code(i),i=1,3)
         write(11,*) 'Kd ',(color_code(i),i=1,3)
      endif
      write(11,*)
      close(11)
      
      return
      end


      
      subroutine generate_mtl_file(mtl_is_st,
     &     mtl_name,mtl_filename)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
      include 'formats.inc'
c     
c     Purpose: to generate a mtl file for a given material
c     
c     Input:
c       + mtl_is_st: true if material is semi-transparent
c       + mtl_name: name of the material or medium
c       + mtl_filename: name of the mtl file
c     
c     Output:
c       + the required mtl file in /results
c     
c     I/O
      logical mtl_is_st
      character*(Nchar_mx) mtl_name
      character*(Nchar_mx) mtl_filename
c     temp
      logical found,is_st,found_in_db
      integer index
      integer ilambda
      character*(Nchar_mx) aspect
      integer Nlambda,Nlambda_LW,Nlambda_SW
      double precision lambda(1:Nlambda_mx)
      double precision lambda_LW(1:Nlambda_mx)
      double precision lambda_SW(1:Nlambda_mx)
      double precision nref(1:Nlambda_mx)
      double precision ka(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision emissivity(1:Nlambda_mx)
      character*(Nchar_mx) brdf_type
      double precision lambda_SW_min,lambda_SW_max
      double precision lambda_LW_min,lambda_LW_max
      logical debug
c     label
      character*(Nchar_mx) label
      label='subroutine generate_mtl_file'

      lambda_SW_min=0.00D+0
      lambda_SW_max=0.99D+0
      lambda_LW_min=1.00D+0
      lambda_LW_max=1.00D+2

c     Debug
c      write(*,*) 'In:',trim(label),' mtl_name=',trim(mtl_name)
c      if (trim(mtl_name).eq.'glass') then
c         debug=.true.
c      else
c         debug=.false.
c      endif
c     Debug

c     Debug
c      if (debug) then
c         write(*,*) 'mtl_name=',trim(mtl_name)
c         write(*,*) 'mtl_is_st=',mtl_is_st
c         stop
c      endif
c     Debug

      open(14,file=trim(mtl_filename))
      call identify_name(mtl_is_st,mtl_name,found,index)
c     ---- special case of 'air' ----
      if ((.not.found).and.
     &     ((trim(mtl_name).eq.'air').or.
     &     (trim(mtl_name).eq.'internal_air'))) then
         goto 112
      endif
c     ---- special case of 'air' ----
c     ----------------------------------------------------------------
c     First attempt when material or medium was not found in the lists
c     of known materials or media:
c     try to identify (if a material) in the materials database (S. Kotthaus)
      if ((.not.found).and.(.not.is_st)) then
         call mat_db_lookup(mtl_name,found_in_db,index)
         if (found_in_db) then
            found=found_in_db
            call add_dbmat_to_material_list(mtl_name,index)
            index=Nmaterial_known
         endif                  ! found_in_db
      endif                     ! .not.found
c     ----------------------------------------------------------------
c     Second attempt when material or medium was not found in the lists
c     of known materials or media:
c     look for 'name' in the list of additionnal materials and media
      if (.not.found) then
         if (is_st) then
            call add_name_to_medium_list(mtl_name)
            index=Nmedium_known
         else
            call add_name_to_material_list(mtl_name)
            index=Nmaterial_known
         endif                  ! is_st
      endif                     ! .not.found
      if (is_st) then
         Nlambda=medium_Nlambda(index)
         if (Nlambda.eq.0) then
            nref(1)=medium_nref(index,1)
            ka(1)=medium_ka(index,1)
            ks(1)=medium_ks(index,1)
         else
            do ilambda=1,Nlambda
               lambda(ilambda)=medium_lambda(index,ilambda)
               nref(ilambda)=medium_nref(index,ilambda)
               ka(ilambda)=medium_ka(index,ilambda)
               ks(ilambda)=medium_ks(index,ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
      else                      ! is_st=F
         aspect=material_aspect(index)
         Nlambda_LW=material_Nlambda_LW(index)
         if (Nlambda_LW.eq.0) then
            emissivity(1)=material_emissivity(index,1)
         else
            do ilambda=1,Nlambda_LW
               lambda_LW(ilambda)=material_lambda_LW(index,ilambda)
               emissivity(ilambda)=material_emissivity(index,ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda_LW=0
         Nlambda_SW=material_Nlambda_SW(index)
         if (Nlambda_SW.eq.0) then
            reflectivity(1)=material_reflectivity(index,1)
         else
            do ilambda=1,Nlambda_SW
               lambda_SW(ilambda)=material_lambda_SW(index,ilambda)
               reflectivity(ilambda)=
     &              material_reflectivity(index,ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda_SW=0
c     Debug
c         if (debug) then
c            write(*,*) 'aspect=',aspect
c            write(*,*) 'Nlambda_LW=',Nlambda_LW
c            write(*,*) 'Nlambda_SW=',Nlambda_SW
c            do ilambda=1,Nlambda_SW
c               write(*,*) 'lambda_SW(',ilambda,')=',lambda_SW(ilambda)
c            enddo               ! ilambda
c         endif
c     Debug
      endif                     ! is_st
      if (mtl_is_st) then
         write(14,10) '# Refraction_index:'
         if (Nlambda.eq.0) then
            write(14,*) nref(1)
         else
            do ilambda=1,Nlambda
               write(14,*) lambda(ilambda),nref(ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
         write(14,10) '# Absorption_coefficient:'
         if (Nlambda.eq.0) then
            write(14,*) ka(1)
         else
            do ilambda=1,Nlambda
               write(14,*) lambda(ilambda),ka(ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
         write(14,10) '# Scattering_coefficient:'
         if (Nlambda.eq.0) then
            write(14,*) ks(1)
         else
            do ilambda=1,Nlambda
               write(14,*) lambda(ilambda),ks(ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
      else                      ! mtl_is_st=F
c     set brdf_type
         if (trim(aspect).eq.'matte') then
            brdf_type='lambertian'
         else if (trim(aspect).eq.'mirror') then
            brdf_type='specular'
         endif
c     record data
         if ((Nlambda_SW.eq.0).and.(Nlambda_LW.eq.0)) then
            write(14,23) 'bands',2
            write(14,*) lambda_SW_min,
     &           lambda_SW_max,
     &           trim(brdf_type),
     &           reflectivity(1)
            write(14,*) lambda_LW_min,
     &           lambda_LW_max,
     &           trim(brdf_type),
     &           1.0D+0-emissivity(1)
         else if ((Nlambda_SW.gt.0).and.(Nlambda_LW.gt.0)) then
c     count the number of relevant wavelengths
c     Debug
c            if (debug) then
c               write(*,*) 'lambda_SW_min=',lambda_SW_min
c               write(*,*) 'lambda_SW_max=',lambda_SW_max
c            endif
c     Debug
            Nlambda=0
            do ilambda=1,Nlambda_SW
               if ((lambda_SW(ilambda).ge.lambda_SW_min).and.
     &              (lambda_SW(ilambda).le.lambda_SW_max)) then
                  Nlambda=Nlambda+1
               endif
            enddo               ! ilambda
            do ilambda=1,Nlambda_LW
               if ((lambda_LW(ilambda).ge.lambda_LW_min).and.
     &              (lambda_LW(ilambda).le.lambda_LW_max)) then
                  Nlambda=Nlambda+1
               endif
            enddo               ! ilambda
c     record data
            write(14,23) 'wavelengths',Nlambda
            do ilambda=1,Nlambda_SW
               if ((lambda_SW(ilambda).ge.lambda_SW_min).and.
     &              (lambda_SW(ilambda).le.lambda_SW_max)) then
                  write(14,*) lambda_SW(ilambda)*1.0D+3,
     &                 trim(brdf_type),
     &                 reflectivity(ilambda)
               endif
            enddo               ! ilambda
            do ilambda=1,Nlambda_LW
               if ((lambda_LW(ilambda).ge.lambda_LW_min).and.
     &              (lambda_LW(ilambda).le.lambda_LW_max)) then
                  write(14,*) lambda_LW(ilambda)*1.0D+3,
     &                 trim(brdf_type),
     &                 1.0D+0-emissivity(ilambda)
               endif
            enddo               ! ilambda
         endif
      endif                     ! st
 112  continue
      close(14)

c     Debug
c      write(*,*) 'Out:',trim(label)
c      if (debug) then
c         stop
c      endif
c     Debug
      return
      end
      


      subroutine identify_name(is_st,name,found,index)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to identify a name in the list of material or the list of medium
c     
c     Input:
c       + is_st: true if semi-transparent
c       + name: name
c     
c     Output:
c       + found: true if name was found in the "material_name" list for is_st=F
c                and in the "medium_name" list for is_st=T
c       + index: index in the list for found=T
c
c     I/O
      logical is_st
      character*(Nchar_mx) name
      logical found
      integer index
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine identify_name'

      found=.false.
      if (is_st) then
         do i=1,Nmedium_known
            if (trim(name).eq.trim(medium_name(i))) then
               found=.true.
               index=i
               goto 111
            endif
         enddo                  ! i
      else                      ! is_st=F
         do i=1,Nmaterial_known
            if (trim(name).eq.trim(material_name(i))) then
               found=.true.
               index=i
               goto 111
            endif
         enddo                  ! i
      endif                     ! st
 111  continue

      return
      end



      subroutine mat_db_lookup(name,found,index)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to try to identify a material in the materials database (S. Kotthaus)
c     
c     Input:
c       + name: name of the material
c     
c     Output:
c       + found: true if 'name' was found in the "mat_name_db" list
c       + index: index in the list for found=T
c
c     I/O
      character*(Nchar_mx) name
      logical found
      integer index
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine mat_db_lookup'

      found=.false.
      do i=1,Nmat_db
         if (trim(name).eq.trim(mat_name_db(i))) then
            found=.true.
            index=i
            goto 111
         endif
      enddo                     ! i
 111  continue

      return
      end

      

      subroutine add_name_to_material_list(name)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to add a material to the material list
c     
c     Input:
c       + name: name of the material to add
c     
c     Output:
c       + updated values of "Nmaterial_known", "material_name", "material_Nlambda",
c         "material_reflectivity" and "material_emissivity"
c         (defined in "mtl.inc")
c      
c     I/O
      character*(Nchar_mx) name
c     temp
      character*(Nchar_mx) datafile
      logical file_exists
      character*(Nchar_mx) name_read,aspect_read
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision emissivity(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      integer ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine add_name_to_material_list'

      datafile='./data/material_properties/'//trim(name)//'.properties'
      inquire(file=trim(datafile),exist=file_exists)
      if (.not.file_exists) then
         call error(label)
         write(*,*) 'Material definition file not found for:'
         write(*,*) trim(name)
         stop
      else
         call read_material_properties(datafile,
     &        name_read,aspect_read,
     &        Nlambda,lambda,reflectivity,emissivity)
         if (trim(name).ne.trim(name_read)) then
            call error(label)
            write(*,*) 'In file: ',trim(datafile)
            write(*,*) 'material label is: ',trim(name_read)
            write(*,*) 'while looking for name= ',trim(name)
            stop
         endif
         Nmaterial_known=Nmaterial_known+1
         if (Nmaterial_known.gt.Nmaterial_mx) then
            call error(label)
            write(*,*) 'Nmaterial_mx was reached'
            stop
         endif
         material_name(Nmaterial_known)=trim(name_read)
         material_aspect(Nmaterial_known)=trim(aspect_read)
         material_Nlambda_LW(Nmaterial_known)=Nlambda
         material_Nlambda_SW(Nmaterial_known)=Nlambda
         if (Nlambda.eq.0) then
            material_reflectivity(Nmaterial_known,1)=reflectivity(1)
            material_emissivity(Nmaterial_known,1)=emissivity(1)
         else
            do ilambda=1,Nlambda
               material_lambda_LW(Nmaterial_known,ilambda)=
     &              lambda(ilambda)
               material_lambda_SW(Nmaterial_known,ilambda)=
     &              lambda(ilambda)
               material_reflectivity(Nmaterial_known,ilambda)=
     &              reflectivity(ilambda)
               material_emissivity(Nmaterial_known,ilambda)=
     &              emissivity(ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
      endif                     ! file_exists

      return
      end
      

      
      subroutine add_dbmat_to_material_list(name,index)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to add a database (S. Kotthaus) material to the material list
c     
c     Input:
c       + name: name of the material to add
c       + index: index of the material in the "mat_name_db" list
c     
c     Output:
c       + updated values of "Nmaterial_known", "material_name", "material_Nlambda",
c         "material_reflectivity" and "material_emissivity"
c         (defined in "mtl.inc")
c      
c     I/O
      character*(Nchar_mx) name
      integer index
c     temp
      character*(Nchar_mx) datafile
      logical file_exists
      character*(Nchar_mx) name_read,aspect_read
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision emissivity(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      integer ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine add_dbmat_to_material_list'

c     double-check "name" corresponds to the "mat_name_db(i)" element
      if (trim(name).ne.trim(mat_name_db(index))) then
         call error(label)
         write(*,*) 'name=',trim(name)
         write(*,*) 'should be identical to mat_name_db(',index,')=',
     &        trim(mat_name_db(index))
         stop
      else                      ! ok
         Nmaterial_known=Nmaterial_known+1
         if (Nmaterial_known.gt.Nmaterial_mx) then
            call error(label)
            write(*,*) 'Nmaterial_mx was reached'
            stop
         endif
         material_name(Nmaterial_known)=trim(mat_name_db(index))
         material_aspect(Nmaterial_known)='matte' ! all materials of the database are matte
         material_Nlambda_SW(Nmaterial_known)=Nlambda_db_SW
         if (Nlambda_db_SW.eq.0) then
            material_reflectivity(Nmaterial_known,1)=
     &           reflectivity_db(1,index)
         else
            do ilambda=1,Nlambda_db_SW
               material_lambda_SW(Nmaterial_known,ilambda)=
     &              lambda_db_SW(ilambda)
               material_reflectivity(Nmaterial_known,ilambda)=
     &              reflectivity_db(ilambda,index)
            enddo               ! ilambda
         endif
         material_Nlambda_LW(Nmaterial_known)=Nlambda_db_LW
         if (Nlambda_db_LW.eq.0) then
            material_emissivity(Nmaterial_known,1)=
     &           emissivity_db(1,index)
         else
            do ilambda=1,Nlambda_db_LW
               material_lambda_LW(Nmaterial_known,ilambda)=
     &              lambda_db_LW(ilambda)
               material_emissivity(Nmaterial_known,ilambda)=
     &              emissivity_db(ilambda,index)
            enddo               ! ilambda
         endif                  ! Nlambda=0
      endif                     ! file_exists

      return
      end



      subroutine add_name_to_medium_list(name)
      implicit none
      include 'max.inc'
      include 'mtl.inc'
c     
c     Purpose: to add a medium to the medium list
c     
c     Input:
c       + name: name of the medium to add
c     
c     Output:
c       + updated values of "Nmedium_known", "medium_name", "medium_Nlambda",
c         "medium_nref", "medium_ka" and "medium_ks"
c         (defined in "mtl.inc")
c      
c     I/O
      character*(Nchar_mx) name
c     temp
      character*(Nchar_mx) datafile
      logical file_exists
      character*(Nchar_mx) name_read
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision nref(1:Nlambda_mx)
      double precision ka(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
      integer ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine add_name_to_medium_list'

      datafile='./data/medium_properties/'//trim(name)//'.properties'
      inquire(file=trim(datafile),exist=file_exists)
      if (.not.file_exists) then
         call error(label)
         write(*,*) 'Medium definition file not found for:'
         write(*,*) trim(name)
         stop
      else
         call read_medium_properties(datafile,
     &        name_read,Nlambda,lambda,nref,ka,ks)
         if (trim(name).ne.trim(name_read)) then
            call error(label)
            write(*,*) 'In file: ',trim(datafile)
            write(*,*) 'medium label is: ',trim(name_read)
            write(*,*) 'while looking for name= ',trim(name)
            stop
         endif
         Nmedium_known=Nmedium_known+1
         if (Nmedium_known.gt.Nmedium_mx) then
            call error(label)
            write(*,*) 'Nmedium_mx was reached'
            stop
         endif
         medium_name(Nmedium_known)=trim(name_read)
         medium_Nlambda(Nmedium_known)=Nlambda
         if (Nlambda.eq.0) then
            medium_nref(Nmedium_known,1)=nref(1)
            medium_ka(Nmedium_known,1)=ka(1)
            medium_ks(Nmedium_known,1)=ks(1)
         else
            do ilambda=1,Nlambda
               medium_lambda(Nmedium_known,ilambda)=lambda(ilambda)
               medium_nref(Nmedium_known,ilambda)=nref(ilambda)
               medium_ka(Nmedium_known,ilambda)=ka(ilambda)
               medium_ks(Nmedium_known,ilambda)=ks(ilambda)
            enddo               ! ilambda
         endif                  ! Nlambda=0
      endif                     ! file_exists

      return
      end
