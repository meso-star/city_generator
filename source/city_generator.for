c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      program city_generator
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'parameters.inc'
      include 'mtl.inc'
c
c     Purpose: to generate a city
c
c     Variables
      integer dim
      character*(Nchar_mx) datafile
c     ------------------------------------------------------
c     Global data
c     ------------------------------------------------------
      logical draw_ground
      logical draw_buildings
      logical draw_roads
      logical draw_rivers
      logical draw_bridges
      logical draw_lakes
      logical draw_zones
      logical draw_trees
      logical draw_decorations
      logical draw_swimmingpools
      logical generate_individual_obj_files
c     ------------------------------------------------------
c     Ground
c     ------------------------------------------------------
      double precision xmin,xmax
      double precision ymin,ymax
      character*(Nchar_mx) ground_mat
c     ------------------------------------------------------
c     Buildings
c     ------------------------------------------------------
      integer Nbuilding
      integer type
      integer Nx,Ny,Nz
      double precision a(1:Ndim_mx)
      double precision e(1:2)
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision wwf
      double precision fhf
      double precision e_glass
      double precision roof_height
      double precision roof_thickness
      character*(Nchar_mx) internal_mat
      character*(Nchar_mx) external_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      integer Ncontour
      integer Nppt(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
c     ------------------------------------------------------
c     Roads
c     ------------------------------------------------------
      integer Nroad
      double precision width
      logical draw_wbands
      integer Np
      double precision central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wband_mat
c     ------------------------------------------------------
c     Rivers
c     ------------------------------------------------------
      integer Nriver
      character*(Nchar_mx) river_mat
c     ------------------------------------------------------
c     Bridges
c     ------------------------------------------------------
      integer Nbridge
      double precision length
      double precision deck_width
      double precision side_width
      double precision deck_height
      double precision side_height
      character*(Nchar_mx) deck_mat
      character*(Nchar_mx) side_mat
c     ------------------------------------------------------
c     Lakes
c     ------------------------------------------------------
      integer Nlake
      double precision scale
      character*(Nchar_mx) lake_mat
c     ------------------------------------------------------
c     Zones
c     ------------------------------------------------------
      integer Nzone
      character*(Nchar_mx) zone_mat
      double precision zone_thickness
c     ------------------------------------------------------
c     Swimming pools
c     ------------------------------------------------------
      integer Nswimmingpool
      character*(Nchar_mx) edge_mat
      double precision edge_width
      double precision edge_thickness
c     ------------------------------------------------------
c     temp
c     ------------------------------------------------------
      integer Nobj,ibuilding,iroad,iriver,ibridge,ilake,izone,itree,isp
      integer icontour,ipoint,ilambda,imat,ic,ip
      integer Nv,Nf,i,j,k
      double precision ground_position(1:Ndim_mx-1)
      double precision ground_angle
      double precision track(1:Nppt_mx,1:2)
      integer external_wall_color(1:3)
      logical err
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      character*(Nchar_mx) file0
      character*(Nchar_mx) file1
      character*(Nchar_mx) file2
      character*(Nchar_mx) command
      integer Npoints(1:Ncontour_mx)
      double precision points(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx)
      double precision Abuilding(0:4)
      double precision Aglass(0:4)
      double precision Ab,Ag
      double precision Vroof(0:4,1:2)
      double precision Vroof_solid,Vroof_air
      character*(Nchar_mx) seed_file
      integer seed
c     status
      integer len,ndone,Ntodo
      logical cerr
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      character*(Nchar_mx) filename
      logical debug
c     ------------------------------------------------------
c     label
c     ------------------------------------------------------
      character*(Nchar_mx) label
      label='program city_generator'

c     space dimension
      dim=3

c     reading Kotthaus material database
      datafile='./data/LUMA_SLUM_IR.csv'
      call read_material_spectral_data(datafile,
     &     Nmat_db,mat_name_db,Nlambda_db_LW,lambda_db_LW,
     &     emissivity_db)
      datafile='./data/LUMA_SLUM_SW.csv'
      call read_material_spectral_data(datafile,
     &     Nmat_db,mat_name_db,Nlambda_db_SW,lambda_db_SW,
     &     reflectivity_db)
c     conversions
      do ilambda=1,Nlambda_db_SW
         lambda_db_SW(ilambda)=lambda_db_SW(ilambda)/1.0D+3 ! nm -> micrometers
      enddo                     ! ilambda
      do imat=1,Nmat_db
         do ilambda=1,Nlambda_db_SW
            reflectivity_db(ilambda,imat)=
     &           reflectivity_db(ilambda,imat)/1.0D+2 ! 100 -> 1
         enddo                  ! ilambda
      enddo                     ! imat

c     read global settings
      datafile='./data/global_settings.in'
      call read_global_data(datafile,
     &     draw_ground,
     &     draw_buildings,
     &     draw_roads,
     &     draw_rivers,
     &     draw_bridges,
     &     draw_lakes,
     &     draw_zones,
     &     draw_trees,
     &     draw_decorations,
     &     draw_swimmingpools)
      
c     read RGB codes for various materials/media
      datafile='./data/rgb_color_codes.in'
      call read_rgb_codes(datafile)

c     more initialization
      Nobj=0                    ! total number of objects in the scene
      Nv=0                      ! total number of vertices in the scene
      Nf=0                      ! total number of faces in the scene
      obj_file='./results/city.obj' ! OBJ file for recording the scene
      mtllib_file='materials.mtl'   ! mtllib file
      off_file='./results/city.off' ! OFF file for recording the scene
c      generate_individual_obj_files=.false. ! has to be set to .true. if individual object files should be generated
      generate_individual_obj_files=.true. ! has to be set to .true. if individual object files should be generated

c     Initialization of the random number generator (needed by 'point_in_contour2'
      seed_file='./configuration/seed'
      call read_seed(seed_file,seed)
      call initialize_random_generator(seed)
      
c     perform initialization tasks (remove existing output files and initiate obj file)
      call init(obj_file,mtllib_file)

c     Generate results .dat file for all known materials
      call generate_dat_for_all_materials(mtllib_file)
  
      len=6
      call num2str(len,str,cerr)
      if (.not.cerr) then
         fmt0='(a,i'//trim(str)//',a)'
         fmt='(i'//trim(str)//',a)'
      endif

c     ----------------------------------------------------------------
c     Generate ground
c     ----------------------------------------------------------------
      if (draw_ground) then
         write(*,*) '+ Drawing ground...'
         datafile='./data/ground.in'
         call read_ground_data(datafile,
     &        xmin,xmax,ymin,ymax,ground_mat)
         ground_position(1)=0.0D+0
         ground_position(2)=0.0D+0
         ground_angle=0.0D+0
         call ground(dim,xmin,xmax,xmin,xmax,
     &        ground_mat,
     &        ground_position,ground_angle,
     &        generate_individual_obj_files,
     &        obj_file,mtllib_file,off_file,
     &        Nobj,Nv,Nf)
      endif                     ! draw_ground
      
c     ----------------------------------------------------------------
c     Generate buildings
c     ----------------------------------------------------------------
      do i=0,4
         Abuilding(i)=0.0D+0
         Aglass(i)=0.0D+0
         do j=1,2
            Vroof(i,j)=0.0D+0
         enddo                  ! j
      enddo                     ! i
      if (draw_buildings) then
         call identify_Nbuilding(Nbuilding)
         if (Nbuilding.gt.0) then
            write(*,*) '+ Drawing buildings...'
            fdone0=0
            ndone=0
            pifdone=0
            Ntodo=Nbuilding
            write(*,trim(fmt0),advance='no')
     &           'Done:   ',floor(fdone0),' %'
            do ibuilding=1,Nbuilding
c     Debug
c               write(*,*)
c               write(*,*) 'ibuilding=',ibuilding,' /',Nbuilding
c     Debug
               call read_building_data(dim,
     &              Nbuilding,ibuilding,
     &              type,Nx,Ny,Nz,a,e,position,angle,
     &              wwf,fhf,e_glass,roof_height,roof_thickness,
     &              internal_mat,external_mat,glass_panes_mat,roof_mat,
     &              Ncontour,Nppt,contour)
c     Debug
c               write(*,*) 'type=',type
c     Debug
               if (type.eq.0) then
                  call building00(dim,
     &                 Nx,Ny,Nz,
     &                 a(1),a(2),a(3),
     &                 e(1),e(2),
     &                 wwf,e_glass,
     &                 internal_mat,external_mat,glass_panes_mat,
     &                 position,angle,
     &                 generate_individual_obj_files,
     &                 obj_file,mtllib_file,off_file,
     &                 Nobj,Nv,Nf,Ab,Ag,Vroof_solid,Vroof_air)
               else if (type.eq.1) then
                  call building01(dim,
     &                 Nx,Ny,Nz,
     &                 a(1),a(2),a(3),
     &                 e(1),e(2),
     &                 wwf,e_glass,roof_height,roof_thickness,
     &                 internal_mat,external_mat,
     &                 glass_panes_mat,roof_mat,
     &                 position,angle,
     &                 generate_individual_obj_files,
     &                 obj_file,mtllib_file,off_file,
     &                 Nobj,Nv,Nf,Ab,Ag,Vroof_solid,Vroof_air)
               else if (type.eq.2) then
                  call building02(dim,
     &                 Nx,Ny,Nz,
     &                 a(1),a(2),a(3),
     &                 e(1),e(2),
     &                 wwf,e_glass,roof_height,roof_thickness,
     &                 internal_mat,external_mat,
     &                 glass_panes_mat,roof_mat,
     &                 position,angle,
     &                 generate_individual_obj_files,
     &                 obj_file,mtllib_file,off_file,
     &                 Nobj,Nv,Nf,Ab,Ag,Vroof_solid,Vroof_air)
               else if (type.eq.3) then
                  call building03(dim,
     &                 Nx,Ny,Nz,
     &                 a(1),a(2),a(3),
     &                 e(1),e(2),
     &                 wwf,e_glass,roof_height,roof_thickness,
     &                 internal_mat,external_mat,
     &                 glass_panes_mat,roof_mat,
     &                 position,angle,
     &                 generate_individual_obj_files,
     &                 obj_file,mtllib_file,off_file,
     &                 Nobj,Nv,Nf,Ab,Ag,Vroof_solid,Vroof_air)
               else if (type.eq.4) then
                  call building04(dim,.false.,
     &                 xmin,xmax,ymin,ymax,
     &                 Nz,a(3),
     &                 e(1),e(2),position,angle,
     &                 wwf,fhf,e_glass,roof_height,roof_thickness,
     &                 internal_mat,external_mat,
     &                 glass_panes_mat,roof_mat,
     &                 Ncontour,Nppt,contour,
     &                 generate_individual_obj_files,
     &                 obj_file,mtllib_file,off_file,
     &                 Nobj,Nv,Nf,Ab,Ag,Vroof_solid,Vroof_air)
               else             ! type is not defined
                  write(*,*) 'Building index:',ibuilding,
     &                 ' type=',type
                  write(*,*) 'has not been implemented yet'
               endif            ! type
c     areas are classified by building type
               Abuilding(type)=Abuilding(type)+Ab
               Aglass(type)=Aglass(type)+Ag
c     volumes are classified by building type
               Vroof(type,1)=Vroof(type,1)+Vroof_solid
               Vroof(type,2)=Vroof(type,2)+Vroof_air
c     print status
               ndone=ndone+1
               fdone=dble(ndone)/dble(Ntodo)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no')
     &                 floor(fdone),' %'
                  pifdone=ifdone
               endif            ! ifdone > pifdone
c     
            enddo               ! ibuilding
            write(*,*)
         endif                  ! Nbuilding > 0
      endif                     ! draw_buildings

c     ----------------------------------------------------------------
c     Generate roads
c     ----------------------------------------------------------------
      if (draw_roads) then
         call identify_Nroad(Nroad)
         if (Nroad.gt.0) then
            write(*,*) '+ Drawing roads...'
            fdone0=0
            ndone=0
            pifdone=0
            Ntodo=Nroad
            write(*,trim(fmt0),advance='no')
     &           'Done:   ',floor(fdone0),' %'
            do iroad=1,Nroad
               call read_road_data(dim,
     &              Nroad,iroad,width,draw_wbands,
     &              Np,central_track,
     &              road_mat,wband_mat)
               call road(dim,iroad,
     &              Nroad,width,draw_wbands,
     &              Np,central_track,
     &              road_mat,wband_mat,
     &              draw_ground,xmin,xmax,ymin,ymax,
     &              generate_individual_obj_files,
     &              obj_file,mtllib_file,off_file,
     &              Nobj,Nv,Nf)
c     print status
               ndone=ndone+1
               fdone=dble(ndone)/dble(Ntodo)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no')
     &                 floor(fdone),' %'
                  pifdone=ifdone
               endif            ! ifdone > pifdone
c     
            enddo               ! iroad
            write(*,*)
         endif                  ! Nroad > 0
      endif                     ! draw_roads

c     ----------------------------------------------------------------
c     Generate rivers
c     ----------------------------------------------------------------
      if (draw_rivers) then
         call identify_Nriver(Nriver)
         if (Nriver.gt.0) then
            write(*,*) '+ Drawing rivers...'
            do iriver=1,Nriver
               call read_river_data(dim,
     &              Nriver,iriver,width,
     &              Np,central_track,
     &              river_mat)
               call river(dim,
     &              width,Np,central_track,river_mat,
     &              draw_ground,xmin,xmax,ymin,ymax,
     &              generate_individual_obj_files,
     &              obj_file,mtllib_file,off_file,
     &              Nobj,Nv,Nf)
            enddo               ! iriver
         endif                  ! Nriver > 0
      endif                     ! draw_rivers

c     ----------------------------------------------------------------
c     Generate bridges
c     ----------------------------------------------------------------
      if (draw_bridges) then
         call identify_Nbridge(Nbridge)
         if (Nbridge.gt.0) then
            write(*,*) '+ Drawing bridges...'
            do ibridge=1,Nbridge
               call read_bridge_data(dim,
     &              Nbridge,ibridge,position,angle,length,
     &              deck_width,side_width,
     &              deck_height,side_height,
     &              deck_mat,side_mat,
     &              draw_wbands,wband_mat)
               call bridge(dim,
     &              position,angle,length,
     &              deck_width,side_width,
     &              deck_height,side_height,
     &              deck_mat,side_mat,
     &              draw_wbands,wband_mat,
     &              generate_individual_obj_files,
     &              obj_file,mtllib_file,off_file,
     &              Nobj,Nv,Nf)
            enddo               ! ibridge
         endif                  ! Nbridge > 0
      endif                     ! draw_bridges

c     ----------------------------------------------------------------
c     Generate lakes
c     ----------------------------------------------------------------
      if (draw_lakes) then
         call identify_Nlake(Nlake)
         if (Nlake.gt.0) then
            write(*,*) '+ Drawing lakes...'
            do ilake=1,Nlake
               call read_lake_data(dim,
     &              Nlake,ilake,position,scale,lake_mat,
     &              Ncontour,Nppt,contour)
               call lake(dim,
     &              Ncontour,Nppt,contour,
     &              position,scale,lake_mat,
     &              generate_individual_obj_files,
     &              obj_file,mtllib_file,off_file,
     &              Nobj,Nv,Nf)
            enddo               ! ilake
         endif                  ! Nlake > 0
      endif                     ! draw_lakes

c     ----------------------------------------------------------------
c     Generate zones
c     ----------------------------------------------------------------
      if (draw_zones) then
         call identify_Nzone(Nzone)
         if (Nzone.gt.0) then
            write(*,*) '+ Drawing zones...'
            fdone0=0
            ndone=0
            pifdone=0
            Ntodo=Nzone
            write(*,trim(fmt0),advance='no')
     &           'Done:   ',floor(fdone0),' %'
            do izone=1,Nzone
               call read_zone_data(dim,
     &              Nzone,izone,zone_mat,zone_thickness,
     &              Ncontour,Nppt,contour)
               call zone(dim,zone_thickness,zone_mat,
     &              Ncontour,Nppt,contour,
     &              generate_individual_obj_files,
     &              obj_file,mtllib_file,off_file,
     &              Nobj,Nv,Nf)
c     print status
               ndone=ndone+1
               fdone=dble(ndone)/dble(Ntodo)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no')
     &                 floor(fdone),' %'
                  pifdone=ifdone
               endif            ! ifdone > pifdone
c     
            enddo               ! izone
            write(*,*)
         endif                  ! Nzone > 0
      endif                     ! draw_zones

c     ----------------------------------------------------------------
c     Generate trees
c     ----------------------------------------------------------------
      if (draw_trees) then
         write(*,*) '+ Drawing trees...'
         datafile='./data/vegetation.in'
         call tree(dim,datafile,
     &        generate_individual_obj_files,
     &        obj_file,mtllib_file,off_file,
     &        Nobj,Nv,Nf)
      endif                     ! draw_trees

c     ----------------------------------------------------------------
c     Generate decorations
c     ----------------------------------------------------------------
      if (draw_decorations) then
         write(*,*) '+ Drawing decorations...'
         datafile='./data/decoration.in'
         call decoration(dim,datafile,
     &        generate_individual_obj_files,
     &        obj_file,mtllib_file,off_file,
     &        Nobj,Nv,Nf)
      endif                     ! draw_decorations
      
c     ----------------------------------------------------------------
c     Generate swimmingpools
c     ----------------------------------------------------------------
      if (draw_swimmingpools) then
         call identify_Nswimmingpool(Nswimmingpool)
         if (Nswimmingpool.gt.0) then
            write(*,*) '+ Drawing swimming pools...'
            fdone0=0
            ndone=0
            pifdone=0
            Ntodo=Nswimmingpool
            write(*,trim(fmt0),advance='no')
     &           'Done:   ',floor(fdone0),' %'
            do isp=1,Nswimmingpool
               call read_swimmingpool_data(dim,
     &              Nswimmingpool,isp,
     &              edge_mat,edge_width,edge_thickness,
     &              Ncontour,Nppt,contour)
               call sp(dim,edge_thickness-2.0D-2,
     &              Ncontour,Nppt,contour,
     &              edge_mat,edge_width,edge_thickness,
     &              generate_individual_obj_files,
     &              obj_file,mtllib_file,off_file,
     &              Nobj,Nv,Nf)
c     print status
               ndone=ndone+1
               fdone=dble(ndone)/dble(Ntodo)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no')
     &                 floor(fdone),' %'
                  pifdone=ifdone
               endif            ! ifdone > pifdone
c     
            enddo               ! isp
            write(*,*)
         endif                  ! Nswimmingpool > 0
      endif                     ! draw_trees
c     record areas
      filename='./results/areas.txt'
      call record_area_file(filename,dim,Abuilding,Aglass)
      filename='./results/volumes.txt'
      call record_volume_file(filename,dim,Vroof)
c     If all went well, print some statistics
      write(*,*) 'Total number of objects:',Nobj
      write(*,*) 'Total number of vertices:',Nv
      write(*,*) 'Total number of faces:',Nf

c     remove temporary files
      command='rm -f ./nlines*'
      call exec(command)

      end
