c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine building04(dim,debug,
     &     xmin,xmax,ymin,ymax,
     &     Nz,az,e_in,e_ext,position,angle,wwf,fhf,e_glass,
     &     roof_height,roof_thickness,
     &     internal_mat,external_mat,glass_panes_mat,roof_mat,
     &     Ncontour,Nppt,contour,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf,Abuilding,Aglass,Vroof_solid,Vroof_air)
      implicit none
      include 'max.inc'
c
c     Purpose: to produce a type 4 building: the building is defined by its contour,
c     a number of floors, and has a roof.
c
c     Input:
c       + dim: dimension of space
c       + xmin: lowest value of X map extension
c       + xmax: highest value of X map extension
c       + ymin: lowest value of Y map extension
c       + ymax: highest value of Y map extension
c       + Nz: number of floors
c       + az: floor height (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + position: (x,y) shift of the building (m)
c       + angle: rotation angle (deg)
c       + wwf: fraction of the wall used for windows
c       + fhf: fraction of the floor height used by windows
c       + e_glass: thickness of a single glass panel (m)
c       + roof_height: height of the roof (m)
c       + roof_thickness: thickness of the roof (m)
c       + internal_mat: name of the material for internal walls
c       + external_mat: name of the material for external walls
c       + glass_panes_mat: name of the material for glass_panes
c       + roof_mat: name of the material for the roof
c       + Ncontour: number of contours for the building
c       + Nppt: number of points that define each contour of the current building
c       + contour: list of coordinates for each point that define each contour of the current building
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c       + Abuilding: total lateral area of the building [m²]
c       + Aglass: glass are of the building [m²]
c       + Vroof_solid: volume of solid for the roof [m³]
c       + Vroof_air: volume of internal air in the attic [m³]
c
c     Input
      integer dim
      logical debug
      double precision xmin,xmax,ymin,ymax
      integer Nz
      double precision az
      double precision e_in,e_ext
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision wwf
      double precision fhf
      double precision e_glass
      double precision roof_height
      double precision roof_thickness
      character*(Nchar_mx) internal_mat
      character*(Nchar_mx) external_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      integer Ncontour
      integer Nppt(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      logical generate_individual_obj_files
      integer Nobj,Nv,Nf
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      double precision Abuilding
      double precision Aglass
      double precision Vroof_solid
      double precision Vroof_air
c     temp
      logical dbg
      logical invert_normal
      integer iobj,i,j,ifloor,ic
      double precision center(1:Ndim_mx)
      logical valid
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      double precision axe(1:Ndim_mx)
      double precision axe_x(1:Ndim_mx)
      double precision axe_y(1:Ndim_mx)
      double precision axe_z(1:Ndim_mx)
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) back_name
      integer Ncontour2
      integer Nppt2(1:Ncontour_mx)
      double precision contour2(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
      logical inside
      integer new_Npoints
      double precision new_track(1:Nppt_mx,1:2)
      integer Nv1,Nf1
      double precision v1(1:Nv_so_mx,1:Ndim_mx)
      integer f1(1:Nf_so_mx,1:3)
      integer Nv2,Nf2
      double precision v2(1:Nv_so_mx,1:Ndim_mx)
      integer f2(1:Nf_so_mx,1:3)
      integer Nv3,Nf3
      double precision v3(1:Nv_so_mx,1:Ndim_mx)
      integer f3(1:Nf_so_mx,1:3)
      integer Nv4,Nf4
      double precision v4(1:Nv_so_mx,1:Ndim_mx)
      integer f4(1:Nf_so_mx,1:3)
      integer Nv5,Nf5
      double precision v5(1:Nv_so_mx,1:Ndim_mx)
      integer f5(1:Nf_so_mx,1:3)
      integer Ncontour_tmp
      integer Nppc_tmp(1:Ncontour_mx)
      double precision contour_tmp(1:Ncontour_mx,1:Nppt_mx,1:2)
      logical keep_trying,success
      double precision width
      integer Niter
      character*(Nchar_mx) filename
      double precision width_min,width_max
      logical try_best_fit,clockwise
      double precision length
      double precision contours_length
      double precision building_height
      integer iface
      double precision face_area
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      logical best_fit
      double precision wall_width(1:Ncontour_mx)
      logical draw_windows
c     Debug
      integer Ncontour0
      integer Np0(1:Ncontour_mx)
      double precision contour0(1:Ncontour_mx,1:Nppt_mx,1:2)
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine building04'

c     Debug
c$$$      write(*,*) 'Ncontour=',Ncontour
c$$$      do ic=1,Ncontour
c$$$         write(*,*) 'Nppt(',ic,')=',Nppt(ic)
c$$$      enddo                     ! ic
c$$$      filename='./gnuplot/contour.dat'
c$$$      call record_gnuplot_contour(filename,dim,
c$$$     &     Ncontour,Nppt,contour)
c     Debug
      
      iobj=0                    ! total number of individual surfaces groups for the building
      axe_x(1)=1.0D+0
      axe_x(2)=0.0D+0
      axe_x(3)=0.0D+0
      axe_y(1)=0.0D+0
      axe_y(2)=1.0D+0
      axe_y(3)=0.0D+0
      axe_z(1)=0.0D+0
      axe_z(2)=0.0D+0
      axe_z(3)=1.0D+0
      
      draw_windows=.true.
c     Global option for drawing windows:
c     - can be based on a geometrical criterion
      if (wwf.le.0.0D+0) then
         draw_windows=.false.
      endif
c     - can be manually set:
c      draw_windows=.false.
c     
c     This global option has the following behaviour:
c      - when set to T, each individual window must be drawn, unless specified otherwise by a local event
c      - when set to F, each individual window will not be drawn
c      
c     ---------------------------------------------------------------------------
c     Preliminary tasks
      Ncontour2=0
      contours_length=0.0D+0
      do ic=1,Ncontour
         call get_contour(Ncontour,Nppt,contour,ic,Npoints,track)
c     This generates a track, with number of points Npoints=Nppt(ic)
c     length of the contour
         call contour_length(dim,Npoints,track,length)
         contours_length=contours_length+length
c     generate close contour
         if (ic.eq.1) then
            inside=.true.
         else
            inside=.false.
         endif
         width_min=e_glass*1.01D+0
         width_max=e_ext
         if (inside) then
            try_best_fit=.true.
         else
            try_best_fit=.true.
         endif
c     Debug
c$$$         write(*,*) 'ok0, ic=',ic
c$$$         if (ic.eq.2) then
c$$$            Ncontour0=1
c$$$            Np0(1)=Npoints
c$$$            do i=1,Np0(1)
c$$$               do j=1,dim-1
c$$$                  contour0(1,i,j)=track(i,j)
c$$$               enddo            ! j
c$$$            enddo               ! i
c$$$            filename='./gnuplot/contour02.dat'
c$$$            call record_gnuplot_contour(filename,dim,
c$$$     &           Ncontour0,Np0,contour0)
c$$$            dbg=.true.
c$$$         else
c$$$            dbg=.false.
c$$$         endif                  ! ic=5
c     Debug
         dbg=.false.
         call generate_close_contour2(dim,dbg,Npoints,track,
     &        width_min,width_max,inside,try_best_fit,success,
     &        best_fit,width,new_Npoints,new_track)
c     Debug
c         write(*,*) 'ok1, ic=',ic,' success=',success
c     Debug
         if (.not.success) then
            goto 666
         endif
c     set the width of the wall for each contour
         if ((try_best_fit).and.(best_fit).and.(width.ne.width_max))
     &        then
            wall_width(ic)=width
         else
            wall_width(ic)=width_max
         endif
c     test against map limits
         do i=1,new_Npoints
            if (new_track(i,1).lt.xmin) then
               call error(label)
               write(*,*) 'new_track(',i,',1)=',new_track(i,1)
               write(*,*) '< xmin=',xmin
               stop
            endif
            if (new_track(i,1).gt.xmax) then
               call error(label)
               write(*,*) 'new_track(',i,',1)=',new_track(i,1)
               write(*,*) '> xmax=',xmax
               stop
            endif
            if (new_track(i,2).lt.ymin) then
               call error(label)
               write(*,*) 'new_track(',i,',2)=',new_track(i,2)
               write(*,*) '< ymin=',ymin
               stop
            endif
            if (new_track(i,2).gt.ymax) then
               call error(label)
               write(*,*) 'new_track(',i,',2)=',new_track(i,2)
               write(*,*) '> ymax=',ymax
               stop
            endif
         enddo                  ! i
         call add_contour(Ncontour2,Nppt2,contour2,
     &        new_Npoints,new_track)
      enddo                     ! ic
      call generate_walls_from_contour(dim,
     &     Ncontour,Nppt,contour,
     &     Ncontour2,Nppt2,contour2,
     &     Nz,az,wwf,fhf,wall_width,e_ext,e_in,e_glass,
     &     draw_windows,
     &     Nv1,Nf1,v1,f1,       ! ext. air / ext. material
     &     Nv2,Nf2,v2,f2,       ! int. air / int. material
     &     Nv3,Nf3,v3,f3,       ! wall /glass
     &     Nv4,Nf4,v4,f4,       ! ext. air / glass
     &     Nv5,Nf5,v5,f5)       ! int. air / glass
c     Debug
c      write(*,*) 'Nf1=',Nf1
c      write(*,*) 'Nf2=',Nf2
c      write(*,*) 'Nf3=',Nf3
c      write(*,*) 'Nf4=',Nf4
c      write(*,*) 'Nf5=',Nf5
c     Debug
c     compute total lateral area of the building
      building_height=Nz*az+(Nz-1)*e_in+2.0D+0*e_ext
      Abuilding=contours_length*building_height
c     compute glass area of the building
      Aglass=0.0D+0
      do iface=1,Nf4
         do j=1,dim
            x1(j)=v4(f4(iface,1),j)
            x2(j)=v4(f4(iface,2),j)
            x3(j)=v4(f4(iface,3),j)
         enddo                  ! j
         call triangle_area(dim,x1,x2,x3,face_area)
         Aglass=Aglass+face_area
      enddo                     ! iface
      Aglass=Nz*Aglass          ! the same object is repeated on each floor !
c     ---------------------------------------------------------------------------
      
c     ---------------------------------------------------------------------------
c     OUTER SURFACES
c     ---------------------------------------------------------------------------
c     
c     ---------------------------------------------------------------------------
c     Base of the building
c     create the base: interface between the ground and the building
      call delaunay_triangulation(dim,Ncontour,Nppt,contour,
     &     Nv01,Nf01,v01,f01)   ! @ altitude 0; normals are directed upward
      call duplicate_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)   ! save it for later (top of the building)
      iobj=iobj+1
      invert_normal=.true.
      front_is_st=.false.
      front_name='grass'
      back_is_st=.false.
      back_name=trim(external_mat)
      
      call record_object(dim,Nobj,iobj,Nv,Nf,
     &     angle,axe_z,position,
     &     invert_normal,generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     front_is_st,back_is_st,
     &     front_name,back_name,
     &     Nv01,Nf01,v01,f01)
c     ---------------------------------------------------------------------------
c     Lateral surface of the contour, of height "e_ext"
c     + bottom
      call generate_contour_lateral_surface(dim,
     &     Ncontour,Nppt,contour,e_ext,
     &     Nv01,Nf01,v01,f01)
      invert_normal=.false.
      front_is_st=.true.
      front_name='air'
      back_is_st=.false.
      back_name=trim(external_mat)
      iobj=iobj+1
      call record_object(dim,Nobj,iobj,Nv,Nf,
     &     angle,axe_z,position,
     &     invert_normal,generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     front_is_st,back_is_st,
     &     front_name,back_name,
     &     Nv01,Nf01,v01,f01)
c     + top
      center(1)=0.0
      center(2)=0.0
      center(3)=e_ext+Nz*az+(Nz-1)*e_in
      call move_obj(dim,Nv01,v01,center)
      iobj=iobj+1
      call record_object(dim,Nobj,iobj,Nv,Nf,
     &     angle,axe_z,position,
     &     invert_normal,generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     front_is_st,back_is_st,
     &     front_name,back_name,
     &     Nv01,Nf01,v01,f01)
c     ---------------------------------------------------------------------------
      if (Nz.gt.1) then
c     Lateral surface of the contour, of height "e_in"
         call generate_contour_lateral_surface(dim,
     &        Ncontour,Nppt,contour,e_in,
     &        Nv01,Nf01,v01,f01)
         center(1)=0.0
         center(2)=0.0
         center(3)=e_ext+az
         call move_obj(dim,Nv01,v01,center)
c      
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(external_mat)
         do ifloor=1,Nz-1
            iobj=iobj+1
            call record_object(dim,Nobj,iobj,Nv,Nf,
     &           angle,axe_z,position,
     &           invert_normal,generate_individual_obj_files,
     &           obj_file,mtllib_file,off_file,
     &           front_is_st,back_is_st,
     &           front_name,back_name,
     &           Nv01,Nf01,v01,f01)
            center(1)=0.0
            center(2)=0.0
            center(3)=az+e_in
            call move_obj(dim,Nv01,v01,center)
         enddo                  ! ifloor
      endif                     ! Nz>1
c     ---------------------------------------------------------------------------
c     floor external surfaces
      center(1)=0.0
      center(2)=0.0
      center(3)=e_ext
      call move_obj(dim,Nv1,v1,center)
c     
      invert_normal=.false.
      front_is_st=.true.
      front_name='air'
      back_is_st=.false.
      back_name=trim(external_mat)
      do ifloor=1,Nz
         iobj=iobj+1
         call record_object(dim,Nobj,iobj,Nv,Nf,
     &        angle,axe_z,position,
     &        invert_normal,generate_individual_obj_files,
     &        obj_file,mtllib_file,off_file,
     &        front_is_st,back_is_st,
     &        front_name,back_name,
     &        Nv1,Nf1,v1,f1)
         center(1)=0.0
         center(2)=0.0
         center(3)=az+e_in
         call move_obj(dim,Nv1,v1,center)
      enddo                     ! ifloor
c     ---------------------------------------------------------------------------
c     Top surface
      center(1)=0.0
      center(2)=0.0
      center(3)=2*e_ext+Nz*az+(Nz-1)*e_in
      call move_obj(dim,Nv02,v02,center)
c      
      invert_normal=.false.
      front_is_st=.true.
      if (roof_height.gt.0.0D+0) then ! a roof will be drawn: the top surface is a interface with internal attic air
         front_name='internal_air'
      else                      ! no roof: the top surface is a interface with the atmosphere
         front_name='air'
      endif
      back_is_st=.false.
      back_name=trim(external_mat)
      iobj=iobj+1
      call record_object(dim,Nobj,iobj,Nv,Nf,
     &     angle,axe_z,position,
     &     invert_normal,generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     front_is_st,back_is_st,
     &     front_name,back_name,
     &     Nv02,Nf02,v02,f02)
c     ---------------------------------------------------------------------------
c     INNER SURFACES
c     ---------------------------------------------------------------------------
c     create the base: interface between the inner material and inner air
      call delaunay_triangulation(dim,Ncontour2,Nppt2,contour2,
     &     Nv01,Nf01,v01,f01)   ! @ altitude 0; normals are directed upward
      call duplicate_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)
      center(1)=0.0
      center(2)=0.0
      center(3)=e_ext
      call move_obj(dim,Nv01,v01,center)
      center(1)=0.0
      center(2)=0.0
      center(3)=e_ext+az
      call move_obj(dim,Nv02,v02,center)
      call invert_normals(Nf02,f02)
      call add_obj_to_obj(dim,Nv02,Nf02,v02,f02,
     &     Nv01,Nf01,v01,f01)
      center(1)=0.0
      center(2)=0.0
      center(3)=e_ext
      call move_obj(dim,Nv2,v2,center)
      call add_obj_to_obj(dim,Nv2,Nf2,v2,f2,
     &     Nv01,Nf01,v01,f01)
c      
      invert_normal=.false.
      front_is_st=.true.
      front_name='internal_air'
      back_is_st=.false.
      back_name=trim(internal_mat)
      do ifloor=1,Nz
         iobj=iobj+1
         call record_object(dim,Nobj,iobj,Nv,Nf,
     &        angle,axe_z,position,
     &        invert_normal,generate_individual_obj_files,
     &        obj_file,mtllib_file,off_file,
     &        front_is_st,back_is_st,
     &        front_name,back_name,
     &        Nv01,Nf01,v01,f01)
         center(1)=0.0
         center(2)=0.0
         center(3)=az+e_in
         call move_obj(dim,Nv01,v01,center)
      enddo                     ! ifloor
c     ---------------------------------------------------------------------------
c     wall-glass interfaces
c     ---------------------------------------------------------------------------
      if (Nf3.gt.0) then
         center(1)=0.0
         center(2)=0.0
         center(3)=e_ext
         call move_obj(dim,Nv3,v3,center)
         invert_normal=.false.
         front_is_st=.false.
         front_name=trim(glass_panes_mat)
         back_is_st=.false.
         back_name=trim(external_mat)
         do ifloor=1,Nz
            iobj=iobj+1
            call record_object(dim,Nobj,iobj,Nv,Nf,
     &           angle,axe_z,position,
     &           invert_normal,generate_individual_obj_files,
     &           obj_file,mtllib_file,off_file,
     &           front_is_st,back_is_st,
     &           front_name,back_name,
     &           Nv3,Nf3,v3,f3)
            center(1)=0.0
            center(2)=0.0
            center(3)=az+e_in
            call move_obj(dim,Nv3,v3,center)
         enddo                  ! ifloor
      endif                     ! Nf3 >0
c     ---------------------------------------------------------------------------
c     glass-air interfaces
c     ---------------------------------------------------------------------------
      if (Nf4.gt.0) then
         center(1)=0.0
         center(2)=0.0
         center(3)=e_ext
         call move_obj(dim,Nv4,v4,center)
         call move_obj(dim,Nv5,v5,center)
         invert_normal=.false.
         front_is_st=.true.
         back_is_st=.false.
         back_name=trim(glass_panes_mat)
         do ifloor=1,Nz
            iobj=iobj+1
            front_name='air'
            call record_object(dim,Nobj,iobj,Nv,Nf,
     &           angle,axe_z,position,
     &           invert_normal,generate_individual_obj_files,
     &           obj_file,mtllib_file,off_file,
     &           front_is_st,back_is_st,
     &           front_name,back_name,
     &           Nv4,Nf4,v4,f4)
            center(1)=0.0
            center(2)=0.0
            center(3)=az+e_in
            call move_obj(dim,Nv4,v4,center)
         enddo                  ! ifloor
      endif                     ! Nf4 > 0
      if (Nf5.gt.0) then
         center(1)=0.0
         center(2)=0.0
         center(3)=e_ext
         call move_obj(dim,Nv4,v4,center)
         call move_obj(dim,Nv5,v5,center)
         invert_normal=.false.
         front_is_st=.true.
         back_is_st=.false.
         back_name=trim(glass_panes_mat)
         do ifloor=1,Nz
            iobj=iobj+1
            front_name='internal_air'
            call record_object(dim,Nobj,iobj,Nv,Nf,
     &           angle,axe_z,position,
     &           invert_normal,generate_individual_obj_files,
     &           obj_file,mtllib_file,off_file,
     &           front_is_st,back_is_st,
     &           front_name,back_name,
     &           Nv5,Nf5,v5,f5)
            center(1)=0.0
            center(2)=0.0
            center(3)=az+e_in
            call move_obj(dim,Nv5,v5,center)
         enddo                  ! ifloor
      endif                     ! Nf5 > 0
c     ---------------------------------------------------------------------------
c     ROOF
c     ---------------------------------------------------------------------------
      if (roof_height.gt.0.0D+0) then
         call generate_roof_from_contour(dim,debug,
     &        Ncontour,Nppt,contour,
     &        Nz,az,e_ext,e_in,roof_height,roof_thickness,
     &        valid,
     &        Nv01,Nf01,v01,f01,
     &        Nv02,Nf02,v02,f02,
     &        Vroof_solid,Vroof_air)
         if (valid) then
c     object 01: air/roof interface
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(roof_mat)
            iobj=iobj+1
            call record_object(dim,Nobj,iobj,Nv,Nf,
     &           angle,axe_z,position,
     &           invert_normal,generate_individual_obj_files,
     &           obj_file,mtllib_file,off_file,
     &           front_is_st,back_is_st,
     &           front_name,back_name,
     &           Nv01,Nf01,v01,f01)
c     object 02: internal_air/roof interface
            invert_normal=.false.
            front_is_st=.true.
            front_name='internal_air'
            back_is_st=.false.
            back_name=trim(roof_mat)
            iobj=iobj+1
            call record_object(dim,Nobj,iobj,Nv,Nf,
     &           angle,axe_z,position,
     &           invert_normal,generate_individual_obj_files,
     &           obj_file,mtllib_file,off_file,
     &           front_is_st,back_is_st,
     &           front_name,back_name,
     &           Nv02,Nf02,v02,f02)
         endif                  ! valid
      endif                     ! roof_height > 0
c     
c     ---------------------------------------------------------------------------
      Nobj=Nobj+iobj
c     Debug
c      write(*,*) 'iobj=',iobj
c      write(*,*) 'Nobj=',Nobj
c     Debug
c
 666  continue
c     
      return
      end
      
      
      
      subroutine record_object(dim,Nobj,iobj,Nv,Nf,
     &     angle,axe,position,
     &     invert_normal,generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     front_is_st,back_is_st,
     &     front_name,back_name,
     &     Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to record single object to file(s)
c     
c     Input:
c       + dim: dimension of space
c       + Nobj: total number of objects in the scene
c       + iobj: local index of the object
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c       + angle: rotation angle [deg]
c       + axe: rotation axe
c       + position: (x,y) shift of the building [m]
c       + invert_normal: true if normals must be inverted
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + front_is_st: true if front is semi-transparent
c       + back_is_st: 1 if back is semi-transparent
c       + front_name: name of the material or medium on the front
c       + back_name: name of the material or medium on the back
c       + Nv01: number of vertices of the object
c       + Nf01: number of faces of the object
c       + v01: array of verices definition (3D position of each vertex)
c       + f01: array of faces (index of vertices that belong to each face)
c     
c     I/O
      integer dim
      integer Nobj
      integer iobj
      integer Nv
      integer Nf
      double precision angle
      double precision axe(1:Ndim_mx)
      double precision position(1:Ndim_mx-1)
      logical invert_normal
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) back_name
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      double precision alpha
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision center(1:Ndim_mx)
      character*(Nchar_mx) middle_name
      integer sindex
      character*(Nchar_mx) str
      logical err
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
c     label
      character*(Nchar_mx) label
      label='subroutine record_object'

      call duplicate_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)
      
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv02,v02,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv02,v02,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv02,Nf02,v02,f02)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv02,Nf02,v02,f02)
         endif
         Nv=Nv+Nv02
         Nf=Nf+Nf02
      endif                     ! err

      return
      end
