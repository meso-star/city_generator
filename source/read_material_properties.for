c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_material_properties(datafile,
     &     name,aspect,Nlambda,lambda,reflectivity,emissivity)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read the raw material properties file
c     
c     Input:
c       + datafile: file to read
c
c     Output:
c       + name: label for the material
c       + aspect: material aspect (matte, mirror, etc)
c       + Nlambda: number of wavelength points
c       + lambda: wavelength data [micrometers]
c       + reflectivity: reflectivity data
c       + emissivity: emissivity data
c     
c     I/O
      character*(Nchar_mx) datafile
      character*(Nchar_mx) name
      character*(Nchar_mx) aspect
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision emissivity(1:Nlambda_mx)
c     temp
      integer i,ios
      logical debug
c     label
      character*(Nchar_mx) label
      label='subroutine read_material_properties'

c     Debug
c      if (trim(datafile).eq.
c     &     './data/material_properties/glass.properties') then
c         debug=.true.
c      else
c         debug=.false.
c      endif
c     Debug
      
      open(12,file=trim(datafile),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         read(12,*)
         read(12,10) name
         read(12,*)
         read(12,10) aspect
         if ((trim(aspect).ne.'matte').and.
     &        (trim(aspect).ne.'mirror')) then
            call error(label)
            write(*,*) 'Material: ',trim(name)
            write(*,*) 'aspect=',trim(aspect)
            write(*,*) 'possible values are: matte, mirror'
            stop
         endif                  ! aspect='matte' or aspect='mirror'
         read(12,*)
         read(12,*) Nlambda
c     Debug
c         if (debug) then
c            write(*,*) 'Nlambda=',Nlambda
c         endif
c     Debug
         if (Nlambda.lt.0) then
            call error(label)
            write(*,*) 'Material: ',trim(name)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) 'should be >= 0'
            stop
         endif                  ! Nlambda < 0
         if (Nlambda.gt.Nlambda_mx) then
            call error(label)
            write(*,*) 'Material: ',trim(name)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) 'should be < Nlambda_mx=',Nlambda_mx
            stop
         endif                  ! Nlambda > Nlambda_mx
         read(12,*)
         if (Nlambda.eq.0) then
            read(12,*) reflectivity(1),emissivity(1)
c     ------------- consistency -------------
            if (reflectivity(1).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Material: ',trim(name)
               write(*,*) 'reflectivity=',reflectivity(1)
               write(*,*) 'should be >= 0'
               stop
            endif
            if (reflectivity(1).gt.1.0D+0) then
               call error(label)
               write(*,*) 'Material: ',trim(name)
               write(*,*) 'reflectivity=',reflectivity(1)
               write(*,*) 'should be <= 1'
               stop
            endif
            if (emissivity(1).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Material: ',trim(name)
               write(*,*) 'emissivity=',emissivity(1)
               write(*,*) 'should be >= 0'
               stop
            endif
            if (emissivity(1).gt.1.0D+0) then
               call error(label)
               write(*,*) 'Material: ',trim(name)
               write(*,*) 'emissivity=',emissivity(1)
               write(*,*) 'should be <= 1'
               stop
            endif
c     ------------- consistency -------------
         else
            do i=1,Nlambda
               read(12,*) lambda(i),reflectivity(i),emissivity(i)
c     Debug
c               if (debug) then
c                  write(*,*) 'reflectivity(',i,')=',reflectivity(i)
c               endif
c     Debug
c     ------------- consistency -------------
               if (reflectivity(i).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'reflectivity(',i,')=',reflectivity(i)
                  write(*,*) 'should be >= 0'
                  stop
               endif
               if (reflectivity(i).gt.1.0D+0) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'reflectivity(',i,')=',reflectivity(i)
                  write(*,*) 'should be <= 1'
                  stop
               endif
               if (emissivity(i).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'emissivity(',i,')=',emissivity(i)
                  write(*,*) 'should be >= 0'
                  stop
               endif
               if (emissivity(i).gt.1.0D+0) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'emissivity(',i,')=',emissivity(i)
                  write(*,*) 'should be <= 1'
                  stop
               endif
c     ------------- consistency -------------
            enddo               ! i
            do i=2,Nlambda
c     ------------- consistency -------------
               if (lambda(i).le.lambda(i-1)) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'lambda(',i,')=',lambda(i)
                  write(*,*) 'should be > lambda(',i-1,')=',lambda(i-1)
                  stop
               endif
c     ------------- consistency -------------
            enddo               ! i
         endif                  ! Nlambda=0
c     
      endif                     ! ios.ne.0
      close(12)

c     Debug
c      if (debug) then
c         stop
c      endif
c     Debug      

      return
      end
      
