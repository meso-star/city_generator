c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine road(dim,iroad,
     &     Nroad,width,draw_wbands,
     &     Nppt,central_track,
     &     road_mat,wband_mat,
     &     draw_ground,xmin,xmax,ymin,ymax,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
c
c     Purpose: to produce a road
c
c     Input:
c       + dim: dimension of space
c       + iroad: road index
c       + Nroad: number of roads
c       + width: width of each road [m]
c       + draw_wbands: T if drawing white bands is required
c       + Nppt: number of points for the description of the central track for each road
c       + central_track: list of positions that describe the central track of each road
c       + road_mat: material for the road
c       + wband_mat: material for white bands
c       + draw_ground: T when drawing ground is required
c       + xmin,xmax,ymin,ymax: limits of the ground
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      integer iroad
      integer Nroad
      double precision width
      logical draw_wbands
      integer Nppt
      double precision central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) road_material
      character*(Nchar_mx) wband_material
      logical draw_ground
      double precision xmin,xmax,ymin,ymax
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wband_mat
      integer iseg,Nseg,iobj,i,j,k
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n_tmp(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision d(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
c     label
      character*(Nchar_mx) label
      label='subroutine road'

      Nseg=Nppt-1
      if (Nseg.lt.1) then
         call error(label)
         write(*,*) 'Nseg=',Nseg
         write(*,*) 'should be > 0'
         stop
      endif
      iobj=0                    ! total number of individual surfaces groups for the road
      do iseg=1,Nseg
         p1(1)=central_track(iseg,1)
         p1(2)=central_track(iseg,2)
         p1(3)=0.0D+0
         p2(1)=central_track(iseg+1,1)
         p2(2)=central_track(iseg+1,2)
         p2(3)=0.0D+0
         call normal_for_segment(dim,p1,p2,n)
c     n is the normal for the current segment
         if (iseg.eq.1) then
            call copy_vector(dim,n,n1)
         else                   ! iseg>1
            p0(1)=central_track(iseg-1,1)
            p0(2)=central_track(iseg-1,2)
            p0(3)=0.0D+0
            call normal_for_segment(dim,p0,p1,n_tmp)
            call add_vectors(dim,n_tmp,n,u)
            call normalize_vector(dim,u,n1)
         endif                  ! iseg=1
c     n1 is the normal to use for P1
         if (iseg.eq.Nseg) then
            call copy_vector(dim,n,n2)
         else                   ! iseg>1
            p0(1)=central_track(iseg+2,1)
            p0(2)=central_track(iseg+2,2)
            p0(3)=0.0D+0
            call normal_for_segment(dim,p2,p0,n_tmp)
            call add_vectors(dim,n_tmp,n,u)
            call normalize_vector(dim,u,n2)
         endif                  ! iseg=Nseg
c     n2 is the normal to use for P2
         call scalar_vector(dim,width/2.0D+0,n1,d)
         call substract_vectors(dim,p1,d,x1)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x1)
         endif
         call add_vectors(dim,p1,d,x4)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x4)
         endif
         call scalar_vector(dim,width/2.0D+0,n2,d)
         call substract_vectors(dim,p2,d,x2)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x2)
         endif
         call add_vectors(dim,p2,d,x3)
         if (draw_ground) then
            call clamp_on_ground(dim,xmin,xmax,ymin,ymax,x3)
         endif
c     ---------------------------------------------------------------------------
c     ---------------------------------------------------------------------------
c     Production of objects:
c     ---------------------------------------------------------------------------
c     + section of the road
c     ---------------------------------------------------------------------------
         iobj=iobj+1
         call obj_segment(dim,x1,x2,x3,x4,
     &        Nv01,Nf01,v01,f01)
c     Add individual surface to global trianglemesh (for visualization)
         invert_normal=.false.
         sindex=Nobj+iobj
         call num2str(sindex,str,err)
         if (err) then
            call error(label)
            write(*,*) 'while converting to string:'
            write(*,*) 'sindex=',sindex
            stop
         else
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(road_mat)
            middle_name=''
            call append_single_obj_with_mtl(dim,obj_file,
     &           mtllib_file,
     &           invert_normal,
     &           front_is_st,back_is_st,.false.,
     &           front_name,middle_name,back_name,
     &           Nv,Nv01,Nf01,v01,f01)
c            call retrieve_code(back_name,color_code)
c            call append_single_off(dim,off_file,invert_normal,
c     &           Nv,Nv01,Nf01,v01,f01,color_code)
            if (generate_individual_obj_files) then
               filename='S'//trim(str)//'.obj'
               file_out='./results/obj/'//trim(filename)
               call write_single_obj(dim,file_out,invert_normal,
     &              Nv01,Nf01,v01,f01)
            endif
            Nv=Nv+Nv01
            Nf=Nf+Nf01
         endif                  ! err
         if (draw_wbands) then
c     ---------------------------------------------------------------------------
c     + white bands
c     ---------------------------------------------------------------------------
            iobj=iobj+1
            call obj_wband(dim,
     &           Nroad,width,draw_wbands,
     &           Nppt,central_track,
     &           road_mat,wband_mat,
     &           iroad,iseg,
     &           p1,p2,x1,x2,x3,x4,n,n1,n2,
     &           Nv01,Nf01,v01,f01)
c     Add individual surface to global trianglemesh (for visualization)
            invert_normal=.false.
            sindex=Nobj+iobj
            call num2str(sindex,str,err)
            if (err) then
               call error(label)
               write(*,*) 'while converting to string:'
               write(*,*) 'sindex=',sindex
               stop
            else
               invert_normal=.false.
               front_is_st=.true.
               front_name='air'
               back_is_st=.false.
               back_name=trim(wband_mat)
               middle_name=''
               call append_single_obj_with_mtl(dim,obj_file,
     &              mtllib_file,
     &              invert_normal,
     &              front_is_st,back_is_st,.false.,
     &              front_name,middle_name,back_name,
     &              Nv,Nv01,Nf01,v01,f01)
c               call retrieve_code(back_name,color_code)
c               call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
               if (generate_individual_obj_files) then
                  filename='S'//trim(str)//'.obj'
                  file_out='./results/obj/'//trim(filename)
                  call write_single_obj(dim,file_out,invert_normal,
     &                 Nv01,Nf01,v01,f01)
               endif
               Nv=Nv+Nv01
               Nf=Nf+Nf01
            endif               ! err
c
         endif                  ! draw_wbands
      enddo                     ! iseg
c     ---------------------------------------------------------------------------
c     
      Nobj=Nobj+iobj
c
      return
      end

      

      subroutine normal_for_segment(dim,P1,P2,n)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the normal between the two points that define a segment
c     The returned normal points towards the inside of a closed contour if P1 and P2
c     are two consecutive positions over a closed contour defined counter-clockwise
c     
c     Input:
c       + dim: dimension of space
c       + P1: first point of the segment
c       + P2: second point of the segment
c     
c     Output:
c       + n: normal for the segment
c     
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision n(1:Ndim_mx)
c     temp
      double precision t(1:Ndim_mx)
      double precision u(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine normal_for_segment'

      call substract_vectors(dim,P2,P1,t)
      u(1)=-t(2)
      u(2)=t(1)
      u(3)=t(3)
      call normalize_vector(dim,u,n)

      return
      end
      


      subroutine clamp_on_ground(dim,xmin,xmax,ymin,ymax,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to clamp a position on the ground zone
c     
c     Input:
c       + dim: dimension of space
c       + xmin,xmax,ymin,ymax: limits of the ground
c       + x: position to clamp
c     
c     Output:
c       + x: updated
c     
c     I/O
      integer dim
      double precision xmin,xmax,ymin,ymax
      double precision x(1:Ndim_mx)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine clamp_on_ground'

      if (x(1).lt.xmin) then
         x(1)=xmin
      endif
      if (x(1).gt.xmax) then
         x(1)=xmax
      endif
      if (x(2).lt.ymin) then
         x(2)=ymin
      endif
      if (x(2).gt.ymax) then
         x(2)=ymax
      endif

      return
      end
      
