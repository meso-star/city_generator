c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine window_border(dim,Nx,Ny,Nz,
     &     ix,iy,iz,type,
     &     ax,ay,az,e_in,e_ext,wwf,e_glass,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the trianglemesh for a window border
c
c     Input:
c       + dim: dimension of space
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + (ix,iy,iz): index of the cell along each axis
c       + type: 1 for north, 2 for south, 3 for west, 4 for east
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + az: dimension of a single cubic cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + wwf: fraction of the wall used for windows
c       + e_glass: thickness of the glass (m)
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      integer Nx,Ny,Nz
      integer ix,iy,iz
      integer type
      double precision ax,ay,az
      double precision e_in,e_ext
      double precision wwf
      double precision e_glass
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer inode,iface,i,j
      double precision box(1:8,1:Ndim_mx)
      double precision rectangle(1:4,1:Ndim_mx)
      integer Nn1,Nf1
      double precision v1(1:Nv_so_mx,1:Ndim_mx)
      integer f1(1:Nf_so_mx,1:3)
      integer Nn2,Nf2
      double precision v2(1:Nv_so_mx,1:Ndim_mx)
      integer f2(1:Nf_so_mx,1:3)
c     label
      character*(Nchar_mx) label
      label='subroutine window_border'

      Nnode=0
      Nface=0
      call rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)

c     -------------------------------------------------------------------------
      if (type.eq.1) then ! window in on the north face
c     Create north face only
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(4,j)
            rectangle(3,j)=box(8,j)
            rectangle(4,j)=box(5,j)
         enddo                  ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ x=e_ext-e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,1)=v1(inode,1)-e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
c     lateral face 1
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(4,j)
            rectangle(3,j)=box(8,j)
            rectangle(4,j)=box(5,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 2
         do j=1,dim
            rectangle(1,j)=box(2,j)
            rectangle(2,j)=box(6,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(3,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 3
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(5,j)
            rectangle(3,j)=box(6,j)
            rectangle(4,j)=box(2,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 4
         do j=1,dim
            rectangle(1,j)=box(4,j)
            rectangle(2,j)=box(3,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(8,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     
c     -------------------------------------------------------------------------
      else if (type.eq.2) then
c     Create south face only
            do j=1,dim
               rectangle(1,j)=box(2,j)
               rectangle(2,j)=box(6,j)
               rectangle(3,j)=box(7,j)
               rectangle(4,j)=box(3,j)
            enddo               ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ x=Lx-e_ext+e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,1)=v1(inode,1)+e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
c     lateral face 1
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(4,j)
            rectangle(3,j)=box(8,j)
            rectangle(4,j)=box(5,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 2
         do j=1,dim
            rectangle(1,j)=box(2,j)
            rectangle(2,j)=box(6,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(3,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 3
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(5,j)
            rectangle(3,j)=box(6,j)
            rectangle(4,j)=box(2,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 4
         do j=1,dim
            rectangle(1,j)=box(4,j)
            rectangle(2,j)=box(3,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(8,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     
c     -------------------------------------------------------------------------
      else if (type.eq.3) then
c     Create west face only
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(5,j)
            rectangle(3,j)=box(6,j)
            rectangle(4,j)=box(2,j)
         enddo                  ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ y=e_ext-e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,2)=v2(inode,2)-e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
c     lateral face 1
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(4,j)
            rectangle(3,j)=box(8,j)
            rectangle(4,j)=box(5,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 2
         do j=1,dim
            rectangle(1,j)=box(2,j)
            rectangle(2,j)=box(6,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(3,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 3
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(5,j)
            rectangle(3,j)=box(6,j)
            rectangle(4,j)=box(2,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 4
         do j=1,dim
            rectangle(1,j)=box(4,j)
            rectangle(2,j)=box(3,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(8,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     
c     -------------------------------------------------------------------------
      else if (type.eq.4) then
c     Create east face only
         do j=1,dim
            rectangle(1,j)=box(4,j)
            rectangle(2,j)=box(3,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(8,j)
         enddo                  ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ y=Ly-e_ext+e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,2)=v2(inode,2)+e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
c     lateral face 1
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(4,j)
            rectangle(3,j)=box(8,j)
            rectangle(4,j)=box(5,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 2
         do j=1,dim
            rectangle(1,j)=box(2,j)
            rectangle(2,j)=box(6,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(3,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 3
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(5,j)
            rectangle(3,j)=box(6,j)
            rectangle(4,j)=box(2,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     lateral face 4
         do j=1,dim
            rectangle(1,j)=box(4,j)
            rectangle(2,j)=box(3,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(8,j)
         enddo                  ! j
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     
c     -------------------------------------------------------------------------
      else
         call error(label)
         write(*,*) 'type=',type
         write(*,*) 'should be in the [1-4] range'
         stop
      endif
      
      return
      end


      
      subroutine window_glass_panes(dim,Nx,Ny,Nz,
     &     ix,iy,iz,type,pane_index,
     &     ax,ay,az,e_in,e_ext,wwf,e_glass,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the trianglemesh for a window border
c
c     Input:
c       + dim: dimension of space
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + (ix,iy,iz): index of the cell along each axis
c       + type: 1 for north, 2 for south, 3 for west, 4 for east
c       + pane_index: 1 for inside page, 2 for outisde pane
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + az: dimension of a single cubic cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + wwf: fraction of the wall used for windows
c       + e_glass: thickness of the glass (m)
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      integer Nx,Ny,Nz
      integer ix,iy,iz
      integer type
      integer pane_index
      double precision ax,ay,az
      double precision e_in,e_ext
      double precision wwf
      double precision e_glass
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer inode,iface,i,j
      double precision box(1:8,1:Ndim_mx)
      double precision rectangle(1:4,1:Ndim_mx)
      integer Nn1,Nf1
      double precision v1(1:Nv_so_mx,1:Ndim_mx)
      integer f1(1:Nf_so_mx,1:3)
      integer Nn2,Nf2
      double precision v2(1:Nv_so_mx,1:Ndim_mx)
      integer f2(1:Nf_so_mx,1:3)
c     label
      character*(Nchar_mx) label
      label='subroutine window_glass_panes'

      Nnode=0
      Nface=0
      call rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)

c     -------------------------------------------------------------------------
      if (type.eq.1) then ! window in on the north face
c     Create north face only
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(4,j)
            rectangle(3,j)=box(8,j)
            rectangle(4,j)=box(5,j)
         enddo                  ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ x=e_ext-e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,1)=v1(inode,1)-e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
         if (pane_index.eq.1) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(i,j)
               enddo            ! j
            enddo               ! i
         else if (pane_index.eq.2) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(9-i,j)
               enddo            ! j
            enddo               ! i
         else
            call error(label)
            write(*,*) 'pane_index=',pane_index
            write(*,*) 'should be in the [1-2] range'
            stop
         endif
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     -------------------------------------------------------------------------
      else if (type.eq.2) then
c     Create south face only
            do j=1,dim
               rectangle(1,j)=box(2,j)
               rectangle(2,j)=box(6,j)
               rectangle(3,j)=box(7,j)
               rectangle(4,j)=box(3,j)
            enddo               ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ x=Lx-e_ext+e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,1)=v1(inode,1)+e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
         if (pane_index.eq.1) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(i,j)
               enddo            ! j
            enddo               ! i
         else if (pane_index.eq.2) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(9-i,j)
               enddo            ! j
            enddo               ! i
         else
            call error(label)
            write(*,*) 'pane_index=',pane_index
            write(*,*) 'should be in the [1-2] range'
            stop
         endif
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     
c     -------------------------------------------------------------------------
      else if (type.eq.3) then
c     Create west face only
         do j=1,dim
            rectangle(1,j)=box(1,j)
            rectangle(2,j)=box(5,j)
            rectangle(3,j)=box(6,j)
            rectangle(4,j)=box(2,j)
         enddo                  ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ y=e_ext-e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,2)=v2(inode,2)-e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
         if (pane_index.eq.1) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(i,j)
               enddo            ! j
            enddo               ! i
         else if (pane_index.eq.2) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(9-i,j)
               enddo            ! j
            enddo               ! i
         else
            call error(label)
            write(*,*) 'pane_index=',pane_index
            write(*,*) 'should be in the [1-2] range'
            stop
         endif
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     
c     -------------------------------------------------------------------------
      else if (type.eq.4) then
c     Create east face only
         do j=1,dim
            rectangle(1,j)=box(4,j)
            rectangle(2,j)=box(3,j)
            rectangle(3,j)=box(7,j)
            rectangle(4,j)=box(8,j)
         enddo                  ! j
         Nn1=0
         Nf1=0
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nn1,Nf1,v1,f1)
c     Duplicate face @ y=Ly-e_ext+e_glass
         Nn2=Nn1
         Nf2=Nf1
         do inode=1,Nn1
            do j=1,dim
               v2(inode,j)=v1(inode,j)
            enddo               ! j
            v2(inode,2)=v2(inode,2)+e_glass
         enddo                  ! inode
         do iface=1,Nf1
            do j=1,3
               f2(iface,j)=f1(iface,j)
            enddo               ! j
         enddo                  ! iface
c     Lateral faces (window border)
         do i=1,4
            do j=1,dim
               box(i,j)=v1(i+4,j)
            enddo               ! j
         enddo                  ! i
         do i=5,8
            do j=1,dim
               box(i,j)=v2(i,j)
            enddo               ! j
         enddo                  ! i
         if (pane_index.eq.1) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(i,j)
               enddo            ! j
            enddo               ! i
         else if (pane_index.eq.2) then
            do i=1,4
               do j=1,dim
                  rectangle(i,j)=box(9-i,j)
               enddo            ! j
            enddo               ! i
         else
            call error(label)
            write(*,*) 'pane_index=',pane_index
            write(*,*) 'should be in the [1-2] range'
            stop
         endif
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
c     
c     -------------------------------------------------------------------------
      else
         call error(label)
         write(*,*) 'type=',type
         write(*,*) 'should be in the [1-4] range'
         stop
      endif
      
      return
      end



      subroutine global_enveloppe(dim,Nx,Ny,Nz,
     &     ax,ay,az,e_in,e_ext,wwf,e_glass,
     &     draw_specific_window,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the trianglemesh for a cubic cell
c
c     Input:
c       + dim: dimension of space
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + az: dimension of a single cubic cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + wwf: fraction of the wall used for windows
c       + e_glass: thickness of the glass (m)
c       + draw_specific_window: when set to T, the window will be drawn
c
c     Output:
c       + Nv01, Nf01, v01, f01: lateral surface
c       + Nv02, Nf02, v02, f02: top surface
c
c     I/O
      integer dim
      integer Nx,Ny,Nz
      double precision ax,ay,az
      double precision e_in,e_ext
      double precision wwf
      double precision e_glass
      logical draw_specific_window(1:Nx_mx,1:Ny_mx,1:Nz_mx)
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
c     temp
      integer ix,iy,iz
      integer inode,iface,i,j
      double precision box(1:8,1:Ndim_mx)
      double precision rectangle(1:4,1:Ndim_mx)
      integer Nn1,Nf1
      double precision v1(1:Nv_so_mx,1:Ndim_mx)
      integer f1(1:Nf_so_mx,1:3)
      integer Nn2,Nf2
      double precision v2(1:Nv_so_mx,1:Ndim_mx)
      integer f2(1:Nf_so_mx,1:3)
      double precision Lx,Ly,Lz
c     label
      character*(Nchar_mx) label
      label='subroutine global_enveloppe'

      call total_length(Nx,ax,e_in,e_ext,Lx)
      call total_length(Ny,ay,e_in,e_ext,Ly)
      call total_length(Nz,az,e_in,e_ext,Lz)

      Nv01=0
      Nf01=0

c     -------------------------------------------------------------------------
c     North face
c     1 - room faces only
      ix=1
      do iy=1,Ny
         do iz=1,Nz
            call rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)
c     Create north face only
            do j=1,dim
               rectangle(1,j)=box(1,j)
               rectangle(2,j)=box(4,j)
               rectangle(3,j)=box(8,j)
               rectangle(4,j)=box(5,j)
            enddo               ! j
            Nn1=0
            Nf1=0
            if (draw_specific_window(ix,iy,iz)) then
               call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &              Nn1,Nf1,v1,f1)
            else
               call add_face_with_rectangle(dim,rectangle,
     &              Nn1,Nf1,v1,f1)
            endif
c     Duplicate north face @ x=0
            Nn2=Nn1
            Nf2=Nf1
            do inode=1,Nn1
               do j=1,dim
                  v2(inode,j)=v1(inode,j)
               enddo            ! j
               v2(inode,1)=v1(inode,1)-e_ext
            enddo               ! inode
            do iface=1,Nf1
               do j=1,3
                  f2(iface,j)=f1(iface,j)
               enddo            ! j
            enddo               ! iface
c     Add object to global obj
            call add2_single_obj(dim,
     &           Nv01,Nf01,v01,f01,
     &           .true.,
     &           Nn2,Nf2,v2,f2)
            if (draw_specific_window(ix,iy,iz)) then
c     Lateral faces (window border)
               do i=1,4
                  do j=1,dim
                     box(i,j)=v2(i+4,j)
                  enddo         ! j
               enddo            ! i
               do i=5,8
                  do j=1,dim
                     box(i,j)=box(i-4,j)
                  enddo         ! j
                  box(i,1)=box(i,1)+(e_ext-e_glass)
               enddo            ! i
c     lateral face 1
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(4,j)
                  rectangle(3,j)=box(8,j)
                  rectangle(4,j)=box(5,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 2
               do j=1,dim
                  rectangle(1,j)=box(2,j)
                  rectangle(2,j)=box(6,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(3,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 3
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(5,j)
                  rectangle(3,j)=box(6,j)
                  rectangle(4,j)=box(2,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 4
               do j=1,dim
                  rectangle(1,j)=box(4,j)
                  rectangle(2,j)=box(3,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(8,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
            endif
c            
         enddo                  ! iz
      enddo                     ! iy
c     2 - vertical bands
      do iy=0,Ny
         do i=1,4
            rectangle(i,1)=0.0D+0
         enddo                  ! j
         if (iy.eq.0) then
            rectangle(1,2)=0.0D+0
         else if (iy.eq.1) then
            rectangle(1,2)=e_ext+ay
         else
            rectangle(1,2)=e_ext+ay*iy+e_in*(iy-1)
         endif
         rectangle(1,3)=0.0D+0
         rectangle(2,2)=rectangle(1,2)
         rectangle(2,3)=Lz
         if ((iy.eq.0).or.(iy.eq.Ny)) then
            rectangle(3,2)=rectangle(2,2)+e_ext
         else
            rectangle(3,2)=rectangle(2,2)+e_in
         endif
         rectangle(3,3)=rectangle(2,3)
         rectangle(4,2)=rectangle(3,2)
         rectangle(4,3)=rectangle(1,3)
         call add_face_with_rectangle(dim,rectangle,
     &        Nv01,Nf01,v01,f01)
      enddo ! iy
c     3 - horizontal bands
      do iy=1,Ny
         do iz=0,Nz
            do i=1,4
               rectangle(i,1)=0.0D+0
            enddo               ! j
            rectangle(1,2)=(ay+e_in)*(iy-1)+e_ext
            if (iz.eq.0) then
               rectangle(1,3)=0.0D+0
            else if (iz.eq.1) then
               rectangle(1,3)=e_ext+az
            else
               rectangle(1,3)=e_ext+az*iz+e_in*(iz-1)
            endif
            rectangle(2,2)=rectangle(1,2)
            if ((iz.eq.0).or.(iz.eq.Nz)) then
               rectangle(2,3)=rectangle(1,3)+e_ext
            else
               rectangle(2,3)=rectangle(1,3)+e_in
            endif
            rectangle(3,2)=rectangle(2,2)+ay
            rectangle(3,3)=rectangle(2,3)
            rectangle(4,2)=rectangle(3,2)
            rectangle(4,3)=rectangle(1,3)
            call add_face_with_rectangle(dim,rectangle,
     &           Nv01,Nf01,v01,f01)
         enddo                  ! iz
      enddo                     ! iy
c     -------------------------------------------------------------------------
c     South face
c     1 - room faces only
      ix=Nx
      do iy=1,Ny
         do iz=1,Nz
            call rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)
c     Create south face only
            do j=1,dim
               rectangle(1,j)=box(2,j)
               rectangle(2,j)=box(6,j)
               rectangle(3,j)=box(7,j)
               rectangle(4,j)=box(3,j)
            enddo               ! j
            Nn1=0
            Nf1=0
            if (draw_specific_window(ix,iy,iz)) then
               call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &              Nn1,Nf1,v1,f1)
            else
               call add_face_with_rectangle(dim,rectangle,
     &              Nn1,Nf1,v1,f1)
            endif
c     Duplicate north face @ x=0
            Nn2=Nn1
            Nf2=Nf1
            do inode=1,Nn1
               do j=1,dim
                  v2(inode,j)=v1(inode,j)
               enddo            ! j
               v2(inode,1)=v1(inode,1)+e_ext
            enddo               ! inode
            do iface=1,Nf1
               do j=1,3
                  f2(iface,j)=f1(iface,j)
               enddo            ! j
            enddo               ! iface
c     Add object to global obj
            call add2_single_obj(dim,
     &           Nv01,Nf01,v01,f01,
     &           .true.,
     &           Nn2,Nf2,v2,f2)
            if (draw_specific_window(ix,iy,iz)) then
c     Lateral faces (window border)
               do i=1,4
                  do j=1,dim
                     box(i,j)=v2(i+4,j)
                  enddo         ! j
               enddo            ! i
               do i=5,8
                  do j=1,dim
                     box(i,j)=box(i-4,j)
                  enddo         ! j
                  box(i,1)=box(i,1)-(e_ext-e_glass)
               enddo            ! i
c     lateral face 1
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(4,j)
                  rectangle(3,j)=box(8,j)
                  rectangle(4,j)=box(5,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 2
               do j=1,dim
                  rectangle(1,j)=box(2,j)
                  rectangle(2,j)=box(6,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(3,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 3
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(5,j)
                  rectangle(3,j)=box(6,j)
                  rectangle(4,j)=box(2,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 4
               do j=1,dim
                  rectangle(1,j)=box(4,j)
                  rectangle(2,j)=box(3,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(8,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
            endif
c            
         enddo                  ! iz
      enddo                     ! iy
c     2 - vertical bands
      do iy=0,Ny
         do i=1,4
            rectangle(i,1)=Lx
         enddo                  ! j
         if (iy.eq.0) then
            rectangle(1,2)=0.0D+0
         else if (iy.eq.1) then
            rectangle(1,2)=e_ext+ay
         else
            rectangle(1,2)=e_ext+ay*iy+e_in*(iy-1)
         endif
         rectangle(1,3)=0.0D+0
         if ((iy.eq.0).or.(iy.eq.Ny)) then
            rectangle(2,2)=rectangle(1,2)+e_ext
         else
            rectangle(2,2)=rectangle(1,2)+e_in
         endif
         rectangle(2,3)=rectangle(1,3)
         rectangle(3,2)=rectangle(2,2)
         rectangle(3,3)=Lz
         rectangle(4,2)=rectangle(1,2)
         rectangle(4,3)=rectangle(3,3)
         call add_face_with_rectangle(dim,rectangle,
     &        Nv01,Nf01,v01,f01)
      enddo ! iy
c     3 - horizontal bands
      do iy=1,Ny
         do iz=0,Nz
            do i=1,4
               rectangle(i,1)=Lx
            enddo               ! j
            rectangle(1,2)=(ay+e_in)*(iy-1)+e_ext
            if (iz.eq.0) then
               rectangle(1,3)=0.0D+0
            else if (iz.eq.1) then
               rectangle(1,3)=e_ext+az
            else
               rectangle(1,3)=e_ext+az*iz+e_in*(iz-1)
            endif
            rectangle(2,2)=rectangle(1,2)+ay
            rectangle(2,3)=rectangle(1,3)
            rectangle(3,2)=rectangle(2,2)
            if ((iz.eq.0).or.(iz.eq.Nz)) then
               rectangle(3,3)=rectangle(2,3)+e_ext
            else
               rectangle(3,3)=rectangle(2,3)+e_in
            endif
            rectangle(4,2)=rectangle(1,2)
            rectangle(4,3)=rectangle(3,3)
            call add_face_with_rectangle(dim,rectangle,
     &           Nv01,Nf01,v01,f01)
         enddo                  ! iz
      enddo                     ! iy
c     -------------------------------------------------------------------------
c     West face
c     1 - room faces only
      iy=1
      do ix=1,Nx
         do iz=1,Nz
            call rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)
c     Create west face only
            do j=1,dim
               rectangle(1,j)=box(1,j)
               rectangle(2,j)=box(5,j)
               rectangle(3,j)=box(6,j)
               rectangle(4,j)=box(2,j)
            enddo               ! j
            Nn1=0
            Nf1=0
            if (draw_specific_window(ix,iy,iz)) then
               call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &              Nn1,Nf1,v1,f1)
            else
               call add_face_with_rectangle(dim,rectangle,
     &              Nn1,Nf1,v1,f1)
            endif
c     Duplicate north face @ x=0
            Nn2=Nn1
            Nf2=Nf1
            do inode=1,Nn1
               do j=1,dim
                  v2(inode,j)=v1(inode,j)
               enddo            ! j
               v2(inode,2)=v1(inode,2)-e_ext
            enddo               ! inode
            do iface=1,Nf1
               do j=1,3
                  f2(iface,j)=f1(iface,j)
               enddo            ! j
            enddo               ! iface
c     Add object to global obj
            call add2_single_obj(dim,
     &           Nv01,Nf01,v01,f01,
     &           .true.,
     &           Nn2,Nf2,v2,f2)
            if (draw_specific_window(ix,iy,iz)) then
c     Lateral faces (window border)
               do i=1,4
                  do j=1,dim
                     box(i,j)=v2(i+4,j)
                  enddo         ! j
               enddo            ! i
               do i=5,8
                  do j=1,dim
                     box(i,j)=box(i-4,j)
                  enddo         ! j
                  box(i,2)=box(i,2)+(e_ext-e_glass)
               enddo            ! i
c     lateral face 1
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(4,j)
                  rectangle(3,j)=box(8,j)
                  rectangle(4,j)=box(5,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 2
               do j=1,dim
                  rectangle(1,j)=box(2,j)
                  rectangle(2,j)=box(6,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(3,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 3
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(5,j)
                  rectangle(3,j)=box(6,j)
                  rectangle(4,j)=box(2,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 4
               do j=1,dim
                  rectangle(1,j)=box(4,j)
                  rectangle(2,j)=box(3,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(8,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
            endif
         enddo                  ! iz
      enddo                     ! ix
c     
c     2 - vertical bands
      do ix=0,Nx
         do i=1,4
            rectangle(i,2)=0.0D+0
         enddo                  ! j
         if (ix.eq.0) then
            rectangle(1,1)=0.0D+0
         else if (ix.eq.1) then
            rectangle(1,1)=e_ext+ax
         else
            rectangle(1,1)=e_ext+ax*ix+e_in*(ix-1)
         endif
         rectangle(1,3)=0.0D+0
         if ((ix.eq.0).or.(ix.eq.Nx)) then
            rectangle(2,1)=rectangle(1,1)+e_ext
         else
            rectangle(2,1)=rectangle(1,1)+e_in
         endif
         rectangle(2,3)=rectangle(1,3)
         rectangle(3,1)=rectangle(2,1)
         rectangle(3,3)=Lz
         rectangle(4,1)=rectangle(1,1)
         rectangle(4,3)=rectangle(3,3)
         call add_face_with_rectangle(dim,rectangle,
     &        Nv01,Nf01,v01,f01)
      enddo ! ix
c     3 - horizontal bands
      do ix=1,Nx
         do iz=0,Nz
            do i=1,4
               rectangle(i,2)=0.0D+0
            enddo               ! j
            rectangle(1,1)=(ax+e_in)*(ix-1)+e_ext
            if (iz.eq.0) then
               rectangle(1,3)=0.0D+0
            else if (iz.eq.1) then
               rectangle(1,3)=e_ext+az
            else
               rectangle(1,3)=e_ext+az*iz+e_in*(iz-1)
            endif
            rectangle(2,1)=rectangle(1,1)+ax
            rectangle(2,3)=rectangle(1,3)
            rectangle(3,1)=rectangle(2,1)
            if ((iz.eq.0).or.(iz.eq.Nz)) then
               rectangle(3,3)=rectangle(2,3)+e_ext
            else
               rectangle(3,3)=rectangle(2,3)+e_in
            endif
            rectangle(4,1)=rectangle(1,1)
            rectangle(4,3)=rectangle(3,3)
            call add_face_with_rectangle(dim,rectangle,
     &           Nv01,Nf01,v01,f01)
         enddo                  ! iz
      enddo                     ! ix
c      
c     -------------------------------------------------------------------------
c     East face
c     1 - room faces only
      iy=Ny
      do ix=1,Nx
         do iz=1,Nz
            call rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)
c     Create east face only
            do j=1,dim
               rectangle(1,j)=box(4,j)
               rectangle(2,j)=box(3,j)
               rectangle(3,j)=box(7,j)
               rectangle(4,j)=box(8,j)
            enddo               ! j
            Nn1=0
            Nf1=0
            if (draw_specific_window(ix,iy,iz)) then
               call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &              Nn1,Nf1,v1,f1)
            else
               call add_face_with_rectangle(dim,rectangle,
     &              Nn1,Nf1,v1,f1)
            endif
c     Duplicate north face @ x=0
            Nn2=Nn1
            Nf2=Nf1
            do inode=1,Nn1
               do j=1,dim
                  v2(inode,j)=v1(inode,j)
               enddo            ! j
               v2(inode,2)=v1(inode,2)+e_ext
            enddo               ! inode
            do iface=1,Nf1
               do j=1,3
                  f2(iface,j)=f1(iface,j)
               enddo            ! j
            enddo               ! iface
c     Add object to global obj
            call add2_single_obj(dim,
     &           Nv01,Nf01,v01,f01,
     &           .true.,
     &           Nn2,Nf2,v2,f2)
            if (draw_specific_window(ix,iy,iz)) then
c     Lateral faces (window border)
               do i=1,4
                  do j=1,dim
                     box(i,j)=v2(i+4,j)
                  enddo         ! j
               enddo            ! i
               do i=5,8
                  do j=1,dim
                     box(i,j)=box(i-4,j)
                  enddo         ! j
                  box(i,2)=box(i,2)-(e_ext-e_glass)
               enddo            ! i
c     lateral face 1
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(4,j)
                  rectangle(3,j)=box(8,j)
                  rectangle(4,j)=box(5,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 2
               do j=1,dim
                  rectangle(1,j)=box(2,j)
                  rectangle(2,j)=box(6,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(3,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 3
               do j=1,dim
                  rectangle(1,j)=box(1,j)
                  rectangle(2,j)=box(5,j)
                  rectangle(3,j)=box(6,j)
                  rectangle(4,j)=box(2,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c     lateral face 4
               do j=1,dim
                  rectangle(1,j)=box(4,j)
                  rectangle(2,j)=box(3,j)
                  rectangle(3,j)=box(7,j)
                  rectangle(4,j)=box(8,j)
               enddo            ! j
               call add_face_with_rectangle(dim,rectangle,
     &              Nv01,Nf01,v01,f01)
c
            endif
         enddo                  ! iz
      enddo                     ! ix
c     
c     2 - vertical bands
      do ix=0,Nx
         do i=1,4
            rectangle(i,2)=Ly
         enddo                  ! j
         if (ix.eq.0) then
            rectangle(1,1)=0.0D+0
         else if (ix.eq.1) then
            rectangle(1,1)=e_ext+ax
         else
            rectangle(1,1)=e_ext+ax*ix+e_in*(ix-1)
         endif
         rectangle(1,3)=0.0D+0
         rectangle(2,1)=rectangle(1,1)
         rectangle(2,3)=Lz
         if ((ix.eq.0).or.(ix.eq.Nx)) then
            rectangle(3,1)=rectangle(2,1)+e_ext
         else
            rectangle(3,1)=rectangle(2,1)+e_in
         endif
         rectangle(3,3)=rectangle(2,3)
         rectangle(4,1)=rectangle(3,1)
         rectangle(4,3)=rectangle(1,3)
         call add_face_with_rectangle(dim,rectangle,
     &        Nv01,Nf01,v01,f01)
      enddo ! iy
c     3 - horizontal bands
      do ix=1,Nx
         do iz=0,Nz
            do i=1,4
               rectangle(i,2)=Ly
            enddo               ! j
            rectangle(1,1)=(ax+e_in)*(ix-1)+e_ext
            if (iz.eq.0) then
               rectangle(1,3)=0.0D+0
            else if (iz.eq.1) then
               rectangle(1,3)=e_ext+az
            else
               rectangle(1,3)=e_ext+az*iz+e_in*(iz-1)
            endif
            rectangle(2,1)=rectangle(1,1)
            if ((iz.eq.0).or.(iz.eq.Nz)) then
               rectangle(2,3)=rectangle(1,3)+e_ext
            else
               rectangle(2,3)=rectangle(1,3)+e_in
            endif
            rectangle(3,1)=rectangle(2,1)+ax
            rectangle(3,3)=rectangle(2,3)
            rectangle(4,1)=rectangle(3,1)
            rectangle(4,3)=rectangle(1,3)
            call add_face_with_rectangle(dim,rectangle,
     &           Nv01,Nf01,v01,f01)
         enddo                  ! iz
      enddo                     ! iy
c     -------------------------------------------------------------------------
c     Top face
      Nv02=0
      Nf02=0
      rectangle(1,1)=0.0D+0
      rectangle(1,2)=0.0D+0
      rectangle(2,1)=rectangle(1,1)+Lx
      rectangle(2,2)=rectangle(1,2)
      rectangle(3,1)=rectangle(2,1)
      rectangle(3,2)=rectangle(2,2)+Ly
      rectangle(4,1)=rectangle(1,1)
      rectangle(4,2)=rectangle(3,2)
      do i=1,4
         rectangle(i,3)=Lz
      enddo ! i 
      call add_face_with_rectangle(dim,rectangle,
     &     Nv02,Nf02,v02,f02)

      return
      end

      

      subroutine roof01(dim,ax,ay,az,e_in,e_ext,Nx,Ny,Nz,
     &     roof_height,roof_thickness,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02,
     &     Vroof_solid,Vroof_air)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c
c     Purpose: to provide the trianglemesh for a type 1 roof: 4 sides
c     that reunite on a line that has the same length as the building
c     
c     The thickness of the roof is set by parameter "roof_thickness" in parameters.inc
c     
c     Input:
c       + dim: dimension of space
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + az: dimension of a single cubic cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + roof_height: height of the roof (m)
c       + roof_thickness: thickness of the roof (m)
c
c     Output:
c       + Nv01, Nf01, v01, f01: roof / internal_air interface
c       + Nv02, Nf02, v02, f02: roof / atmosphere interface
c       + Vroof_solid: volume of solid for the roof
c       + Vroof_air: volume of internal air in the attic
c
c     I/O
      integer dim
      double precision ax,ay,az
      double precision e_in,e_ext
      integer Nx,Ny,Nz
      double precision roof_height
      double precision roof_thickness
      integer Nv01
      integer Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02
      integer Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      double precision Vroof_solid,Vroof_air
c     temp
      integer iface,j
      double precision Lx,Ly,Lz
      integer Nv_enc,Nf_enc
      double precision v_enc(1:Nv_so_mx,1:Ndim_mx)
      integer f_enc(1:Nf_so_mx,1:3)
      double precision Vtotal
      double precision volume_enc
c     label
      character*(Nchar_mx) label
      label='subroutine roof1'
      
      call total_length(Nx,ax,e_in,e_ext,Lx)
      call total_length(Ny,ay,e_in,e_ext,Ly)
      call total_length(Nz,az,e_in,e_ext,Lz)
      Nv01=6
      if (Nv01.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv01=',Nv01
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
c     node 1
      v01(1,1)=0.0D+0
      v01(1,2)=0.0D+0
      v01(1,3)=Lz
c     node 2
      v01(2,1)=Lx
      v01(2,2)=0.0D+0
      v01(2,3)=Lz
c     node 3
      v01(3,1)=0.0D+0
      v01(3,2)=Ly/2.0D+0
      v01(3,3)=Lz+roof_height
c     node 4
      v01(4,1)=Lx
      v01(4,2)=Ly/2.0D+0
      v01(4,3)=Lz+roof_height
c     node 5
      v01(5,1)=0.0D+0
      v01(5,2)=Ly
      v01(5,3)=Lz
c     node 6
      v01(6,1)=Lx
      v01(6,2)=Ly
      v01(6,3)=Lz
      
      Nf01=6
      if (Nf01.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      f01(1,1)=1
      f01(1,2)=4
      f01(1,3)=2
c     face 2
      f01(2,1)=1
      f01(2,2)=3
      f01(2,3)=4
c     face 2
      f01(3,1)=3
      f01(3,2)=6
      f01(3,3)=4
c     face 4
      f01(4,1)=3
      f01(4,2)=5
      f01(4,3)=6
c     face 5
      f01(5,1)=2
      f01(5,2)=4
      f01(5,3)=6
c     face 6
      f01(6,1)=1
      f01(6,2)=5
      f01(6,3)=3

      Nv02=10
      if (Nv02.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv02=',Nv02
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
c     node 1
      do j=1,dim
         v02(1,j)=v01(1,j)
      enddo                     ! j
c     node 2
      do j=1,dim
         v02(2,j)=v01(2,j)
      enddo                     ! j
c     node 3
      do j=1,dim
         v02(3,j)=v01(5,j)
      enddo                     ! j
c     node 4
      do j=1,dim
         v02(4,j)=v01(6,j)
      enddo                     ! j
c     node 5
      v02(5,1)=v01(1,1)-roof_thickness
      v02(5,2)=v01(1,2)-roof_thickness
      v02(5,3)=v01(1,3)
c     node 6
      v02(6,1)=v01(2,1)+roof_thickness
      v02(6,2)=v01(2,2)-roof_thickness
      v02(6,3)=v01(2,3)
c     node 7
      v02(7,1)=v01(3,1)-roof_thickness
      v02(7,2)=v01(3,2)
      v02(7,3)=v01(3,3)+roof_thickness
c     node 8
      v02(8,1)=v01(4,1)+roof_thickness
      v02(8,2)=v01(4,2)
      v02(8,3)=v01(4,3)+roof_thickness
c     node 9
      v02(9,1)=v01(5,1)-roof_thickness
      v02(9,2)=v01(5,2)+roof_thickness
      v02(9,3)=v01(5,3)
c     node 10
      v02(10,1)=v01(6,1)+roof_thickness
      v02(10,2)=v01(6,2)+roof_thickness
      v02(10,3)=v01(6,3)

      Nf02=14
      if (Nf02.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf02=',Nf02
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      f02(1,1)=5
      f02(1,2)=6
      f02(1,3)=8
c     face 2
      f02(2,1)=5
      f02(2,2)=8
      f02(2,3)=7
c     face 3
      f02(3,1)=7
      f02(3,2)=8
      f02(3,3)=10
c     face 4
      f02(4,1)=7
      f02(4,2)=10
      f02(4,3)=9
c     face 5
      f02(5,1)=6
      f02(5,2)=10
      f02(5,3)=8
c     face 6
      f02(6,1)=5
      f02(6,2)=7
      f02(6,3)=9
c     face 7
      f02(7,1)=1
      f02(7,2)=2
      f02(7,3)=6
c     face 8
      f02(8,1)=1
      f02(8,2)=6
      f02(8,3)=5
c     face 9
      f02(9,1)=2
      f02(9,2)=10
      f02(9,3)=6
c     face 10
      f02(10,1)=2
      f02(10,2)=4
      f02(10,3)=10
c     face 11
      f02(11,1)=4
      f02(11,2)=3
      f02(11,3)=10
c     face 12
      f02(12,1)=3
      f02(12,2)=9
      f02(12,3)=10
c     face 13
      f02(13,1)=1
      f02(13,2)=5
      f02(13,3)=3
c     face 14
      f02(14,1)=3
      f02(14,2)=5
      f02(14,3)=9

c     volumes:
c     + Attic internal air
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=1
      f_enc(Nf_enc,2)=2
      f_enc(Nf_enc,3)=5
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=5
      f_enc(Nf_enc,2)=1
      f_enc(Nf_enc,3)=6
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vroof_air)
c     + Total
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv02,Nf02,v02,f02,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      call invert_normals(Nf_enc,f_enc)
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=1
      f_enc(Nf_enc,2)=2
      f_enc(Nf_enc,3)=3
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=3
      f_enc(Nf_enc,2)=1
      f_enc(Nf_enc,3)=4
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vtotal)
c     + Roof (solid)
      Vroof_solid=Vtotal-Vroof_air
      if (Vroof_solid.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Vroof_solid=',Vroof_solid
         write(*,*) 'Vtotal=',Vtotal
         write(*,*) 'Vroof_air=',Vroof_air
         stop
      endif
      
      return
      end

      

      subroutine roof02(dim,ax,ay,az,e_in,e_ext,Nx,Ny,Nz,
     &     roof_height,roof_thickness,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02,
     &     Vroof_solid,Vroof_air)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c
c     Purpose: to provide the trianglemesh for a type 2 roof: 4 sides
c     that reunite on a line that has a length of roof_length_clp x length of the building
c     
c     The thickness of the roof is set by parameter "roof_thickness" in parameters.inc
c     The length of the central line is set by parameter "roof_length_clp" in parameter.inc
c     
c     Input:
c       + dim: dimension of space
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + az: dimension of a single cubic cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + roof_height: height of the roof (m)
c       + roof_thickness: thickness of the roof (m)
c
c     Output:
c       + Nv01, Nf01, v01, f01: roof / internal_air interface
c       + Nv02, Nf02, v02, f02: roof / atmosphere interface
c       + Vroof_solid: volume of solid for the roof
c       + Vroof_air: volume of internal air in the attic
c
c     I/O
      integer dim
      double precision ax,ay,az
      double precision e_in,e_ext
      integer Nx,Ny,Nz
      double precision roof_height
      double precision roof_thickness
      integer Nv01
      integer Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02
      integer Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      double precision Vroof_solid,Vroof_air
c     temp
      integer iface,j
      double precision Lx,Ly,Lz
      integer Nv_enc,Nf_enc
      double precision v_enc(1:Nv_so_mx,1:Ndim_mx)
      integer f_enc(1:Nf_so_mx,1:3)
      double precision Vtotal
      double precision volume_enc
c     label
      character*(Nchar_mx) label
      label='subroutine roof2'
      
      call total_length(Nx,ax,e_in,e_ext,Lx)
      call total_length(Ny,ay,e_in,e_ext,Ly)
      call total_length(Nz,az,e_in,e_ext,Lz)
      Nv01=6
      if (Nv01.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv01=',Nv01
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
c     node 1
      v01(1,1)=0.0D+0
      v01(1,2)=0.0D+0
      v01(1,3)=Lz
c     node 2
      v01(2,1)=Lx
      v01(2,2)=0.0D+0
      v01(2,3)=Lz
c     node 3
      v01(3,1)=Lx*(1.0D+0-roof_length_clp)/2.0D+0
      v01(3,2)=Ly/2.0D+0
      v01(3,3)=Lz+roof_height
c     node 4
      v01(4,1)=v01(3,1)+Lx*roof_length_clp
      v01(4,2)=v01(3,2)
      v01(4,3)=v01(3,3)
c     node 5
      v01(5,1)=0.0D+0
      v01(5,2)=Ly
      v01(5,3)=Lz
c     node 6
      v01(6,1)=Lx
      v01(6,2)=Ly
      v01(6,3)=Lz
      
      Nf01=6
      if (Nf01.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      f01(1,1)=1
      f01(1,2)=4
      f01(1,3)=2
c     face 2
      f01(2,1)=1
      f01(2,2)=3
      f01(2,3)=4
c     face 2
      f01(3,1)=3
      f01(3,2)=6
      f01(3,3)=4
c     face 4
      f01(4,1)=3
      f01(4,2)=5
      f01(4,3)=6
c     face 5
      f01(5,1)=2
      f01(5,2)=4
      f01(5,3)=6
c     face 6
      f01(6,1)=1
      f01(6,2)=5
      f01(6,3)=3

      Nv02=10
      if (Nv02.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv02=',Nv02
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
c     node 1
      do j=1,dim
         v02(1,j)=v01(1,j)
      enddo                     ! j
c     node 2
      do j=1,dim
         v02(2,j)=v01(2,j)
      enddo                     ! j
c     node 3
      do j=1,dim
         v02(3,j)=v01(5,j)
      enddo                     ! j
c     node 4
      do j=1,dim
         v02(4,j)=v01(6,j)
      enddo                     ! j
c     node 5
      v02(5,1)=v01(1,1)-roof_thickness
      v02(5,2)=v01(1,2)-roof_thickness
      v02(5,3)=v01(1,3)
c     node 6
      v02(6,1)=v01(2,1)+roof_thickness
      v02(6,2)=v01(2,2)-roof_thickness
      v02(6,3)=v01(2,3)
c     node 7
      v02(7,1)=v01(3,1)-roof_thickness
      v02(7,2)=v01(3,2)
      v02(7,3)=v01(3,3)+roof_thickness
c     node 8
      v02(8,1)=v01(4,1)+roof_thickness
      v02(8,2)=v01(4,2)
      v02(8,3)=v01(4,3)+roof_thickness
c     node 9
      v02(9,1)=v01(5,1)-roof_thickness
      v02(9,2)=v01(5,2)+roof_thickness
      v02(9,3)=v01(5,3)
c     node 10
      v02(10,1)=v01(6,1)+roof_thickness
      v02(10,2)=v01(6,2)+roof_thickness
      v02(10,3)=v01(6,3)

      Nf02=14
      if (Nf02.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf02=',Nf02
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      f02(1,1)=5
      f02(1,2)=6
      f02(1,3)=8
c     face 2
      f02(2,1)=5
      f02(2,2)=8
      f02(2,3)=7
c     face 3
      f02(3,1)=7
      f02(3,2)=8
      f02(3,3)=10
c     face 4
      f02(4,1)=7
      f02(4,2)=10
      f02(4,3)=9
c     face 5
      f02(5,1)=6
      f02(5,2)=10
      f02(5,3)=8
c     face 6
      f02(6,1)=5
      f02(6,2)=7
      f02(6,3)=9
c     face 7
      f02(7,1)=1
      f02(7,2)=2
      f02(7,3)=6
c     face 8
      f02(8,1)=1
      f02(8,2)=6
      f02(8,3)=5
c     face 9
      f02(9,1)=2
      f02(9,2)=10
      f02(9,3)=6
c     face 10
      f02(10,1)=2
      f02(10,2)=4
      f02(10,3)=10
c     face 11
      f02(11,1)=4
      f02(11,2)=3
      f02(11,3)=10
c     face 12
      f02(12,1)=3
      f02(12,2)=9
      f02(12,3)=10
c     face 13
      f02(13,1)=1
      f02(13,2)=5
      f02(13,3)=3
c     face 14
      f02(14,1)=3
      f02(14,2)=5
      f02(14,3)=9
      
c     volumes:
c     + Attic internal air
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=1
      f_enc(Nf_enc,2)=2
      f_enc(Nf_enc,3)=5
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=5
      f_enc(Nf_enc,2)=1
      f_enc(Nf_enc,3)=6
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vroof_air)
c     + Total
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv02,Nf02,v02,f02,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      call invert_normals(Nf_enc,f_enc)
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=1
      f_enc(Nf_enc,2)=2
      f_enc(Nf_enc,3)=3
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=3
      f_enc(Nf_enc,2)=1
      f_enc(Nf_enc,3)=4
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vtotal)
c     + Roof (solid)
      Vroof_solid=Vtotal-Vroof_air
      if (Vroof_solid.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Vroof_solid=',Vroof_solid
         write(*,*) 'Vtotal=',Vtotal
         write(*,*) 'Vroof_air=',Vroof_air
         stop
      endif
      
      return
      end

      

      subroutine roof03(dim,ax,ay,az,e_in,e_ext,Nx,Ny,Nz,
     &     roof_height,roof_thickness,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02,
     &     Vroof_solid,Vroof_air)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c
c     Purpose: to provide the trianglemesh for a type 3 roof: 4 sides
c     that reunite on a central point
c     
c     The thickness of the roof is set by parameter "roof_thickness" in parameters.inc
c     
c     Input:
c       + dim: dimension of space
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + az: dimension of a single cubic cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + roof_height: height of the roof (m)
c       + roof_thickness: thickness of the roof (m)
c
c     Output:
c       + Nv01, Nf01, v01, f01: roof / internal_air interface
c       + Nv02, Nf02, v02, f02: roof / atmosphere interface
c       + Vroof_solid: volume of solid for the roof
c       + Vroof_air: volume of internal air in the attic
c
c     I/O
      integer dim
      double precision ax,ay,az
      double precision e_in,e_ext
      integer Nx,Ny,Nz
      double precision roof_height
      double precision roof_thickness
      integer Nv01
      integer Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02
      integer Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      double precision Vroof_solid,Vroof_air
c     temp
      integer iface,j
      double precision Lx,Ly,Lz
      integer Nv_enc,Nf_enc
      double precision v_enc(1:Nv_so_mx,1:Ndim_mx)
      integer f_enc(1:Nf_so_mx,1:3)
      double precision Vtotal
      double precision volume_enc
c     label
      character*(Nchar_mx) label
      label='subroutine roof3'
      
      call total_length(Nx,ax,e_in,e_ext,Lx)
      call total_length(Ny,ay,e_in,e_ext,Ly)
      call total_length(Nz,az,e_in,e_ext,Lz)
      Nv01=5
      if (Nv01.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv01=',Nv01
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
c     node 1
      v01(1,1)=0.0D+0
      v01(1,2)=0.0D+0
      v01(1,3)=Lz
c     node 2
      v01(2,1)=Lx
      v01(2,2)=0.0D+0
      v01(2,3)=Lz
c     node 3
      v01(3,1)=Lx/2.0D+0
      v01(3,2)=Ly/2.0D+0
      v01(3,3)=Lz+roof_height
c     node 4
      v01(4,1)=0.0D+0
      v01(4,2)=Ly
      v01(4,3)=Lz
c     node 5
      v01(5,1)=Lx
      v01(5,2)=Ly
      v01(5,3)=Lz
      
      Nf01=4
      if (Nf01.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      f01(1,1)=3
      f01(1,2)=2
      f01(1,3)=1
c     face 2
      f01(2,1)=5
      f01(2,2)=2
      f01(2,3)=3
c     face 2
      f01(3,1)=4
      f01(3,2)=5
      f01(3,3)=3
c     face 4
      f01(4,1)=1
      f01(4,2)=4
      f01(4,3)=3

      Nv02=9
      if (Nv02.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv02=',Nv02
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
c     node 1
      do j=1,dim
         v02(1,j)=v01(1,j)
      enddo                     ! j
c     node 2
      do j=1,dim
         v02(2,j)=v01(2,j)
      enddo                     ! j
c     node 3
      do j=1,dim
         v02(3,j)=v01(4,j)
      enddo                     ! j
c     node 4
      do j=1,dim
         v02(4,j)=v01(5,j)
      enddo                     ! j
c     node 5
      v02(5,1)=v01(1,1)-roof_thickness
      v02(5,2)=v01(1,2)-roof_thickness
      v02(5,3)=v01(1,3)
c     node 6
      v02(6,1)=v01(2,1)+roof_thickness
      v02(6,2)=v01(2,2)-roof_thickness
      v02(6,3)=v01(2,3)
c     node 7
      v02(7,1)=v01(3,1)
      v02(7,2)=v01(3,2)
      v02(7,3)=v01(3,3)+roof_thickness
c     node 8
      v02(8,1)=v01(4,1)-roof_thickness
      v02(8,2)=v01(4,2)+roof_thickness
      v02(8,3)=v01(4,3)
c     node 9
      v02(9,1)=v01(5,1)+roof_thickness
      v02(9,2)=v01(5,2)+roof_thickness
      v02(9,3)=v01(5,3)

      Nf02=12
      if (Nf02.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf02=',Nf02
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      f02(1,1)=5
      f02(1,2)=6
      f02(1,3)=7
c     face 2
      f02(2,1)=6
      f02(2,2)=9
      f02(2,3)=7
c     face 3
      f02(3,1)=9
      f02(3,2)=8
      f02(3,3)=7
c     face 4
      f02(4,1)=8
      f02(4,2)=5
      f02(4,3)=7
c     face 5
      f02(5,1)=1
      f02(5,2)=2
      f02(5,3)=6
c     face 6
      f02(6,1)=1
      f02(6,2)=6
      f02(6,3)=5
c     face 7
      f02(7,1)=9
      f02(7,2)=6
      f02(7,3)=2
c     face 8
      f02(8,1)=9
      f02(8,2)=2
      f02(8,3)=4
c     face 9
      f02(9,1)=4
      f02(9,2)=3
      f02(9,3)=9
c     face 10
      f02(10,1)=3
      f02(10,2)=8
      f02(10,3)=9
c     face 11
      f02(11,1)=8
      f02(11,2)=3
      f02(11,3)=5
c     face 12
      f02(12,1)=5
      f02(12,2)=3
      f02(12,3)=1

c     volumes:
c     + Attic internal air
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=1
      f_enc(Nf_enc,2)=2
      f_enc(Nf_enc,3)=4
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=4
      f_enc(Nf_enc,2)=1
      f_enc(Nf_enc,3)=5
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vroof_air)
c     + Total
      Nv_enc=0
      Nf_enc=0
      call add_obj_to_obj(dim,Nv02,Nf02,v02,f02,
     &     Nv_enc,Nf_enc,v_enc,f_enc)
      call invert_normals(Nf_enc,f_enc)
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=1
      f_enc(Nf_enc,2)=2
      f_enc(Nf_enc,3)=3
      Nf_enc=Nf_enc+1
      f_enc(Nf_enc,1)=3
      f_enc(Nf_enc,2)=1
      f_enc(Nf_enc,3)=4
      call enclosure_volume(dim,Nv_enc,Nf_enc,v_enc,f_enc,Vtotal)
c     + Roof (solid)
      Vroof_solid=Vtotal-Vroof_air
      if (Vroof_solid.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Vroof_solid=',Vroof_solid
         write(*,*) 'Vtotal=',Vtotal
         write(*,*) 'Vroof_air=',Vroof_air
         stop
      endif
      
      return
      end



      subroutine base(dim,ax,ay,e_in,e_ext,Nx,Ny,Nz,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the trianglemesh for a cubic cell
c
c     Input:
c       + dim: dimension of space
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      double precision ax,ay
      double precision e_in,e_ext
      integer Nx,Ny,Nz
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer iface
      double precision Lx,Ly
c     label
      character*(Nchar_mx) label
      label='subroutine base'

      Nnode=4
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif

      call total_length(Nx,ax,e_in,e_ext,Lx)
      call total_length(Ny,ay,e_in,e_ext,Ly)

c     Coordinates the "Nnode" nodes
c     node 1
      vertices(1,1)=0.0D+0
      vertices(1,2)=0.0D+0
      vertices(1,3)=0.0D+0
c     node 2
      vertices(2,1)=Lx
      vertices(2,2)=vertices(1,2)
      vertices(2,3)=vertices(1,3)
c     node 3
      vertices(3,1)=vertices(2,1)
      vertices(3,2)=Ly
      vertices(3,3)=vertices(1,3)
c     node 4
      vertices(4,1)=vertices(1,1)
      vertices(4,2)=vertices(3,2)
      vertices(4,3)=vertices(1,3)

c     Indexes of each node that belong to the cells
      Nface=2
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      faces(1,1)=1
      faces(1,2)=2
      faces(1,3)=3
c     face 2
      faces(2,1)=3
      faces(2,2)=4
      faces(2,3)=1

      return
      end



      subroutine cubic_cell(dim,Nx,Ny,Nz,ix,iy,iz,
     &     a,e,hff,wwf,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the trianglemesh for a cubic cell
c
c     Input:
c       + dim: dimension of space
c       + Nx: number of cubic cells in X direction for each building
c       + Ny: number of cubic cells in Y direction for each building
c       + Nz: number of cubic cells in Z direction for each building
c       + (ix,iy,iz): index of the cell along each axis
c       + a: dimension of a single cubic cell (micrometers)
c       + e: thickness of solid boundaries (micrometers)
c       + hff: fraction of the floor used for heating
c       + wwf: fraction of the wall used for windows
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      integer Nx,Ny,Nz
      integer ix,iy,iz
      double precision a
      double precision e
      double precision hff
      double precision wwf
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j
      double precision box(1:8,1:Ndim_mx)
      double precision rectangle(1:4,1:Ndim_mx)
      integer iface
      logical window_is_present
c     label
      character*(Nchar_mx) label
      label='subroutine cubic_cell'

      call cubic_box(ix,iy,iz,a,e,box)

      Nnode=0
      Nface=0
c     Bottom face
c     Debug
c      write(*,*) '+ bottom face'
c     Debug
      do j=1,dim
         rectangle(1,j)=box(1,j)
         rectangle(2,j)=box(2,j)
         rectangle(3,j)=box(3,j)
         rectangle(4,j)=box(4,j)
      enddo                     ! j
      call add_face_with_rectangle_and_hole(dim,rectangle,hff,
     &     Nnode,Nface,vertices,faces)
      
c     North face
c     Debug
c      write(*,*) '+ north face'
c     Debug
      do j=1,dim
         rectangle(1,j)=box(1,j)
         rectangle(2,j)=box(4,j)
         rectangle(3,j)=box(8,j)
         rectangle(4,j)=box(5,j)
      enddo                     ! j
      call north_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      if (window_is_present) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif

c     South face
c     Debug
c      write(*,*) '+ south face'
c     Debug
      do j=1,dim
         rectangle(1,j)=box(2,j)
         rectangle(2,j)=box(6,j)
         rectangle(3,j)=box(7,j)
         rectangle(4,j)=box(3,j)
      enddo                     ! j
      call south_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      if (window_is_present) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif
      
c     West face
c     Debug
c      write(*,*) '+ west face'
c     Debug
      do j=1,dim
         rectangle(1,j)=box(1,j)
         rectangle(2,j)=box(5,j)
         rectangle(3,j)=box(6,j)
         rectangle(4,j)=box(2,j)
      enddo                     ! j
      call west_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      if (window_is_present) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif

c     East face
c     Debug
c      write(*,*) '+ east face'
c     Debug
      do j=1,dim
         rectangle(1,j)=box(4,j)
         rectangle(2,j)=box(3,j)
         rectangle(3,j)=box(7,j)
         rectangle(4,j)=box(8,j)
      enddo                     ! j
      call east_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      if (window_is_present) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif

c     Top face
c     Debug
c      write(*,*) '+ top face'
c     Debug
      do j=1,dim
         rectangle(1,j)=box(5,j)
         rectangle(2,j)=box(8,j)
         rectangle(3,j)=box(7,j)
         rectangle(4,j)=box(6,j)
      enddo                     ! j
      call add_face_with_rectangle(dim,rectangle,
     &     Nnode,Nface,vertices,faces)
 666  continue
      
      return
      end



      subroutine rectangular_cell(dim,Nx,Ny,Nz,ix,iy,iz,
     &     ax,ay,az,e_in,e_ext,wwf,draw_window,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the trianglemesh for a rectangular cell (parallellepipedic)
c
c     Input:
c       + dim: dimension of space
c       + Nx: number of cubic cells in X direction
c       + Ny: number of cubic cells in Y direction
c       + Nz: number of cubic cells in Z direction
c       + (ix,iy,iz): index of the cell along each axis
c       + ax: dimension of a single cubic cell along the X axis (m)
c       + ay: dimension of a single cubic cell along the Y axis (m)
c       + az: dimension of a single cubic cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + wwf: fraction of the wall used for windows
c       + draw_window: when set to T, the window will be drawn
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      integer Nx,Ny,Nz
      integer ix,iy,iz
      double precision ax,ay,az
      double precision e_in,e_ext
      double precision wwf
      logical draw_window
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j
      double precision box(1:8,1:Ndim_mx)
      double precision rectangle(1:4,1:Ndim_mx)
      integer iface
      logical window_is_present
c     label
      character*(Nchar_mx) label
      label='subroutine rectangular_cell'

      call rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)
      
      Nnode=0
      Nface=0
c     Bottom face
      do j=1,dim
         rectangle(1,j)=box(1,j)
         rectangle(2,j)=box(2,j)
         rectangle(3,j)=box(3,j)
         rectangle(4,j)=box(4,j)
      enddo                     ! j
      call add_face_with_rectangle(dim,rectangle,
     &     Nnode,Nface,vertices,faces)
      
c     North face
      do j=1,dim
         rectangle(1,j)=box(1,j)
         rectangle(2,j)=box(4,j)
         rectangle(3,j)=box(8,j)
         rectangle(4,j)=box(5,j)
      enddo                     ! j
      call north_face_window(Nx,Ny,Nz,ix,iy,iz,window_is_present)
      if ((window_is_present).and.(draw_window)) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif

c     South face
      do j=1,dim
         rectangle(1,j)=box(2,j)
         rectangle(2,j)=box(6,j)
         rectangle(3,j)=box(7,j)
         rectangle(4,j)=box(3,j)
      enddo                     ! j
      call south_face_window(Nx,Ny,Nz,ix,iy,iz,window_is_present)
      if ((window_is_present).and.(draw_window)) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif
      
c     West face
      do j=1,dim
         rectangle(1,j)=box(1,j)
         rectangle(2,j)=box(5,j)
         rectangle(3,j)=box(6,j)
         rectangle(4,j)=box(2,j)
      enddo                     ! j
      call west_face_window(Nx,Ny,Nz,ix,iy,iz,window_is_present)
      if ((window_is_present).and.(draw_window)) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif

c     East face
      do j=1,dim
         rectangle(1,j)=box(4,j)
         rectangle(2,j)=box(3,j)
         rectangle(3,j)=box(7,j)
         rectangle(4,j)=box(8,j)
      enddo                     ! j
      call east_face_window(Nx,Ny,Nz,ix,iy,iz,window_is_present)
      if ((window_is_present).and.(draw_window)) then
         call add_face_with_rectangle_and_hole(dim,rectangle,wwf,
     &        Nnode,Nface,vertices,faces)
      else
         call add_face_with_rectangle(dim,rectangle,
     &        Nnode,Nface,vertices,faces)
      endif

c     Top face
      do j=1,dim
         rectangle(1,j)=box(5,j)
         rectangle(2,j)=box(8,j)
         rectangle(3,j)=box(7,j)
         rectangle(4,j)=box(6,j)
      enddo                     ! j
      call add_face_with_rectangle(dim,rectangle,
     &     Nnode,Nface,vertices,faces)
      
      return
      end



      subroutine obj_ground(dim,Xmin,Xmax,Ymin,Ymax,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the trianglemesh for the ground
c
c     Input:
c       + dim: dimension of space
c       + Xmin: min X for the ground
c       + Xmax: max X for the ground
c       + Ymin: min Y for the ground
c       + Ymax: max Y for the ground
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      double precision Xmin,Xmax
      double precision Ymin,Ymax
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j
      integer iface
c     label
      character*(Nchar_mx) label
      label='subroutine obj_ground'

      Nnode=4
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif

c     Coordinates the "Nnode" nodes
c     node 1
      vertices(1,1)=Xmin
      vertices(1,2)=Ymin
      vertices(1,3)=0.0D+0
c     node 2
      vertices(2,1)=Xmax
      vertices(2,2)=vertices(1,2)
      vertices(2,3)=vertices(1,3)
c     node 3
      vertices(3,1)=vertices(2,1)
      vertices(3,2)=Ymax
      vertices(3,3)=vertices(1,3)
c     node 4
      vertices(4,1)=vertices(1,1)
      vertices(4,2)=vertices(3,2)
      vertices(4,3)=vertices(1,3)
c     Debug
c      do i=1,8
c         write(*,*) 'vertices(',i,')=',(vertices(i,j),j=1,3)
c      enddo ! i
c     Debug

c     Indexes of each node that belong to the cells
      Nface=2
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      faces(1,1)=1
      faces(1,2)=2
      faces(1,3)=3
c     face 2
      faces(2,1)=1
      faces(2,2)=3
      faces(2,3)=4
      
      return
      end



      subroutine obj_segment(dim,x1,x2,x3,x4,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c
c     Purpose: to provide the trianglemesh for a road segment
c
c     Input:
c       + dim: dimension of space
c       + x1, x2, x3, x4: the 4 positions defining the road segment, at ground level
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j
      integer iface
c     label
      character*(Nchar_mx) label
      label='subroutine obj_segment'

      Nnode=8
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif

c     Coordinates the "Nnode" nodes
      do i=1,dim
         vertices(1,i)=x1(i)
         vertices(2,i)=x2(i)
         vertices(3,i)=x3(i)
         vertices(4,i)=x4(i)
         vertices(5,i)=x1(i)
         vertices(6,i)=x2(i)
         vertices(7,i)=x3(i)
         vertices(8,i)=x4(i)
      enddo                     ! i
      do i=5,8
         vertices(i,3)=vertices(i,3)+road_thickness
      enddo                     ! i

c     Indexes of each node that belong to the cells
      Nface=10
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      faces(1,1)=1
      faces(1,2)=2
      faces(1,3)=6
c     face 2
      faces(2,1)=1
      faces(2,2)=6
      faces(2,3)=5
c     face 3
      faces(3,1)=2
      faces(3,2)=3
      faces(3,3)=7
c     face 4
      faces(4,1)=2
      faces(4,2)=7
      faces(4,3)=6
c     face 5
      faces(5,1)=3
      faces(5,2)=4
      faces(5,3)=8
c     face 6
      faces(6,1)=3
      faces(6,2)=8
      faces(6,3)=7
c     face 7
      faces(7,1)=4
      faces(7,2)=1
      faces(7,3)=5
c     face 8
      faces(8,1)=4
      faces(8,2)=5
      faces(8,3)=8
c     face 9
      faces(9,1)=5
      faces(9,2)=6
      faces(9,3)=7
c     face 10
      faces(10,1)=5
      faces(10,2)=7
      faces(10,3)=8
      
      return
      end



      subroutine obj_deck(dim,length,deck_width,deck_height,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c
c     Purpose: to provide the trianglemesh for a bridge's deck
c
c     Input:
c       + dim: dimension of space
c       + length: total length of the bridge [m]
c       + deck_width: width of the deck [m]
c       + deck_height: height of the deck [m]
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      double precision length
      double precision deck_width
      double precision deck_height
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j
      integer iface
c     label
      character*(Nchar_mx) label
      label='subroutine obj_deck'

      Nnode=16
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif

c     Coordinates the "Nnode" nodes
c     node 1
      vertices(1,1)=0.0D+0
      vertices(1,2)=0.0D+0
      vertices(1,3)=0.0D+0
c     node 2
      vertices(2,1)=length*(1.0D+0-bridge_length_clp)/2.0D+0
      vertices(2,2)=0.0D+0
      vertices(2,3)=deck_height
c     node 3
      vertices(3,1)=length*(1.0D+0+bridge_length_clp)/2.0D+0
      vertices(3,2)=0.0D+0
      vertices(3,3)=deck_height
c     node 4
      vertices(4,1)=length
      vertices(4,2)=0.0D+0
      vertices(4,3)=0.0D+0
c     node 5
      vertices(5,1)=vertices(4,1)
      vertices(5,2)=vertices(4,2)+deck_width
      vertices(5,3)=vertices(4,3)
c     node 6
      vertices(6,1)=vertices(3,1)
      vertices(6,2)=vertices(3,2)+deck_width
      vertices(6,3)=vertices(3,3)
c     node 7
      vertices(7,1)=vertices(2,1)
      vertices(7,2)=vertices(2,2)+deck_width
      vertices(7,3)=vertices(2,3)
c     node 8
      vertices(8,1)=vertices(1,1)
      vertices(8,2)=vertices(1,2)+deck_width
      vertices(8,3)=vertices(1,3)
c     nodes 9-16
      do i=9,16
         vertices(i,1)=vertices(i-8,1)
         vertices(i,2)=vertices(i-8,2)
         vertices(i,3)=vertices(i-8,3)+road_thickness
      enddo                     ! i

c     Indexes of each node that belong to the cells
      Nface=28
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      faces(1,1)=1
      faces(1,2)=2
      faces(1,3)=9
c     face 2
      faces(2,1)=2
      faces(2,2)=10
      faces(2,3)=9
c     face 3
      faces(3,1)=2
      faces(3,2)=3
      faces(3,3)=11
c     face 4
      faces(4,1)=2
      faces(4,2)=11
      faces(4,3)=10
c     face 5
      faces(5,1)=3
      faces(5,2)=4
      faces(5,3)=12
c     face 6
      faces(6,1)=3
      faces(6,2)=12
      faces(6,3)=11
c     face 7
      faces(7,1)=4
      faces(7,2)=5
      faces(7,3)=12
c     face 8
      faces(8,1)=5
      faces(8,2)=13
      faces(8,3)=12
c     face 9
      faces(9,1)=5
      faces(9,2)=6
      faces(9,3)=13
c     face 10
      faces(10,1)=13
      faces(10,2)=6
      faces(10,3)=14
c     face 11
      faces(11,1)=6
      faces(11,2)=7
      faces(11,3)=14
c     face 12
      faces(12,1)=7
      faces(12,2)=15
      faces(12,3)=14
c     face 13
      faces(13,1)=15
      faces(13,2)=7
      faces(13,3)=8
c     face 14
      faces(14,1)=15
      faces(14,2)=8
      faces(14,3)=16
c     face 15
      faces(15,1)=8
      faces(15,2)=1
      faces(15,3)=9
c     face 16
      faces(16,1)=16
      faces(16,2)=8
      faces(16,3)=9
c     face 17
      faces(17,1)=9
      faces(17,2)=10
      faces(17,3)=16
c     face 18
      faces(18,1)=10
      faces(18,2)=15
      faces(18,3)=16
c     face 19
      faces(19,1)=10
      faces(19,2)=11
      faces(19,3)=14
c     face 20
      faces(20,1)=10
      faces(20,2)=14
      faces(20,3)=15
c     face 21
      faces(21,1)=11
      faces(21,2)=12
      faces(21,3)=13
c     face 22
      faces(22,1)=11
      faces(22,2)=13
      faces(22,3)=14
c     face 23
      faces(23,1)=1
      faces(23,2)=8
      faces(23,3)=2
c     face 24
      faces(24,1)=8
      faces(24,2)=7
      faces(24,3)=2
c     face 25
      faces(25,1)=2
      faces(25,2)=7
      faces(25,3)=6
c     face 26
      faces(26,1)=6
      faces(26,2)=3
      faces(26,3)=2
c     face 27
      faces(27,1)=5
      faces(27,2)=4
      faces(27,3)=3
c     face 28
      faces(28,1)=6
      faces(28,2)=5
      faces(28,3)=3
      
      return
      end



      subroutine obj_bridge_side(dim,length,deck_width,deck_height,
     &     side_width,side_height,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c
c     Purpose: to provide the trianglemesh for a bridge's deck
c
c     Input:
c       + dim: dimension of space
c       + legnth: total length of the bridge [m]
c       + deck_width: width of the deck [m]
c       + deck_height: height of the deck [m]
c       + side_width: width of the side [m]
c       + side_height: height of the side [m]
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      double precision length
      double precision deck_width
      double precision deck_height
      double precision side_width
      double precision side_height
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j
      integer iface
c     label
      character*(Nchar_mx) label
      label='subroutine obj_deck'

      Nnode=16
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
c     Coordinates the "Nnode" nodes
c     node 1
      vertices(1,1)=0.0D+0
      vertices(1,2)=0.0D+0
      vertices(1,3)=0.0D+0
c     node 2
      vertices(2,1)=length*(1.0D+0-bridge_length_clp)/2.0D+0
      vertices(2,2)=0.0D+0
      vertices(2,3)=deck_height
c     node 3
      vertices(3,1)=length*(1.0D+0+bridge_length_clp)/2.0D+0
      vertices(3,2)=0.0D+0
      vertices(3,3)=deck_height
c     node 4
      vertices(4,1)=length
      vertices(4,2)=0.0D+0
      vertices(4,3)=0.0D+0
c     node 5
      vertices(5,1)=vertices(4,1)
      vertices(5,2)=vertices(4,2)+side_width
      vertices(5,3)=vertices(4,3)
c     node 6
      vertices(6,1)=vertices(3,1)
      vertices(6,2)=vertices(3,2)+side_width
      vertices(6,3)=vertices(3,3)
c     node 7
      vertices(7,1)=vertices(2,1)
      vertices(7,2)=vertices(2,2)+side_width
      vertices(7,3)=vertices(2,3)
c     node 8
      vertices(8,1)=vertices(1,1)
      vertices(8,2)=vertices(1,2)+side_width
      vertices(8,3)=vertices(1,3)
c     move along Y
      do i=1,8
         vertices(i,2)=vertices(i,2)-side_width
      enddo                     ! i
c     nodes 9-16
      do i=9,16
         vertices(i,1)=vertices(i-8,1)
         vertices(i,2)=vertices(i-8,2)
         vertices(i,3)=vertices(i-8,3)+side_height
      enddo                     ! i

c     Indexes of each node that belong to the cells
      Nface=28
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
c     face 1
      faces(1,1)=1
      faces(1,2)=2
      faces(1,3)=9
c     face 2
      faces(2,1)=2
      faces(2,2)=10
      faces(2,3)=9
c     face 3
      faces(3,1)=2
      faces(3,2)=3
      faces(3,3)=11
c     face 4
      faces(4,1)=2
      faces(4,2)=11
      faces(4,3)=10
c     face 5
      faces(5,1)=3
      faces(5,2)=4
      faces(5,3)=12
c     face 6
      faces(6,1)=3
      faces(6,2)=12
      faces(6,3)=11
c     face 7
      faces(7,1)=4
      faces(7,2)=5
      faces(7,3)=12
c     face 8
      faces(8,1)=5
      faces(8,2)=13
      faces(8,3)=12
c     face 9
      faces(9,1)=5
      faces(9,2)=6
      faces(9,3)=13
c     face 10
      faces(10,1)=13
      faces(10,2)=6
      faces(10,3)=14
c     face 11
      faces(11,1)=6
      faces(11,2)=7
      faces(11,3)=14
c     face 12
      faces(12,1)=7
      faces(12,2)=15
      faces(12,3)=14
c     face 13
      faces(13,1)=15
      faces(13,2)=7
      faces(13,3)=8
c     face 14
      faces(14,1)=15
      faces(14,2)=8
      faces(14,3)=16
c     face 15
      faces(15,1)=8
      faces(15,2)=1
      faces(15,3)=9
c     face 16
      faces(16,1)=16
      faces(16,2)=8
      faces(16,3)=9
c     face 17
      faces(17,1)=9
      faces(17,2)=10
      faces(17,3)=16
c     face 18
      faces(18,1)=10
      faces(18,2)=15
      faces(18,3)=16
c     face 19
      faces(19,1)=10
      faces(19,2)=11
      faces(19,3)=14
c     face 20
      faces(20,1)=10
      faces(20,2)=14
      faces(20,3)=15
c     face 21
      faces(21,1)=11
      faces(21,2)=12
      faces(21,3)=13
c     face 22
      faces(22,1)=11
      faces(22,2)=13
      faces(22,3)=14
c     face 23
      faces(23,1)=1
      faces(23,2)=8
      faces(23,3)=2
c     face 24
      faces(24,1)=8
      faces(24,2)=7
      faces(24,3)=2
c     face 25
      faces(25,1)=2
      faces(25,2)=7
      faces(25,3)=6
c     face 26
      faces(26,1)=6
      faces(26,2)=3
      faces(26,3)=2
c     face 27
      faces(27,1)=5
      faces(27,2)=4
      faces(27,3)=3
c     face 28
      faces(28,1)=6
      faces(28,2)=5
      faces(28,3)=3

      return
      end



      subroutine obj_wband(dim,
     &     Nroad,iroad_width,draw_wbands_iroad,
     &     Nppt_iroad,iroad_central_track,
     &     iroad_mat,iwband_mat,
     &     iroad,iseg,
     &     p1,p2,x1,x2,x3,x4,n,n1,n2,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
      include 'parameters.inc'
c
c     Purpose: to provide the trianglemesh for the white bands of a road segment
c
c     Input:
c       + dim: dimension of space
c       + Nroad: number of roads
c       + iroad_width: width of current road [m]
c       + draw_wbands_iroad: T if drawing white bands is required
c       + Nppt_iroad: number of points for the description of the central track for current road
c       + iroad_central_track: list of positions that describe the central track of current road
c       + iroad_mat: material for current road
c       + iwband_mat: material for white bands
c       + iroad: index of current road
c       + iseg: segment index
c       + p1, p2: segment limit positions
c       + x1, x2, x3, x4: the 4 positions defining the road segment, at ground level
c       + n: normal for the current segment
c       + n1, n2: normals to use at both ends of the segment
c
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      integer Nroad
      double precision iroad_width
      logical draw_wbands_iroad
      integer Nppt_iroad
      double precision iroad_central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) iroad_mat
      character*(Nchar_mx) iwband_mat
      integer iroad,iseg
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      double precision p0j(1:Ndim_mx)
      double precision p1j(1:Ndim_mx)
      double precision p2j(1:Ndim_mx)
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision v3(1:Ndim_mx)
      double precision v4(1:Ndim_mx)
      double precision v5(1:Ndim_mx)
      double precision v6(1:Ndim_mx)
      double precision v7(1:Ndim_mx)
      double precision v8(1:Ndim_mx)
      double precision d(1:Ndim_mx)
      double precision p1p2(1:Ndim_mx)
      double precision p1jp2j(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision u_x1x2(1:Ndim_mx)
      double precision v1v2(1:Ndim_mx)
      double precision x3x4(1:Ndim_mx)
      double precision u_x3x4(1:Ndim_mx)
      double precision v3v4(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision u_tmp(1:Ndim_mx)
      double precision uj(1:Ndim_mx)
      double precision n_tmp(1:Ndim_mx)
      double precision nj(1:Ndim_mx)
      double precision ni_int(1:Ndim_mx)
      double precision nj_int(1:Ndim_mx)
      double precision n1j(1:Ndim_mx)
      double precision n2j(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      double precision t1(1:Ndim_mx)
      double precision t2(1:Ndim_mx)
      double precision t3(1:Ndim_mx)
      double precision t4(1:Ndim_mx)
      double precision t5(1:Ndim_mx)
      double precision r1(1:Ndim_mx)
      double precision r2(1:Ndim_mx)
      double precision Delta_n(1:Ndim_mx)
      double precision Delta_nj(1:Ndim_mx)
      double precision nr1(1:Ndim_mx)
      double precision nr2(1:Ndim_mx)
      double precision normal1(1:Ndim_mx)
      double precision normal2(1:Ndim_mx)
      double precision dist,d12,length,L0,L1,L2,Li,Lj
      double precision denom,k1,k2,sp,sp0,sp1,sp2
      double precision w1,w2,Delta_w,w
      double precision w1j,w2j,Delta_wj,wj
      double precision pint1(1:Ndim_mx)
      double precision pint2(1:Ndim_mx)
      integer i,j,isegment,jroad,jseg,Nseg_jroad,pass
      integer iface
      logical keep_going,intersection
      double precision jroad_width
      logical draw_wbands_jroad
      integer Nppt_jroad
      double precision jroad_central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) jroad_mat
      character*(Nchar_mx) jwband_mat
c     label
      character*(Nchar_mx) label
      label='subroutine obj_wband'

      call substract_vectors(dim,p2,p1,p1p2)
      call normalize_vector(dim,p1p2,u)
      call substract_vectors(dim,x2,x1,x1x2)
      call normalize_vector(dim,x1x2,u_x1x2)
      call substract_vectors(dim,x4,x3,x3x4)
      call normalize_vector(dim,x3x4,u_x3x4)
      call vector_length(dim,p1p2,L1)
      call substract_vectors(dim,n2,n1,t)
      call scalar_vector(dim,1.0D+0/L1,t,Delta_n)
         
      if ((iroad.eq.0).and.(iseg.eq.0)) then ! bridge
         intersection=.false.
         goto 112
      endif
      
c     w1 is the road "iroad" width @ p1
      call scalar_product(dim,n,n1,sp1)
      w1=iroad_width*sp1
c     w2 is the road "iroad" width @ p2
      call scalar_product(dim,n,n2,sp2)
      w2=iroad_width*sp2
c     Delta_w is the slope of road width
      Delta_w=(w2-w1)/L1
      
      do jroad=1,Nroad
         call read_road_data(dim,
     &        Nroad,jroad,jroad_width,draw_wbands_jroad,
     &        Nppt_jroad,jroad_central_track,
     &        jroad_mat,jwband_mat)
         Nseg_jroad=Nppt_jroad-1
         do jseg=1,Nseg_jroad
            intersection=.false.
c     Do not consider intersections with self, previous or next segment
            if ((jroad.eq.iroad).and.
     &           ((jseg.eq.iseg-1).or.
     &           (jseg.eq.iseg).or.
     &           (jseg.eq.iseg+1))) then
               goto 111
            endif
            p1j(1)=jroad_central_track(jseg,1)
            p1j(2)=jroad_central_track(jseg,2)
            p1j(3)=0.0D+0
            p2j(1)=jroad_central_track(jseg+1,1)
            p2j(2)=jroad_central_track(jseg+1,2)
            p2j(3)=0.0D+0
            call normal_for_segment(dim,p1j,p2j,nj)
            call substract_vectors(dim,p2j,p1j,p1jp2j)
            call vector_length(dim,p1jp2j,L2)
            call normalize_vector(dim,p1jp2j,uj)
            denom=u(1)*uj(2)-u(2)*uj(1)
            if (denom.ne.0.0D+0) then
               k1=(uj(2)*(p1j(1)-p1(1))+uj(1)*(p1(2)-p1j(2)))/denom
               if (uj(2).ne.0.0D+0) then
                  k2=(p1(2)-p1j(2)+k1*u(2))/uj(2)
               else
                  if (uj(1).eq.0.0D+0) then
                     call error(label)
                     write(*,*) 'uj(1)=',uj(1)
                     write(*,*) 'uj(2)=',uj(2)
                     stop
                  else
                     k2=(p1(1)-p1j(1)+k1*u(1))/uj(1)
                  endif         ! uj(1)=0
               endif            ! uj(2).ne.0
               if ((k1.gt.0.0D+0).and.(k1.lt.L1)
     &              .and.(k2.gt.0.0D+0).and.(k2.lt.L2)) then
                  call scalar_vector(dim,k1,u,t)
                  call add_vectors(dim,p1,t,pint1)
                  call scalar_vector(dim,k2,uj,t)
                  call add_vectors(dim,p1j,t,pint2)
                  call substract_vectors(dim,pint2,pint1,t)
                  call vector_length(dim,t,dist)
                  if (dist.le.1.0D-5) then
                     intersection=.true.
c     Debug
c                     write(*,*) 'intersection found for iroad=',
c     &                    iroad,' iseg=',iseg
c                     write(*,*) 'with: jroad=',jroad,' jseg=',jseg
c                     write(*,*) 'Xint=',(pint1(j),j=1,dim)
c     Debug
c     assumption: there can be only a intersection with a single other road segment
c     (no multiple intersections)
c     -----------------------------------------------------------------------------------------------------
c     computations for road "iroad":
                     call substract_vectors(dim,pint1,p1,t)
                     call vector_length(dim,t,Li) ! Li is the local abscissa of pint1 along (iroad,iseg)
                     call scalar_vector(dim,Li,Delta_n,t)
                     call add_vectors(dim,n1,t,ni_int) ! local normal @ Li
c     -----------------------------------------------------------------------------------------------------
c     computations for road "jroad":
c     + n1j is the normal @ p1j
                     if (jseg.eq.1) then
                        call copy_vector(dim,nj,n1j)
                     else       ! iseg>1
                        p0j(1)=jroad_central_track(jseg-1,1)
                        p0j(2)=jroad_central_track(jseg-1,2)
                        p0j(3)=0.0D+0
                        call normal_for_segment(dim,p0j,p1j,n_tmp)
                        call add_vectors(dim,n_tmp,nj,u_tmp)
                        call normalize_vector(dim,u_tmp,n1j)
                     endif      ! jseg=1
c     + n2j is the normal @ p2j
                     if (jseg.eq.Nseg_jroad) then
                        call copy_vector(dim,nj,n2j)
                     else       ! iseg>1
                        p0j(1)=jroad_central_track(jseg+2,1)
                        p0j(2)=jroad_central_track(jseg+2,2)
                        p0j(3)=0.0D+0
                        call normal_for_segment(dim,p2j,p0j,n_tmp)
                        call add_vectors(dim,n_tmp,nj,u_tmp)
                        call normalize_vector(dim,u_tmp,n2j)
                     endif      ! jseg=Nseg_jroad
c     w1j is the road "jroad" width @ p1j
                     call scalar_product(dim,nj,n1j,sp1)
                     w1j=jroad_width*sp1
c     w2j is the road "jroad" width @ p2j
                     call scalar_product(dim,nj,n2j,sp2)
                     w2j=jroad_width*sp2
c     Delta_wj is the slope of road "jroad" width
                     Delta_wj=(w2j-w1j)/L2
c     Lj: local abscissa of pint1 alors (jroad,jseg)
                     call substract_vectors(dim,pint1,p1j,t)
                     call vector_length(dim,t,Lj)
c     nj_int: local normal @ Lj
                     call substract_vectors(dim,n2j,n1j,t)
                     call scalar_vector(dim,1.0D+0/L2,t,Delta_nj)
                     call scalar_vector(dim,Lj,Delta_nj,t)
                     call add_vectors(dim,n1j,t,nj_int) ! local normal @ Lj
c     -----------------------------------------------------------------------------------------------------
                     goto 112
                  endif         ! dist<10^-5
               endif            ! k1>0 and k1<L1 and k2>0 and k2<L2
            endif               ! denom.ne.0
 111        continue
         enddo                  ! jseg
      enddo                     ! jroad
 112  continue
      
      Nnode=0
      Nface=0
c     lower continous band
      pass=0
 222  continue
      pass=pass+1
      if (intersection) then
         call scalar_product(dim,uj,n,sp)
         w=w1+Delta_w*Li        ! road "iroad" width @ pint1
         call scalar_vector(dim,w/(2.0D+0*dabs(sp)),uj,t1)
         if (sp.gt.0.0D+0) then
            call scalar_vector(dim,-1.0D+0,t1,t1)
         endif
         if (pass.eq.1) then
            call scalar_vector(dim,-1.0D+0,u,t2)
         else
            call copy_vector(dim,u,t2)
         endif                  ! pass
         wj=w1j+Delta_wj*Lj     ! road "jroad" width @ pint1
         call scalar_vector(dim,wj/(2.0D+0*dabs(sp)),t2,t3)
         call add_vectors(dim,pint1,t1,t4)
         if (pass.eq.1) then
            call copy_vector(dim,x1,v1)
            call add_vectors(dim,t4,t3,v2)
c            call copy_vector(dim,n1,normal1)
c            call copy_vector(dim,ni_int,normal2)
         else if (pass.eq.2) then
            call add_vectors(dim,t4,t3,v1)
            call copy_vector(dim,x2,v2)
c            call copy_vector(dim,ni_int,normal1)
c            call copy_vector(dim,n2,normal2)
         endif                  ! pass
         if (sp.lt.0.0D+0) then
            call scalar_vector(dim,-1.0D+0,uj,normal1)
         else
            call copy_vector(dim,uj,normal1)
         endif
         call copy_vector(dim,normal1,normal2)
         call substract_vectors(dim,v2,v1,t5)
         call vector_length(dim,t5,dist)
         call scalar_vector(dim,dist,u_x1x2,v1v2)
         if (pass.eq.1) then
            call add_vectors(dim,v1,v1v2,v2)
         else if (pass.eq.2) then
            call substract_vectors(dim,v2,v1v2,v1)
         endif                  ! pass
      else                      ! no intersection found
         call copy_vector(dim,x1,v1)
         call copy_vector(dim,x2,v2)
         call copy_vector(dim,n1,normal1)
         call copy_vector(dim,n2,normal2)
      endif                     ! intersection
      if ((.not.intersection).or.
     &     ((intersection).and.(dist.gt.0.0D+0))) then
         v1(3)=v1(3)+road_thickness
         v2(3)=v2(3)+road_thickness
         call scalar_vector(dim,wband_width,normal1,d)
         call add_vectors(dim,v1,d,v4)
         call scalar_vector(dim,wband_width,normal2,d)
         call add_vectors(dim,v2,d,v3)
         call copy_vector(dim,v1,v5)
         call copy_vector(dim,v2,v6)
         call copy_vector(dim,v3,v7)
         call copy_vector(dim,v4,v8)
         v5(3)=v5(3)+wband_thickness
         v6(3)=v6(3)+wband_thickness
         v7(3)=v7(3)+wband_thickness
         v8(3)=v8(3)+wband_thickness
         do i=1,dim
            vertices(Nnode+1,i)=v1(i)
            vertices(Nnode+2,i)=v2(i)
            vertices(Nnode+3,i)=v3(i)
            vertices(Nnode+4,i)=v4(i)
            vertices(Nnode+5,i)=v5(i)
            vertices(Nnode+6,i)=v6(i)
            vertices(Nnode+7,i)=v7(i)
            vertices(Nnode+8,i)=v8(i)
         enddo                  ! i
         faces(Nface+1,1)=Nnode+1
         faces(Nface+1,2)=Nnode+2
         faces(Nface+1,3)=Nnode+6
         faces(Nface+2,1)=Nnode+1
         faces(Nface+2,2)=Nnode+6
         faces(Nface+2,3)=Nnode+5
         faces(Nface+3,1)=Nnode+2
         faces(Nface+3,2)=Nnode+3
         faces(Nface+3,3)=Nnode+7
         faces(Nface+4,1)=Nnode+2
         faces(Nface+4,2)=Nnode+7
         faces(Nface+4,3)=Nnode+6
         faces(Nface+5,1)=Nnode+7
         faces(Nface+5,2)=Nnode+3
         faces(Nface+5,3)=Nnode+4
         faces(Nface+6,1)=Nnode+7
         faces(Nface+6,2)=Nnode+4
         faces(Nface+6,3)=Nnode+8
         faces(Nface+7,1)=Nnode+8
         faces(Nface+7,2)=Nnode+4
         faces(Nface+7,3)=Nnode+1
         faces(Nface+8,1)=Nnode+8
         faces(Nface+8,2)=Nnode+1
         faces(Nface+8,3)=Nnode+5
         faces(Nface+9,1)=Nnode+5
         faces(Nface+9,2)=Nnode+6
         faces(Nface+9,3)=Nnode+7
         faces(Nface+10,1)=Nnode+5
         faces(Nface+10,2)=Nnode+7
         faces(Nface+10,3)=Nnode+8
         Nnode=Nnode+8
         if (Nnode.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nnode=',Nnode
            write(*,*) 'while Nv_so_mx=',Nv_so_mx
            stop
         endif
         Nface=Nface+10
         if (Nface.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nface=',Nface
            write(*,*) 'while Nf_so_mx=',Nf_so_mx
            stop
         endif
      endif                     ! if need to draw the band
      if ((intersection).and.(pass.eq.1)) then
         goto 222
      endif
      
c     upper continous band
      pass=0
 333  continue
      pass=pass+1
      if (intersection) then
         call scalar_product(dim,uj,n,sp)
         w=w1+Delta_w*Li        ! road "iroad" width @ pint1
         call scalar_vector(dim,-w/(2.0D+0*dabs(sp)),uj,t1)
         if (sp.gt.0.0D+0) then
            call scalar_vector(dim,-1.0D+0,t1,t1)
         endif
         if (pass.eq.1) then
            call scalar_vector(dim,-1.0D+0,u,t2)
         else
            call copy_vector(dim,u,t2)
         endif                  ! pass
         wj=w1j+Delta_wj*Lj     ! road "jroad" width @ pint1
         call scalar_vector(dim,wj/(2.0D+0*dabs(sp)),t2,t3)
         call add_vectors(dim,pint1,t1,t4)
         if (pass.eq.1) then
            call add_vectors(dim,t4,t3,v3)
            call copy_vector(dim,x4,v4)
         else if (pass.eq.2) then
            call copy_vector(dim,x3,v3)
            call add_vectors(dim,t4,t3,v4)
         endif                  ! pass
         if (sp.lt.0.0D+0) then
            call scalar_vector(dim,-1.0D+0,uj,normal1)
         else
            call copy_vector(dim,uj,normal1)
         endif
         call substract_vectors(dim,v4,v3,t5)
         call vector_length(dim,t5,dist)
         call scalar_vector(dim,dist,u_x3x4,v3v4)
         if (pass.eq.1) then
            call substract_vectors(dim,v4,v3v4,v3)
         else if (pass.eq.2) then
            call add_vectors(dim,v3,v3v4,v4)
         endif                  ! pass
      else
         call copy_vector(dim,x3,v3)
         call copy_vector(dim,x4,v4)
         call copy_vector(dim,n1,normal1)
         call copy_vector(dim,n2,normal2)
      endif                     ! intersection
      if ((.not.intersection).or.
     &     ((intersection).and.(dist.gt.0.0D+0))) then
         v3(3)=v3(3)+road_thickness
         v4(3)=v4(3)+road_thickness
         call scalar_vector(dim,wband_width,normal1,d)
         call substract_vectors(dim,v4,d,v1)
         call scalar_vector(dim,wband_width,normal2,d)
         call substract_vectors(dim,v3,d,v2)
         call copy_vector(dim,v1,v5)
         call copy_vector(dim,v2,v6)
         call copy_vector(dim,v3,v7)
         call copy_vector(dim,v4,v8)
         v5(3)=v5(3)+wband_thickness
         v6(3)=v6(3)+wband_thickness
         v7(3)=v7(3)+wband_thickness
         v8(3)=v8(3)+wband_thickness
         do i=1,dim
            vertices(Nnode+1,i)=v1(i)
            vertices(Nnode+2,i)=v2(i)
            vertices(Nnode+3,i)=v3(i)
            vertices(Nnode+4,i)=v4(i)
            vertices(Nnode+5,i)=v5(i)
            vertices(Nnode+6,i)=v6(i)
            vertices(Nnode+7,i)=v7(i)
            vertices(Nnode+8,i)=v8(i)
         enddo                  ! i
         faces(Nface+1,1)=Nnode+1
         faces(Nface+1,2)=Nnode+2
         faces(Nface+1,3)=Nnode+6
         faces(Nface+2,1)=Nnode+1
         faces(Nface+2,2)=Nnode+6
         faces(Nface+2,3)=Nnode+5
         faces(Nface+3,1)=Nnode+2
         faces(Nface+3,2)=Nnode+3
         faces(Nface+3,3)=Nnode+7
         faces(Nface+4,1)=Nnode+2
         faces(Nface+4,2)=Nnode+7
         faces(Nface+4,3)=Nnode+6
         faces(Nface+5,1)=Nnode+7
         faces(Nface+5,2)=Nnode+3
         faces(Nface+5,3)=Nnode+4
         faces(Nface+6,1)=Nnode+7
         faces(Nface+6,2)=Nnode+4
         faces(Nface+6,3)=Nnode+8
         faces(Nface+7,1)=Nnode+8
         faces(Nface+7,2)=Nnode+4
         faces(Nface+7,3)=Nnode+1
         faces(Nface+8,1)=Nnode+8
         faces(Nface+8,2)=Nnode+1
         faces(Nface+8,3)=Nnode+5
         faces(Nface+9,1)=Nnode+5
         faces(Nface+9,2)=Nnode+6
         faces(Nface+9,3)=Nnode+7
         faces(Nface+10,1)=Nnode+5
         faces(Nface+10,2)=Nnode+7
         faces(Nface+10,3)=Nnode+8
         Nnode=Nnode+8
         if (Nnode.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nnode=',Nnode
            write(*,*) 'while Nv_so_mx=',Nv_so_mx
            stop
         endif
         Nface=Nface+10
         if (Nface.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nface=',Nface
            write(*,*) 'while Nf_so_mx=',Nf_so_mx
            stop
         endif
      endif                     ! need to draw the band
      if ((intersection).and.(pass.eq.1)) then
         goto 333
      endif
c     discontinuous central band
      call substract_vectors(dim,p2,p1,p1p2)
      call vector_length(dim,p1p2,d12)
      call normalize_vector(dim,p1p2,u)
      call substract_vectors(dim,n2,n1,t)
      call scalar_vector(dim,1.0D+0/d12,t,Delta_n)
      if (intersection) then
         call substract_vectors(dim,pint1,p1,t)
         call vector_length(dim,t,L0)
         L0=L0-central_band_length/2.0D+0
         L0=modulo(L0,(central_band_length+central_band_interval))
      else
         L0=0.0D+0
      endif                     ! intersection
      isegment=0
      keep_going=.true.
      do while (keep_going)
         length=L0+isegment*(central_band_length+central_band_interval)
         call scalar_vector(dim,length,u,t)
         call add_vectors(dim,p1,t,r1)
         r1(3)=r1(3)+road_thickness
         call scalar_vector(dim,length,Delta_n,t)
         call add_vectors(dim,n1,t,nr1)
         length=length+central_band_length
         call scalar_vector(dim,length,u,t)
         call add_vectors(dim,p1,t,r2)
         r2(3)=r2(3)+road_thickness
         call scalar_vector(dim,length,Delta_n,t)
         call add_vectors(dim,n1,t,nr2)
         call normalize_vector(dim,nr2,nr2)
c
c         call scalar_vector(dim,wband_width/2.0D+0,nr1,t)
         call scalar_vector(dim,wband_width/2.0D+0,n,t)
         call substract_vectors(dim,r1,t,v1)
         call add_vectors(dim,r1,t,v4)
c         call scalar_vector(dim,wband_width/2.0D+0,nr2,t)
         call scalar_vector(dim,wband_width/2.0D+0,n,t)
         call substract_vectors(dim,r2,t,v2)
         call add_vectors(dim,r2,t,v3)
         call copy_vector(dim,v1,v5)
         call copy_vector(dim,v2,v6)
         call copy_vector(dim,v3,v7)
         call copy_vector(dim,v4,v8)
         v5(3)=v5(3)+wband_thickness
         v6(3)=v6(3)+wband_thickness
         v7(3)=v7(3)+wband_thickness
         v8(3)=v8(3)+wband_thickness
         do i=1,dim
            vertices(Nnode+1,i)=v1(i)
            vertices(Nnode+2,i)=v2(i)
            vertices(Nnode+3,i)=v3(i)
            vertices(Nnode+4,i)=v4(i)
            vertices(Nnode+5,i)=v5(i)
            vertices(Nnode+6,i)=v6(i)
            vertices(Nnode+7,i)=v7(i)
            vertices(Nnode+8,i)=v8(i)
         enddo                  ! i
         faces(Nface+1,1)=Nnode+1
         faces(Nface+1,2)=Nnode+2
         faces(Nface+1,3)=Nnode+6
         faces(Nface+2,1)=Nnode+1
         faces(Nface+2,2)=Nnode+6
         faces(Nface+2,3)=Nnode+5
         faces(Nface+3,1)=Nnode+2
         faces(Nface+3,2)=Nnode+3
         faces(Nface+3,3)=Nnode+7
         faces(Nface+4,1)=Nnode+2
         faces(Nface+4,2)=Nnode+7
         faces(Nface+4,3)=Nnode+6
         faces(Nface+5,1)=Nnode+7
         faces(Nface+5,2)=Nnode+3
         faces(Nface+5,3)=Nnode+4
         faces(Nface+6,1)=Nnode+7
         faces(Nface+6,2)=Nnode+4
         faces(Nface+6,3)=Nnode+8
         faces(Nface+7,1)=Nnode+8
         faces(Nface+7,2)=Nnode+4
         faces(Nface+7,3)=Nnode+1
         faces(Nface+8,1)=Nnode+8
         faces(Nface+8,2)=Nnode+1
         faces(Nface+8,3)=Nnode+5
         faces(Nface+9,1)=Nnode+5
         faces(Nface+9,2)=Nnode+6
         faces(Nface+9,3)=Nnode+7
         faces(Nface+10,1)=Nnode+5
         faces(Nface+10,2)=Nnode+7
         faces(Nface+10,3)=Nnode+8
         Nnode=Nnode+8
         if (Nnode.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nnode=',Nnode
            write(*,*) 'while Nv_so_mx=',Nv_so_mx
            stop
         endif
         Nface=Nface+10
         if (Nface.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nface=',Nface
            write(*,*) 'while Nf_so_mx=',Nf_so_mx
            stop
         endif
         isegment=isegment+1
c     
         length=L0+(isegment+1)*central_band_length
     &        +isegment*central_band_interval
         if (length.gt.d12) then
            keep_going=.false.
         endif                  ! length > d12
      enddo                     ! while (keep_going)
      
      return
      end



      subroutine obj_contour(dim,Ncontour,Np,contour,thickness,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce a "flat" obj of a object defined by its contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours (including holes)
c       + Np: number of 2D coordinates that define the contour of the object
c       + contour: list of 2D coordinates that define the contour of the object
c       + thickness: thickness of the object
c     
c     Output:
c       + Nnode: number of nodes
c       + Nface: number of triangle faces
c       + vertices: (x,y,z) cartesian coordinates of each node
c       + faces: indexes of the 3 nodes that belong to each face
c
c     I/O
      integer dim
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      double precision thickness
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer iv,iface,i,j,iseg,icontour,ip
      integer i1,i2,i3,i4,order
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      integer Npt,Np0
      character*(Nchar_mx) outfile
c     label
      character*(Nchar_mx) label
      label='subroutine obj_contour'

      Nnode=0
      Nface=0
      call delaunay_triangulation(dim,Ncontour,Np,contour,
     &     Nv01,Nf01,v01,f01)   ! @ altitude 0
      call duplicate_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)
      call invert_normals(Nf01,f01)
      call add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nnode,Nface,vertices,faces)
      do iv=1,Nv02
         v02(iv,dim)=v02(iv,dim)+thickness
      enddo                     ! iv
      call add_obj_to_obj(dim,Nv02,Nf02,v02,f02,
     &     Nnode,Nface,vertices,faces)
      Npt=0
      do icontour=1,Ncontour
         Npt=Npt+Np(icontour)
      enddo                     ! icontour
      ip=0
      do icontour=1,Ncontour
         do i=1,Np(icontour)
            ip=ip+1
            do j=1,dim-1
               vertices(Nnode+ip,j)=contour(icontour,i,j)
            enddo               ! j
            vertices(Nnode+ip,dim)=0.0D+0
         enddo                  ! i
      enddo                     ! icontour
      do icontour=1,Ncontour
         do i=1,Np(icontour)
            ip=ip+1
            do j=1,dim-1
               vertices(Nnode+ip,j)=contour(icontour,i,j)
            enddo               ! j
            vertices(Nnode+ip,dim)=thickness
         enddo                  ! i
      enddo                     ! icontour
c     
      if (Nnode+2*Npt.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode+2*Npt=',Nnode+2*Npt
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
      if (Nface+2*Npt.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface+2*Npt=',Nface+2*Npt
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
      iface=Nface
      Np0=Nnode
      do icontour=1,Ncontour
         if (icontour.eq.1) then
            order=1
         else
            order=-1
         endif
         do iseg=1,Np(icontour)
            i1=Np0+iseg
            if (iseg.lt.Np(icontour)) then
               i2=Np0+iseg+1
            else
               i2=Np0+1
            endif
            i3=i2+Npt
            i4=i1+Npt
c     
            iface=iface+1
            if (order.eq.1) then
               faces(iface,1)=i1
               faces(iface,2)=i2
               faces(iface,3)=i4
            else
               faces(iface,1)=i1
               faces(iface,2)=i4
               faces(iface,3)=i2
            endif
c     
            iface=iface+1
            if (order.eq.1) then
               faces(iface,1)=i2
               faces(iface,2)=i3
               faces(iface,3)=i4
            else
               faces(iface,1)=i2
               faces(iface,2)=i4
               faces(iface,3)=i3
            endif
         enddo                  ! iseg
         Np0=Np0+Np(icontour)
      enddo                     ! icontour
      Nface=Nface+2*Npt
      Nnode=Nnode+2*Npt

      return
      end



      subroutine add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to add a obj to a obj
c     
c     Input:
c       + dim: dimension of space
c       + Nv01, Nf01, v01, f01: object to add
c       + Nnode, Nface, vertices, faces: object to be completed
c     
c     Output:
c       + Nnode, Nface, vertices, faces: updated
c     
c     I/O
      integer dim
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer iv,iface,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_obj_to_obj'
      
      if (Nnode+Nv01.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) '+Nv01=',Nv01
         write(*,*) '=',Nnode+Nv01
         write(*,*) '> Nv_so_mx=',Nv_so_mx
         stop
      else
         do iv=1,Nv01
            do j=1,dim
               vertices(Nnode+iv,j)=v01(iv,j)
            enddo               ! j
         enddo                  ! iv
      endif
      if (Nface+Nf01.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) '+Nf01=',Nf01
         write(*,*) '=',Nface+Nf01
         write(*,*) '> Nf_so_mx=',Nf_so_mx
         stop
      else
         do iface=1,Nf01
            do j=1,3
               faces(Nface+iface,j)=Nnode+f01(iface,j)
            enddo               ! j
         enddo                  ! iface
      endif
      Nnode=Nnode+Nv01
      Nface=Nface+Nf01

      return
      end


      
      subroutine add_face_with_rectangle(dim,rectangle,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to increment a given object with a rectangle (NO HOLE)
c     
c     Input:
c       + dim: dimension of space
c       + rectangle: (x,y,z) of the 4 nodes of the rectangle;
c         the order of the nodes is provided so that the normal
c         is directed toward the inside of the volume
c       + Nnode: number of nodes in the current object
c       + Nface: number of faces in the current object
c       + vertices: vertices of the current object
c       + faces: faces definition for the current object
c      
c     Output:
c       + (Nnode, Nface, vertices, faces): updated definition
c         of the object
c      
c     I/O
      integer dim
      double precision rectangle(1:4,1:Ndim_mx)
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_face_with_rectangle'

c     Add the 4 vertices that define the rectangle to the object
      do i=1,4
         do j=1,dim
            vertices(Nnode+i,j)=rectangle(i,j)
         enddo                  ! j
      enddo                     ! i

c     Add 2 triangular faces
c     Nface+1
      faces(Nface+1,1)=Nnode+1
      faces(Nface+1,2)=Nnode+2
      faces(Nface+1,3)=Nnode+3
c     Nface+2
      faces(Nface+2,1)=Nnode+1
      faces(Nface+2,2)=Nnode+3
      faces(Nface+2,3)=Nnode+4
      
      Nnode=Nnode+4
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
      Nface=Nface+2
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif

      return
      end

      

      subroutine add_face_with_rectangle_and_hole(dim,rectangle,hsf,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to increment a given object with a pierced rectangle
c     The hole in the rectangle takes a fraction "hsf" of the rectangle's surface
c     and is centered
c     
c     Input:
c       + dim: dimension of space
c       + rectangle: (x,y,z) of the 4 nodes of the rectangle;
c         the order of the nodes is provided so that the normal
c         is directed toward the inside of the volume
c       + hsf: fraction of the rectangle's surface used by the hole
c       + Nnode: number of nodes in the current object
c       + Nface: number of faces in the current object
c       + vertices: vertices of the current object
c       + faces: faces definition for the current object
c      
c     Output:
c       + (Nnode, Nface, vertices, faces): updated definition
c         of the object
c      
c     I/O
      integer dim
      double precision rectangle(1:4,1:Ndim_mx)
      double precision hsf
      integer Nnode,Nface
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      double precision a,b,hx,hy
      integer i,j
      integer inode,iface
      integer v1,v2,c
c     label
      character*(Nchar_mx) label
      label='subroutine add_face_with_rectangle_and_hole'

      call rectangle_analysis(dim,rectangle,v1,v2,c)
c     Debug
c      write(*,*) 'v1=',v1,' v2=',v2,' c=',c
c     Debug
      
      a=rectangle(2,v1)-rectangle(1,v1)
      hx=a*dsqrt(hsf)
      b=rectangle(3,v2)-rectangle(2,v2)
      hy=b*dsqrt(hsf)
c     Debug
c      write(*,*) 'a=',a,' hx=',hx
c      write(*,*) 'b=',b,' hy=',hy
c     Debug
      
c     Add the 4 vertices that define the rectangle to the object
      do i=1,4
         do j=1,dim
            vertices(Nnode+i,j)=rectangle(i,j)
         enddo                  ! j
      enddo                     ! i

c     Create 12 additionnal nodes in order to define the hole
c     Nnode+5
      vertices(Nnode+5,v1)=vertices(Nnode+1,v1)+(a-hx)/2.0D+0
      vertices(Nnode+5,v2)=vertices(Nnode+1,v2)+(b-hy)/2.0D+0
c     Nnode+6
      vertices(Nnode+6,v1)=vertices(Nnode+5,v1)+hx
      vertices(Nnode+6,v2)=vertices(Nnode+5,v2)
c     Nnode+7
      vertices(Nnode+7,v1)=vertices(Nnode+6,v1)
      vertices(Nnode+7,v2)=vertices(Nnode+6,v2)+hy
c     Nnode+8
      vertices(Nnode+8,v1)=vertices(Nnode+5,v1)
      vertices(Nnode+8,v2)=vertices(Nnode+7,v2)
c     Nnode+9
      vertices(Nnode+9,v1)=vertices(Nnode+5,v1)
      vertices(Nnode+9,v2)=vertices(Nnode+1,v2)
c     Nnode+10
      vertices(Nnode+10,v1)=vertices(Nnode+6,v1)
      vertices(Nnode+10,v2)=vertices(Nnode+1,v2)
c     Nnode+11
      vertices(Nnode+11,v1)=vertices(Nnode+2,v1)
      vertices(Nnode+11,v2)=vertices(Nnode+6,v2)
c     Nnode+12
      vertices(Nnode+12,v1)=vertices(Nnode+2,v1)
      vertices(Nnode+12,v2)=vertices(Nnode+7,v2)
c     Nnode+13
      vertices(Nnode+13,v1)=vertices(Nnode+6,v1)
      vertices(Nnode+13,v2)=vertices(Nnode+3,v2)
c     Nnode+14
      vertices(Nnode+14,v1)=vertices(Nnode+5,v1)
      vertices(Nnode+14,v2)=vertices(Nnode+3,v2)
c     Nnode+15
      vertices(Nnode+15,v1)=vertices(Nnode+1,v1)
      vertices(Nnode+15,v2)=vertices(Nnode+12,v2)
c     Nnode+16
      vertices(Nnode+16,v1)=vertices(Nnode+1,v1)
      vertices(Nnode+16,v2)=vertices(Nnode+11,v2)

c     constant dimension
      do inode=Nnode+5,Nnode+16
         vertices(inode,c)=vertices(Nnode+1,c)
      enddo ! inode

c     Debug
c      do inode=Nnode+1,Nnode+16
c         write(*,*) inode,(vertices(inode,j),j=1,dim)
c      enddo ! inode
c     Debug
      
c     Add 16 faces to define the rectangle minus the hole
c     Nface+1
      faces(Nface+1,1)=Nnode+1
      faces(Nface+1,2)=Nnode+9
      faces(Nface+1,3)=Nnode+5
c     Nface+2
      faces(Nface+2,1)=Nnode+1
      faces(Nface+2,2)=Nnode+5
      faces(Nface+2,3)=Nnode+16
c     Nface+3
      faces(Nface+3,1)=Nnode+9
      faces(Nface+3,2)=Nnode+10
      faces(Nface+3,3)=Nnode+6
c     Nface+4
      faces(Nface+4,1)=Nnode+9
      faces(Nface+4,2)=Nnode+6
      faces(Nface+4,3)=Nnode+5
c     Nface+5
      faces(Nface+5,1)=Nnode+10
      faces(Nface+5,2)=Nnode+2
      faces(Nface+5,3)=Nnode+11
c     Nface+6
      faces(Nface+6,1)=Nnode+10
      faces(Nface+6,2)=Nnode+11
      faces(Nface+6,3)=Nnode+6
c     Nface+7
      faces(Nface+7,1)=Nnode+6
      faces(Nface+7,2)=Nnode+11
      faces(Nface+7,3)=Nnode+12
c     Nface+8
      faces(Nface+8,1)=Nnode+6
      faces(Nface+8,2)=Nnode+12
      faces(Nface+8,3)=Nnode+7
c     Nface+9
      faces(Nface+9,1)=Nnode+7
      faces(Nface+9,2)=Nnode+12
      faces(Nface+9,3)=Nnode+3
c     Nface+10
      faces(Nface+10,1)=Nnode+7
      faces(Nface+10,2)=Nnode+3
      faces(Nface+10,3)=Nnode+13
c     Nface+11
      faces(Nface+11,1)=Nnode+8
      faces(Nface+11,2)=Nnode+7
      faces(Nface+11,3)=Nnode+13
c     Nface+12
      faces(Nface+12,1)=Nnode+8
      faces(Nface+12,2)=Nnode+13
      faces(Nface+12,3)=Nnode+14
c     Nface+13
      faces(Nface+13,1)=Nnode+15
      faces(Nface+13,2)=Nnode+8
      faces(Nface+13,3)=Nnode+14
c     Nface+14
      faces(Nface+14,1)=Nnode+15
      faces(Nface+14,2)=Nnode+14
      faces(Nface+14,3)=Nnode+4
c     Nface+15
      faces(Nface+15,1)=Nnode+16
      faces(Nface+15,2)=Nnode+5
      faces(Nface+15,3)=Nnode+8
c     Nface+16
      faces(Nface+16,1)=Nnode+16
      faces(Nface+16,2)=Nnode+8
      faces(Nface+16,3)=Nnode+15
      
      Nnode=Nnode+16
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'while Nv_so_mx=',Nv_so_mx
         stop
      endif
      Nface=Nface+16
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'while Nf_so_mx=',Nf_so_mx
         stop
      endif
      
      return
      end
      


      subroutine rectangle_analysis(dim,rectangle,
     &     v1,v2,c)
      implicit none
      include 'max.inc'
c      
c     Purpose: to analyze the geometric configuration of a given rectangle 
c     defined by 4 points
c      
c     Input:
c       + dim: dimension of space
c       + rectangle: points that define the rectangle
c      
c     Output:
c        + v1, v2: indexes of the 2 axes that define the plane of the rectangle
c          1: X
c          2: Y
c          3: Z
c       + c: index (1, 2 or 3) of the axis where the points are aligned
c     
c     I/O
      integer dim
      double precision rectangle(1:4,1:Ndim_mx)
      integer v1,v2,c
c     temp
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-6)
c     label
      character*(Nchar_mx) label
      label='subroutine rectangle_analysis'
      
      if ((dabs(rectangle(2,1)-rectangle(1,1)).gt.epsilon)
     &     .and.(dabs(rectangle(2,2)-rectangle(1,2)).le.epsilon)
     &     .and.(dabs(rectangle(2,3)-rectangle(1,3)).le.epsilon)) then
         v1=1
      endif
      if ((dabs(rectangle(2,1)-rectangle(1,1)).le.epsilon)
     &     .and.(dabs(rectangle(2,2)-rectangle(1,2)).gt.epsilon)
     &     .and.(dabs(rectangle(2,3)-rectangle(1,3)).le.epsilon)) then
         v1=2
      endif
      if ((dabs(rectangle(2,1)-rectangle(1,1)).le.epsilon)
     &     .and.(dabs(rectangle(2,2)-rectangle(1,2)).le.epsilon)
     &     .and.(dabs(rectangle(2,3)-rectangle(1,3)).gt.epsilon)) then
         v1=3
      endif
      if ((dabs(rectangle(3,1)-rectangle(2,1)).gt.epsilon)
     &     .and.(dabs(rectangle(3,2)-rectangle(2,2)).le.epsilon)
     &     .and.(dabs(rectangle(3,3)-rectangle(2,3)).le.epsilon)) then
         v2=1
      endif
      if ((dabs(rectangle(3,1)-rectangle(2,1)).le.epsilon)
     &     .and.(dabs(rectangle(3,2)-rectangle(2,2)).gt.epsilon)
     &     .and.(dabs(rectangle(3,3)-rectangle(2,3)).le.epsilon)) then
         v2=2
      endif
      if ((dabs(rectangle(3,1)-rectangle(2,1)).le.epsilon)
     &     .and.(dabs(rectangle(3,2)-rectangle(2,2)).le.epsilon)
     &     .and.(dabs(rectangle(3,3)-rectangle(2,3)).gt.epsilon)) then
         v2=3
      endif

      if (((v1.eq.2).and.(v2.eq.3)).or.((v1.eq.3).and.(v2.eq.2))) then
         c=1
      endif
      if (((v1.eq.1).and.(v2.eq.3)).or.((v1.eq.3).and.(v2.eq.1))) then
         c=2
      endif
      if (((v1.eq.1).and.(v2.eq.2)).or.((v1.eq.2).and.(v2.eq.1))) then
         c=3
      endif

      return
      end
      

      subroutine cubic_box(ix,iy,iz,a,e,box)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce the nodes of a cubic box of dimensions "a"
c     separated from other identical boxes by thickness "e"
c     
c     Input:
c       + (ix,iy,iz): indexes of the box along each axis
c       + a: length of the cubic box
c       + e: thickness of the wall separating two boxes
c     
c     Output:
c       + box: coordinates of the 8 nodes of the box
c     
c     I/O
      integer ix,iy,iz
      double precision a
      double precision e
      double precision box(1:8,1:Ndim_mx)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine cubic_box'
      
c     Box corners
c     node 1
      box(1,1)=ix*e+(ix-1)*a
      box(1,2)=iy*e+(iy-1)*a
      box(1,3)=iz*e+(iz-1)*a
c     node 2
      box(2,1)=box(1,1)+a
      box(2,2)=box(1,2)
      box(2,3)=box(1,3)
c     node 3
      box(3,1)=box(2,1)
      box(3,2)=box(1,2)+a
      box(3,3)=box(1,3)
c     node 4
      box(4,1)=box(1,1)
      box(4,2)=box(3,2)
      box(4,3)=box(1,3)
c     node 5
      box(5,1)=box(1,1)
      box(5,2)=box(1,2)
      box(5,3)=box(1,3)+a
c     node 6
      box(6,1)=box(2,1)
      box(6,2)=box(2,2)
      box(6,3)=box(5,3)
c     node 7
      box(7,1)=box(3,1)
      box(7,2)=box(3,2)
      box(7,3)=box(5,3)
c     node 8
      box(8,1)=box(4,1)
      box(8,2)=box(4,2)
      box(8,3)=box(5,3)

      return
      end
      

      subroutine rectangular_box(ix,iy,iz,ax,ay,az,e_in,e_ext,box)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce the nodes of a rectangular box of dimensions "(ax,ay,az)"
c     separated from other identical boxes by thickness "e_in"
c     
c     Input:
c       + (ix,iy,iz): indexes of the box along each axis
c       + ax: length of the box in the X dimension
c       + ay: length of the box in the Y dimension
c       + az: length of the box in the Z dimension
c       + e_in: thickness of internal walls separating two boxes
c       + e_ext: thickness of external walls
c     
c     Output:
c       + box: coordinates of the 8 nodes of the box
c     
c     I/O
      integer ix,iy,iz
      double precision ax,ay,az
      double precision e_in,e_ext
      double precision box(1:8,1:Ndim_mx)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine rectangular_box'
      
c     Box corners
c     node 1
      box(1,1)=e_ext+(ix-1)*(ax+e_in)
      box(1,2)=e_ext+(iy-1)*(ay+e_in)
      box(1,3)=e_ext+(iz-1)*(az+e_in)
c     node 2
      box(2,1)=box(1,1)+ax
      box(2,2)=box(1,2)
      box(2,3)=box(1,3)
c     node 3
      box(3,1)=box(2,1)
      box(3,2)=box(1,2)+ay
      box(3,3)=box(1,3)
c     node 4
      box(4,1)=box(1,1)
      box(4,2)=box(3,2)
      box(4,3)=box(1,3)
c     node 5
      box(5,1)=box(1,1)
      box(5,2)=box(1,2)
      box(5,3)=box(1,3)+az
c     node 6
      box(6,1)=box(2,1)
      box(6,2)=box(2,2)
      box(6,3)=box(5,3)
c     node 7
      box(7,1)=box(3,1)
      box(7,2)=box(3,2)
      box(7,3)=box(5,3)
c     node 8
      box(8,1)=box(4,1)
      box(8,2)=box(4,2)
      box(8,3)=box(5,3)

      return
      end
      

      subroutine invert_normals(Nf,f01)
      implicit none
      include 'max.inc'
c     
c     Purpose: invert the normal for all faces of a given object
c     
c     Input:
c       + Nf: number of faces of the object
c       + f01: definition of faces
c     
c     Output:
c       + f01: updated
c     
c     I/O
      integer Nf
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine invert_normals'

      do iface=1,Nf
         tmp1=f01(iface,2)
         f01(iface,2)=f01(iface,3)
         f01(iface,3)=tmp1
      enddo                     ! iface

      return
      end



      subroutine duplicate_obj(dim,Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)
      implicit none
      include 'max.inc'
c
c     Purpose: duplicate a object
c
c     Input:
c       + dim: dimension of space
c       + Nv01, Nf01, v01, f01: definition of the object to duplicate
c
c     Output:
c       + Nv02, Nf02, v02, f02: definition of the duplicated object
c
c     I/O
      integer dim
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
c     temp
      integer i,iv,iface
c     label
      character*(Nchar_mx) label
      label='subroutine duplicate_obj'

      Nv02=Nv01
      Nf02=Nf01
      do iv=1,Nv02
         do i=1,dim
            v02(iv,i)=v01(iv,i)
         enddo                  ! i
      enddo                     ! iv
      do iface=1,Nf02
         do i=1,3
            f02(iface,i)=f01(iface,i)
         enddo                  ! i
      enddo                     ! iface

      return
      end
