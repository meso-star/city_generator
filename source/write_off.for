c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine write_off(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces,rgb_codes)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to write the main mesh to a .off file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output .off file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + rgb_codes: RGB code for each face
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      double precision rgb_codes(1:Nf_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1,i
      character*(Nchar_mx) line
      double precision transparency
c     label
      character*(Nchar_mx) label
      label='subroutine write_off'

      transparency=1.0D+0

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif

      if (invert_normal) then
         call invert_normals(Nf,faces)
      endif ! invert_normal

      open(12,file=trim(output_file))
      write(*,*) 'Recording OFF file: ',trim(output_file)

      write(12,10) 'OFF'
      write(12,*) Nv,Nf,100
      do iv=1,Nv
         write(12,*) (vertices(iv,i),i=1,dim)
      enddo                     ! iv
      do iface=1,Nf
         write(12,*) 3,
     &        (faces(iface,i)-1,i=1,3),
     &        (rgb_codes(iface,i),i=1,3),
     &        transparency
      enddo                     ! iface
      close(12)
      
      return
      end


      subroutine write_single_off(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces,rgb_codes)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to write the main mesh to a .off file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output .off file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + rgb_codes: RGB code for each face
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
      double precision rgb_codes(1:Nf_so_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1,i
      character*(Nchar_mx) line
      double precision transparency
c     label
      character*(Nchar_mx) label
      label='subroutine write_single_off'

      transparency=1.0D+0

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif

      if (invert_normal) then
         call invert_normals(Nf,faces)
      endif ! invert_normal

      open(12,file=trim(output_file))
c      write(*,*) 'Recording OFF file: ',trim(output_file)

      write(12,10) 'OFF'
      write(12,*) Nv,Nf,100
      do iv=1,Nv
         write(12,*) (vertices(iv,i),i=1,dim)
      enddo                     ! iv
      do iface=1,Nf
         write(12,*) 3,
     &        (faces(iface,i)-1,i=1,3),
     &        (rgb_codes(iface,i),i=1,3),
     &        transparency
      enddo                     ! iface
      close(12)
      
      return
      end


      
      subroutine append_single_off(dim,output_file,invert_normal,
     &     Nv,Nv01,Nf01,v01,f01,rgb01)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to append the main mesh to a .off file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output .off file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: total number of vertices
c       + Nv01: number of vertices
c       + Nf01: number of faces
c       + v01: array of verices definition (3D position of each vertex)
c       + f01: array of faces (index of vertices that belong to each face)
c       + rgb01: RGB code for each face
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      double precision rgb01(1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1,i
      character*(Nchar_mx) line
      double precision transparency
      character*(Nchar_mx) file1,file2
c     label
      character*(Nchar_mx) label
      label='subroutine append_single_off'
c
      transparency=1.0D+0
c
      if (Nv01.gt.10000000) then
         call error(label)
         write(*,*) 'Number of v01:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif
c
      if (invert_normal) then
         call invert_normals(Nf01,f01)
      endif ! invert_normal
c
      file1=trim(output_file)//'.1'
      file2=trim(output_file)//'.2'
      open(22,file=trim(file1),access='append')
      do iv=1,Nv01
         write(22,*) (v01(iv,i),i=1,dim)
      enddo                     ! iv
      close(22)
      open(23,file=trim(file2),access='append')
      do iface=1,Nf01
         write(23,*) 3,
     &        (Nv+f01(iface,i)-1,i=1,3),
     &        (rgb01(i),i=1,3),
     &        transparency
      enddo                     ! iface
      close(23)
c
      return
      end
