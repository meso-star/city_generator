c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine building01(dim,
     &     Nx,Ny,Nz,
     &     ax,ay,az,e_in,e_ext,wwf,e_glass,
     &     roof_height,roof_thickness,
     &     internal_mat,
     &     external_mat,
     &     glass_panes_mat,
     &     roof_mat,
     &     position,angle,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf,Abuilding,Aglass,Vroof_solid,Vroof_air)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to produce a type 1 building
c
c     Input:
c       + dim: dimension of space
c       + Nx: number of cubic cells in X direction for each building
c       + Ny: number of cubic cells in Y direction for each building
c       + Nz: number of cubic cells in Z direction for each building
c       + ax: dimension of a single cell along the X axis (m)
c       + ay: dimension of a single cell along the Y axis (m)
c       + az: dimension of a single cell along the Z axis (m)
c       + e_in: thickness of internal solid boundaries (m)
c       + e_ext: thickness of external solid boundaries (m)
c       + wwf: fraction of the wall used for windows
c       + e_glass: thickness of a single glass panel (m)
c       + roof_height: height of the roof (m)
c       + roof_thickness: thickness of the roof (m)
c       + internal_mat: name of the material for internal walls
c       + external_mat: name of the material for external walls
c       + glass_panes_mat: name of the material for glass_panes
c       + roof_mat: name of the material for the roof
c       + position: (x,y) shift of the building (m)
c       + angle: rotation angle (deg)
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c       + Abuilding: total lateral area of the building [m²]
c       + Aglass: glass are of the building [m²]
c       + Vroof_solid: volume of solid for the roof [m³]
c       + Vroof_air: volume of internal air in the attic [m³]
c
c     Input
      integer dim
      integer Nx,Ny,Nz
      double precision ax,ay,az
      double precision e_in,e_ext
      double precision wwf
      double precision e_glass
      double precision roof_height
      double precision roof_thickness
      character*(Nchar_mx) internal_mat
      character*(Nchar_mx) external_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      double precision position(1:Ndim_mx-1)
      double precision angle
      logical generate_individual_obj_files
      integer Nobj,Nv,Nf
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      double precision Abuilding
      double precision Aglass
      double precision Vroof_solid
      double precision Vroof_air
c     temp
      character*(Nchar_mx) file_out,filename
      logical invert_normal
      integer iobj,type,pane_index
      integer sindex
      integer ix,iy,iz,j
      character*(Nchar_mx) str
      logical err
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:3)
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
      double precision wall_e,a_max
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      logical draw_windows
      logical draw_specific_window(1:Nx_mx,1:Ny_mx,1:Nz_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine building01'
      
      axe(1)=0.0D+0
      axe(2)=0.0D+0
      axe(3)=1.0D+0
c     wall_e is the smallest wall dimension
      if (e_in.lt.e_ext) then
         wall_e=e_in
      else
         wall_e=e_ext
      endif
c     a_max is the largest of ax, ay and az
      if ((ax.ge.ay).and.(ax.ge.az)) then
         a_max=ax
      endif
      if ((ay.ge.ax).and.(ay.ge.az)) then
         a_max=ay
      endif
      if ((az.ge.ax).and.(az.ge.ax)) then
         a_max=az
      endif                     ! j

      draw_windows=.true.
c     Global option for drawing windows:
c     - can be based on a geometrical criterion
      if (wwf.le.0.0D+0) then
         draw_windows=.false.
      endif
c     - can be manually set:
c      draw_windows=.false.
c     
c     This global option has the following behaviour:
c      - when set to T, each individual window must be drawn, unless specified otherwise by a local event
c      - when set to F, each individual window will not be drawn
c     
      iobj=0                    ! total number of individual surfaces groups for the building
c     ---------------------------------------------------------------------------
c     Rooms: inner surface
      do ix=1,Nx
         do iy=1,Ny
            do iz=1,Nz
c     surface index
               iobj=iobj+1
c     Produce individual surface
c     Option for drawing individual windows: is currently based only on the
c     global setting
               draw_specific_window(ix,iy,iz)=draw_windows
               call rectangular_cell(dim,Nx,Ny,Nz,ix,iy,iz,
     &              ax,ay,az,e_in,e_ext,wwf,
     &              draw_specific_window(ix,iy,iz),
     &              Nv01,Nf01,v01,f01)
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.false.
               sindex=Nobj+iobj
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='internal_air'
                  back_is_st=.false.
                  back_name=trim(internal_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c                  call retrieve_code(back_name,color_code)
c                  call append_single_off(dim,off_file,invert_normal,
c     &                 Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            enddo               ! iz
         enddo                  ! iy
      enddo                     ! ix
      
c     ---------------------------------------------------------------------------
c     Enveloppe of the building
      call global_enveloppe(dim,Nx,Ny,Nz,
     &     ax,ay,az,e_in,e_ext,wwf,e_glass,
     &     draw_specific_window,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)
      iobj=iobj+1
      sindex=Nobj+iobj
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(external_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
      iobj=iobj+1
      sindex=Nobj+iobj
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv02,v02,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv02,v02,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         front_name='internal_air'
         back_name=trim(external_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv02,Nf02,v02,f02)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv02,Nf02,v02,f02)
         endif
         Nv=Nv+Nv02
         Nf=Nf+Nf02
      endif                     ! err
c     
c     roof
c     ---------------------------------------------------------------------------
      call roof01(dim,ax,ay,az,e_in,e_ext,Nx,Ny,Nz,
     &     roof_height,roof_thickness,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02,
     &     Vroof_solid,Vroof_air)
c     =================================
c     01: roof / internal_air interface
c     =================================
      iobj=iobj+1
      sindex=Nobj+iobj
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='internal_air'
         back_is_st=.false.
         middle_name=''
         back_name=trim(roof_mat)
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     =================================
c     02: roof / atmosphere interface
c     =================================
      iobj=iobj+1
      sindex=Nobj+iobj
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv02,v02,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv02,v02,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         middle_name=''
         back_name=trim(roof_mat)
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv02,Nf02,v02,f02)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv02,Nf02,v02,f02)
         endif
         Nv=Nv+Nv02
         Nf=Nf+Nf02
      endif                     ! err
c     
c     building-glass interfaces
c     ---------------------------------------------------------------------------
c     North face
      ix=1
      type=1
      do iy=1,Ny
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_border(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.false.
                  front_name=trim(glass_panes_mat)
                  back_is_st=.false.
                  back_name=trim(external_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! iy
c     
c     ---------------------------------------------------------------------------
c     South face
      ix=Nx
      type=2
      do iy=1,Ny
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_border(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.false.
                  front_name=trim(glass_panes_mat)
                  back_is_st=.false.
                  back_name=trim(external_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! iy
c     
c     ---------------------------------------------------------------------------
c     West face
      iy=1
      type=3
      do ix=1,Nx
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_border(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.false.
                  front_name=trim(glass_panes_mat)
                  back_is_st=.false.
                  back_name=trim(external_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! ix
c     
c     ---------------------------------------------------------------------------
c     East face
      iy=Ny
      type=4
      do ix=1,Nx
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_border(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.false.
                  front_name=trim(glass_panes_mat)
                  back_is_st=.false.
                  back_name=trim(external_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! ix
c      
c     Window glass pane #1 (inside pane)
c
c     Window surfaces have a IR emissivity of 1 and a visible emissivity of 0
c     so that they behave as blackbody in the infrared, but no solar
c     energy is deposited on them (perfectly transparent material in the visible)
      pane_index=1
c     ---------------------------------------------------------------------------
c     North face
      ix=1
      type=1
      do iy=1,Ny
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.false.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='internal_air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! iy
c     
c     ---------------------------------------------------------------------------
c     South face
      ix=Nx
      type=2
      do iy=1,Ny
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.false.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='internal_air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! iy
c     
c     ---------------------------------------------------------------------------
c     West face
      iy=1
      type=3
      do ix=1,Nx
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.false.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='internal_air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! ix
c     
c     ---------------------------------------------------------------------------
c     East face
      iy=Ny
      type=4
      do ix=1,Nx
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.false.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='internal_air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! ix
c     
c     Window glass pane #2 (outside pane)
      pane_index=2
c     ---------------------------------------------------------------------------
c     North face
      ix=1
      type=1
      do iy=1,Ny
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! iy
c     
c     ---------------------------------------------------------------------------
c     South face
      ix=Nx
      type=2
      do iy=1,Ny
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! iy
c     
c     ---------------------------------------------------------------------------
c     West face
      iy=1
      type=3
      do ix=1,Nx
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! ix
c     
c     ---------------------------------------------------------------------------
c     East face
      iy=Ny
      type=4
      do ix=1,Nx
         do iz=1,Nz
            if (draw_specific_window(ix,iy,iz)) then
               iobj=iobj+1
               call window_glass_panes(dim,Nx,Ny,Nz,
     &              ix,iy,iz,type,pane_index,
     &              ax,ay,az,e_in,e_ext,wwf,e_glass,
     &              Nv01,Nf01,v01,f01)
               sindex=Nobj+iobj
               if (angle.ne.0.0D+0) then
                  alpha=angle*pi/180.0D+0 ! rad
                  call rotation_matrix(dim,alpha,axe,M)
                  call rotate_obj(dim,Nv01,v01,M)
               endif
               if ((position(1).ne.0.0D+0).or.
     &              (position(2).ne.0.0D+0)) then
                  center(1)=position(1)
                  center(2)=position(2)
                  center(3)=0.0D+0
                  call move_obj(dim,Nv01,v01,center)
               endif
c     Add individual surface to global trianglemesh (for visualization)
               invert_normal=.true.
               call num2str(sindex,str,err)
               if (err) then
                  call error(label)
                  write(*,*) 'while converting to string:'
                  write(*,*) 'sindex=',sindex
                  stop
               else
                  invert_normal=.false.
                  front_is_st=.true.
                  front_name='air'
                  back_is_st=.false.
                  back_name=trim(glass_panes_mat)
                  middle_name=''
                  call append_single_obj_with_mtl(dim,obj_file,
     &                 mtllib_file,
     &                 invert_normal,
     &                 front_is_st,back_is_st,.false.,
     &                 front_name,middle_name,back_name,
     &                 Nv,Nv01,Nf01,v01,f01)
c     call retrieve_code(back_name,color_code)
c     call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
                  if (generate_individual_obj_files) then
                     filename='S'//trim(str)//'.obj'
                     file_out='./results/obj/'//trim(filename)
                     call write_single_obj(dim,file_out,invert_normal,
     &                    Nv01,Nf01,v01,f01)
                  endif
                  Nv=Nv+Nv01
                  Nf=Nf+Nf01
               endif            ! err
            endif
         enddo                  ! iz
      enddo                     ! ix
c     ---------------------------------------------------------------------------
c     Building's base
c     Produce individual surface
      iobj=iobj+1
      call base(dim,ax,ay,e_in,e_ext,
     &     Nx,Ny,Nz,
     &     Nv01,Nf01,v01,f01)
      sindex=Nobj+iobj
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.false.
         front_name='grass'
         back_is_st=.false.
         back_name=trim(external_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,
     &        mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     ---------------------------------------------------------------------------
c     
      Nobj=Nobj+iobj
      Abuilding=2.0D+0*(Nx*ax+(Nx-1)*e_in+2.0D+0*e_ext
     &     +Ny*ay+(Ny-1)*e_in+2.0D+0*e_ext)
     &     *(Nz*az+(Nz-1)*e_in+2.0D+0*e_ext)
      Aglass=2.0D+0*(Nx*ax+Ny*ay)*az*Nz*wwf
c
      return
      end
