c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine identify_Nswimmingpool(Nswimmingpool)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of swimmingpools defined by the user
c
c     Input:
c
c     Output:
c       + Nswimmingpool: number of swimmingpools
c
c     I/O
      integer Nswimmingpool
c     temp
      logical keep_looking,file_exists
      integer i
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine identify_Nswimmingpool'

      Nswimmingpool=0
      keep_looking=.true.
      do while (keep_looking)
         i=Nswimmingpool+1
         call num2str3(i,str3)
         filename='./data/swimmingpool'//trim(str3)//'.in'
         inquire(file=trim(filename),exist=file_exists)
         if (file_exists) then
            Nswimmingpool=Nswimmingpool+1
         else
            keep_looking=.false.
         endif                  ! file_exists
      enddo                     ! while (keep_looking)

      return
      end



      subroutine read_swimmingpool_data(dim,
     &     Nswimmingpool,iswimmingpool,
     &     edge_mat,edge_width,edge_thickness,
     &     Ncontour,Nppt,contour_track)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to read input data file for swimmingpools
c
c     Input:
c       + dim: dimension of space
c
c     Output:
c       + Nswimmingpool: number of swimmingpools
c       + iswimmingpool: index of the swimmingpool definition file to read
c       + edge_mat: name of the material for the edge
c       + edge_width: width of the edge [m]
c       + edge_thickness: thickness of the edge [m]
c       + Ncontour: number of contours for each swimming area
c       + Nppt: number of points for the description of the contour track for each swimming area
c       + contour_track: contour tracks of the swimming area
c
c     I/O
      integer dim
      integer Nswimmingpool
      integer iswimmingpool
      character*(Nchar_mx) edge_mat
      double precision edge_width
      double precision edge_thickness
      integer Ncontour
      integer Nppt(1:Ncontour_mx)
      double precision contour_track(1:Ncontour_mx,1:Nppt_mx,1:2)
c     temp
      integer ios,j,k,nl
      logical keep_reading
      character*3 str3
      character*(Nchar_mx) filename
      character*(Nchar_mx) line
c     functions
      integer nitems
c     label
      character*(Nchar_mx) label
      label='subroutine read_swimmingpool_data'
      
      call num2str3(iswimmingpool,str3)
      filename='./data/swimmingpool'//trim(str3)//'.in'
      open(22,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do j=1,3
            read(22,*)
         enddo                  ! j
         read(22,*) edge_mat
         read(22,*)
         read(22,*) edge_width
         read(22,*)
         read(22,*) edge_thickness
c     Consistency tests
         if (edge_width.lt.0.0D+0) then
            call error(label)
            write(*,*) 'edge_width(',iswimmingpool,')=',edge_width
            write(*,*) 'should be > 0'
            stop
         endif                  ! edge_width<0
         if (edge_thickness.lt.0.0D+0) then
            call error(label)
            write(*,*) 'edge_thickness(',iswimmingpool,')=',
     &           edge_thickness
            write(*,*) 'should be > 0'
            stop
         endif                  ! edge_thickness<0
c     End of tests
         read(22,*)
         Ncontour=1
         Nppt(Ncontour)=0
         keep_reading=.true.
         do while (keep_reading)
            read(22,10,iostat=ios) line
            if (ios.eq.0) then
               if (nitems(line).eq.2) then
                  backspace(22)
                  Nppt(Ncontour)=Nppt(Ncontour)+1
                  if (Nppt(Ncontour).gt.Nppt_mx) then
                     call error(label)
                     write(*,*) 'Nppt(',Ncontour,')=',Nppt(Ncontour)
                     write(*,*) '> Nppt_mx=',Nppt_mx
                     stop
                  endif
                  read(22,*) (contour_track(Ncontour,Nppt(Ncontour),j),
     &                 j=1,2)
               else             ! nitems(line).ne.2
                  Ncontour=Ncontour+1
                  if (Ncontour.gt.Ncontour_mx) then
                     call error(label)
                     write(*,*) 'Ncontour=',Ncontour
                     write(*,*) '> Ncontour_mx=',Ncontour_mx
                     stop
                  endif
                  Nppt(Ncontour)=0
               endif            ! nitems(line)=2
            else
               keep_reading=.false.
            endif               ! ios=0
         enddo                  ! while (keep_reading)
      endif                     !  file exists
      close(22)
      
      return
      end
