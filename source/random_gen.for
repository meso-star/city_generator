c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_seed(seed_file,seed)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the random seed
c     
c     Input:
c       + seed_file: name of the seed file to read
c     
c     Output:
c       + seed: value of the random seed
c     
c     I/O
      character*(Nchar_mx) seed_file
      integer seed
c     temp
      integer ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_seed'
      
      open(10,file=trim(seed_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(seed_file)
         stop
      else
         read(10,*) seed
      endif                     ! ios.ne.0
      close(10)

      return
      end
      


      subroutine record_seed(seed_file,seed)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record the random seed
c     
c     Input:
c       + seed_file: name of the seed file to record
c       + seed: value of the random seed
c     
c     I/O
      character*(Nchar_mx) seed_file
      integer seed
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine record_seed'
      
      open(10,file=trim(seed_file))
      write(10,*) seed
      close(10)

      return
      end


      subroutine initialize_random_generator(seed)
      implicit none
      include 'max.inc'
c     
c     Purpose: to initialize the random number generator
c     
c     Input:
c       + seed: value of the random seed
c     
c     I/O
      integer seed
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine initialize_random_generator'

      call zufalli(seed)

      return
      end
      


      subroutine random_gen(r)
      implicit none
      include 'max.inc'
c
c     Main pseudo-random number generator routine
c     (the one that must be called)
c     
c     Input:
c     
c     Output:
c       + r: random number in the [0,1] range
c
c     I/O
      double precision r
c     temp
      integer n,t
c     label
      character*(Nchar_mx) label
      label='subroutine random_gen'

      n=1
      t=0
 10   continue
      call zufall(n,r)
      if (r.eq.0.or.r.eq.1) then
         t=t+1
         if (t.gt.10) then
            call error(label)
            write(*,*) 'Too many zeroes'
            stop
         endif
         goto 10
      endif

      return
      end

      

      subroutine uniform(val1,val2,alpha)
      implicit none
      include 'max.inc'
c
c     Purpose: to return a value between "val1" and "val2", chosen uniformally
c     
c     Input:
c       + val1: lower boundary
c       + val2: higher boundary
c
c     Output:
c       + alpha: random number in [val1, val2] range
c
c     I/O
      double precision val1,val2,alpha
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine uniform'

      if (val2.lt.val1) then
         call error(label)
         write(*,*) 'val2=',val2
         write(*,*) '< val1=',val1
         stop
      else
         call random_gen(R)
         alpha=val1+(val2-val1)*R
      endif

      return
      end

      

      subroutine choose_uniform_integer(N1,N2,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to choose an integer between N1 and N2 (included)
c     on a random uniform basis
c
c     Inputs:
c       + N1: minimum value the integer can take
c       + N2: maximum value the integer can take
c
c     Output:
c       + i: integer value between N1 and N2
c
c     I/O
      integer N1,N2,i
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine choose_uniform_integer'

      call random_gen(R)
      i=N1+int((N2-N1+1)*R)

      return
      end

      

      subroutine choose_integer(N,cdf,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to choose an integer according to a set of probabilities
c     This routine uses a dichotomy-based scheme in order to invert the cumulated probability function
c     This is supposed to speed things up for large values of N
c
c     Inputs:
c       + N: number of probabilities
c       + cdf: cumulated probabilities
c
c     Output:
c       + i: selected integer value
c
c     I/O
      integer N
      double precision cdf(0:Nproba_mx)
      integer i
c     temp
      double precision R
      integer idx,idx1,idx2
      logical keep_looking,i_found
      integer iter
c     parameters
      double precision epsilon_cdf
      parameter(epsilon_cdf=1.0D-6) ! max. error over the last value of the cdf
c     label
      character*(Nchar_mx) label
      label='subroutine choose_integer'

      if (N.gt.Nproba_mx) then
         call error(label)
         write(*,*) 'Number of elements in cdf=',N
         write(*,*) '> Nproba_mx=',Nproba_mx
         stop
      endif
      if (dabs(cdf(N)-1.0D+0).gt.epsilon_cdf) then
         call error(label)
         write(*,*) 'cdf(',N,')=',cdf(N)
         write(*,*) 'should be equal to 1'
         write(*,*) '|cdf(',N,')-1|=',dabs(cdf(N)-1.0D+0)
         write(*,*) '> epsilon_cdf=',epsilon_cdf
         stop
      endif

      call random_gen(R)
      keep_looking=.true.
      i_found=.false.
      idx1=0
      idx2=N
      iter=0
      do while (keep_looking)
         iter=iter+1
         if (iter.gt.Niter_mx) then
            call error(label)
            write(*,*) 'iter=',iter
            write(*,*) '> Niter_mx=',Niter_mx
            write(*,*) 'R=',R
            do idx=0,N
               write(*,*) 'cdf(',idx,')=',cdf(idx)
            enddo               ! idx
            stop
         else
            idx=(idx1+idx2)/2
            if ((R.gt.cdf(idx)).and.(R.le.cdf(idx+1))) then
               keep_looking=.false.
               i=idx+1
               i_found=.true.
            else
               if (R.gt.cdf(idx)) then
                  idx1=idx
               else
                  idx2=idx
               endif
            endif               ! cdf(idx) < R < cdf(idx+1)
         endif                  ! iter > Niter_mx            
      enddo                     ! while (keep_looping)
      if (.not.i_found) then
         call error(label)
         write(*,*) 'cdf could not be inverted'
         write(*,*) 'idx=',idx
         write(*,*) 'R=',R
         write(*,*) 'cdf(',Nproba_mx,')=',cdf(Nproba_mx)
         stop
      endif

      return
      end
