c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine decoration(dim,datafile,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to generate decs
c
c     Input:
c       + dim: dimension of space
c       + datafile: decoration definition file to read
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      character*(Nchar_mx) datafile
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer Ndec
      double precision dec_position(1:Ndim_mx)
      double precision dec_scale
      double precision dec_angle
      character*(Nchar_mx) dec_mat
      character*(Nchar_mx) dec_file
      integer iobj,idec,iv,j,i,ios,nl
      character*(Nchar_mx) decfile
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine decoration'
      
      axe(1)=0.0D+0
      axe(2)=0.0D+0
      axe(3)=1.0D+0

      iobj=0                    ! total number of individual surfaces groups for the road
c     ---------------------------------------------------------------------------
      open(21,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         call get_nlines(datafile,nl)
         Ndec=Nl-3
         if (Ndec.lt.1) then
            call error(label)
            write(*,*) 'Number of definition lines in file: ',
     &           trim(datafile)
            write(*,*) 'was found to be:',Ndec
            write(*,*) 'should be at least equal to 1'
            stop
         endif                  ! Ndec<1
         do i=1,3
            read(21,*)
         enddo                  ! i         
         do idec=1,Ndec
            read(21,*) (dec_position(j),j=1,dim),
     &           dec_scale,
     &           dec_angle,
     &           dec_mat,
     &           dec_file
            iobj=iobj+1
c     read object file
            decfile='./data/obj_bank/'//trim(dec_file)
            call read_obj(decfile,dim,
     &           Nv01,Nf01,v01,f01)
c     scale, move and rotate
            if (dec_scale.ne.1.0D+0) then
               do iv=1,Nv01
                  do j=1,dim
                     v01(iv,j)=v01(iv,j)*dec_scale
                  enddo         ! j
               enddo            ! iv
            endif               ! dec_scale.ne.1
            if (dec_angle.ne.0.0D+0) then
               alpha=dec_angle*pi/180.0D+0 ! rad
               call rotation_matrix(dim,alpha,axe,M)
               call rotate_obj(dim,Nv01,v01,M)
            endif
            if ((dec_position(1).ne.0.0D+0).or.
     &           (dec_position(2).ne.0.0D+0).or.
     &           (dec_position(3).ne.0.0D+0)) then
               do j=1,dim
                  center(j)=dec_position(j)
               enddo            ! j
               call move_obj(dim,Nv01,v01,center)
            endif
c     Add individual surface to global trianglemesh (for visualization)
            invert_normal=.false.
            sindex=Nobj+iobj
            call num2str(sindex,str,err)
            if (err) then
               call error(label)
               write(*,*) 'while converting to string:'
               write(*,*) 'sindex=',sindex
               stop
            else
               invert_normal=.false.
               front_is_st=.true.
               front_name='air'
               back_is_st=.false.
               back_name=trim(dec_mat)
               middle_name=''
               call append_single_obj_with_mtl(dim,obj_file,
     &              mtllib_file,
     &              invert_normal,
     &              front_is_st,back_is_st,.false.,
     &              front_name,middle_name,back_name,
     &              Nv,Nv01,Nf01,v01,f01)
c               call retrieve_code(back_name,color_code)
c               call append_single_off(dim,off_file,invert_normal,
c     &              Nv,Nv01,Nf01,v01,f01,color_code)
               if (generate_individual_obj_files) then
                  filename='S'//trim(str)//'.obj'
                  file_out='./results/obj/'//trim(filename)
                  call write_single_obj(dim,file_out,invert_normal,
     &                 Nv01,Nf01,v01,f01)
               endif
               Nv=Nv+Nv01
               Nf=Nf+Nf01
            endif               ! err
c     
         enddo                  ! idec
c     ---------------------------------------------------------------------------
      endif                     ! ios.ne.0
      close(21)
c     
      Nobj=Nobj+iobj
c
      return
      end
