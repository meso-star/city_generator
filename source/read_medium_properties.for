c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_medium_properties(datafile,
     &     name,Nlambda,lambda,nref,ka,ks)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read the raw medium properties file
c     
c     Input:
c       + datafile: file to read
c
c     Output:
c       + name: label for the medium
c       + Nlambda: number of wavelength points
c       + lambda: wavelength data [micrometers]
c       + nref: refraction index
c       + ka: absorption coefficient
c       + ks: scattering coefficient
c     
c     I/O
      character*(Nchar_mx) datafile
      character*(Nchar_mx) name
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision nref(1:Nlambda_mx)
      double precision ka(1:Nlambda_mx)
      double precision ks(1:Nlambda_mx)
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_medium_properties'

      open(12,file=trim(datafile),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         read(12,*)
         read(12,10) name
         read(12,*)
         read(12,*) Nlambda
         if (Nlambda.lt.0) then
            call error(label)
            write(*,*) 'Material: ',trim(name)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) 'should be >= 0'
            stop
         endif                  ! Nlambda < 0
         if (Nlambda.gt.Nlambda_mx) then
            call error(label)
            write(*,*) 'Material: ',trim(name)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) 'should be < Nlambda_mx=',Nlambda_mx
            stop
         endif                  ! Nlambda > Nlambda_mx
         read(12,*)
         if (Nlambda.eq.0) then
            read(12,*) nref(1),ka(1),ks(1)
c     ------------- consistency -------------
            if (nref(1).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Material: ',trim(name)
               write(*,*) 'refraction index=',nref(1)
               write(*,*) 'should be >= 0'
               stop
            endif
            if (ka(1).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Material: ',trim(name)
               write(*,*) 'absorption coefficient=',ka(1)
               write(*,*) 'should be >= 0'
               stop
            endif
            if (ks(1).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Material: ',trim(name)
               write(*,*) 'scattering coefficient=',ks(1)
               write(*,*) 'should be >= 0'
               stop
            endif
c     ------------- consistency -------------
         else
            do i=1,Nlambda
               read(12,*) lambda(i),nref(i),ka(i),ks(i)
c     ------------- consistency -------------
               if (nref(i).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'refraction index(',i,')=',nref(i)
                  write(*,*) 'should be >= 0'
                  stop
               endif
               if (ka(i).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'absorption coefficient(',i,')=',ka(i)
                  write(*,*) 'should be >= 0'
                  stop
               endif
               if (ks(i).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'scattering coefficient(',i,')=',ks(i)
                  write(*,*) 'should be >= 0'
                  stop
               endif
c     ------------- consistency -------------
            enddo               ! i
            do i=2,Nlambda
c     ------------- consistency -------------
               if (lambda(i).le.lambda(i-1)) then
                  call error(label)
                  write(*,*) 'Material: ',trim(name)
                  write(*,*) 'lambda(',i,')=',lambda(i)
                  write(*,*) 'should be > lambda(',i-1,')=',lambda(i-1)
                  stop
               endif
c     ------------- consistency -------------
            enddo               ! i
         endif                  ! Nlambda=0
c     
      endif                     ! ios.ne.0
      close(12)

      return
      end
      
