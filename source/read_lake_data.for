c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine identify_Nlake(Nlake)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of lakes defined by the user
c
c     Input:
c
c     Output:
c       + Nlake: number of lakes
c
c     I/O
      integer Nlake
c     temp
      logical keep_looking,file_exists
      integer i
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine identify_Nlake'

      Nlake=0
      keep_looking=.true.
      do while (keep_looking)
         i=Nlake+1
         call num2str3(i,str3)
         filename='./data/lake'//trim(str3)//'.in'
         inquire(file=trim(filename),exist=file_exists)
         if (file_exists) then
            Nlake=Nlake+1
         else
            keep_looking=.false.
         endif                  ! file_exists
      enddo                     ! while (keep_looking)

      return
      end



      subroutine read_lake_data(dim,
     &     Nlake,ilake,position,scale,lake_mat,
     &     Ncontour,Nppt,contour_track)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to read input data file for lakes
c
c     Input:
c       + dim: dimension of space
c
c     Output:
c       + Nlake: number of lakes
c       + ilake: index of the lake definition file to read
c       + position: position of each lake
c       + scale: scaling factor for each lake
c       + lake_mat: material for the lake water
c       + Ncontour: number of contours that define each lake
c       + Nppt: number of points for the description of the contour track for each lake
c       + contour_track: list of positions that describe the central track of each lake
c
c     I/O
      integer dim
      integer Nlake
      integer ilake
      double precision position(1:2)
      double precision scale
      character*(Nchar_mx) lake_mat
      integer Ncontour
      integer Nppt(1:Ncontour_mx)
      double precision contour_track(1:Ncontour_mx,1:Nppt_mx,1:2)
c     temp
      integer ios,j,k,nl
      logical keep_reading
      character*3 str3
      character*(Nchar_mx) filename,line
c     functions
      integer nitems
c     label
      character*(Nchar_mx) label
      label='subroutine read_lake_data'
      
      call num2str3(ilake,str3)
      filename='./data/lake'//trim(str3)//'.in'
      open(18,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do j=1,3
            read(18,*)
         enddo                  ! j
         read(18,*) (position(j),j=1,dim-1)
         read(18,*)
         read(18,*) scale
         read(18,*)
         read(18,*) lake_mat
         read(18,*)
         Ncontour=1
         Nppt(Ncontour)=0
         keep_reading=.true.
         do while (keep_reading)
            read(18,10,iostat=ios) line
            if (ios.eq.0) then
               if (nitems(line).eq.2) then
                  backspace(18)
                  Nppt(Ncontour)=Nppt(Ncontour)+1
                  if (Nppt(Ncontour).gt.Nppt_mx) then
                     call error(label)
                     write(*,*) 'Nppt(',Ncontour,')=',Nppt(Ncontour)
                     write(*,*) '> Nppt_mx=',Nppt_mx
                     stop
                  endif
                  read(18,*) (contour_track(Ncontour,
     &                 Nppt(Ncontour),j),j=1,2)
               else             ! nitems(line).ne.2
                  Ncontour=Ncontour+1
                  if (Ncontour.gt.Ncontour_mx) then
                     call error(label)
                     write(*,*) 'Ncontour=',Ncontour
                     write(*,*) '> Ncontour_mx=',Ncontour_mx
                     stop
                  endif
                  Nppt(Ncontour)=0
               endif            ! nitems(line)=2
            else
               keep_reading=.false.
            endif               ! ios=0
         enddo                  ! while (keep_reading)
      endif                     !  file exists
      close(18)
      
      return
      end
