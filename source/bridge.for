c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine bridge(dim,
     &     position,angle,length,
     &     deck_width,side_width,
     &     deck_height,side_height,
     &     deck_mat,side_mat,
     &     draw_wbands,wband_mat,
     &     generate_individual_obj_files,
     &     obj_file,mtllib_file,off_file,
     &     Nobj,Nv,Nf)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'parameters.inc'
c
c     Purpose: to produce a bridge
c
c     Input:
c       + dim: dimension of space
c       + position: (x,y) shift of the bridge (m)
c       + angle: rotation angle (deg)
c       + length: length of each bridge [m]
c       + deck_width: width of the deck for each bridge [m]
c       + side_width: width of the sides for each bridge [m]
c       + deck_height: height of the deck for each bridge [m]
c       + side_height: height of the sides for each bridge [m]
c       + deck_mat: material for the deck
c       + side_mat: material for the sides
c       + draw_wbands: T if drawing white bands is required
c       + wband_mat: material for white bands
c       + generate_individual_obj_files: true if individual OBJ files have to be generated
c       + obj_file: path to the obj file to update if append_obj=T
c       + mtllib_file: mtllib file that will be used by the obj file
c       + off_file: path to the off file to update if append_obj=T
c       + Nobj: total number of objects in the scene
c       + Nv: total number of vertices in the scene
c       + Nf: total number of faces in the scene
c
c     Output:
c       + Nobj: total number of objects in the scene (updated)
c       + Nv: total number of vertices in the scene (updated)
c       + Nf: total number of faces in the scene (updated)
c
c     I/O
      integer dim
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision length
      double precision deck_width
      double precision side_width
      double precision deck_height
      double precision side_height
      character*(Nchar_mx) deck_mat
      character*(Nchar_mx) side_mat
      logical draw_wbands
      character*(Nchar_mx) wband_mat
      logical generate_individual_obj_files
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) off_file
      integer Nobj,Nv,Nf
c     temp
      integer iobj,i
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal
      integer sindex
      character*(Nchar_mx) str
      logical err
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision alpha
      double precision center(1:Ndim_mx)
      logical front_is_st
      logical back_is_st
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      double precision color_code(1:3)
      character*(Nchar_mx) file_out,filename
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
c     Dummy
      integer Nroad
      double precision road_width
      logical draw_wbands_road
      integer Nppt_road
      double precision road_central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) road_material
      character*(Nchar_mx) wband_material
c     label
      character*(Nchar_mx) label
      label='subroutine bridge'

      axe(1)=0.0D+0
      axe(2)=0.0D+0
      axe(3)=1.0D+0
      
      iobj=0                    ! total number of individual surfaces groups for the bridge
c     ---------------------------------------------------------------------------
c     Production of objects:
c     ---------------------------------------------------------------------------
c     + Deck
c     ---------------------------------------------------------------------------
      iobj=iobj+1
      call obj_deck(dim,length,deck_width,deck_height,
     &     Nv01,Nf01,v01,f01)
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(deck_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     ---------------------------------------------------------------------------
c     + white bands
c     ---------------------------------------------------------------------------
      if (draw_wbands) then
c     +-------------------------------+
c     First section
c     +-------------------------------+
         p1(1)=0.0D+0
         p1(2)=deck_width/2.0D+0
         p1(3)=0.0D+0
         call copy_vector(dim,p1,p2)
         p2(1)=p1(1)+length*(1.0D+0-bridge_length_clp)/2.0D+0
         p2(3)=p2(3)+deck_height
         call copy_vector(dim,p1,x1)
         x1(2)=x1(2)-deck_width/2.0D+0
         call copy_vector(dim,p1,x4)
         x4(2)=x4(2)+deck_width/2.0D+0
         call copy_vector(dim,p2,x2)
         x2(2)=x2(2)-deck_width/2.0D+0
         call copy_vector(dim,p2,x3)
         x3(2)=x3(2)+deck_width/2.0D+0
         n1(1)=0.0D+0
         n1(2)=1.0D+0
         n1(3)=0.0D+0
         call copy_vector(dim,n1,n2)
c     
         iobj=iobj+1
         call obj_wband(dim,
     &        Nroad,road_width,draw_wbands_road,
     &        Nppt_road,road_central_track,
     &        road_material,wband_material,
     &        0,0,
     &        p1,p2,x1,x2,x3,x4,n1,n1,n2,
     &        Nv01,Nf01,v01,f01)
         if (angle.ne.0.0D+0) then
            alpha=angle*pi/180.0D+0 ! rad
            call rotation_matrix(dim,alpha,axe,M)
            call rotate_obj(dim,Nv01,v01,M)
         endif
         if ((position(1).ne.0.0D+0).or.
     &        (position(2).ne.0.0D+0)) then
            center(1)=position(1)
            center(2)=position(2)
            center(3)=0.0D+0
            call move_obj(dim,Nv01,v01,center)
         endif
c     Add individual surface to global trianglemesh (for visualization)
         invert_normal=.false.
         sindex=Nobj+iobj
         call num2str(sindex,str,err)
         if (err) then
            call error(label)
            write(*,*) 'while converting to string:'
            write(*,*) 'sindex=',sindex
            stop
         else
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(wband_mat)
            middle_name=''
            call append_single_obj_with_mtl(dim,obj_file,mtllib_file,
     &           invert_normal,
     &           front_is_st,back_is_st,.false.,
     &           front_name,middle_name,back_name,
     &           Nv,Nv01,Nf01,v01,f01)
c            call retrieve_code(back_name,color_code)
c            call append_single_off(dim,off_file,invert_normal,
c     &           Nv,Nv01,Nf01,v01,f01,color_code)
            if (generate_individual_obj_files) then
               filename='S'//trim(str)//'.obj'
               file_out='./results/obj/'//trim(filename)
               call write_single_obj(dim,file_out,invert_normal,
     &              Nv01,Nf01,v01,f01)
            endif
            Nv=Nv+Nv01
            Nf=Nf+Nf01
         endif                  ! err
c     +-------------------------------+
c     Second section
c     +-------------------------------+
         p1(1)=length*(1.0D+0-bridge_length_clp)/2.0D+0
         p1(2)=deck_width/2.0D+0
         p1(3)=deck_height
         call copy_vector(dim,p1,p2)
         p2(1)=p1(1)+length*bridge_length_clp
         call copy_vector(dim,p1,x1)
         x1(2)=x1(2)-deck_width/2.0D+0
         call copy_vector(dim,p1,x4)
         x4(2)=x4(2)+deck_width/2.0D+0
         call copy_vector(dim,p2,x2)
         x2(2)=x2(2)-deck_width/2.0D+0
         call copy_vector(dim,p2,x3)
         x3(2)=x3(2)+deck_width/2.0D+0
         n1(1)=0.0D+0
         n1(2)=1.0D+0
         n1(3)=0.0D+0
         call copy_vector(dim,n1,n2)
c     
         iobj=iobj+1
         call obj_wband(dim,
     &        Nroad,road_width,draw_wbands_road,
     &        Nppt_road,road_central_track,
     &        road_material,wband_material,
     &        0,0,
     &        p1,p2,x1,x2,x3,x4,n1,n1,n2,
     &        Nv01,Nf01,v01,f01)
         if (angle.ne.0.0D+0) then
            alpha=angle*pi/180.0D+0 ! rad
            call rotation_matrix(dim,alpha,axe,M)
            call rotate_obj(dim,Nv01,v01,M)
         endif
         if ((position(1).ne.0.0D+0).or.
     &        (position(2).ne.0.0D+0)) then
            center(1)=position(1)
            center(2)=position(2)
            center(3)=0.0D+0
            call move_obj(dim,Nv01,v01,center)
         endif
c     Add individual surface to global trianglemesh (for visualization)
         invert_normal=.false.
         sindex=Nobj+iobj
         call num2str(sindex,str,err)
         if (err) then
            call error(label)
            write(*,*) 'while converting to string:'
            write(*,*) 'sindex=',sindex
            stop
         else
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(wband_mat)
            middle_name=''
            call append_single_obj_with_mtl(dim,obj_file,mtllib_file,
     &           invert_normal,
     &           front_is_st,back_is_st,.false.,
     &           front_name,middle_name,back_name,
     &           Nv,Nv01,Nf01,v01,f01)
c            call retrieve_code(back_name,color_code)
c            call append_single_off(dim,off_file,invert_normal,
c     &           Nv,Nv01,Nf01,v01,f01,color_code)
            if (generate_individual_obj_files) then
               filename='S'//trim(str)//'.obj'
               file_out='./results/obj/'//trim(filename)
               call write_single_obj(dim,file_out,invert_normal,
     &              Nv01,Nf01,v01,f01)
            endif
            Nv=Nv+Nv01
            Nf=Nf+Nf01
         endif                  ! err
c     +-------------------------------+
c     Third section
c     +-------------------------------+
         p1(1)=length*(1.0D+0+bridge_length_clp)/2.0D+0
         p1(2)=deck_width/2.0D+0
         p1(3)=deck_height
         p2(1)=length
         p2(2)=p1(2)
         p2(3)=0.0D+0
         call copy_vector(dim,p1,x1)
         x1(2)=x1(2)-deck_width/2.0D+0
         call copy_vector(dim,p1,x4)
         x4(2)=x4(2)+deck_width/2.0D+0
         call copy_vector(dim,p2,x2)
         x2(2)=x2(2)-deck_width/2.0D+0
         call copy_vector(dim,p2,x3)
         x3(2)=x3(2)+deck_width/2.0D+0
         n1(1)=0.0D+0
         n1(2)=1.0D+0
         n1(3)=0.0D+0
         call copy_vector(dim,n1,n2)
c     
         iobj=iobj+1
         call obj_wband(dim,
     &        Nroad,road_width,draw_wbands_road,
     &        Nppt_road,road_central_track,
     &        road_material,wband_material,
     &        0,0,
     &        p1,p2,x1,x2,x3,x4,n1,n1,n2,
     &        Nv01,Nf01,v01,f01)
         if (angle.ne.0.0D+0) then
            alpha=angle*pi/180.0D+0 ! rad
            call rotation_matrix(dim,alpha,axe,M)
            call rotate_obj(dim,Nv01,v01,M)
         endif
         if ((position(1).ne.0.0D+0).or.
     &        (position(2).ne.0.0D+0)) then
            center(1)=position(1)
            center(2)=position(2)
            center(3)=0.0D+0
            call move_obj(dim,Nv01,v01,center)
         endif
c     Add individual surface to global trianglemesh (for visualization)
         invert_normal=.false.
         sindex=Nobj+iobj
         call num2str(sindex,str,err)
         if (err) then
            call error(label)
            write(*,*) 'while converting to string:'
            write(*,*) 'sindex=',sindex
            stop
         else
            invert_normal=.false.
            front_is_st=.true.
            front_name='air'
            back_is_st=.false.
            back_name=trim(wband_mat)
            middle_name=''
            call append_single_obj_with_mtl(dim,obj_file,mtllib_file,
     &           invert_normal,
     &           front_is_st,back_is_st,.false.,
     &           front_name,middle_name,back_name,
     &           Nv,Nv01,Nf01,v01,f01)
c            call retrieve_code(back_name,color_code)
c            call append_single_off(dim,off_file,invert_normal,
c     &           Nv,Nv01,Nf01,v01,f01,color_code)
            if (generate_individual_obj_files) then
               filename='S'//trim(str)//'.obj'
               file_out='./results/obj/'//trim(filename)
               call write_single_obj(dim,file_out,invert_normal,
     &              Nv01,Nf01,v01,f01)
            endif
            Nv=Nv+Nv01
            Nf=Nf+Nf01
         endif                  ! err
      endif                     ! draw_wbands
c     ---------------------------------------------------------------------------
c     + right side
c     ---------------------------------------------------------------------------
      iobj=iobj+1
      call obj_bridge_side(dim,length,deck_width,deck_height,
     &     side_width,side_height,
     &     Nv01,Nf01,v01,f01)
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(side_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     ---------------------------------------------------------------------------
c     + left side
c     ---------------------------------------------------------------------------
      iobj=iobj+1
      call obj_bridge_side(dim,length,deck_width,deck_height,
     &     side_width,side_height,
     &     Nv01,Nf01,v01,f01)
      center(1)=0.0D+0
      center(2)=deck_width+side_width
      center(3)=0.0D+0
      call move_obj(dim,Nv01,v01,center)
      if (angle.ne.0.0D+0) then
         alpha=angle*pi/180.0D+0 ! rad
         call rotation_matrix(dim,alpha,axe,M)
         call rotate_obj(dim,Nv01,v01,M)
      endif
      if ((position(1).ne.0.0D+0).or.
     &     (position(2).ne.0.0D+0)) then
         center(1)=position(1)
         center(2)=position(2)
         center(3)=0.0D+0
         call move_obj(dim,Nv01,v01,center)
      endif
c     Add individual surface to global trianglemesh (for visualization)
      invert_normal=.false.
      sindex=Nobj+iobj
      call num2str(sindex,str,err)
      if (err) then
         call error(label)
         write(*,*) 'while converting to string:'
         write(*,*) 'sindex=',sindex
         stop
      else
         invert_normal=.false.
         front_is_st=.true.
         front_name='air'
         back_is_st=.false.
         back_name=trim(side_mat)
         middle_name=''
         call append_single_obj_with_mtl(dim,obj_file,mtllib_file,
     &        invert_normal,
     &        front_is_st,back_is_st,.false.,
     &        front_name,middle_name,back_name,
     &        Nv,Nv01,Nf01,v01,f01)
c         call retrieve_code(back_name,color_code)
c         call append_single_off(dim,off_file,invert_normal,
c     &        Nv,Nv01,Nf01,v01,f01,color_code)
         if (generate_individual_obj_files) then
            filename='S'//trim(str)//'.obj'
            file_out='./results/obj/'//trim(filename)
            call write_single_obj(dim,file_out,invert_normal,
     &           Nv01,Nf01,v01,f01)
         endif
         Nv=Nv+Nv01
         Nf=Nf+Nf01
      endif                     ! err
c     
      Nobj=Nobj+iobj
c
      return
      end
