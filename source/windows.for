c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine north_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      include 'max.inc'
c      
c     Purpose: to identify whether or not a window is present on the north face
c     of a given room
c      
c           Y
c           ^
c           |
c           |               east
c           ----------------------------------------
c    north  |                                      |  south
c           |                                      |
c           |                                      |
c      ------------------------------------------------------------------> X
c           |                                      |
c           |                                      |
c           |                                      |
c           ----------------------------------------
c                           west
c      
c      Input:
c        + Nx: number of cubic cells in X direction for each building
c        + Ny: number of cubic cells in Y direction for each building
c        + Nz: number of cubic cells in Z direction for each building
c        + ibulding: index of the building
c        + (ix,iy,iz): indexes of the room
c      
c      Output:
c        + window_is_present: true if a window is present
c
c     I/O
      integer Nx,Ny,Nz
      integer ix,iy,iz
      logical window_is_present
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine north_face_window'

      if (ix.eq.1) then
         window_is_present=.true.
      else
         window_is_present=.false.
      endif
      
      return
      end

      
      
      subroutine south_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      include 'max.inc'
c      
c     Purpose: to identify whether or not a window is present on the south face
c     of a given room
c      
c           Y
c           ^
c           |
c           |               east
c           ----------------------------------------
c    north  |                                      |  south
c           |                                      |
c           |                                      |
c      ------------------------------------------------------------------> X
c           |                                      |
c           |                                      |
c           |                                      |
c           ----------------------------------------
c                           west
c      
c      Input:
c        + Nx: number of cubic cells in X direction for each building
c        + Ny: number of cubic cells in Y direction for each building
c        + Nz: number of cubic cells in Z direction for each building
c        + ibulding: index of the building
c        + (ix,iy,iz): indexes of the room
c      
c      Output:
c        + window_is_present: true if a window is present
c
c     I/O
      integer Nx,Ny,Nz
      integer ix,iy,iz
      logical window_is_present
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine south_face_window'

      if (ix.eq.Nx) then
         window_is_present=.true.
      else
         window_is_present=.false.
      endif
      
      return
      end
      

      
      subroutine west_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      include 'max.inc'
c      
c     Purpose: to identify whether or not a window is present on the west face
c     of a given room
c      
c           Y
c           ^
c           |
c           |               east
c           ----------------------------------------
c    north  |                                      |  south
c           |                                      |
c           |                                      |
c      ------------------------------------------------------------------> X
c           |                                      |
c           |                                      |
c           |                                      |
c           ----------------------------------------
c                           west
c      
c      Input:
c        + Nx: number of cubic cells in X direction for each building
c        + Ny: number of cubic cells in Y direction for each building
c        + Nz: number of cubic cells in Z direction for each building
c        + ibulding: index of the building
c        + (ix,iy,iz): indexes of the room
c      
c      Output:
c        + window_is_present: true if a window is present
c
c     I/O
      integer Nx,Ny,Nz
      integer ix,iy,iz
      logical window_is_present
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine west_face_window'

      if (iy.eq.1) then
         window_is_present=.true.
      else
         window_is_present=.false.
      endif
      
      return
      end

      
      
      subroutine east_face_window(Nx,Ny,Nz,ix,iy,iz,
     &     window_is_present)
      include 'max.inc'
c      
c     Purpose: to identify whether or not a window is present on the east face
c     of a given room
c      
c           Y
c           ^
c           |
c           |               east
c           ----------------------------------------
c    north  |                                      |  south
c           |                                      |
c           |                                      |
c      ------------------------------------------------------------------> X
c           |                                      |
c           |                                      |
c           |                                      |
c           ----------------------------------------
c                           west
c      
c      Input:
c        + Nx: number of cubic cells in X direction for each building
c        + Ny: number of cubic cells in Y direction for each building
c        + Nz: number of cubic cells in Z direction for each building
c        + ibulding: index of the building
c        + (ix,iy,iz): indexes of the room
c      
c      Output:
c        + window_is_present: true if a window is present
c
c     I/O
      integer Nx,Ny,Nz
      integer ix,iy,iz
      logical window_is_present
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine east_face_window'

      if (iy.eq.Ny) then
         window_is_present=.true.
      else
         window_is_present=.false.
      endif
      
      return
      end
      
