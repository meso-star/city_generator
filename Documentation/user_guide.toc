\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Pr\IeC {\'e}sentation}{2}{section.2}
\contentsline {section}{\numberline {3}Entr\IeC {\'e}es}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Le fichier \textit {data/global\_settings.in}}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Les fichiers de d\IeC {\'e}finition des \IeC {\'e}l\IeC {\'e}ments de g\IeC {\'e}om\IeC {\'e}trie}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Les fichiers de propri\IeC {\'e}t\IeC {\'e}s optique de surface des mat\IeC {\'e}riaux}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Le fichier \textit {data/rgb\_color\_codes.in}}{6}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Les fichiers de g\IeC {\'e}om\IeC {\'e}trie additionnels}{6}{subsection.3.5}
\contentsline {section}{\numberline {4}Sorties}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Le fichier de g\IeC {\'e}om\IeC {\'e}trie}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Les fichiers de r\IeC {\'e}flectivit\IeC {\'e} spectrale}{7}{subsection.4.2}
\contentsline {section}{\numberline {5}Evolution du code}{7}{section.5}
\contentsline {section}{\numberline {6}Copyright}{7}{section.6}
