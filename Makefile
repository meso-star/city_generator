# SHELL = /bin/sh
MAINDIR=$(shell /bin/pwd)
UN=$(shell echo ${USER})
HOST=$(shell echo `hostname`)
EXEC=city_generator.exe
#FOR=$(shell echo ${F77})
FOR=gfortran #-L/usr/lib
MAKE=make
#ARCH=-m486 -msparclite -mips3 # options pour une machine 32 bits
#ARCH=-march=i686 -m64 # options pour un i686 en 64 bits

ARCH=-m64 #-march=i686
OPTI=-O3 -static -fbackslash #-fast
WARNING=-fbounds-check -Warray-bounds -Walign-commons
DEBUG=
OSV=Linux
OSTYPEV=$(shell echo ${OSTYPE})


#############################
# FFLAGS= -O
# FFLAGS= -ff90
# FFLAGS= -ftrength-reduce (les loop vont plus vite cf gcc)
# FFLAGS= -g -ftrenght-reduce (cf gcc option va unused corteu)
############################Mieux vaut ajuster par des directives de compilation
############################inserer dans le prog quand le compilateur le permet.
FFLAGS = $(ARCH) $(OPTI) $(WARNING) $(IEEE) $(EMUL90) $(DEBUG)

####################################################################################
#   DIR is the path to source files directory				#
#   OBJ is the path to object files directory				#
#   SRC_PROG is the list of source files to be compiled			#
#   INCLUDES is: -I+path to directory that contains 'include' files	#
####################################################################################

DIR=${MAINDIR}/source
OBJ=${MAINDIR}/objects
DAT=${MAINDIR}/data


SRC_PROG = \
	$(DIR)/add2obj.for \
	$(DIR)/add_slash_if_absent.for \
	$(DIR)/add_semicolon_if_absent.for \
	$(DIR)/building00.for \
	$(DIR)/building01.for \
	$(DIR)/building02.for \
	$(DIR)/building03.for \
	$(DIR)/building04.for \
	$(DIR)/bridge.for \
	$(DIR)/city_generator.for \
	$(DIR)/contours.for \
	$(DIR)/coordinates.for \
	$(DIR)/decoration.for \
	$(DIR)/delaunay_triangulation.for \
	$(DIR)/error.for \
	$(DIR)/exec.for \
	$(DIR)/files.for \
	$(DIR)/geometry.for \
	$(DIR)/get_nlines.for \
	$(DIR)/ground.for \
	$(DIR)/init.for \
	$(DIR)/intersections.for \
	$(DIR)/lake.for \
	$(DIR)/matrix.for \
	$(DIR)/modify_obj.for \
	$(DIR)/mtl.for \
	$(DIR)/navigation.for \
	$(DIR)/nitems.for \
	$(DIR)/num2str.for \
	$(DIR)/objects.for \
	$(DIR)/random_gen.for \
	$(DIR)/read_building_data.for \
	$(DIR)/read_bridge_data.for \
	$(DIR)/read_global_data.for \
	$(DIR)/read_ground_data.for \
	$(DIR)/read_lake_data.for \
	$(DIR)/read_material_properties.for \
	$(DIR)/read_material_spectral_data.for \
	$(DIR)/read_medium_properties.for \
	$(DIR)/read_obj.for \
	$(DIR)/read_river_data.for \
	$(DIR)/read_road_data.for \
	$(DIR)/read_swimmingpool_data.for \
	$(DIR)/read_zone_data.for \
	$(DIR)/rgb.for \
	$(DIR)/river.for \
	$(DIR)/road.for \
	$(DIR)/rotation.for \
	$(DIR)/swimmingpool.for \
	$(DIR)/tree.for \
	$(DIR)/triangle_files.for \
	$(DIR)/windows.for \
	$(DIR)/write_obj.for \
	$(DIR)/write_off.for \
	$(DIR)/zone.for \
	$(DIR)/zufall.for

INCLUDES= -I./includes

#dependencies
#quoi compiler ?
#OBJ_PROG=$(SRC_PROG:$(DIR)/%.for=$(OBJ)/%.o)
OBJ_PROG=$(SRC_PROG:$(DIR)/%.for=$(OBJ)/%.o)
#comment compiler ?
$(OBJ)/%.o: $(DIR)/%.for
	$(FOR) $(FFLAGS) $(INCLUDES) -c -o $@ $(@:$(OBJ)/%.o=$(DIR)/%.for)

to :
	read leurre
	touch *

clean :
	@rm -f *.o $(OBJ)/*.o *.oub *.oup *.oug
	@rm -f *%
	@rm -f *~
	@rm -f $(DIR)/*.~
	@rm -f $(DIR)/*.*~
	@rm -f tmp.tmp
	@rm -f *.sum
	@rm -f *.eps
	@rm -f *.ps
	@rm -f *.jpg
	@rm -f core
	@rm -f last.kumac
	@rm -f paw.metafile
	@rm -f core
	@touch core
	@chmod 000 core
	@chmod a+r core
	@echo 'Files have been removed'

dat :
	@cd ./data;f0;make_data.exe
	@echo 'Program has been compiled'

wipe :
	@rm -f *%
	@rm -f *~
	@rm -f $(DIR)/*.~
	@rm -f $(DIR)/*.*~
	@rm -f tmp.tmp
	@rm -f *.sum
	@rm -f *.eps
	@rm -f *.ps
	@rm -f *.jpg
	@rm -f core
	@rm -f last.kumac
	@rm -f paw.metafile
	@rm -f core
	@touch core
	@chmod 000 core
	@echo 'Files have been removed'

skel :	clean
	rm -f $(EXEC)
	@echo 'Files have been removed'

where :
	@echo Current directory: $(MAINDIR)
	@echo Source directory: $(DIR)

info :
	@echo Host name: $(HOSTNAME)
	@echo Operating system: $(OSV)
	@echo Type of system: $(OSTYPEV)
	@echo User name: $(USER)
	@echo Special compilation options: $(SPEC)


all :
#	@echo "UN="$(UN)
#	@echo "HOST="$(HOST)
	@echo $(DIR)
	@echo '------ Compilation -------'
	make -f Makefile allend

allend :	main
main :	$(OBJ_PROG)
	@echo '------ Edition de liens -------'
	$(FOR) -o $(EXEC) $(OBJ_PROG) $(SPEC)
	@echo '--- Use program ' $(EXEC)  '(compiled for :' $(OSV) $(OSTYPEV) 'by:' $(USER) ')'
	@date > compdate.in
#	@echo 'ln -s main'

run :
	@$(EXEC)
	@echo 'Done'

runmulti :
	@echo 'Differents MCM stationnaires'
	@echo '/!\Pousuivre si les fichiers *.in existent'
	@echo 'de 1 a 19'
	@echo '#! /bin/sh' > z1.txt
	@echo 'N=1' > z2.txt
	@echo 'FIN=19' > z3.txt
	read leurre
	cat z1.txt z2.txt z3.txt monlotmulti2_sstete.bat > monlotmulti2.bat
	rm z1.txt z2.txt z3.txt
	at now +0 minutes < monlotmulti.bat

#outils pour debugger autres que print
gbd :
	@echo 'gdb ou xxgdb (main)'

#outils pour profiling : optimisations scalaire (ou vectorielle)
gprof :
#	@gprof -s -l main.exe gmon.out > gprof_sl	
	@gprof $(EXEC) gmon.out > gprof_sl
	@more gprof_sl

edit :
	xemacs &

save :
	@sauve.1d
	@echo 'done'

shell :
	xterm

man :
	make help

help :
	@echo '|------------------------------------------|'
	@echo '|------------------------------------------|'
	@echo '|  AIDE-MEMOIRE                            |'
	@echo '|------------------------------------------|'
	@echo '| help, edit, shell                        |'
	@echo '| gdb, gprof for debug                     |'
	@echo '|------------------------------------------|'
	@echo '| clean: remove  (.o, *~, etc.)            |'
	@echo '| skel: remove ' $(EXEC) '                 |'
	@echo '| to, all, run, runmulti                   |'
	@echo '|------------------------------------------|'
