	double precision road_thickness
	double precision wband_width
	double precision wband_thickness
	double precision central_band_length
	double precision central_band_interval
	double precision roof_length_clp
	double precision river_thickness
	double precision bridge_length_clp
	double precision residential_house_max_extension
	double precision window_max_width
	double precision roof_extension_length
	double precision roof_slope_length
	double precision default_roof_thickness

	parameter(road_thickness=1.0D-2)		! m
	parameter(wband_width=6.0D-2)			! m
	parameter(wband_thickness=1.0D-3)		! m
	parameter(central_band_length=2.0D+0)		! m
	parameter(central_band_interval=1.0D+0)		! m
	parameter(roof_length_clp=0.50)			! fraction
	parameter(river_thickness=1.0D-2)		! m
	parameter(bridge_length_clp=0.80)		! fraction
	parameter(residential_house_max_extension=20.0) ! m
	parameter(window_max_width=1.50D+0)		! m
	parameter(roof_extension_length=0.25D+0)	! m
	parameter(roof_slope_length=0.50D+0)		! m
	parameter(default_roof_thickness=2.0D-2)		! m
	