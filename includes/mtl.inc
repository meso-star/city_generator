	integer Nmaterial_known
	character*(Nchar_mx) material_name(1:Nmaterial_mx)
	character*(Nchar_mx) material_aspect(1:Nmaterial_mx)
	integer material_Nlambda_LW(1:Nmaterial_mx)
	double precision material_lambda_LW(1:Nmaterial_mx,1:Nlambda_mx)
	integer material_Nlambda_SW(1:Nmaterial_mx)
	double precision material_lambda_SW(1:Nmaterial_mx,1:Nlambda_mx)
	double precision material_reflectivity(1:Nmaterial_mx,
     &		1:Nlambda_mx)
	double precision material_emissivity(1:Nmaterial_mx,
     &		1:Nlambda_mx)
	integer Nmedium_known
	character*(Nchar_mx) medium_name(1:Nmedium_mx)
	integer medium_Nlambda(1:Nmedium_mx)
	double precision medium_lambda(1:Nmedium_mx,1:Nlambda_mx)
	double precision medium_nref(1:Nmedium_mx,1:Nlambda_mx)
	double precision medium_ka(1:Nmedium_mx,1:Nlambda_mx)
	double precision medium_ks(1:Nmedium_mx,1:Nlambda_mx)
	integer Nmat_db
	character*(Nchar_mx) mat_name_db(1:Nmaterial_mx)
	integer Nlambda_db_LW
	double precision lambda_db_LW(1:Nlambda_mx)
	integer Nlambda_db_SW
	double precision lambda_db_SW(1:Nlambda_mx)
	double precision emissivity_db(1:Nlambda_mx,1:Nmaterial_mx)
	double precision reflectivity_db(1:Nlambda_mx,1:Nmaterial_mx)
	

	common /aa/ Nmaterial_known
	common /ab/ material_name
	common /ac/ material_aspect
	common /ad/ material_Nlambda_LW
	common /ae/ material_lambda_LW
	common /af/ material_Nlambda_SW
	common /ag/ material_lambda_SW
	common /ah/ material_reflectivity
	common /ai/ material_emissivity
	common /ba/ Nmedium_known
	common /bb/ medium_name
	common /bc/ medium_Nlambda
	common /bd/ medium_lambda
	common /be/ medium_nref
	common /bf/ medium_ka
	common /bg/ medium_ks
	common /ca/ Nmat_db
	common /cb/ mat_name_db
	common /cc/ Nlambda_db_LW
	common /cd/ lambda_db_LW
	common /ce/ Nlambda_db_SW
	common /cf/ lambda_db_SW
	common /cg/ emissivity_db
	common /ch/ reflectivity_db

	save /aa/,/ab/,/ac/,/ad/,/ae/,/af/,/ag/,/ah/,/ai/,
     &	     /ba/,/bb/,/bc/,/bd/,/be/,/bf/,/bg/,
     &	     /ca/,/cb/,/cc/,/cd/,/ce/,/cf/,/cg/,/ch/
     