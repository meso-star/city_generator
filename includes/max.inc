	integer Ndim_mx
	integer Nx_mx
	integer Ny_mx
	integer Nz_mx
	integer Ncontour_mx
	integer Nppt_mx
	integer Nppc_mx
        integer Nv_so_mx
        integer Nf_so_mx
	integer Nv_mx
	integer Nf_mx
	integer Ncode_mx
	integer Nmaterial_mx
	integer Nmedium_mx
	integer Nmtl_mx
	integer Ndist_mx
	integer Nlambda_mx
	integer Niter_mx
	integer Nfolders_mx
	integer Nfiles_mx
	integer Nquotes_mx
	integer Nproba_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Nx_mx=100)
	parameter(Ny_mx=100)
	parameter(Nz_mx=100)
	parameter(Ncontour_mx=1000)
	parameter(Nppt_mx=1000)
	parameter(Nppc_mx=Nppt_mx)
        parameter(Nv_so_mx=1000000)
        parameter(Nf_so_mx=1000000)
        parameter(Nv_mx=10000000)
        parameter(Nf_mx=10000000)
        parameter(Ncode_mx=200)
        parameter(Nmaterial_mx=1000)
        parameter(Nmedium_mx=20)
        parameter(Nmtl_mx=100)
        parameter(Ndist_mx=100)
        parameter(Nlambda_mx=1000)
        parameter(Niter_mx=100)
        parameter(Nfolders_mx=1000)
        parameter(Nfiles_mx=10000)
	parameter(Nquotes_mx=100)
	parameter(Nproba_mx=100)
	parameter(Nchar_mx=1000)
